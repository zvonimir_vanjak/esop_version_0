TODO list

1. Dovr�iti forme za SOWC, MO i MOWC rezultate
		- u konstruktorima iz predanog rje�enja izvu�i podatke o broju ciljeva, ograni�enja, ...i u skladu s time kreirati list kontrole
		- u ovisnosti o tim podacima kreirati kontrolu za pirkaz Pareto set-a i sva rje�enja prikazati na kontroli
2. Implementirati MOWC (CHNA) i MO (SPEA2) algoritme
3. ESOPVisualizationControls
		- kontrola za iscrtavanje grafa funkcije
		- Double kolekcija 
		- DoubleArray kolekcija
		- ParetoSet2D
		- StringArray kolekcija
4. dovr�iti forme frmComplete i frmIndividual i frmViewVisualizationControls
5. KOMPOZITNE REPREZENTACIJE
		- "standardne" - RealArray, IntegerArray
		- ugraditi u ESOP ConstructorParametre
		- vidjeti kako to handlati u formi za postavljanje parametara
6. postavljanje parametara problema
7. pobolj�ati toolbar
8. dodati horz i vert slidere za frmOptPlan
9. provjera tipa operatora kod pridru�ivanja u ESOP-u
		- da li da se i u Base stavi provera i definira na�in da svi operatori znaju re�i nad kakvim tipom operiraju
10. pomicanje i brisanje objekata u OptPlan-u
11. PERZISTENCIJA OPTPLAN-a
12. EXCEPTION HANDLING
		- svugdje kod cast-ova u kodu provjeriti tipove !!!
		- uspostaviti konzistentni na�in obrade izuzetaka, a u ESOP-u posebnu pa�nju obratiti na njihovo hvatanje
13. pobolj�ati sve GetStringRepresentation() funkcije
		- razmisliti o overloadu ToString() funkcije
14. problem najboljih poznatih rje�enja za test cases
15. WinTester zapo�eti
16. ugraditi pra�enje broja evaluacija funkcije cilja
17. STATISTIKA i ANALIZA
		- kako uspore�ivati generirane podatke !!??
18. statisti�ko pra�enje rada operatora
		- algoritam kontrolira kada se pozivaju odre�eni operatori i omogu�uje obavljanje odre�enih akcija
19. Provjera EKSTENZIBILNOSTI ESOP-a
		- definirati neki problem, operatore i algoritme izvan ESOP-a i vidjeti kako se uklapaju
		- pitanje korisni�ki definiranih reprezentacija

		
		
TEHNI�KI DIO
1. Algoritmi:
		- SSGA, Holland GA (EC1 - str 65), Whitley GENITOR, Syswerda GA
				- definirati FitnessBasedGA
		- CHNA, CH_I1, SPEA, MOGA
		- SA
		- Local search - istra�iti koncept susjedstva (neighborhood)
				- stohastic hill climber
2. Operatori
		- operatori za Real, Integer i Permutation
		- KOMPOZITNI OPERATORI ??!!
		- selekcija: tournament i SUS
3. Problemi
		- dovr�iti test cases za SOWC
		- definirati QAP
				- Assignment reprezentacija ?
		- definirati JSSP
		- definirati VRP
4. Observed savers
		- fitness related
		- constraint related
		- convergence related
		- improvement rate
		- age-related stuff
		- kako pratiti razvoj cijele populacije rje�enja - kontrole za vizualizaciju ?
5. Dovr�iti vizualizacijske kontrole
		- Function2D (s mogu�no��u prikaza vi�e krivulja na jednom grafu)
				- mogu�nost automatskog pode�avanja osi nakon analize skupa podataka
		- ParetoSet2D