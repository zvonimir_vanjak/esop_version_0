// Base.h

#pragma once

#include <vector>

using namespace System;

using std::vector;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			public enum class EObjectiveType { MAX_OBJECTIVE, MIN_OBJECTIVE };

			ref class	ISingleObjectiveSolution;
			ref class	ISingleObjectiveSolutionWC;
			ref class	IMultiObjectiveSolution;
			ref class	IMultiObjectiveSolutionWC;

			/*********************************************************************************************************/
			public ref class VectorDouble
			{
			public:
				VectorDouble() {
					_vecObjValues = new vector<double>();
				}
				operator	vector<double> *() { return _vecObjValues; }
				void			resize(int n) { _vecObjValues->resize(n); }
				double		getValue(int i) { return (*_vecObjValues)[i]; }
				void			setValue(int i, double val) { (*_vecObjValues)[i] = val; }

			public:
				vector<double> *_vecObjValues;
			};


			/*********************************************************************************************************/
			public ref class IProblem abstract
			{
			};

			public ref class IProblemWC abstract : public IProblem
			{
			public:
				virtual int			getNumConstraints() = 0;
				virtual String^	getConstraintName(int i) = 0;
			};

			public ref class ISingleObjectiveProblem abstract : public IProblem
			{
			public:
				virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance) = 0;

				void						setObjectiveType(EObjectiveType inType);
				EObjectiveType	getObjectiveType();

			protected:
				EObjectiveType		_enProblemType;
			};

			public ref class ISingleObjectiveProblemWC abstract : public IProblemWC
			{
			public:
				virtual double	calcObjective(ISingleObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcConstrValues) =0;

				void						setObjectiveType(EObjectiveType inType);
				EObjectiveType	getObjectiveType();

			protected:
				EObjectiveType		_enProblemType;
			};

			public ref class IMultiObjectiveProblem abstract : public IProblem
			{
			public:
				IMultiObjectiveProblem();

				virtual void		calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^val) = 0;

				virtual int			getNumObjectives() = 0;
				virtual String^	getObjectiveName(int i) = 0;

				void										setObjectiveType(int ObjFuncInd, EObjectiveType inType);
				EObjectiveType					getObjectiveType(int ObjFuncInd);
				array<EObjectiveType>^	getObjectiveType();

			protected:
				array<EObjectiveType>		^_enProblemType;
			};

			public ref class IMultiObjectiveProblemWC abstract : public IProblemWC
			{
			public:
				IMultiObjectiveProblemWC();

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) = 0;

				virtual int			getNumObjectives() = 0;
				virtual String^	getObjectiveName(int i) = 0;

				void										setObjectiveType(int ObjFuncInd, EObjectiveType inType);
				EObjectiveType					getObjectiveType(int ObjFuncInd);
				array<EObjectiveType>^	getObjectiveType();

			protected:
				array<EObjectiveType>		^_enProblemType;
			};
		}
	}
}