// Base.h

#pragma once

//#include "DynamicSupport.h"

#include "BaseSolutions.h"
#include "BaseProblems.h"

#include "BaseOperatorInterfaces.h"
#include "BaseResults.h"
#include "BaseResultsIntermediate.h"

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			// forward declarations
			ref class IterativeAlgorithm;

			/*********************************************************************************************************/
			public interface class IOptimizationTerminator
			{
			public:
				virtual String^	GetRequestedValueProviderClassName() = 0;
				virtual String^	GetTerminatorDesc() = 0;
				virtual void		SetParameter(int par) = 0;

				virtual	bool		IsTermConditionSatisfied(IterativeAlgorithm ^CurrContext) = 0;
			};

			public interface class IOptTermIterNumProvider
			{
				virtual int			getCurrIterNum() = 0;
			};
			public interface class IOptTermDurationProvider
			{
				virtual double	getCurrTimeSpanInMs() = 0;
			};
			public interface class IOptTermFuncEvalNumProvider
			{
				virtual int			getTotalFuncEvalNum() = 0;
			};

			/*********************************************************************************************************/
			public ref class IterativeAlgorithm abstract :	public IOptTermIterNumProvider,
																											public IOptTermDurationProvider
			{
			public:
				IterativeAlgorithm();

				virtual	int			Initialize() = 0;
				virtual	int			PerformIteration() = 0;
				virtual	void		Run(OptimizationResult^	outRes);

				virtual int				getCurrIterNum();
				virtual double		getCurrTimeSpanInMs();

				void			setOptTerminator(IOptimizationTerminator^ inOptTerm);
				IntermediateValueMasterObserver^	getMasterObserver();

			protected:
				int				_CurrIterNum;
				DateTime	_OptStartTime;

				IOptimizationTerminator						^_OptTerm;
				IntermediateValueMasterObserver		^_IntermedResObserver;
			};

			/*********************************************************************************************************/
			public ref class GeneralIterativeAlgorithm abstract : public IterativeAlgorithm
			{
			public:
				void	setSolutionInstance(ISolution ^inSolutionInstance) { _SolutionInstance = inSolutionInstance; }

			protected:
				ISolution		^_SolutionInstance;
			};
			public ref class SingleObjectiveIterativeAlgorithm abstract : public IterativeAlgorithm
			{
			public:
				void	setSolutionInstance(ISingleObjectiveSolution ^inSolutionInstance);

			protected:
				ISingleObjectiveSolution		^_SolutionInstance;
			};
			public ref class SingleObjectiveIterativeAlgorithmWC abstract : public IterativeAlgorithm
			{
			public:
				void	setSolutionInstance(ISingleObjectiveSolutionWC ^inSolutionInstance);

			protected:
				ISingleObjectiveSolutionWC		^_SolutionInstance;
			};
			public ref class MultiObjectiveIterativeAlgorithm abstract : public IterativeAlgorithm
			{
			public:
				void	setSolutionInstance(IMultiObjectiveSolution ^inSolutionInstance);

			protected:
				IMultiObjectiveSolution		^_SolutionInstance;
			};

			public ref class MultiObjectiveIterativeAlgorithmWC abstract : public IterativeAlgorithm
			{
			public:
				void	setSolutionInstance(IMultiObjectiveSolutionWC ^inSolutionInstance);

			protected:
				IMultiObjectiveSolutionWC		^_SolutionInstance;
			};
		}
	}
}