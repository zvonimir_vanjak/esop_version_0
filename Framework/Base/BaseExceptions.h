#pragma once

#include "DynamicSupport.h"

using namespace System::Exception;
using namespace ESOP::Framework::DynamicSupport;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			public ref class ESOPException : public System::Exception
			{
			};

			public ref class NotYetImplemented : public ESOPException
			{
			};

			public ref class WrongObjectType : public ESOPException
			{
			public:
				WrongObjectType(String ^ExpectedType, String ^FunctionDesc) {}
			};
		}
	}
}