// Base.h

#pragma once

#include <vcclr.h>
#include <vector>

#include "BaseResults.h"

using namespace std;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			// forward declarations
			ref class IterativeAlgorithm;

			/*********************************************************************************************************/
			public interface class IIntermediateTimer
			{
			public:
				virtual void	SetParameter(int par) = 0;
				virtual	bool	IsTimeToSave(ObservedValueTag ^inTag) = 0;
			};

			public interface class IIntermediateValueProvider
			{
			};

			public interface class IObservedValueSaver
			{
			public:
				// TODO - zahtijeva sigurno i nekog IIntermediateValueProvider-a !!!
				virtual String^		getRequestedValueProviderClassName() = 0;
				virtual String^		getObserverDesc() = 0;
				
				virtual void			setCollection(ObservedValuesCollection ^inCollToSave) = 0;
				// kod implementacije ove funkcije u izvedenim klasama, provider se cast-a na neku izvedenicu od IIntermediateValueProvider-a
				virtual void			setProvider(IIntermediateValueProvider ^provider) = 0;

				virtual void			Save(ObservedValueTag ^inTag) = 0;
			};

			public ref class IntermediateValueObserver 
			{
			public:
				void		setTimer(IIntermediateTimer ^inTimer);
				void		setSaver(IObservedValueSaver ^inSaver);

				IIntermediateTimer^			getTimer();
				IObservedValueSaver^		getSaver();

			private:
				IIntermediateTimer		^_Timer;
				IObservedValueSaver		^_Saver;
			};

			typedef gcroot<IntermediateValueObserver ^>		GC_IntermedValueObserver;

			public ref class IntermediateValueMasterObserver
			{
			public:
				IntermediateValueMasterObserver()
				{
					_listObs = new vector<GC_IntermedValueObserver>();
				}
				~IntermediateValueMasterObserver()
				{
					delete _listObs;
				}
				void		AddObserver(IntermediateValueObserver ^inObs);

				void		Check(ObservedValueTag ^inTag);
				void		UpdateTimers();

			public:
				vector<GC_IntermedValueObserver>		*_listObs;
			};
		}
	}
}