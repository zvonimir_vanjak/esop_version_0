#include "stdafx.h"

#include "BaseResultsIntermediate.h"

using namespace ESOP::Framework::Base;


/***************************************************************************************/
void		IntermediateValueObserver::setTimer(IIntermediateTimer ^inTimer)
{
	_Timer = inTimer;
}
void		IntermediateValueObserver::setSaver(IObservedValueSaver ^inSaver)
{
	_Saver = inSaver;
}
IIntermediateTimer^			IntermediateValueObserver::getTimer()
{
	return _Timer;
}
IObservedValueSaver^		IntermediateValueObserver::getSaver()
{
	return _Saver;
}

/***************************************************************************************/
void		IntermediateValueMasterObserver::AddObserver(IntermediateValueObserver ^ inObs)
{
	_listObs->push_back(inObs);
}

void		IntermediateValueMasterObserver::Check(ObservedValueTag ^inTag) 
{
	std::vector<GC_IntermedValueObserver>::const_iterator it = _listObs->begin();

	while( it != _listObs->end() )
	{
		IIntermediateTimer^ timer = (*it)->getTimer();
		if( timer->IsTimeToSave(inTag) )
			(*it)->getSaver()->Save(inTag);
		
		it++;
	}
}
void		IntermediateValueMasterObserver::UpdateTimers() 
{
}

