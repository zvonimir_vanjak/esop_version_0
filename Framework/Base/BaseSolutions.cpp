#include "stdafx.h"

#include "BaseSolutions.h"

using namespace ESOP::Framework::Base;


/***************************************************************************************/
ISolutionContainer::ISolutionContainer()
{
	_vecSolutions = new vector<gcroot<ISolution ^> >();
}
void		ISolutionContainer::Initialize(int inPopSize, ISolution ^Sol)
{
	// isprazni stari sadr�aj
	for( int i=0; i<(int) _vecSolutions->size(); i++ )
		delete (*_vecSolutions)[i];
	_vecSolutions->clear();

	for( int i=0; i<inPopSize; i++ )
	{
		gcroot<ISolution^> NewSol = Sol->Clone();
		
		NewSol->GenerateRandom();
		_vecSolutions->push_back(NewSol);
	}
}
int			ISolutionContainer::getSize() 
{ 
	return (int) _vecSolutions->size(); 
}
ISolution^	ISolutionContainer::getIndividual(int Index)
{
	return ((*_vecSolutions)[Index]);
}
void		ISolutionContainer::setIndividual(int Index, ISolution ^inInd)
{
	(*_vecSolutions)[Index] = inInd;
}

void		ISolutionContainer::replaceIndividual(int IndToReplace, ISolution ^newIndividual)
{
	(*_vecSolutions)[IndToReplace] = newIndividual;
}
void		ISolutionContainer::addNewSolution(ISolution ^NewInd)
{
	_vecSolutions->push_back(NewInd);
}
void		ISolutionContainer::removeIndividual(int IndToRemove)
{
	_vecSolutions->erase(_vecSolutions->begin() + IndToRemove);
}

void	ISolutionContainer::Destroy()
{
	for( int i=0; i<getSize(); i++ )
		(*_vecSolutions)[i] = 0;

	_vecSolutions->clear();
}
void	ISolutionContainer::Clear()
{
//	_vecSolutions.clear();
}

/**************************************************************************************/
ISingleObjectiveSolution::ISingleObjectiveSolution()
{
}
ISingleObjectiveSolution::ISingleObjectiveSolution(ISingleObjectiveSolution ^copy)
{
	_CurrObjectiveVal = copy->_CurrObjectiveVal;

	_Problem = copy->_Problem;
	_CurrSol = copy->_CurrSol->Clone();
}
void		ISingleObjectiveSolution::UpdateObjectiveValue()
{
	_CurrObjectiveVal = _Problem->calcObjective(this);
}
double	ISingleObjectiveSolution::getCurrObjectiveValue()
{
	return _CurrObjectiveVal;
}
void		ISingleObjectiveSolution::setSingleObjectiveProblem(ISingleObjectiveProblem ^inProblem)
{
	_Problem = inProblem;
}

/**************************************************************************************/
ISingleObjectiveSolutionWC::ISingleObjectiveSolutionWC()
{
}
ISingleObjectiveSolutionWC::ISingleObjectiveSolutionWC(ISingleObjectiveSolutionWC ^copy)
{
	_CurrObjectiveVal = copy->_CurrObjectiveVal;

	int		NumConstr = copy->getNumConstraints();
	_vecCurrConstraintVal->resize(NumConstr);
	for( int i=0; i<NumConstr; i++ )
		(*_vecCurrConstraintVal)[i] = (*copy->_vecCurrConstraintVal)[i];

	_Problem = copy->_Problem;
	_CurrSol = copy->_CurrSol->Clone();
}
void		ISingleObjectiveSolutionWC::setSingleObjectiveProblemWC(ISingleObjectiveProblemWC ^inProblem)
{
	_Problem = inProblem;
	_vecCurrConstraintVal->resize(_Problem->getNumConstraints());
}
void		ISingleObjectiveSolutionWC::UpdateObjectiveValue()
{
	_CurrObjectiveVal = _Problem->calcObjective(this, _vecCurrConstraintVal);
}
double	ISingleObjectiveSolutionWC::getCurrObjectiveValue()
{
	return _CurrObjectiveVal;
}
int			ISingleObjectiveSolutionWC::getNumConstraints()
{
	return _Problem->getNumConstraints();
}
int			ISingleObjectiveSolutionWC::getNumViolatedConstraints()
{
	int		Count = 0;
	for( int i=0; i<getNumConstraints(); i++ )
	{
		if( (*_vecCurrConstraintVal)[i] > 0 )
			Count++;
	}
	return Count;
}

bool		ISingleObjectiveSolutionWC::isFeasible()
{
	// ako su vrijednosti svih ogranicenja < 0 , onda je
	for( int i=0; i<getNumConstraints(); i++ )
	{
		if( (*_vecCurrConstraintVal)[i] > 0 )
			return false;
	}
	return true;
}

VectorDouble^	ISingleObjectiveSolutionWC::getCurrConstraintValues()
{
	return _vecCurrConstraintVal;
}
/**************************************************************************************/
IMultiObjectiveSolution::IMultiObjectiveSolution()
{
	_vecCurrObjectiveVal = gcnew VectorDouble();
}
IMultiObjectiveSolution::IMultiObjectiveSolution(IMultiObjectiveSolution ^copy)
{
	int		NumObj = copy->getNumObjectives();

	_vecCurrObjectiveVal = gcnew VectorDouble();
	_vecCurrObjectiveVal->resize(NumObj);
	for( int i=0; i<NumObj; i++ )
		_vecCurrObjectiveVal->setValue(i, copy->_vecCurrObjectiveVal->getValue(i));

	_Problem = copy->_Problem;
	_CurrSol = copy->_CurrSol->Clone();
}
VectorDouble^ IMultiObjectiveSolution::getCurrObjectiveValues()
{
//	return *_vecCurrObjectiveVal;
	return _vecCurrObjectiveVal;
}
void		IMultiObjectiveSolution::UpdateObjectiveValue()
{
	_Problem->calcObjective(this, _vecCurrObjectiveVal);
}
void		IMultiObjectiveSolution::setMultiObjectiveProblem(IMultiObjectiveProblem ^inProblem)
{
	_Problem = inProblem;
	_vecCurrObjectiveVal->resize(_Problem->getNumObjectives());
}
int			IMultiObjectiveSolution::getNumObjectives()
{
	return _Problem->getNumObjectives();
}

// NAPOMENA: vrijedi za funkciju cilja u kojoj se sve komponente minimiziraju !!!
bool		IMultiObjectiveSolution::isDominatedBy(IMultiObjectiveSolution ^Sol2)
{
	IMultiObjectiveSolution ^Sol1 = this;

	VectorDouble	^a = Sol1->getCurrObjectiveValues();
	VectorDouble	^b = Sol2->getCurrObjectiveValues();

	// da bi SolInstance dominirao nad this jedinkom, moraju sve vrijednosti komponenti funkcija cilja biti bolje ili jednake
	bool		bFoundBetter = false;
	for( int i=0; i<Sol2->getNumObjectives(); i++ )
	{
		if( Sol2->_Problem->getObjectiveType(i) == EObjectiveType::MIN_OBJECTIVE )
		{
			if( b->getValue(i) > a->getValue(i) )		return false;				
			if( b->getValue(i) < a->getValue(i) )		bFoundBetter = true;
		}
		else {
			if( b->getValue(i) < a->getValue(i) )		return false;				
			if( b->getValue(i) > a->getValue(i) )		bFoundBetter = true;
		}
	}

	// ako smo do�li dovde zna�i da su komponente od b[i] bile manje ili barem jednake onima od a
	if( bFoundBetter == true )
		return true;
	else
		return false;

}

bool		IMultiObjectiveSolution::isCoveredBy(IMultiObjectiveSolution ^Sol2)
{
	IMultiObjectiveSolution ^Sol1 = this;

	if( Sol1->isDominatedBy(Sol2) )
		return true;

	// ako nije dominirana moramo provjeriti jesu li jednake
	VectorDouble	^a = Sol1->getCurrObjectiveValues();
	VectorDouble	^b = Sol2->getCurrObjectiveValues();

	for( int i=0; i<Sol2->getNumObjectives(); i++ ) 
	{
		if( b->getValue(i) != a->getValue(i) )		
			return false;					// onda sigurno nije pokrivena
	}
	return true;
}

/**************************************************************************************/
IMultiObjectiveSolutionWC::IMultiObjectiveSolutionWC()
{
	_vecCurrObjectiveVal = gcnew VectorDouble();
	_vecCurrConstraintVal = gcnew VectorDouble();
}
IMultiObjectiveSolutionWC::IMultiObjectiveSolutionWC(IMultiObjectiveSolutionWC ^copy)
{
	int		NumObj		= copy->getNumObjectives();
	int		NumConstr = copy->getNumConstraints();

	_vecCurrObjectiveVal = gcnew VectorDouble();
	_vecCurrObjectiveVal->resize(NumObj);
	for( int i=0; i<NumObj; i++ )
		_vecCurrObjectiveVal->setValue(i, copy->_vecCurrObjectiveVal->getValue(i));

	_vecCurrConstraintVal = gcnew VectorDouble();
	_vecCurrConstraintVal->resize(NumConstr);
	for( int i=0; i<NumConstr; i++ )
		_vecCurrConstraintVal->setValue(i, copy->_vecCurrConstraintVal->getValue(i));

	_Problem = copy->_Problem;
	_CurrSol = copy->_CurrSol->Clone();
}
VectorDouble^	IMultiObjectiveSolutionWC::getCurrObjectiveValues()
{
	return _vecCurrObjectiveVal;
}
VectorDouble^	IMultiObjectiveSolutionWC::getCurrConstraintValues()
{
	return _vecCurrConstraintVal;
}
void		IMultiObjectiveSolutionWC::UpdateObjectiveValue()
{
	_Problem->calcObjective(this, _vecCurrObjectiveVal, _vecCurrConstraintVal);
}
void		IMultiObjectiveSolutionWC::setMultiObjectiveProblemWC(IMultiObjectiveProblemWC ^inProblem)
{
	_Problem = inProblem;
	_vecCurrObjectiveVal->resize(_Problem->getNumObjectives());
	_vecCurrConstraintVal->resize(_Problem->getNumConstraints());
}
int			IMultiObjectiveSolutionWC::getNumObjectives()
{
	return _Problem->getNumObjectives();
}
int			IMultiObjectiveSolutionWC::getNumConstraints()
{
	return _Problem->getNumConstraints();
}
bool		IMultiObjectiveSolutionWC::isDominatedBy(IMultiObjectiveSolutionWC ^Sol2)
{
	IMultiObjectiveSolutionWC ^Sol1 = this;

	VectorDouble	^a = Sol1->getCurrObjectiveValues();
	VectorDouble	^b = Sol2->getCurrObjectiveValues();

	// da bi SolInstance dominirao nad this jedinkom, moraju sve vrijednosti komponenti funkcija cilja biti bolje ili jednake
	bool		bFoundBetter = false;
	for( int i=0; i<Sol2->getNumObjectives(); i++ )
	{
		if( Sol2->_Problem->getObjectiveType(i) == EObjectiveType::MIN_OBJECTIVE )
		{
			if( b->getValue(i) > a->getValue(i) )		return false;				
			if( b->getValue(i) < a->getValue(i) )		bFoundBetter = true;
		}
		else {
			if( b->getValue(i) < a->getValue(i) )		return false;				
			if( b->getValue(i) > a->getValue(i) )		bFoundBetter = true;
		}
	}

	// ako smo do�li dovde zna�i da su komponente od b[i] bile manje ili barem jednake onima od a
	if( bFoundBetter == true )
		return true;
	else
		return false;
}

bool		IMultiObjectiveSolutionWC::isCoveredBy(IMultiObjectiveSolutionWC ^Sol2)
{
	IMultiObjectiveSolutionWC ^Sol1 = this;

	if( Sol1->isDominatedBy(Sol2) )
		return true;

	// ako nije dominirana moramo provjeriti jesu li jednake
	VectorDouble	^a = Sol1->getCurrObjectiveValues();
	VectorDouble	^b = Sol2->getCurrObjectiveValues();

	for( int i=0; i<Sol2->getNumObjectives(); i++ ) {
		if( b->getValue(i) != a->getValue(i) )		
			return false;					// onda sigurno nije pokrivena
	}
	return true;
}
bool		IMultiObjectiveSolutionWC::isDominatedByFeasible(IMultiObjectiveSolutionWC ^Sol2)
{
	IMultiObjectiveSolutionWC ^Sol1 = this;

	VectorDouble	^a = Sol1->getCurrObjectiveValues();
	VectorDouble	^b = Sol2->getCurrObjectiveValues();

	// da bi SolInstance dominirao nad this jedinkom, moraju sve vrijednosti komponenti funkcija cilja biti bolje ili jednake
	bool		bFoundBetter = false;
	for( int i=0; i<Sol2->getNumObjectives(); i++ )
	{
		if( Sol2->_Problem->getObjectiveType(i) == EObjectiveType::MIN_OBJECTIVE )
		{
			if( b->getValue(i) > a->getValue(i) )		return false;				
			if( b->getValue(i) < a->getValue(i) )		bFoundBetter = true;
		}
		else {
			if( b->getValue(i) < a->getValue(i) )		return false;				
			if( b->getValue(i) > a->getValue(i) )		bFoundBetter = true;
		}
	}

	// ako smo do�li dovde zna�i da su komponente od b[i] bile manje ili barem jednake onima od a
	if( bFoundBetter == true )
	{
		// e sad jos mora biti i feasible
		if( Sol2->isFeasible() == true )
			return true;	
		else
			return false;
	}
	else
		return false;
}

bool		IMultiObjectiveSolutionWC::isCoveredByFeasible(IMultiObjectiveSolutionWC ^Sol2)
{
	IMultiObjectiveSolutionWC ^Sol1 = this;

	if( Sol1->isDominatedBy(Sol2) )
		return true;

	// ako nije dominirana moramo provjeriti jesu li jednake
	VectorDouble	^a = Sol1->getCurrObjectiveValues();
	VectorDouble	^b = Sol2->getCurrObjectiveValues();

	for( int i=0; i<Sol2->getNumObjectives(); i++ ) {
		if( b->getValue(i) != a->getValue(i) )		
			return false;					// onda sigurno nije pokrivena
	}
	// e sad jos mora biti i feasible
	if( Sol2->isFeasible() == true )
		return true;	
	else
		return false;

	return true;
}
bool		IMultiObjectiveSolutionWC::isFeasible()
{
	// ako su vrijednosti svih ogranicenja < 0 , onda je
	for( int i=0; i<getNumConstraints(); i++ )
	{
		if( (*_vecCurrConstraintVal)[i] > 0 )
			return false;
	}
	return true;
}

int			IMultiObjectiveSolutionWC::getNumViolatedConstraints()
{
	int		Count = 0;
	for( int i=0; i<getNumConstraints(); i++ )
	{
		if( (*_vecCurrConstraintVal)[i] > 0 )
			Count++;
	}
	return Count;
}
