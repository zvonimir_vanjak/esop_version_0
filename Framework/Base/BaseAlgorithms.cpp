#include "stdafx.h"

#include "BaseAlgorithms.h"

using namespace ESOP::Framework::Base;

/*********************************************************************************************************************/
IterativeAlgorithm::IterativeAlgorithm()
{
	_IntermedResObserver = gcnew IntermediateValueMasterObserver();
}
void	IterativeAlgorithm::setOptTerminator(IOptimizationTerminator^ inOptTerm)
{
	_OptTerm = inOptTerm;
}
IntermediateValueMasterObserver^	IterativeAlgorithm::getMasterObserver()
{
	return _IntermedResObserver;
}
void	IterativeAlgorithm::Run(OptimizationResult^	outRes)
{
	Initialize();

	_CurrIterNum = 0;
	_OptStartTime = DateTime::Now;

	while( _OptTerm->IsTermConditionSatisfied(this) == false )
	{
		PerformIteration();

		ObservedValueTag		^tag = gcnew ObservedValueTag();
		tag->_IterNum = getCurrIterNum();
		tag->_TimeMs  = getCurrTimeSpanInMs();

		_IntermedResObserver->Check(tag);

		_CurrIterNum++;
	}

	int a = 3, b;
	b = 3 + a;
}
int				IterativeAlgorithm::getCurrIterNum()
{
	return _CurrIterNum;
}
double	IterativeAlgorithm::getCurrTimeSpanInMs()
{
	return (double) (DateTime::Now.Ticks - _OptStartTime.Ticks) / 10000;
}

/*********************************************************************************************************************/
void SingleObjectiveIterativeAlgorithm::setSolutionInstance(ISingleObjectiveSolution ^inSolutionInstance)
{
	_SolutionInstance = inSolutionInstance;
}
/*********************************************************************************************************************/
void SingleObjectiveIterativeAlgorithmWC::setSolutionInstance(ISingleObjectiveSolutionWC ^inSolutionInstance)
{
	_SolutionInstance = inSolutionInstance;
}

/*********************************************************************************************************************/
void MultiObjectiveIterativeAlgorithm::setSolutionInstance(IMultiObjectiveSolution ^inSolutionInstance)
{
	_SolutionInstance = inSolutionInstance;
}

/*********************************************************************************************************************/
void	MultiObjectiveIterativeAlgorithmWC::setSolutionInstance(IMultiObjectiveSolutionWC ^inSolutionInstance)
{
	_SolutionInstance = inSolutionInstance;
}
