// Base.h

#pragma once

#include <vector>
#include <vcclr.h>

#define _M_CEE_PURE

using std::vector;
using namespace System;
using namespace System::Collections;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			/*********************************************************************************************************/
			public interface class IDesignVariable 			
			{
			public:
				virtual IDesignVariable^	Clone() = 0;
				virtual void							GenerateRandom() = 0;

				virtual String^		GetValueStringRepresentation() = 0;
			};
			
			public interface class IInteger : public IDesignVariable
			{
			public:
				virtual	int			getValue() = 0;
				virtual	void		setValue(int	inVal) = 0;
			};
			public interface class IIntegerArray : public IDesignVariable
			{
			public:
				virtual	int			getValue(int VarInd) = 0;
				virtual	void		setValue(int VarInd, int inVal) = 0;
			};
			public interface class IReal : public IDesignVariable
			{
			public:
				virtual	double	getValue() = 0;
				virtual	void		setValue(double inVal) = 0;
			};
			public interface class IRealArray : public IDesignVariable
			{
			public:
				virtual	double	getValue(int VarInd) = 0;
				virtual	void		setValue(int VarInd, double inVal) = 0;
			};
			public interface class IPermutation : public IDesignVariable
			{
			public:
				virtual int			getNth(int i) = 0;
			};
			public interface class ICompositeVariable : public IDesignVariable
			{
			public:
				virtual IDesignVariable^	GetPart(int PartIndex) = 0;
				virtual int								AddPart(IDesignVariable ^inVar) = 0;
			};
		}
	}
}