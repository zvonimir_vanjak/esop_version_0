#include "stdafx.h"

#include "BaseProblems.h"

using namespace ESOP::Framework::Base;

void		ISingleObjectiveProblem::setObjectiveType(EObjectiveType inType) 
{ 
	_enProblemType = inType; 
}

EObjectiveType	ISingleObjectiveProblem::getObjectiveType() 
{ 
	return _enProblemType; 
}

void		ISingleObjectiveProblemWC::setObjectiveType(EObjectiveType inType) 
{ 
	_enProblemType = inType; 
}

EObjectiveType	ISingleObjectiveProblemWC::getObjectiveType() 
{ 
	return _enProblemType; 
}

IMultiObjectiveProblem::IMultiObjectiveProblem()
{
	_enProblemType = gcnew array<EObjectiveType>(getNumObjectives());
}

void		IMultiObjectiveProblem::setObjectiveType(int ObjFuncInd, EObjectiveType inType) 
{ 
	_enProblemType[ObjFuncInd] = inType; 
}

EObjectiveType	IMultiObjectiveProblem::getObjectiveType(int ObjFuncInd) 
{ 
	return _enProblemType[ObjFuncInd]; 
}
array<EObjectiveType> ^IMultiObjectiveProblem::getObjectiveType()
{
	return _enProblemType; 
}

IMultiObjectiveProblemWC::IMultiObjectiveProblemWC()
{
	_enProblemType = gcnew array<EObjectiveType>(getNumObjectives());
}

void		IMultiObjectiveProblemWC::setObjectiveType(int ObjFuncInd, EObjectiveType inType) 
{ 
	_enProblemType[ObjFuncInd] = inType; 
}

EObjectiveType	IMultiObjectiveProblemWC::getObjectiveType(int ObjFuncInd) 
{ 
	return _enProblemType[ObjFuncInd]; 
}
array<EObjectiveType> ^IMultiObjectiveProblemWC::getObjectiveType()
{
	return _enProblemType; 
}
