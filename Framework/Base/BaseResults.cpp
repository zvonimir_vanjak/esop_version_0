#include "stdafx.h"
#include "BaseResults.h"

using namespace System;
using namespace System::Collections;

using namespace ESOP::Framework::Base;

void		SOResult::setResult(ISingleObjectiveSolution ^inSol) 
{ 
	_Solution = inSol; 
}
ISingleObjectiveSolution^		SOResult::getResult() 
{ 
	return _Solution; 
}

String ^SOResult::GetResultStringRepresentation() 
{ 
	return " ";
	/*
	// treba ukomponirati string od postignute vrijednosti funkcije cilja i pripadajućeg rješenja
	double	FuncValue = _Solution->getCurrObjectiveValue();
	String	^SolStringRepr = (_Solution->getDesignVar()->GetValueStringRepresentation());

	String	^RetStr = "Func .val = " + FuncValue.ToString() + " ,  Sol = " + SolStringRepr;

	return RetStr; 
	*/
}

ISingleObjectiveSolutionWC^		SOWCResult::getResult() 
{ 
	return _Solution; 
}

String ^SOWCResult::GetResultStringRepresentation() 
{ 
	return gcnew String(" "); 
}

//////////////////////////////////////////////////////////////////////////
MOResult::MOResult()
{
	_arlSolution = gcnew ArrayList();
}
int		MOResult::getNumResult()
{
	return _arlSolution->Count;
}
void		MOResult::setResult(int Ind, IMultiObjectiveSolution ^inSol) 
{
	_arlSolution->Add(inSol);
}
IMultiObjectiveSolution^		MOResult::getResult(int IndRes) 
{ 
	return (IMultiObjectiveSolution ^) _arlSolution[IndRes]; 
}
String ^MOResult::GetResultStringRepresentation() 
{ 
	return gcnew String(" "); 
}

//////////////////////////////////////////////////////////////////////////
MOWCResult::MOWCResult()
{
	_arlSolution = gcnew ArrayList();
}
int		MOWCResult::getNumResult()
{
	return _arlSolution->Count;
}
void		MOWCResult::setResult(int Ind, IMultiObjectiveSolutionWC ^inSol) 
{ 
	_arlSolution->Add(inSol);
}
IMultiObjectiveSolutionWC^		MOWCResult::getResult(int IndRes) 
{ 
	return (IMultiObjectiveSolutionWC ^) _arlSolution[IndRes]; 
}
String ^MOWCResult::GetResultStringRepresentation() 
{ 
	return gcnew String(" "); 
}
/*********************************************************************************/
ObservedValuesCollection::ObservedValuesCollection(String ^inDesc)
{
	_strDesc = inDesc; 
	_arrValues = gcnew ArrayList();
	_arrValueTags = gcnew ArrayList();
}

void		ObservedValuesCollection::Add(Object ^Value, ObservedValueTag ^inTag)
{
	_arrValues->Add(Value);
	_arrValueTags->Add(inTag);
}

OptimizationResult::OptimizationResult()
{
//	_vecIntermedResults = new vector<GC_ObservedValuesCollection>();
	_arlIntermedResults = gcnew ArrayList();
}
OptimizationResult::~OptimizationResult()
{
//	delete _vecIntermedResults;
}
void		OptimizationResult::SetFinalResult(IResult ^inRes)
{
	_FinalResult = inRes;
}

int			OptimizationResult::getNumObsColl()
{
//	return _vecIntermedResults->size();
	return _arlIntermedResults->Count;
}
ObservedValuesCollection ^OptimizationResult::AddNewObsCollection(String ^collDesc)
{
	ObservedValuesCollection ^newColl = gcnew ObservedValuesCollection(collDesc);
	_arlIntermedResults->Add(newColl);
//	_vecIntermedResults->push_back(newColl);

	return newColl;
}
ObservedValuesCollection ^OptimizationResult::GetObsCollection(int ind)
{
//	return (*_vecIntermedResults)[ind];
	return dynamic_cast<ObservedValuesCollection ^>(_arlIntermedResults[ind]);
}
