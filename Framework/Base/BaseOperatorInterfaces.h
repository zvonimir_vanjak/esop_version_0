#pragma once

//#using "..\debug\base.dll"

#ifndef __BASE_OPERATORS_INTERFACES_H
#define __BASE_OPERATORS_INTERFACES_H

using namespace System;

using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Selection
				public interface class IFitnessBasedSelectionOperator 
				{
				public:
					virtual	void	Select(array<double> ^vecSolFitness, array<int> ^vecOutSelInd, int inReqNumOfSelind) = 0;
				};

				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Fitness assignment
				public interface class IFitnessAssignmentOperator
				{
				public:
					virtual	void	Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness) = 0;
				};

				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Crossover
				public interface class ICrossoverOperator2To1
				{
				public:
					virtual	void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild) = 0;
				};

				public interface class ICrossoverOperator2To2
				{
				public:
					virtual	void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2) = 0;
				};

				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Mutation
				public interface class IMutationOp
				{
				public:
					virtual	void Mutate(IDesignVariable ^Child) = 0;
				};
			}
		}
	}
}

#endif