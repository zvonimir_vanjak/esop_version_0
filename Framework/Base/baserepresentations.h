// Base.h

#pragma once

#include <vector>
#include <vcclr.h>

#define _M_CEE_PURE

using std::vector;


namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			// forward
			interface class IRepresentation;

			/*********************************************************************************************************/
			public interface class IDesignVariable 			{
			public:
				virtual IDesignVariable^	Clone() = 0;
				virtual	IRepresentation^	GetRepresentation() = 0;
			};
			
			public interface class IInteger : public IDesignVariable
			{
			public:
				virtual	int		getValue() = 0;
				virtual	void	setValue(int	inVal) = 0;
			};
			public interface class IIntegerArray : public IDesignVariable
			{
			public:
				virtual	int		getValue(int VarInd) = 0;
				virtual	void	setValue(int VarInd, int inVal) = 0;
			};
			public interface class IReal : public IDesignVariable
			{
			public:
				virtual	double	getValue() = 0;
				virtual	void		setValue(double inVal) = 0;
			};
			public interface class IRealArray : public IDesignVariable
			{
			public:
				virtual	double	getValue(int VarInd) = 0;
				virtual	void		setValue(int VarInd, double inVal) = 0;
			};
			public interface class IPermutation : public IDesignVariable
			{
			public:
			};
			public interface class ICompositeVariable : public IDesignVariable
			{
			public:
				virtual IDesignVariable^	GetPart(int PartIndex) = 0;
				virtual int								AddPart(gcroot<IDesignVariable ^> inVar) = 0;
			};

			/*********************************************************************************************************/
			public interface class IRepresentation 
			{
			public:
				virtual void		GenerateRandom() = 0;
			};

			public interface class ICompositeRepresentation : public IRepresentation
			{
			public:
			};

			public ref class GenericDynamicCompositeRepresentation : public IRepresentation, public ICompositeVariable
			{
			public:
				GenericDynamicCompositeRepresentation() {}
				GenericDynamicCompositeRepresentation(GenericDynamicCompositeRepresentation ^copy)
				{
					for( int i=0; i<(int)copy->_vecParts->size(); i++ )
						AddPart((*(copy->_vecParts))[i]->Clone());
				}

				virtual IDesignVariable^	Clone() 
				{ 
					GenericDynamicCompositeRepresentation	^newObj = gcnew GenericDynamicCompositeRepresentation();

					for( int i=0; i<(int)_vecParts->size(); i++ )
						newObj->AddPart((*_vecParts)[i]->Clone());

					return newObj;
				}
				virtual IRepresentation^	GetRepresentation()  
				{ 
					return this; 
				}

//				virtual String^	getComponentName() { return "Prdo"; }
				virtual void		GenerateRandom()
				{
					// proci kroz sve dijelove i pozvati GenerateRandom
					for( int i=0; i<(int) _vecParts->size(); i++ )
						(*_vecParts)[i]->GetRepresentation()->GenerateRandom();
				}

				virtual IDesignVariable^	GetPart(int PartIndex) 	{	return (*_vecParts)[PartIndex];	}
				virtual int								AddPart(gcroot<IDesignVariable ^> inVar)  { _vecParts->push_back(inVar); return (int) _vecParts->size() - 1;	}

			protected:
				vector<gcroot<IDesignVariable ^> >	*_vecParts;
			};
		}
	}
}