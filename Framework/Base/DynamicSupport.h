#pragma once

#ifndef __DYNAMIC_SUPPORT_H
#define __DYNAMIC_SUPPORT_H

#include "BaseResults.h"

using namespace System;

using ESOP::Framework::Base::OptimizationResult;

namespace ESOP
{
	namespace Framework
	{
		namespace DynamicSupport
		{
			/********************************************************************************/
			// preko ovog interface se prebrojavaju (enumeriraju) dostupni objekti za optimizaciju iz ulaznih DLLova
			public interface class IESOPObject 			{
			public:
				virtual String^		getESOPComponentName() = 0;
				virtual String^		getESOPComponentDesc() = 0;
			};

			/********************************************************************************/
			public interface class IESOPOperator : public IESOPObject
			{
			public:
			};

			public interface class IESOPParameterOperator : public IESOPOperator
			{
			public:
				virtual int				getParamNum() = 0;
				virtual String^		getParamName(int ParamInd) = 0;
				
				virtual void			setParamValue(int Ind, double inVal) = 0;
				virtual double		getParamValue(int Ind) = 0;
			};

			/********************************************************************************/
			public interface class IESOPDesignVariable : public IESOPObject
			{
			public:
				virtual int				getParamNum() = 0;
				virtual String^		getParamName(int ParamInd) = 0;
				
				virtual void			setParamValue(int Ind, double inVal) = 0;
				virtual double		getParamValue(int Ind) = 0;
			};

			public interface class IESOPConstructedDesignVariable : public IESOPDesignVariable 
			{
			public:
				virtual int				getConstructorParamNum() = 0;
				virtual String^		getConstructorParamName(int ParamInd) = 0;
				
				virtual void			setConstructorParamValue(int Ind, double inVal) = 0;
				virtual double		getConstructorParamValue(int Ind) = 0;
			};

			// u stvari, ovo bi trebala biti varijabla inicijalizirana na osnovu problem !!!
			public interface class IESOPFileInitDesignVariable : public IESOPDesignVariable
			{
			};
			/********************************************************************************/
			public interface class IESOPProblem : public IESOPObject
			{
			public:
				// pomo�u ove funkcije Problem ka�e kakvog tipa mora biti _CurrSol dizajn varijabla u SolutionInstance koji �e Problme objekt
				// dobiti kod poziva CalcObjective funkcije
				virtual	String^		getRequestedDesignVar() = 0;  
			};
			public interface class IESOPCompleteProblem : public IESOPProblem
			{
			};
			public interface class IESOPParametrizedProblem : public IESOPProblem
			{
			public:
				virtual int				getParamNum() = 0;
				virtual String^		getParamName(int ParamInd) = 0;
				
				virtual void			setParamValue(int Ind, double inVal) = 0;
				virtual double		getParamValue(int Ind) = 0;
			};
			public interface class IESOPFileInitProblem : public IESOPProblem
			{
			public:
				virtual void	InitFromFile(String ^FileName) = 0;
			};
			public interface class IESOPRuntimeInitProblem : public IESOPProblem
			{
			public:
				virtual void			ShowUIForm() = 0;
			};
			
			/********************************************************************************/
			public interface class IESOPAlgorithm : public IESOPObject
			{
			public:
				virtual	array<String^> ^getAlgorithmInstanceDesc() = 0;

				virtual void			setESOPDesignVariable(IESOPDesignVariable ^SolInst) = 0;
				virtual void			setESOPProblemInstance(IESOPProblem ^SolInst) = 0;

				virtual	int				ESOP_Initialize() = 0;
				virtual	void			ESOP_Run(OptimizationResult ^outRes) = 0;
			};

			public interface class IESOPCompleteAlgorithm  : public IESOPAlgorithm
			{
			public:
				virtual int				getParamNum() = 0;
				virtual String^		getParamName(int ParamInd) = 0;
				
				virtual void			setParamValue(int Ind, double inVal) = 0;
				virtual double		getParamValue(int Ind) = 0;
			};
			public interface class IESOPTemplateAlgorithm  : public IESOPCompleteAlgorithm
			{
			public:
				virtual int				getOperatorNum() = 0;
				virtual String^		getOperatorType(int OperInd) = 0;
				virtual String^		getOperatorID(int OperInd) = 0;

				virtual void			setESOPOperator(int OperIndex, IESOPOperator ^oper) = 0;
			};
		}
	}
}

#endif