// Base.h

#pragma once

#include "BaseSolutions.h"

using namespace System;
using namespace System::Collections;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			/*********************************************************************************************************/
			public interface class IResult
			{
				virtual String ^GetResultStringRepresentation() = 0;
			};

			public ref class SOResult : public IResult
			{
			public:
				void												setResult(ISingleObjectiveSolution ^inSol);
				ISingleObjectiveSolution^		getResult();

				virtual String ^GetResultStringRepresentation();

			public:
				ISingleObjectiveSolution ^_Solution;
			};

			public ref class SOWCResult : public IResult
			{
			public:
				ISingleObjectiveSolutionWC^		getResult();

				virtual String ^GetResultStringRepresentation();

			public:
				int				_NumConstraints;
				ISingleObjectiveSolutionWC ^_Solution;
			};

			public ref class MOResult : public IResult
			{
			public:
				MOResult();

				int				getNumResult();
				void												setResult(int Ind, IMultiObjectiveSolution ^inSol);
				IMultiObjectiveSolution^		getResult(int IndRes);

				virtual String ^GetResultStringRepresentation();

			public:
				int				_NumObjectives;
				ArrayList ^_arlSolution;			// kod Pareto seta ih ima vi�e
			};

			public ref class MOWCResult : public IResult
			{
			public:
				MOWCResult();

				int				getNumResult();
				void													setResult(int Ind, IMultiObjectiveSolutionWC ^inSol);
				IMultiObjectiveSolutionWC^		getResult(int IndRes);

				virtual String ^GetResultStringRepresentation();

			public:
				int				_NumObjectives;
				int				_NumConstraints;
				ArrayList ^_arlSolution;
			};

			/*********************************************************************************/
			public ref class ObservedValueTag
			{
			public:
				int				_IterNum;
				double		_TimeMs;
			};
			public ref class ObservedValuesCollection
			{
			public:
				ObservedValuesCollection(String ^inDesc);
				void		Add(Object ^Value, ObservedValueTag ^inTag);

			public: //TEMP
				String			^_strDesc;
				ArrayList		^_arrValues;
				ArrayList		^_arrValueTags;			// definira trenutak u kojem je snimljenja vrijednost
				ArrayList		^_arrValueToolTips; // kod prikazivanja u grafu, kada se mi�em pre�e preko to�ke, ispisuje se tooltip
			};

			/*********************************************************************************/
			// predstavlja cjelokupno (kona�no) rje�enje optimizacije
			public ref class OptimizationResult
			{
			public:
				OptimizationResult();
				~OptimizationResult();
				void		SetFinalResult(IResult ^inRes);

				int			getNumObsColl();
				ObservedValuesCollection ^AddNewObsCollection(String ^collDesc);
				ObservedValuesCollection ^GetObsCollection(int ind);
			
			public: // TEMP
				String ^_ProblemName;
				String ^_Representation;
				String ^_Result;
				String ^_AlgorithmName;
				array<String ^> ^_AlgorithmDesc;

				int			_TotalIterNum;
				double	_DurationMs;

				IResult			^_FinalResult;
				ArrayList		^_arlIntermedResults;				// kolekcija me�urezultata (ObservedValuesCollection)
			};
		}
	}
}