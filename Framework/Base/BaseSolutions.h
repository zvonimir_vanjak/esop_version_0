// Base.h

#pragma once

#include <vector>
#include <vcclr.h>

#include "BaseDesignVariables.h"
#include "BaseProblems.h"

using std::vector;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			// forward declarations
			ref class	ISingleObjectiveProblem;
			ref class	IMultiObjectiveProblem;
			ref class	ISingleObjectiveProblemWC;
			ref class	IMultiObjectiveProblemWC;

			/*********************************************************************************************************/
			public interface class	ISolution
			{
			public:
				virtual ISolution^				Clone() = 0;
				virtual void							GenerateRandom() = 0;
//				virtual	IRepresentation^	GetRepresentation() = 0;

				virtual void			UpdateObjectiveValue() = 0;
				
				virtual void							setDesignVar(IDesignVariable ^inVar) = 0;
				virtual IDesignVariable^	getDesignVar() = 0;

				virtual	String^		GetStringRepr() = 0;
			};

			public interface class	ISolutionMO : public ISolution
			{
			public:
				virtual int							getNumObjectives();
				virtual VectorDouble^		getCurrObjectiveValues();
			};

			public interface class	ISolutionWC : public ISolution
			{
			public:
				virtual int			getNumConstraints() = 0;
				virtual int			getNumViolatedConstraints() = 0;
				virtual bool		isFeasible() = 0;

				virtual	VectorDouble^		getCurrConstraintValues() = 0;
			};

			//////////////////////////////////////////////////////////////////////////////
			// obavezno uvesti indeksiranje jedinki (po rednom broju) !!! 
			public ref class ISolutionContainer
			{
			public:
				ISolutionContainer();
				virtual ~ISolutionContainer() {}

				virtual	int		getSize() ;
				virtual void	setSize(int Size) {_vecSolutions->resize(Size); }
				virtual	void	addNewSolution(ISolution ^NewInd);

				virtual	ISolution^	getIndividual(int Index);
				virtual	void				setIndividual(int Index, ISolution ^inInd);
				virtual	void	replaceIndividual(int IndToReplace, ISolution ^newIndividual);
				virtual	void	removeIndividual(int IndToRemove);

				void					Destroy();
				void					Clear();

				virtual	void	Initialize(int inSize, ISolution ^inSol);

			public:
				vector<gcroot<ISolution^> >		*_vecSolutions;
			};
			/*********************************************************************************************************/
			public ref class ISingleObjectiveSolution : public ISolution
			{
			public:
				ISingleObjectiveSolution();
				ISingleObjectiveSolution(ISingleObjectiveSolution ^copy);

				void			setSingleObjectiveProblem(ISingleObjectiveProblem ^inProblem);
				const ISingleObjectiveProblem^	getProblem() { return _Problem; }

				double 		getCurrObjectiveValue();
				
				virtual	void							setDesignVar(IDesignVariable ^inVar) { _CurrSol = inVar; }
				virtual	IDesignVariable^	getDesignVar() { 	return _CurrSol; 	}

				virtual void							GenerateRandom()			{	getDesignVar()->GenerateRandom();	}
//				virtual IRepresentation^	GetRepresentation()		{ return _CurrSol->GetRepresentation(); }
				virtual ISolution^				Clone()								{	return gcnew ISingleObjectiveSolution(this);	}
				virtual void							UpdateObjectiveValue() ;

				virtual	String^						GetStringRepr() { return _CurrSol->GetValueStringRepresentation() ; }

			protected:
				double		_CurrObjectiveVal;				// trenutna vrijednost funkcija cilja

				ISingleObjectiveProblem		^_Problem;
				IDesignVariable						^_CurrSol;
			};
			
			public ref class ISingleObjectiveSolutionWC : public ISolutionWC
			{
			public:
				ISingleObjectiveSolutionWC();
				ISingleObjectiveSolutionWC(ISingleObjectiveSolutionWC ^copy);

				void			setSingleObjectiveProblemWC(ISingleObjectiveProblemWC ^inProblem);
				const ISingleObjectiveProblemWC^	getProblem() { return _Problem; }

				double		getCurrObjectiveValue();

				virtual	void							setDesignVar(IDesignVariable ^inVar) { _CurrSol = inVar; }
				virtual	IDesignVariable^	getDesignVar() { 	return _CurrSol; 	}

				virtual	void							GenerateRandom()			{	getDesignVar()->GenerateRandom();	}
//				virtual	IRepresentation^	GetRepresentation()		{ return _CurrSol->GetRepresentation(); }
				virtual ISolution^				Clone()								{	return gcnew ISingleObjectiveSolutionWC(this);	}
				virtual void							UpdateObjectiveValue() ;

				virtual bool							isFeasible();
				virtual int								getNumViolatedConstraints();
				virtual int								getNumConstraints();
				virtual	VectorDouble^			getCurrConstraintValues();

				virtual	String^						GetStringRepr() { return _CurrSol->GetValueStringRepresentation(); }

			protected:
				double					_CurrObjectiveVal;				// trenutne vrijednosti funkcija cilja
				VectorDouble^		_vecCurrConstraintVal;	

				ISingleObjectiveProblemWC	^_Problem;
				IDesignVariable						^_CurrSol;
			};

			public ref class IMultiObjectiveSolution : public ISolutionMO
			{
			public:
				IMultiObjectiveSolution();
				IMultiObjectiveSolution(IMultiObjectiveSolution ^copy);

				void							setMultiObjectiveProblem(IMultiObjectiveProblem ^inProblem);
				const IMultiObjectiveProblem^	getProblem() { return _Problem; }

				virtual int								getNumObjectives();
				virtual VectorDouble^			getCurrObjectiveValues();

				virtual	void							setDesignVar(IDesignVariable ^inVar) { _CurrSol = inVar; }
				virtual	IDesignVariable^	getDesignVar() { 	return _CurrSol; 	}

				virtual void							GenerateRandom()		{	getDesignVar()->GenerateRandom();	}
//				virtual IRepresentation^	GetRepresentation()	{ return _CurrSol->GetRepresentation(); }
				virtual ISolution^				Clone()							{ return gcnew IMultiObjectiveSolution(this);	}
				virtual void							UpdateObjectiveValue() ;

				virtual	String^						GetStringRepr() { return _CurrSol->GetValueStringRepresentation() ; }

				bool	isDominatedBy(IMultiObjectiveSolution ^SolInstance);
				bool	isCoveredBy(IMultiObjectiveSolution ^SolInstance);
	
			protected:
				VectorDouble^		_vecCurrObjectiveVal;

				IMultiObjectiveProblem		^_Problem;
				IDesignVariable						^_CurrSol;
			};

			public ref class IMultiObjectiveSolutionWC : public ISolutionMO, public ISolutionWC
			{
			public:
				IMultiObjectiveSolutionWC();
				IMultiObjectiveSolutionWC(IMultiObjectiveSolutionWC ^copy);

				void										setMultiObjectiveProblemWC(IMultiObjectiveProblemWC ^inProblem);
				const IMultiObjectiveProblemWC^	getProblem() { return _Problem; }

				virtual int								getNumObjectives();
				virtual VectorDouble^ 		getCurrObjectiveValues();

				virtual	void							setDesignVar(IDesignVariable ^inVar) { _CurrSol = inVar; }
				virtual IDesignVariable^	getDesignVar() { 	return _CurrSol; 	}

				virtual ISolution^				Clone()								{ return gcnew IMultiObjectiveSolutionWC(this);}
				virtual	void							GenerateRandom()			{	getDesignVar()->GenerateRandom();	}
//				virtual	IRepresentation^	GetRepresentation()		{ return _CurrSol->GetRepresentation(); }
				virtual void							UpdateObjectiveValue() ;
				
				virtual bool							isFeasible();
				virtual int								getNumViolatedConstraints();
				virtual int								getNumConstraints();
				virtual	VectorDouble^			getCurrConstraintValues();
	
				virtual		String^					GetStringRepr() { return _CurrSol->GetValueStringRepresentation() ; }

				bool	isDominatedBy(IMultiObjectiveSolutionWC ^SolInstance);
				bool	isCoveredBy(IMultiObjectiveSolutionWC ^SolInstance);
				bool	isDominatedByFeasible(IMultiObjectiveSolutionWC ^SolInstance);
				bool	isCoveredByFeasible(IMultiObjectiveSolutionWC ^SolInstance);
				
			protected:
				VectorDouble^		_vecCurrObjectiveVal;				// trenutne vrijednosti funkcija cilja
				VectorDouble^		_vecCurrConstraintVal;	

				IMultiObjectiveProblemWC	^_Problem;
				IDesignVariable						^_CurrSol;
			};
		}
	}
}