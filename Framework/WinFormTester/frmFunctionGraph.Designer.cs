namespace WinFormTester
{
	partial class frmFunctionGraph
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._ctrlNevron = new ESOPVisualizationControls.RealValueCollection_Func2D_Nevron();
			this.SuspendLayout();
			// 
			// _ctrlNevron
			// 
			this._ctrlNevron.Dock = System.Windows.Forms.DockStyle.Fill;
			this._ctrlNevron.Location = new System.Drawing.Point(0, 0);
			this._ctrlNevron.Name = "_ctrlNevron";
			this._ctrlNevron.Size = new System.Drawing.Size(848, 627);
			this._ctrlNevron.TabIndex = 1;
			// 
			// frmFunctionGraph
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(848, 627);
			this.Controls.Add(this._ctrlNevron);
			this.Name = "frmFunctionGraph";
			this.Text = "frmFunctionGraph";
			this.Resize += new System.EventHandler(this.frmFunctionGraph_Resize);
			this.ResumeLayout(false);

		}

		#endregion

		public ESOPVisualizationControls.RealValueCollection_Func2D_Nevron _ctrlNevron;




	}
}