using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace WinFormTester
{
	public partial class frmMOParetoSet : Form
	{
		private ISolutionMO[] _ParetoSet;

		public frmMOParetoSet(ISolutionMO[] inObj)
		{
			_ParetoSet = inObj;
			InitializeComponent();
		}

		private void frmMOParetoSet_Paint(object sender, PaintEventArgs e)
		{
			paretoSet2DColl_as_ScatterGraph1.Show(_ParetoSet);
		}
	}
}