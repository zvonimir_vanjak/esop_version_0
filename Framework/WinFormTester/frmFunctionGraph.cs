using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinFormTester
{
	public partial class frmFunctionGraph : Form
	{
		public frmFunctionGraph()
		{
			InitializeComponent();
		}

		private void frmFunctionGraph_Resize(object sender, EventArgs e)
		{
			_ctrlNevron.Refresh();
		}
	}
}