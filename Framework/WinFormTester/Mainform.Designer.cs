namespace WinFormTester
{
	partial class Mainform
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdSolveAckley = new System.Windows.Forms.Button();
			this.cmdSolveSchwefel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmdSolveSinFunc = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.cmdSolveValenzuelaRendon = new System.Windows.Forms.Button();
			this.cmdSolveSchafferDisconnected = new System.Windows.Forms.Button();
			this.cmdSolveSchaffer = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.cmdSolveSpeedReducer = new System.Windows.Forms.Button();
			this.cmdSolveWeldedBeamDesign = new System.Windows.Forms.Button();
			this.cmdSolveTwoBarTruss = new System.Windows.Forms.Button();
			this.cmdSolveOSY = new System.Windows.Forms.Button();
			this.cmdSolveTNK = new System.Windows.Forms.Button();
			this.cmdSolveSRN = new System.Windows.Forms.Button();
			this.cmdSolveBNH = new System.Windows.Forms.Button();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// cmdSolveAckley
			// 
			this.cmdSolveAckley.Location = new System.Drawing.Point(11, 51);
			this.cmdSolveAckley.Name = "cmdSolveAckley";
			this.cmdSolveAckley.Size = new System.Drawing.Size(111, 23);
			this.cmdSolveAckley.TabIndex = 0;
			this.cmdSolveAckley.Text = "Solve Ackley";
			this.cmdSolveAckley.Click += new System.EventHandler(this.cmdSolveAckley_Click);
			// 
			// cmdSolveSchwefel
			// 
			this.cmdSolveSchwefel.Location = new System.Drawing.Point(11, 80);
			this.cmdSolveSchwefel.Name = "cmdSolveSchwefel";
			this.cmdSolveSchwefel.Size = new System.Drawing.Size(111, 23);
			this.cmdSolveSchwefel.TabIndex = 1;
			this.cmdSolveSchwefel.Text = "Solve Schwefel";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cmdSolveSinFunc);
			this.groupBox1.Controls.Add(this.cmdSolveAckley);
			this.groupBox1.Controls.Add(this.cmdSolveSchwefel);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(145, 115);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Real function SO";
			// 
			// cmdSolveSinFunc
			// 
			this.cmdSolveSinFunc.Location = new System.Drawing.Point(11, 22);
			this.cmdSolveSinFunc.Name = "cmdSolveSinFunc";
			this.cmdSolveSinFunc.Size = new System.Drawing.Size(111, 23);
			this.cmdSolveSinFunc.TabIndex = 2;
			this.cmdSolveSinFunc.Text = "Solve Sin function";
			this.cmdSolveSinFunc.Click += new System.EventHandler(this.cmdSolveSinFunc_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Location = new System.Drawing.Point(177, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(164, 115);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Real function SOWC";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.cmdSolveValenzuelaRendon);
			this.groupBox3.Controls.Add(this.cmdSolveSchafferDisconnected);
			this.groupBox3.Controls.Add(this.cmdSolveSchaffer);
			this.groupBox3.Location = new System.Drawing.Point(360, 15);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(193, 112);
			this.groupBox3.TabIndex = 4;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Real function MO";
			// 
			// cmdSolveValenzuelaRendon
			// 
			this.cmdSolveValenzuelaRendon.Location = new System.Drawing.Point(7, 74);
			this.cmdSolveValenzuelaRendon.Name = "cmdSolveValenzuelaRendon";
			this.cmdSolveValenzuelaRendon.Size = new System.Drawing.Size(169, 23);
			this.cmdSolveValenzuelaRendon.TabIndex = 2;
			this.cmdSolveValenzuelaRendon.Text = "Solve Valenzuela-Rendon";
			this.cmdSolveValenzuelaRendon.Click += new System.EventHandler(this.cmdSolveValenzuelaRendon_Click);
			// 
			// cmdSolveSchafferDisconnected
			// 
			this.cmdSolveSchafferDisconnected.Location = new System.Drawing.Point(7, 45);
			this.cmdSolveSchafferDisconnected.Name = "cmdSolveSchafferDisconnected";
			this.cmdSolveSchafferDisconnected.Size = new System.Drawing.Size(169, 23);
			this.cmdSolveSchafferDisconnected.TabIndex = 1;
			this.cmdSolveSchafferDisconnected.Text = "Solve Schaffer disconnected";
			this.cmdSolveSchafferDisconnected.Click += new System.EventHandler(this.cmdSolveSchafferDisconnected_Click);
			// 
			// cmdSolveSchaffer
			// 
			this.cmdSolveSchaffer.Location = new System.Drawing.Point(7, 16);
			this.cmdSolveSchaffer.Name = "cmdSolveSchaffer";
			this.cmdSolveSchaffer.Size = new System.Drawing.Size(169, 23);
			this.cmdSolveSchaffer.TabIndex = 0;
			this.cmdSolveSchaffer.Text = "Solve Schaffer";
			this.cmdSolveSchaffer.Click += new System.EventHandler(this.cmdSolveSchaffer_Click);
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.cmdSolveSpeedReducer);
			this.groupBox4.Controls.Add(this.cmdSolveWeldedBeamDesign);
			this.groupBox4.Controls.Add(this.cmdSolveTwoBarTruss);
			this.groupBox4.Controls.Add(this.cmdSolveOSY);
			this.groupBox4.Controls.Add(this.cmdSolveTNK);
			this.groupBox4.Controls.Add(this.cmdSolveSRN);
			this.groupBox4.Controls.Add(this.cmdSolveBNH);
			this.groupBox4.Location = new System.Drawing.Point(575, 15);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(175, 245);
			this.groupBox4.TabIndex = 5;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Real function MOWC";
			// 
			// cmdSolveSpeedReducer
			// 
			this.cmdSolveSpeedReducer.Location = new System.Drawing.Point(6, 193);
			this.cmdSolveSpeedReducer.Name = "cmdSolveSpeedReducer";
			this.cmdSolveSpeedReducer.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveSpeedReducer.TabIndex = 6;
			this.cmdSolveSpeedReducer.Text = "Solve speed-reducer design";
			// 
			// cmdSolveWeldedBeamDesign
			// 
			this.cmdSolveWeldedBeamDesign.Location = new System.Drawing.Point(6, 164);
			this.cmdSolveWeldedBeamDesign.Name = "cmdSolveWeldedBeamDesign";
			this.cmdSolveWeldedBeamDesign.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveWeldedBeamDesign.TabIndex = 5;
			this.cmdSolveWeldedBeamDesign.Text = "Solve welded-beam design";
			// 
			// cmdSolveTwoBarTruss
			// 
			this.cmdSolveTwoBarTruss.Location = new System.Drawing.Point(6, 135);
			this.cmdSolveTwoBarTruss.Name = "cmdSolveTwoBarTruss";
			this.cmdSolveTwoBarTruss.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveTwoBarTruss.TabIndex = 4;
			this.cmdSolveTwoBarTruss.Text = "Solve two-bar truss";
			// 
			// cmdSolveOSY
			// 
			this.cmdSolveOSY.Location = new System.Drawing.Point(6, 106);
			this.cmdSolveOSY.Name = "cmdSolveOSY";
			this.cmdSolveOSY.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveOSY.TabIndex = 3;
			this.cmdSolveOSY.Text = "Solve OSY";
			// 
			// cmdSolveTNK
			// 
			this.cmdSolveTNK.Location = new System.Drawing.Point(6, 77);
			this.cmdSolveTNK.Name = "cmdSolveTNK";
			this.cmdSolveTNK.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveTNK.TabIndex = 2;
			this.cmdSolveTNK.Text = "Solve TNK";
			// 
			// cmdSolveSRN
			// 
			this.cmdSolveSRN.Location = new System.Drawing.Point(6, 48);
			this.cmdSolveSRN.Name = "cmdSolveSRN";
			this.cmdSolveSRN.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveSRN.TabIndex = 1;
			this.cmdSolveSRN.Text = "Solve SRN";
			// 
			// cmdSolveBNH
			// 
			this.cmdSolveBNH.Location = new System.Drawing.Point(6, 19);
			this.cmdSolveBNH.Name = "cmdSolveBNH";
			this.cmdSolveBNH.Size = new System.Drawing.Size(154, 23);
			this.cmdSolveBNH.TabIndex = 0;
			this.cmdSolveBNH.Text = "Solve BNH";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.button4);
			this.groupBox5.Controls.Add(this.button3);
			this.groupBox5.Location = new System.Drawing.Point(12, 140);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(145, 100);
			this.groupBox5.TabIndex = 6;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "TSP";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(11, 68);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(111, 23);
			this.button4.TabIndex = 1;
			this.button4.Text = "Solve TSP from file";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(11, 28);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(111, 23);
			this.button3.TabIndex = 0;
			this.button3.Text = "Solve complete TSP";
			// 
			// groupBox6
			// 
			this.groupBox6.Location = new System.Drawing.Point(177, 140);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(164, 100);
			this.groupBox6.TabIndex = 7;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "QAP";
			// 
			// groupBox7
			// 
			this.groupBox7.Location = new System.Drawing.Point(12, 264);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(145, 100);
			this.groupBox7.TabIndex = 8;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "JSSP";
			// 
			// groupBox8
			// 
			this.groupBox8.Location = new System.Drawing.Point(177, 264);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(164, 100);
			this.groupBox8.TabIndex = 9;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "VRP";
			// 
			// Mainform
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(775, 405);
			this.Controls.Add(this.groupBox8);
			this.Controls.Add(this.groupBox7);
			this.Controls.Add(this.groupBox6);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "Mainform";
			this.Text = "Genetic algorithms solvers";
			this.groupBox1.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button cmdSolveAckley;
		private System.Windows.Forms.Button cmdSolveSchwefel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Button cmdSolveSchaffer;
		private System.Windows.Forms.Button cmdSolveValenzuelaRendon;
		private System.Windows.Forms.Button cmdSolveSchafferDisconnected;
		private System.Windows.Forms.Button cmdSolveBNH;
		private System.Windows.Forms.Button cmdSolveSRN;
		private System.Windows.Forms.Button cmdSolveTNK;
		private System.Windows.Forms.Button cmdSolveTwoBarTruss;
		private System.Windows.Forms.Button cmdSolveOSY;
		private System.Windows.Forms.Button cmdSolveWeldedBeamDesign;
		private System.Windows.Forms.Button cmdSolveSpeedReducer;
		private System.Windows.Forms.Button cmdSolveSinFunc;
	}
}

