namespace WinFormTester
{
	partial class frmMOParetoSet
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.paretoSet2DColl_as_ScatterGraph1 = new ESOPVisualizationControls.ParetoSet2DColl_as_ScatterGraph();
			this.SuspendLayout();
			// 
			// paretoSet2DColl_as_ScatterGraph1
			// 
			this.paretoSet2DColl_as_ScatterGraph1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.paretoSet2DColl_as_ScatterGraph1.Location = new System.Drawing.Point(0, 0);
			this.paretoSet2DColl_as_ScatterGraph1.Name = "paretoSet2DColl_as_ScatterGraph1";
			this.paretoSet2DColl_as_ScatterGraph1.Size = new System.Drawing.Size(511, 431);
			this.paretoSet2DColl_as_ScatterGraph1.TabIndex = 0;
			// 
			// frmMOParetoSet
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(511, 431);
			this.Controls.Add(this.paretoSet2DColl_as_ScatterGraph1);
			this.Name = "frmMOParetoSet";
			this.Text = "frmMOParetoSet";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmMOParetoSet_Paint);
			this.ResumeLayout(false);

		}

		#endregion

		private ESOPVisualizationControls.ParetoSet2DColl_as_ScatterGraph paretoSet2DColl_as_ScatterGraph1;

	}
}