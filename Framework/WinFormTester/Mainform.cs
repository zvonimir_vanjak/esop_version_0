using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;
using ESOP.Framework.StandardRepresentations;
using ESOP.Framework.Algorithms.GeneticAlgorithms;
using ESOP.Framework.Operators.GeneticAlgorithms;
using ESOP.Framework.Operators.GeneticAlgorithms.Crossover;
using ESOP.Framework.Operators.GeneticAlgorithms.Mutation;
using ESOP.Framework.Operators.GeneticAlgorithms.Selection;

using ESOP.TestProblems.SOFunctionOptimization;
using ESOP.TestProblems.MOFunctionOptimization;

namespace WinFormTester
{
	public partial class Mainform : Form
	{
		public Mainform()
		{
			InitializeComponent();
		}

		private void cmdSolveSchaffer_Click(object sender, EventArgs e)
		{
			Schaffer_MO_TestProblem		_Problem;
			RealAsBitArray						_Representation;
			IMultiObjectiveSolution		_Solution;
			SPEA											_Algorithm;

			OptimizationResult			_Result;

			_Problem				= new Schaffer_MO_TestProblem();
			_Representation	= new RealAsBitArray();
			_Solution				= new IMultiObjectiveSolution();
			_Algorithm			= new SPEA();
			_Result					= new OptimizationResult();

			double	Low = -5;
			double Upp = 5;
			//			_Problem.getVarBounds(0, out Low, Upp);
			_Problem.setObjectiveType(0, EObjectiveType.MIN_OBJECTIVE);
			_Problem.setObjectiveType(1, EObjectiveType.MIN_OBJECTIVE);

			_Representation.setVarBounds(Low, Upp);
			_Representation.setBitNum(15);

			_Solution.setDesignVar(_Representation);
			_Solution.setMultiObjectiveProblem(_Problem);

			_Algorithm.setSolutionInstance(_Solution);
			_Algorithm.setProblemInstance(_Problem);
			_Algorithm.setPopulationSize(50);
			

			RealAsBitArray_1PointCrossover	cross = new RealAsBitArray_1PointCrossover();
			RealAsBitArray_FlopBitMutation	mutator = new RealAsBitArray_FlopBitMutation();
			RouleteWheelSelectionFitness		selector = new RouleteWheelSelectionFitness();
//			ScaledFitnessAssignment					assignment = new ScaledFitnessAssignment();

		//	_Algorithm.setFitnessAssignmentOp(assignment);
			_Algorithm.setSelectionOp(selector);
			_Algorithm.setRecombinationOp(cross);
			_Algorithm.setMutationOp(mutator);
		/*
			ObservedValuesCollection		newColl = _Result.AddNewObsCollection("Best func.value");
			
			IntermediateValueObserver			newObs = new IntermediateValueObserver();
			IntermedTimerOnEveryIterNum		newTimer = new IntermedTimerOnEveryIterNum();
			CurrObjectiveValueSaver				newSaver = new CurrObjectiveValueSaver(newColl, _Algorithm);

			newObs.setTimer(newTimer);
			newObs.setSaver(newSaver);

			_Algorithm.getMasterObserver().AddObserver(newObs);
			*/

			// optimization terminator
			TerminateOnIterationNum		optTerm = new TerminateOnIterationNum();
			optTerm.SetParameter(100);
			_Algorithm.setOptTerminator(optTerm);

			_Algorithm.Run(_Result);

			MOResult Res = (MOResult) _Result._FinalResult;

			// i sada bi trebalo prikazati rezultate
			ISolutionMO[] obj = new ISolutionMO[Res.getNumResult()];
			for (int i = 0; i < Res.getNumResult(); i++)
				obj[i] = Res.getResult(i);

			frmMOParetoSet frm = new frmMOParetoSet(obj);
			frm.Show();
		}
		private void cmdSolveSchafferDisconnected_Click(object sender, EventArgs e)
		{
			SchafferDisconnected_MO_TestProblem _Problem = new SchafferDisconnected_MO_TestProblem();
			RealAsBitArray											 _Representation = new RealAsBitArray();
			IMultiObjectiveSolution							_Solution = new IMultiObjectiveSolution();
			SPEA												_Algorithm = new SPEA();
			OptimizationResult					_Result = new OptimizationResult();

			double Low = -5;
			double Upp = 5;
			//			_Problem.getVarBounds(0, out Low, Upp);
			_Problem.setObjectiveType(0, EObjectiveType.MIN_OBJECTIVE);
			_Problem.setObjectiveType(1, EObjectiveType.MIN_OBJECTIVE);

			_Representation.setVarBounds(Low, Upp);
			_Representation.setBitNum(15);

			_Solution.setDesignVar(_Representation);
			_Solution.setMultiObjectiveProblem(_Problem);

			_Algorithm.setSolutionInstance(_Solution);
			_Algorithm.setProblemInstance(_Problem);
			_Algorithm.setPopulationSize(50);
			

			RealAsBitArray_1PointCrossover	cross = new RealAsBitArray_1PointCrossover();
			RealAsBitArray_FlopBitMutation	mutator = new RealAsBitArray_FlopBitMutation();
			RouleteWheelSelectionFitness		selector = new RouleteWheelSelectionFitness();
//			ScaledFitnessAssignment					assignment = new ScaledFitnessAssignment();

		//	_Algorithm.setFitnessAssignmentOp(assignment);
			_Algorithm.setSelectionOp(selector);
			_Algorithm.setRecombinationOp(cross);
			_Algorithm.setMutationOp(mutator);

			// optimization terminator
			TerminateOnIterationNum		optTerm = new TerminateOnIterationNum();
			optTerm.SetParameter(100);

			_Algorithm.setOptTerminator(optTerm);
			_Algorithm.Run(_Result);

			MOResult Res = (MOResult) _Result._FinalResult;

			// i sada bi trebalo prikazati rezultate
			ISolutionMO[] obj = new ISolutionMO[Res.getNumResult()];
			for (int i = 0; i < Res.getNumResult(); i++)
				obj[i] = Res.getResult(i);

			frmMOParetoSet frm = new frmMOParetoSet(obj);
			frm.Show();
		}
		private void cmdSolveValenzuelaRendon_Click(object sender, EventArgs e)
		{
			ValenzuelaRendon_MO_TestProblem _Problem				= new ValenzuelaRendon_MO_TestProblem();
			RealArray_AsBitArray _Representation	= new RealArray_AsBitArray();
			IMultiObjectiveSolution _Solution				= new IMultiObjectiveSolution();
			SPEA _Algorithm			= new SPEA();
			OptimizationResult _Result					= new OptimizationResult();

			_Problem.setObjectiveType(0, EObjectiveType.MIN_OBJECTIVE);
			_Problem.setObjectiveType(1, EObjectiveType.MIN_OBJECTIVE);

			int		NumVar = _Problem.getNumVar();
			_Representation.setVarNum(NumVar);
			for( int i=0; i<NumVar; i++ )
			{
				double	Low = -3;
				double Upp = 3;	
//				_Problem.getVarBounds(i, Low, Upp);

				_Representation.setVarBounds(i, Low, Upp);
				_Representation.setBitNum(i, 15);
			}
			_Solution.setDesignVar(_Representation);
			_Solution.setMultiObjectiveProblem(_Problem);

			_Algorithm.setSolutionInstance(_Solution);
			_Algorithm.setProblemInstance(_Problem);
			_Algorithm.setPopulationSize(10);


			RealArrayAsBitArray_1PointCrossover cross = new RealArrayAsBitArray_1PointCrossover();
			RealArrayAsBitArray_FlopBitMutation mutator = new RealArrayAsBitArray_FlopBitMutation();
			RouleteWheelSelectionFitness		selector = new RouleteWheelSelectionFitness();
//			ScaledFitnessAssignment					assignment = new ScaledFitnessAssignment();

		//	_Algorithm.setFitnessAssignmentOp(assignment);
			_Algorithm.setSelectionOp(selector);
			_Algorithm.setRecombinationOp(cross);
			_Algorithm.setMutationOp(mutator);

			// optimization terminator
			TerminateOnIterationNum		optTerm = new TerminateOnIterationNum();
			optTerm.SetParameter(50);
			_Algorithm.setOptTerminator(optTerm);

			_Algorithm.Run(_Result);

			MOResult Res = (MOResult) _Result._FinalResult;

			// i sada bi trebalo prikazati rezultate
			ISolutionMO[] obj = new ISolutionMO[Res.getNumResult()];
			for (int i = 0; i < Res.getNumResult(); i++)
				obj[i] = Res.getResult(i);

			frmMOParetoSet frm = new frmMOParetoSet(obj);
			frm.Show();
		}

		private void cmdSolveSinFunc_Click(object sender, EventArgs e)
		{
			SinFunc_TestProblem _Problem				= new SinFunc_TestProblem();
			RealAsBitArray _Representation	= new RealAsBitArray();
			ISingleObjectiveSolution _Solution				= new ISingleObjectiveSolution();
			GenerationalGA _Algorithm			= new GenerationalGA();
			OptimizationResult _Result					= new OptimizationResult();

			double	Low=0, Upp=5;		
//			_Problem.getVarBounds(0, Low, out Upp);
			_Problem.setObjectiveType(EObjectiveType.MAX_OBJECTIVE);

			_Representation.setVarBounds(Low, Upp);
			_Representation.setBitNum(25);

			_Solution.setDesignVar(_Representation);
			_Solution.setSingleObjectiveProblem(_Problem);

			_Algorithm.setSolutionInstance(_Solution);
			_Algorithm.setProblemInstance(_Problem);
			_Algorithm.setPopulationSize(10);
			
			// operatori
			RealAsBitArray_1PointCrossover	cross = new RealAsBitArray_1PointCrossover();
			RealAsBitArray_FlopBitMutation	mutator = new RealAsBitArray_FlopBitMutation();
			RouleteWheelSelectionFitness		selector = new RouleteWheelSelectionFitness();
			ScaledFitnessAssignment					assignment = new ScaledFitnessAssignment();

			_Algorithm.setFitnessAssignmentOp(assignment);
			_Algorithm.setSelectionOp(selector);
			_Algorithm.setRecombinationOp(cross);
			_Algorithm.setMutationOp(mutator);

			// me�urezultati
			ObservedValuesCollection		newColl = _Result.AddNewObsCollection("Best func.value");
			
			IntermediateValueObserver			newObs = new IntermediateValueObserver();
			IntermedTimerOnEveryIterNum		newTimer = new IntermedTimerOnEveryIterNum();
			CurrObjectiveValueSaver				newSaver = new CurrObjectiveValueSaver(newColl, _Algorithm);

			newObs.setTimer(newTimer);
			newObs.setSaver(newSaver);

			_Algorithm.getMasterObserver().AddObserver(newObs);

			// optimization terminator
			TerminateOnIterationNum		optTerm = new TerminateOnIterationNum();
			optTerm.SetParameter(100);
			_Algorithm.setOptTerminator(optTerm);

			_Algorithm.Run(_Result);

			// sada jo� treba vizualizirati kako se kretala vrijednost funkcije cilja
			frmFunctionGraph frm = new frmFunctionGraph();
			frm.Show();

//			frm._ctrlFunction.ShowCollection((ObservedValuesCollection) _Result._arlIntermedResults[0]);
			frm._ctrlNevron.ShowCollection((ObservedValuesCollection) _Result._arlIntermedResults[0]);
		}

		private void cmdSolveAckley_Click(object sender, EventArgs e)
		{
			int			NumVar = 5;

			AckleyFunction_TestProblem _Problem = new AckleyFunction_TestProblem(NumVar);
			RealArray_AsBitArray _Representation = new RealArray_AsBitArray(NumVar);
			ISingleObjectiveSolution _Solution = new ISingleObjectiveSolution();
			GenerationalGA _Algorithm = new GenerationalGA();
			OptimizationResult _Result = new OptimizationResult();

			for( int i=0; i<NumVar; i++ )
			{
				double	Low=0, Upp=5;		
//				_Problem.getVarBounds(i, Low, Upp);
				_Representation.setVarBounds(i, Low, Upp);
				_Representation.setBitNum(i, 10);
			}

			_Problem.setObjectiveType(EObjectiveType.MIN_OBJECTIVE);

			_Solution.setDesignVar(_Representation);
			_Solution.setSingleObjectiveProblem(_Problem);

			_Algorithm.setSolutionInstance(_Solution);
			_Algorithm.setProblemInstance(_Problem);
			_Algorithm.setPopulationSize(50);

			RealArrayAsBitArray_1PointCrossover	cross = new RealArrayAsBitArray_1PointCrossover();
			RealArrayAsBitArray_FlopBitMutation	mutator = new RealArrayAsBitArray_FlopBitMutation();
			RouleteWheelSelectionFitness		selector = new RouleteWheelSelectionFitness();
			ScaledFitnessAssignment					assignment = new ScaledFitnessAssignment();

			mutator.setMutationProbPerBit(0.001);

			_Algorithm.setFitnessAssignmentOp(assignment);
			_Algorithm.setSelectionOp(selector);
			_Algorithm.setRecombinationOp(cross);
			_Algorithm.setMutationOp(mutator);

			ObservedValuesCollection		newColl1 = _Result.AddNewObsCollection("Best func.value");
			
			IntermediateValueObserver			newObs1 = new IntermediateValueObserver();
			IntermedTimerOnEveryIterNum		newTimer1 = new IntermedTimerOnEveryIterNum();
			CurrObjectiveValueSaver				newSaver1 = new CurrObjectiveValueSaver(newColl1, _Algorithm);

			newObs1.setTimer(newTimer1);
			newObs1.setSaver(newSaver1);

			_Algorithm.getMasterObserver().AddObserver(newObs1);

			ObservedValuesCollection newColl2 = _Result.AddNewObsCollection("Average func.value");

			IntermediateValueObserver							 newObs2 = new IntermediateValueObserver();
			IntermedTimerOnEveryIterNum						 newTimer2 = new IntermedTimerOnEveryIterNum();
			AverageObjectiveValueInPopulationSaver newSaver2 = new AverageObjectiveValueInPopulationSaver(newColl2, _Algorithm);

			newObs2.setTimer(newTimer2);
			newObs2.setSaver(newSaver2);

			_Algorithm.getMasterObserver().AddObserver(newObs2);

			// optimization terminator
			TerminateOnIterationNum optTerm = new TerminateOnIterationNum();
			optTerm.SetParameter(100);

			_Algorithm.setOptTerminator(optTerm);
			_Algorithm.Run(_Result);

			// sada jo� treba vizualizirati kako se kretala vrijednost funkcije cilja
			frmFunctionGraph frm1 = new frmFunctionGraph();
			frm1.Show();
			frm1._ctrlNevron.ShowCollection((ObservedValuesCollection)_Result._arlIntermedResults[0]);

			frmFunctionGraph frm2 = new frmFunctionGraph();
			frm2.Show();
			frm2._ctrlNevron.ShowCollection((ObservedValuesCollection)_Result._arlIntermedResults[1]);
		}

	}
}