#include "ITestCase.h"
#include <vector>
#include <vcclr.h>

using std::vector;

using namespace System::Collections;

public ref class TestRunner
{
public:
	TestRunner();

	void AddTest(ITestCase ^testCase);
	void RunTests();

private:
	vector<gcroot<ITestCase ^> > *_vecTests;
};
