#include "ITestCase.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::TSP;

public ref class TSPComplete10_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

	virtual double GetBestKnownSolution() ;

private:
	TSPComplete10_TestProblem		^_Problem;
	Permutation									^_Representation;
	ISingleObjectiveSolution		^_Solution;
	GenerationalGA							^_Algorithm;

	OptimizationResult			^_Result;
};
