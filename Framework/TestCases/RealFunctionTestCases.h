#include "ITestCase.h"

public ref class SinFunc_TestCase : public ITestCase
{
public:
	virtual void Initialize() override;
	virtual void Run() override;
};
