#include "ITestCase.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

public ref class BNH_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	BNH_TestProblem						^_Problem;
	RealArray_AsBitArray			^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class SRN_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	SRN_TestProblem						^_Problem;
	RealArray_AsBitArray			^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class TNK_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	TNK_TestProblem						^_Problem;
	RealArray_AsBitArray			^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class OSY_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	OSY_TestProblem						^_Problem;
	RealArray_AsBitArray			^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class TwoBarTrussDesign_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	TwoBarTrussDesign_TestProblem		^_Problem;
	RealArray_AsBitArray						^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class WeldedBeamDesign_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	WeldedBeamDesign_TestProblem	^_Problem;
	RealArray_AsBitArray					^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class SpeedReducerDesign_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	SpeedReducerDesignTestProblem						^_Problem;
	GenericDynamicCompositeRepresentation		^_Representation;
	IMultiObjectiveSolutionWC	^_Solution;
	BaseMOWCGA							^_Algorithm;

	OptimizationResult			^_Result;
};

