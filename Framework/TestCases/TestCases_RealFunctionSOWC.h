#include "ITestCase.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

public ref class TestCase_SOWC1 : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

	virtual double GetBestKnownSolution() ;

private:
	SinFunc_TestProblem				^_Problem;
	RealAsBitArray						^_Representation;
	ISingleObjectiveSolution	^_Solution;
	GenerationalGA						^_Algorithm;

	OptimizationResult			^_Result;

	RealAsBitArray_1PointCrossover	^_CrossoverOp;
	RealAsBitArray_FlopBitMutation	^_MutationOp;
	RouleteWheelSelectionFitness		^_SelectionOp;
};
