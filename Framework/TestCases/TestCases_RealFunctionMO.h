#include "ITestCase.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::MOFunctionOptimization;

public ref class TestCase_SchafferMO1 : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	Schaffer_MO_TestProblem		^_Problem;
	RealAsBitArray						^_Representation;
	IMultiObjectiveSolution		^_Solution;
	SPEA											^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class TestCase_SchafferMO2 : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	SchafferDisconnected_MO_TestProblem		^_Problem;
	RealAsBitArray												^_Representation;
	IMultiObjectiveSolution								^_Solution;
	SPEA																	^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class TestCase_ValenzuelaRendon : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	ValenzuelaRendon_MO_TestProblem		^_Problem;
	RealArray_AsBitArray							^_Representation;
	IMultiObjectiveSolution						^_Solution;
	SPEA															^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class TestCase_ValenzuelaRendon_As_Composite : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

private:
	ValenzuelaRendon_As_Composite_MO_TestProblem		^_Problem;
	GenericDynamicCompositeRepresentation						^_Representation;

	IMultiObjectiveSolution						^_Solution;
	SPEA															^_Algorithm;

	OptimizationResult			^_Result;
};
