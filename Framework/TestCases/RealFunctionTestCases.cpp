#include "stdafx.h"
#include "RealFunctionTestCases.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

void SinFunc_TestCase::Initialize()
{
	SinFunc_TestProblem				^problem = gcnew SinFunc_TestProblem();
	RealAsBitArray						^repr		 = gcnew RealAsBitArray();
	ISingleObjectiveSolution	^sol		 = gcnew ISingleObjectiveSolution();
	GenerationalGA						^algor	 = gcnew GenerationalGA();

	repr->setVarBounds(0,3);
	repr->setBitNum(10);

	sol->setCurrSol(repr);
	sol->setSingleObjectiveProblem(problem);

	algor->setESOPDesignVariable(repr);
	algor->setProblemInstance(problem);
	algor->setPopulationSize(10);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelection						^selector = gcnew RouleteWheelSelection();

	algor->setSelectionOp(selector);
	algor->setRecombinationOp(cross);
	algor->setMutationOp(mutator);

	algor->Run();
}

void SinFunc_TestCase::Run()
{
}
