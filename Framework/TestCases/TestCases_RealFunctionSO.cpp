#include "stdafx.h"
#include "TestCases_RealFunctionSO.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

void SinFunc_TestCase::Initialize()
{
	_Problem				= gcnew SinFunc_TestProblem();
	_Representation	= gcnew RealAsBitArray();
	_Solution				= gcnew ISingleObjectiveSolution();
	_Algorithm			= gcnew GenerationalGA();
	_Result					= gcnew OptimizationResult();

	double	Low=0, Upp=0;		
	_Problem->getVarBounds(0, Low, Upp);
	_Problem->setObjectiveType(EObjectiveType::MIN_OBJECTIVE);

	_Representation->setVarBounds(Low, Upp);
	_Representation->setBitNum(25);

	_Solution->setDesignVar(_Representation);
	_Solution->setSingleObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	
	// operatori
	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment					^assignment = gcnew ScaledFitnessAssignment();

	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// mešurezultati
	ObservedValuesCollection		^newColl = _Result->AddNewObsCollection("Best func.value");
	
	IntermediateValueObserver			^newObs = gcnew IntermediateValueObserver();
	IntermedTimerOnEveryIterNum		^newTimer = gcnew IntermedTimerOnEveryIterNum();
	CurrObjectiveValueSaver				^newSaver = gcnew CurrObjectiveValueSaver(newColl, _Algorithm);

	newObs->setTimer(newTimer);
	newObs->setSaver(newSaver);

	_Algorithm->getMasterObserver()->AddObserver(newObs);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(10);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ SinFunc_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

double SinFunc_TestCase::GetBestKnownSolution()
{
	return 0;
}

void AckleyFunc_TestCase::Initialize()
{
	int			NumVar = 5;

	_Problem				= gcnew AckleyFunction_TestProblem(NumVar);
	_Representation	= gcnew RealArray_AsBitArray(NumVar);
	_Solution				= gcnew ISingleObjectiveSolution();
	_Algorithm			= gcnew GenerationalGA();
	_Result					= gcnew OptimizationResult();

	for( int i=0; i<NumVar; i++ )
	{
		double	Low=0, Upp=0;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
		_Representation->setBitNum(i, 10);
	}

	_Problem->setObjectiveType(EObjectiveType::MIN_OBJECTIVE);

	_Solution->setDesignVar(_Representation);
	_Solution->setSingleObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);

	RealArrayAsBitArray_1PointCrossover	^cross = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment					^assignment = gcnew ScaledFitnessAssignment();

	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	ObservedValuesCollection		^newColl = _Result->AddNewObsCollection("Best func.value");
	
	IntermediateValueObserver			^newObs = gcnew IntermediateValueObserver();
	IntermedTimerOnEveryIterNum		^newTimer = gcnew IntermedTimerOnEveryIterNum();
	CurrObjectiveValueSaver				^newSaver = gcnew CurrObjectiveValueSaver(newColl, _Algorithm);

	newObs->setTimer(newTimer);
	newObs->setSaver(newSaver);

	_Algorithm->getMasterObserver()->AddObserver(newObs);
}

OptimizationResult^ AckleyFunc_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

double AckleyFunc_TestCase::GetBestKnownSolution()
{
	return 0;
}

void SchwefelFunc_TestCase::Initialize()
{
	int NumVar = 5;

	_Problem				= gcnew SchwefelFunction_TestProblem(NumVar);
	_Representation	= gcnew RealArray_AsBitArray(NumVar);
	_Solution				= gcnew ISingleObjectiveSolution();
	_Algorithm			= gcnew GenerationalGA();
	_Result					= gcnew OptimizationResult();

	for( int i=0; i<NumVar; i++ )
	{
		double	Low=0, Upp=0;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
		_Representation->setBitNum(i, 10);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setSingleObjectiveProblem(_Problem);

	_Algorithm->setESOPDesignVariable(_Representation);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();

	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);
}

OptimizationResult^ SchwefelFunc_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

double SchwefelFunc_TestCase::GetBestKnownSolution()
{
	return 0;
}

