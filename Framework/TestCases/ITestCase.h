using namespace ESOP::Framework::Base;

public ref class ITestCase abstract
{
public:
	virtual void Initialize() = 0;
	virtual OptimizationResult^ Run() = 0;

//	virtual double GetBestKnownSolution() = 0;
};
