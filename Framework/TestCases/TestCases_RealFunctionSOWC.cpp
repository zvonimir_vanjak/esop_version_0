#include "stdafx.h"
#include "TestCases_RealFunctionSOWC.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

void TestCase_SOWC1::Initialize()
{
	_Problem				= gcnew SinFunc_TestProblem();
	_Representation	= gcnew RealAsBitArray();
	_Solution				= gcnew ISingleObjectiveSolution();
	_Algorithm			= gcnew GenerationalGA();
	_Result					= gcnew OptimizationResult();

	double	Low=0, Upp=0;		
	_Problem->getVarBounds(0, Low, Upp);
	_Problem->setObjectiveType(EObjectiveType::MIN_OBJECTIVE);

	_Representation->setVarBounds(Low, Upp);
	_Representation->setBitNum(25);

	_Solution->setDesignVar(_Representation);
	_Solution->setSingleObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment					^assignment = gcnew ScaledFitnessAssignment();

	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	ObservedValuesCollection		^newColl = _Result->AddNewObsCollection("Best func.value");
	
	IntermediateValueObserver			^newObs = gcnew IntermediateValueObserver();
	IntermedTimerOnEveryIterNum		^newTimer = gcnew IntermedTimerOnEveryIterNum();
	CurrObjectiveValueSaver				^newSaver = gcnew CurrObjectiveValueSaver(newColl, _Algorithm);

	newObs->setTimer(newTimer);
	newObs->setSaver(newSaver);

	_Algorithm->getMasterObserver()->AddObserver(newObs);
}

double TestCase_SOWC1::GetBestKnownSolution()
{
	return 0;
}

OptimizationResult^ TestCase_SOWC1::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}
