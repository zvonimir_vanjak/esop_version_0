#include "stdafx.h"
#include "TestRunner.h"

using namespace System;
using namespace ESOP::Framework::Base;

TestRunner::TestRunner()
{
	_vecTests = new vector<gcroot<ITestCase ^> >();
}

void TestRunner::AddTest(ITestCase ^testCase)
{
	_vecTests->push_back(testCase);
}

void TestRunner::RunTests()
{
	for( int i=0; i<(int) _vecTests->size(); i++ )
	{
		(*_vecTests)[i]->Initialize();
		
		OptimizationResult ^OptRes = (*_vecTests)[i]->Run();

		Console::WriteLine(OptRes->_FinalResult->GetResultStringRepresentation());

		// a da vidimo i �to su nam usput snimili
		int			NumColl = OptRes->getNumObsColl();
		Console::WriteLine("Obs. coll. num = " + NumColl);

		for( int j=0; j<NumColl; j++ )
		{
			ObservedValuesCollection ^coll = OptRes->GetObsCollection(i);
			Console::WriteLine("Coll. name - " + coll->_strDesc);
			for( int  k=0; k<coll->_arrValues->Count; k++ )
			{
				ObservedValueTag		^tag = (ObservedValueTag ^) coll->_arrValueTags[k];
				double		val = (double) coll->_arrValues[k];
				Console::WriteLine(tag->_TimeMs + " - " + val.ToString());
			}
		}
	}
}
