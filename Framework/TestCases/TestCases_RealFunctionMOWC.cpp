#include "stdafx.h"
#include "TestCases_RealFunctionMOWC.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

void BNH_TestCase::Initialize()
{
	_Problem				= gcnew BNH_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray(_Problem->getNumVar());
	_Solution				= gcnew IMultiObjectiveSolutionWC();
	_Result					= gcnew OptimizationResult();

	_Algorithm			= gcnew CHNA();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	for( int i=0; i<_Problem->getNumVar(); i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ BNH_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void SRN_TestCase::Initialize()
{
	_Problem				= gcnew SRN_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray(_Problem->getNumVar());
	_Solution				= gcnew IMultiObjectiveSolutionWC();
//	_Algorithm			= gcnew BaseMOWCGA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	for( int i=0; i<_Problem->getNumVar(); i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ SRN_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void TNK_TestCase::Initialize()
{
	_Problem				= gcnew TNK_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray(_Problem->getNumVar());
	_Solution				= gcnew IMultiObjectiveSolutionWC();
//	_Algorithm			= gcnew BaseMOWCGA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	for( int i=0; i<_Problem->getNumVar(); i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

	//_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ TNK_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void OSY_TestCase::Initialize()
{
	_Problem				= gcnew OSY_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray(_Problem->getNumVar());
	_Solution				= gcnew IMultiObjectiveSolutionWC();
//	_Algorithm			= gcnew BaseMOWCGA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	for( int i=0; i<_Problem->getNumVar(); i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ OSY_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void TwoBarTrussDesign_TestCase::Initialize()
{
	_Problem				= gcnew TwoBarTrussDesign_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray(_Problem->getNumVar());
	_Solution				= gcnew IMultiObjectiveSolutionWC();
//	_Algorithm			= gcnew BaseMOWCGA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	for( int i=0; i<_Problem->getNumVar(); i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

	//_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ TwoBarTrussDesign_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void WeldedBeamDesign_TestCase::Initialize()
{
	_Problem				= gcnew WeldedBeamDesign_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray(_Problem->getNumVar());
	_Solution				= gcnew IMultiObjectiveSolutionWC();
//	_Algorithm			= gcnew BaseMOWCGA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	for( int i=0; i<_Problem->getNumVar(); i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);
		_Representation->setVarBounds(i, Low, Upp);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ WeldedBeamDesign_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void SpeedReducerDesign_TestCase::Initialize()
{
	_Problem				= gcnew SpeedReducerDesignTestProblem();
	_Representation	= gcnew GenericDynamicCompositeRepresentation();
	_Solution				= gcnew IMultiObjectiveSolutionWC();
//	_Algorithm			= gcnew BaseMOWCGA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	_Representation->AddPart(gcnew RealAsBitArray(3.0, 3.6));			// x1 - gear face width
	_Representation->AddPart(gcnew RealAsBitArray(0.7, 0.8));			// x2
	_Representation->AddPart(gcnew Integer(17, 28));							// x3 
	_Representation->AddPart(gcnew RealAsBitArray(7.3, 8.3));			// x4 
	_Representation->AddPart(gcnew RealAsBitArray(7.3, 8.3));			// x5 
	_Representation->AddPart(gcnew RealAsBitArray(2.9, 3.9));			// x6 
	_Representation->AddPart(gcnew RealAsBitArray(5.0, 5.5));			// x7 

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblemWC(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealArrayAsBitArray_1PointCrossover	^cross	 = gcnew RealArrayAsBitArray_1PointCrossover();
	RealArrayAsBitArray_FlopBitMutation	^mutator = gcnew RealArrayAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness				^selector		= gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment							^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(100);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ SpeedReducerDesign_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

