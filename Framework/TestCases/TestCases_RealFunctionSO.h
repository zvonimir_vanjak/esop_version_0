#include "ITestCase.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::SOFunctionOptimization;

public ref class SinFunc_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

	virtual double GetBestKnownSolution() ;

private:
	SinFunc_TestProblem				^_Problem;
	RealAsBitArray						^_Representation;
	ISingleObjectiveSolution	^_Solution;
	GenerationalGA						^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class AckleyFunc_TestCase : public ITestCase
{
public:
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

	virtual double GetBestKnownSolution() ;

private:
	AckleyFunction_TestProblem		^_Problem;
	RealArray_AsBitArray					^_Representation;
	ISingleObjectiveSolution			^_Solution;
	GenerationalGA								^_Algorithm;

	OptimizationResult			^_Result;
};

public ref class SchwefelFunc_TestCase : public ITestCase
{
public:	
	virtual void									Initialize() override;
	virtual OptimizationResult^		Run() override;

	virtual double GetBestKnownSolution() ;

private:
	SchwefelFunction_TestProblem	^_Problem;
	RealArray_AsBitArray					^_Representation;
	ISingleObjectiveSolution			^_Solution;
	GenerationalGA								^_Algorithm;

	OptimizationResult			^_Result;
};
