#include "stdafx.h"
#include "TestCases_TSP.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::TSP;

void TSPComplete10_TestCase::Initialize()
{
	_Problem				= gcnew TSPComplete10_TestProblem();
	_Representation	= gcnew Permutation();
	_Solution				= gcnew ISingleObjectiveSolution();
	_Algorithm			= gcnew GenerationalGA();
	_Result					= gcnew OptimizationResult();

	_Representation->setNum(10);

	_Solution->setDesignVar(_Representation);
	_Solution->setSingleObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	PermutationPMXCrossover				^cross = gcnew PermutationPMXCrossover();
	PermutationSwapMutation				^mutator = gcnew PermutationSwapMutation();
	RouleteWheelSelectionFitness	^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment				^assignment = gcnew ScaledFitnessAssignment();

	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	ObservedValuesCollection		^newColl = _Result->AddNewObsCollection("Best func.value");
	
	IntermediateValueObserver			^newObs = gcnew IntermediateValueObserver();
	IntermedTimerOnEveryIterNum		^newTimer = gcnew IntermedTimerOnEveryIterNum();
	CurrObjectiveValueSaver				^newSaver = gcnew CurrObjectiveValueSaver(newColl, _Algorithm);

	newObs->setTimer(newTimer);
	newObs->setSaver(newSaver);

	_Algorithm->getMasterObserver()->AddObserver(newObs);
}

OptimizationResult^ TSPComplete10_TestCase::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

double TSPComplete10_TestCase::GetBestKnownSolution()
{
	return 0;
}
