#include "stdafx.h"
#include "TestCases_RealFunctionMO.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Selection;

using namespace ESOP::TestProblems::MOFunctionOptimization;

void TestCase_SchafferMO1::Initialize()
{
	_Problem				= gcnew Schaffer_MO_TestProblem();
	_Representation	= gcnew RealAsBitArray();
	_Solution				= gcnew IMultiObjectiveSolution();
	_Algorithm			= gcnew SPEA();
	_Result					= gcnew OptimizationResult();

	double	Low, Upp;		
	_Problem->getVarBounds(0, Low, Upp);
	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
  _Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	_Representation->setVarBounds(Low, Upp);
	_Representation->setBitNum(25);

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(20);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness	^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment			^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	ObservedValuesCollection		^newColl = _Result->AddNewObsCollection("Whole ParetoSet");
	WholeParetoSetSaver					^newSaver = gcnew WholeParetoSetSaver(newColl, _Algorithm);
	
	IntermediateValueObserver			^newObs = gcnew IntermediateValueObserver();
	IntermedTimerOnEveryIterNum		^newTimer = gcnew IntermedTimerOnEveryIterNum();

	newObs->setTimer(newTimer);
	newObs->setSaver(newSaver);

	_Algorithm->getMasterObserver()->AddObserver(newObs);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(10);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ TestCase_SchafferMO1::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void TestCase_SchafferMO2::Initialize()
{
	_Problem				= gcnew SchafferDisconnected_MO_TestProblem();
	_Representation	= gcnew RealAsBitArray();
	_Solution				= gcnew IMultiObjectiveSolution();
	_Algorithm			= gcnew SPEA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	double	Low, Upp;		
	_Problem->getVarBounds(0, Low, Upp);
	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	_Representation->setVarBounds(Low, Upp);
	_Representation->setBitNum(25);

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment					^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(10);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ TestCase_SchafferMO2::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void TestCase_ValenzuelaRendon::Initialize()
{
	_Problem				= gcnew ValenzuelaRendon_MO_TestProblem();
	_Representation	= gcnew RealArray_AsBitArray();
	_Solution				= gcnew IMultiObjectiveSolution();
	_Algorithm			= gcnew SPEA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	int		NumVar = _Problem->getNumVar();
	_Representation->setVarNum(NumVar);
	for( int i=0; i<NumVar; i++ )
	{
		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);

		_Representation->setVarBounds(i, Low, Upp);
		_Representation->setBitNum(i, 15);
	}
	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment					^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(50);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ TestCase_ValenzuelaRendon::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}

void TestCase_ValenzuelaRendon_As_Composite::Initialize()
{
	_Problem				= gcnew ValenzuelaRendon_As_Composite_MO_TestProblem();
	_Representation	= gcnew GenericDynamicCompositeRepresentation();
	_Solution				= gcnew IMultiObjectiveSolution();
	_Algorithm			= gcnew SPEA();
	_Result					= gcnew OptimizationResult();

	_Problem->setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	_Problem->setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);

	int		NumVar = _Problem->getNumVar();
	for( int i=0; i<NumVar; i++ )
	{
		RealAsBitArray	^repr = gcnew RealAsBitArray();

		double	Low, Upp;		
		_Problem->getVarBounds(i, Low, Upp);

		repr->setVarBounds(Low, Upp);
		repr->setBitNum(15);

		_Representation->AddPart(repr);
	}

	_Solution->setDesignVar(_Representation);
	_Solution->setMultiObjectiveProblem(_Problem);

	_Algorithm->setSolutionInstance(_Solution);
//	_Algorithm->setProblemInstance(_Problem);
	_Algorithm->setPopulationSize(10);
	

	RealAsBitArray_1PointCrossover	^cross = gcnew RealAsBitArray_1PointCrossover();
	RealAsBitArray_FlopBitMutation	^mutator = gcnew RealAsBitArray_FlopBitMutation();
	RouleteWheelSelectionFitness		^selector = gcnew RouleteWheelSelectionFitness();
	ScaledFitnessAssignment					^assignment = gcnew ScaledFitnessAssignment();

//	_Algorithm->setFitnessAssignmentOp(assignment);
	_Algorithm->setSelectionOp(selector);
	_Algorithm->setRecombinationOp(cross);
	_Algorithm->setMutationOp(mutator);

	// optimization terminator
	TerminateOnIterationNum		^optTerm = gcnew TerminateOnIterationNum();
	optTerm->SetParameter(50);
	_Algorithm->setOptTerminator(optTerm);
}

OptimizationResult^ TestCase_ValenzuelaRendon_As_Composite::Run()
{
	_Algorithm->Run(_Result);
	return _Result;
}
