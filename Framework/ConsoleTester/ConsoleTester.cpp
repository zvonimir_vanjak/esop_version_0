// ConsoleTester.cpp : main project file.

#include "stdafx.h"

using namespace System;

int main(array<System::String ^> ^args)
{
	TestRunner ^runner = gcnew TestRunner();

	SinFunc_TestCase				^sinFuncTestCase = gcnew SinFunc_TestCase();
	AckleyFunc_TestCase			^ackleyFuncTestCase = gcnew AckleyFunc_TestCase();
	TSPComplete10_TestCase	^tsp10 = gcnew TSPComplete10_TestCase();

	BNH_TestCase						^BNH_MOWC = gcnew BNH_TestCase();
	TestCase_SchafferMO1		^SchafferMO = gcnew TestCase_SchafferMO1();

	TestCase_ValenzuelaRendon_As_Composite		^compositeVR = gcnew TestCase_ValenzuelaRendon_As_Composite();

	runner->AddTest(compositeVR);

//	runner->AddTest(BNH_MOWC);
//	runner->AddTest(SchafferMO);
//	runner->AddTest(tsp10);

	runner->RunTests();

	return 0;
}
