// GeneticOperators.h

#pragma once

using namespace System;

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				public ref class SimpleFitnessAssignment : public IFitnessAssignmentOperator , public IESOPOperator
				{
				public:
					virtual	void	Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness);

					virtual	String^		getESOPComponentName() 		{ return "SimpleFitnessAssignment"; }
					virtual String^		getESOPComponentDesc()		{ return "SimpleFitnessAssignment"; }
				};
				public ref class ScaledFitnessAssignment : public IFitnessAssignmentOperator , public IESOPOperator
				{
				public:
					virtual	void	Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness);

					virtual	String^		getESOPComponentName() 		{ return "ScaledFitnessAssignment"; }
					virtual String^		getESOPComponentDesc()		{ return "ScaledFitnessAssignment"; }
				};

				public ref class SPEAFitnessAssignment : public IFitnessAssignmentOperator , public IESOPOperator
				{
				public:
					virtual	void	Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness);

					virtual	String^		getESOPComponentName() 		{ return "SPEAFitnessAssignment"; }
					virtual String^		getESOPComponentDesc()		{ return "SPEAFitnessAssignment"; }

					void		setParetoSet(ISolutionContainer ^inParetoSet);

				private:
					ISolutionContainer		^_ParetoSet;
				};

				public ref class CHNAFitnessAssignment : public IFitnessAssignmentOperator , public IESOPOperator
				{
				public:
					virtual	void	Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness);

					virtual	String^		getESOPComponentName() 		{ return "CHNAFitnessAssignment"; }
					virtual String^		getESOPComponentDesc()		{ return "CHNAFitnessAssignment"; }
				};
			}
		}
	}
}
