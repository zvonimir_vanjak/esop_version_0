#pragma once

//#include "Base\BaseOperatorInterfaces.h"

using namespace System;
using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					public ref class CompositeCrossoverOperator : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
				}

				namespace Mutation
				{
					public ref class CompositeMutationOperator : public IMutationOp
					{
					public:
						virtual void Mutate(IDesignVariable ^Child);
					};
				}
			}
		}
	}
}