#include "stdafx.h"

#include "FitnessAssignmentOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"
//#include "Components\Representations\StandardRepresentations.h"


using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
//using namespace ESOP::Framework::StandardRepresentations;

void	SimpleFitnessAssignment::Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness)
{
	for( int i=0; i<inPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));

		outFitness[i] = Individual->getCurrObjectiveValue() + 1;
	}
}

void	ScaledFitnessAssignment::Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness)
{
	// racunamo najmanji najveci fitness
	double		MinCurrObjVal = 10e10;
	double		MaxCurrObjVal = -10e10;
	for( int i=0; i<inPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));

		double		CurrObjVal = Individual->getCurrObjectiveValue();
		if( CurrObjVal < MinCurrObjVal )
			MinCurrObjVal = CurrObjVal;
		if( CurrObjVal > MaxCurrObjVal )
			MaxCurrObjVal = CurrObjVal;
	}

	MinCurrObjVal = -1;
	MaxCurrObjVal = 1;

	if( MinCurrObjVal == MaxCurrObjVal )
	{
		for( int i=0; i<inPopulation->getSize(); i++ )
			outFitness[i] = 1;
		return;
	}
	
	ISingleObjectiveProblem		^prob = dynamic_cast<ISingleObjectiveProblem ^>(inProblem);


	// zelimo fitness u rasponu 0 - 1
	if( prob->getObjectiveType() == EObjectiveType::MIN_OBJECTIVE )
	{
		for( int i=0; i<inPopulation->getSize(); i++ )
		{
			ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));

			outFitness[i] = (MaxCurrObjVal - Individual->getCurrObjectiveValue()) / (MaxCurrObjVal - MinCurrObjVal);;
		}
	}
	else
	{
		for( int i=0; i<inPopulation->getSize(); i++ )
		{
			ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));

			outFitness[i] = (Individual->getCurrObjectiveValue() - MinCurrObjVal) / (MaxCurrObjVal - MinCurrObjVal);
		}
	}
}

void		SPEAFitnessAssignment::setParetoSet(ISolutionContainer ^inParetoSet)
{
	_ParetoSet = inParetoSet;
}

void	SPEAFitnessAssignment::Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness)
{
	// najprije ra�unamo Pareto strengths
	int		Count;
	array<double> ^arrStrengths = gcnew array<double>(_ParetoSet->getSize());
	for( int i=0; i<_ParetoSet->getSize(); i++ )
	{
		Count = 0;
		IMultiObjectiveSolution		^SolPareto = dynamic_cast<IMultiObjectiveSolution	^>(_ParetoSet->getIndividual(i));

		for( int j=0; j<inPopulation->getSize(); j++ )
		{
			IMultiObjectiveSolution		^SolPop = dynamic_cast<IMultiObjectiveSolution	^>(inPopulation->getIndividual(j));

			if( SolPop->isCoveredBy(SolPareto) )
				Count++;
		}
		double	Strength = Count / ((double) inPopulation->getSize() + 1);
		arrStrengths[i] = Strength;
	}

	// a onda odre�ujemo fitness za jedinke iz populacije
	double		Sum;
	for( int j=0; j<inPopulation->getSize(); j++ )
	{
		Sum = 0;
		IMultiObjectiveSolution		^SolPop = dynamic_cast<IMultiObjectiveSolution	^>(inPopulation->getIndividual(j));

		for( int i=0; i<_ParetoSet->getSize(); i++ )
		{
			IMultiObjectiveSolution		^SolPareto = dynamic_cast<IMultiObjectiveSolution	^>(_ParetoSet->getIndividual(i));

			if( SolPop->isCoveredBy(SolPareto) )
				Sum += arrStrengths[i]; 
		}
		outFitness[j] = Sum + 1;
	}
}

void	CHNAFitnessAssignment::Assign(ISolutionContainer^ inPopulation, IProblem ^inProblem, array<double>  ^outFitness)
{
	array<bool>		^vecIsDominated;
	array<bool>		^vecIsFeasible;
	array<double>	^vecRank;

	vecIsDominated = gcnew array<bool>(inPopulation->getSize());
	vecIsFeasible	 = gcnew array<bool>(inPopulation->getSize());
	vecRank				 = gcnew array<double> (inPopulation->getSize());

	// naci sva nedominirana rjesenja
	for( int i=0; i<inPopulation->getSize(); i++ ) {
		bool		bDominated = false;
		IMultiObjectiveSolutionWC		^CurrInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (inPopulation->getIndividual(i));

		// provjeri za populaciju
		for( int j=0; j<inPopulation->getSize(); j++ ) {
			if( i != j ) {
				IMultiObjectiveSolutionWC		^PopInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (inPopulation->getIndividual(j));

				if( CurrInd->isCoveredBy(PopInd) ) {
					bDominated = true;
					break;
				}
			}
		}
		vecIsDominated[i] = bDominated;
	}

	for( int i=0; i<inPopulation->getSize(); i++ ) {
		IMultiObjectiveSolutionWC		^SolPop = dynamic_cast<IMultiObjectiveSolutionWC	^>(inPopulation->getIndividual(i));

		if( SolPop->isFeasible() == false) {
			// treba uklju�iti i ovisnost o broju prekr�enih ograni�enja
			int				Num = SolPop->getNumViolatedConstraints();
			double		Amount = 0;
			for( int j=0; j<SolPop->getNumConstraints(); j++ )
			{
				if( SolPop->getCurrConstraintValues()->getValue(j) > 0 )
					Amount += SolPop->getCurrConstraintValues()->getValue(j);
			}

			vecIsFeasible[i] = false;
			vecRank[i] = 0.95 * inPopulation->getSize() / (Num+1) / (1 + Amount);;
		}
		else {
			vecIsFeasible[i] = true;
			if( vecIsDominated[i] == false )
				vecRank[i] = 10;
			else
				vecRank[i] = 0.5 * inPopulation->getSize();
		}
	}
	double	Cmax = 1.2;
	double	Cmin = 0.8;
	int			M = inPopulation->getSize();

	for( int i=0; i<inPopulation->getSize(); i++ ) {
		outFitness[i] = (Cmax - (Cmax - Cmin) * (vecRank[i] - 1) / (M - 1));
	}
}