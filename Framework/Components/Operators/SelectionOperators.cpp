// This is the main DLL file.

#include "stdafx.h"

#include "SelectionOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"


using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;


void	Selection::RouleteWheelSelectionFitness::Select(array<double> ^vecSolFitness, array<int> ^vecOutSelInd, int inReqNumOfSelind)
{
	int Size = vecSolFitness->Length;

	// ra�unamo ukupni fitness
	double		TotalFitness = 0;
	for( int i=0; i<Size; i++ )	TotalFitness += vecSolFitness[i];

	double	Rnd1 = (rand() % 10000) * TotalFitness / 10000;
	// i sada trazimo u ciji "raspon" kumulativnog fitnessa ce upasti ta dva selektirana broja
	int				SelInd1 = -1;
	double		CumulativeFitness=0.f;
	for( int i=0; i<Size; i++ ) {
		double	IndFitness = vecSolFitness[i];

		if( CumulativeFitness <= Rnd1 && Rnd1 <= CumulativeFitness + IndFitness ) {
			SelInd1 = i;
			break;
		}
		CumulativeFitness += IndFitness;
	}

	assert(SelInd1 != -1);

	int		SelInd2 = -1;
	do {
		double	Rnd2 = (rand() % 10000) * TotalFitness / 10000;
		CumulativeFitness=0.f;

		for( int i=0; i<Size; i++ ) {
			double		IndFitness = vecSolFitness[i];

			if( CumulativeFitness <= Rnd2 && Rnd2 <= CumulativeFitness + IndFitness ) {
				SelInd2 = i;
				break;
			}
			CumulativeFitness += IndFitness;
		}
	} while(SelInd2 == SelInd1);

	assert(SelInd2 != -1);

	vecOutSelInd[0] = SelInd1;
	vecOutSelInd[1] = SelInd2;
}
/*
void Selection::RouleteWheelSelection::Select(ISolutionContainer^ inPopulation, PopulationSelection^ SelIndividuals, int inReqNumOfSelind)
{
	// racunamo najmanji najveci fitness
	double		MinFit = 0;
	double		MaxFit = 1000;
	for( int i=0; i<inPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));

		double		Fit = Individual->getCurrObjectiveValue();
		if( Fit < MinFit )
			MinFit = Fit;
		if( Fit > MaxFit )
			MaxFit = Fit;
	}

	// ra�unamo ukupni fitness
	double		TotalFitness = 0;
	for( int i=0; i<inPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));
		TotalFitness += Individual->getCurrObjectiveValue() - MinFit;
//		TotalFitness += (MaxFit - Individual->getCurrObjectiveValue() + 1);
	}
	double	Rnd1 = (rand() % 10000) * TotalFitness / 10000;

	// i sada trazimo u ciji "raspon" kumulativnog fitnessa ce upasti ta dva selektirana broja
	int			SelInd1 = -1;
	double		CumulativeFitness=0.f;

	for( int i=0; i<inPopulation->getSize(); i++ ) {
		ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));
		double			IndFitness = Individual->getCurrObjectiveValue() - MinFit;

		if( CumulativeFitness <= Rnd1 && Rnd1 <= CumulativeFitness + IndFitness ) {
			SelInd1 = i;
			break;
		}
		CumulativeFitness += IndFitness;
	}

	assert(SelInd1 != -1);

	int		SelInd2 = -1;
	do {
		double	Rnd2 = (rand() % 10000) * TotalFitness / 10000;
		CumulativeFitness=0.f;

		for( int i=0; i<inPopulation->getSize(); i++ ) {
			ISingleObjectiveSolution ^Individual = dynamic_cast<ISingleObjectiveSolution ^> (inPopulation->getIndividual(i));
//			float			IndFitness = (MaxFit - Individual->getCurrObjectiveValue() + 1);
			double			IndFitness = Individual->getCurrObjectiveValue() -MinFit;

			if( CumulativeFitness <= Rnd2 && Rnd2 <= CumulativeFitness + IndFitness ) {
				SelInd2 = i;
				break;
			}
			CumulativeFitness += IndFitness;
		}
	} while(SelInd2 == SelInd1);

	assert(SelInd2 != -1);

	SelIndividuals->addSelIndvidual(SelInd1, inPopulation->getIndividual(SelInd1));
	SelIndividuals->addSelIndvidual(SelInd2, inPopulation->getIndividual(SelInd2));
};

void Selection::TournamentSelection::Select(ISolutionContainer^ inPopulation, PopulationSelection^ SelIndividuals, int inReqNumOfSelind)
{

	int		Rnd1, Rnd2, Rnd3;
	do {
		Rnd1 = (rand() % inPopulation->getSize());
		Rnd2 = (rand() % inPopulation->getSize());
		Rnd3 = (rand() % inPopulation->getSize());
	} while(Rnd1 == Rnd2 || Rnd1 == Rnd3 || Rnd2 == Rnd3 );

	IFitnessProvider ^Ind1 = dynamic_cast<IFitnessProvider ^> (inPopulation->getIndividual(Rnd1));
	IFitnessProvider ^Ind2 = dynamic_cast<IFitnessProvider ^> (inPopulation->getIndividual(Rnd2));
	IFitnessProvider ^Ind3 = dynamic_cast<IFitnessProvider ^> (inPopulation->getIndividual(Rnd3));

	float		Fit1 = Ind1->getFitness();
	float		Fit2 = Ind2->getFitness();
	float		Fit3 = Ind3->getFitness();

	if ( Fit1 <= Fit2 )
	{
		if( Fit3 <= Fit2 )
			SelIndividuals->addSelIndvidual(Rnd3, inPopulation->getIndividual(Rnd3));
		else
			SelIndividuals->addSelIndvidual(Rnd2, inPopulation->getIndividual(Rnd2));

		SelIndividuals->addSelIndvidual(Rnd1, inPopulation->getIndividual(Rnd1));
	}
	else
	{
		if( Fit3 <= Fit1 )
			SelIndividuals->addSelIndvidual(Rnd3, inPopulation->getIndividual(Rnd3));
		else
			SelIndividuals->addSelIndvidual(Rnd1, inPopulation->getIndividual(Rnd1));

		SelIndividuals->addSelIndvidual(Rnd2, inPopulation->getIndividual(Rnd2));
	}
	
};
*/