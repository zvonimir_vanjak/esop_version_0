#include "stdafx.h"

#include "IntegerOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"
#include "..\..\Components\Representations\IntegerArray.h"

#include "IntegerArrayOperators.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

void IntegerArray_1PointCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	IntegerArray ^Parent1 = dynamic_cast<IntegerArray ^>(p1);
	IntegerArray ^Parent2 = dynamic_cast<IntegerArray ^>(p2);

	IntegerArray ^Child1 = dynamic_cast<IntegerArray ^>(pChild1);
	IntegerArray ^Child2 = dynamic_cast<IntegerArray ^>(pChild2);

	Integer_1PointCrossover		^Cross = gcnew Integer_1PointCrossover();

	for( int i=0; i<Parent1->getVarNum(); i++ )
	{
		Integer	^p1 = Parent1->getVar(i);
		Integer	^p2 = Parent2->getVar(i);
		Integer	^c1 = Child1->getVar(i);
		Integer	^c2 = Child2->getVar(i);

		Cross->Recombine(p1, p2, c1, c2);
	}

	delete Cross;
}

void IntegerArray_ExchangeCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void IntegerArray_GaussianDistributionCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

/*******************************************************************************************/
void IntegerArray_CreepMutation::Mutate(IDesignVariable ^Child)
{
	// malo cemo (eventualno) promijeniti vrijednost
	IntegerArray ^Ind = dynamic_cast<IntegerArray ^>(Child);

	for( int i=0; i<Ind->getVarNum(); i++ )
	{
		int		Len = Ind->getVar(i)->_Up - Ind->getVar(i)->_Low;
		int		RandWidth = Len;					// mutacija ce mijenjati vrijednost max. za 20 % sirine intervala

		int		RandVal = rand() % RandWidth - RandWidth / 2;

		int		NewVal = Ind->getVar(i)->getValue() + RandVal;
		
		if( NewVal < Ind->getVar(i)->_Low )
			NewVal = Ind->getVar(i)->_Low;
		if( NewVal > Ind->getVar(i)->_Up)
			NewVal = Ind->getVar(i)->_Up;

		Ind->getVar(i)->setValue(NewVal);
	}
}

void IntegerArray_RandomRessetingMutation::Mutate(IDesignVariable ^vecChildren)
{
}