// GeneticOperators.h

#pragma once

using namespace System;
using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					////////////////////////////////////////////////////////////////////////////////////////////
					public ref class Real_GaussianDistributionCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class Real_LineCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class Real_SBXCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
				}

				namespace Mutation
				{
					public ref class Real_GaussianDistributionMutation : public GeneticAlgorithms::IMutationOp
					{
					public:
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
				}
			}
		}
	}
}
