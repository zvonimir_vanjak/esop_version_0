#include "stdafx.h"

#include "PermutationOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"


using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;


void PermutationPMXCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void PermutationEdgeCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void PermutationOrderCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void PermutationCycleCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void PermutationSwapMutation::Mutate(IDesignVariable ^vecChildren)
{
}

void PermutationInsertMutation::Mutate(IDesignVariable ^vecChildren)
{
}

void PermutationScrambleMutation::Mutate(IDesignVariable ^vecChildren)
{
}

void PermutationInversionMutation::Mutate(IDesignVariable ^vecChildren)
{
}