#include "stdafx.h"

#include "RealOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"
#include "..\..\Components\Representations\Real.h"
#include "..\..\Components\Operators\ElementaryOperators.h"


using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators;
using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

void Real_GaussianDistributionCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void Real_LineCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void Real_SBXCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

/*******************************************************************************************/

void Real_GaussianDistributionMutation::Mutate(IDesignVariable ^vecChildren)
{
}

