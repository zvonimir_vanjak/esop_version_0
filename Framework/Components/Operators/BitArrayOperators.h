// GeneticOperators.h

#pragma once

using namespace System;

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::DynamicSupport;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					public ref class BitArray1PointCrossover : public ICrossoverOperator2To2 //, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class BitArray2PointCrossover : public ICrossoverOperator2To2 //, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class BitArrayNPointCrossover : public ICrossoverOperator2To2 //, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class BitArrayUniformCrossover : public ICrossoverOperator2To2 //, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
				}

				namespace Mutation
				{
					public ref class BitArrayMutationFlopBit : public IMutationOp //, public IESOPOperator
					{
					public:
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
				}
			}
		}
	}
}
