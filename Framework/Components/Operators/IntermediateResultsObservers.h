#pragma once

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			public ref class IntermedTimerOnEveryIterNum : public IIntermediateTimer
			{
			public:
				virtual	bool	IsTimeToSave();
			};
		}
	}
}