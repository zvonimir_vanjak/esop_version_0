	// GeneticOperators.h

#pragma once

using namespace System;
using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					public ref class RealArrayAsBitArray_1PointCrossover : public ICrossoverOperator2To2, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

					virtual	String^		getESOPComponentName() 		{ return "RealArrayAsBitArray_1PointCrossover"; }
					virtual String^		getESOPComponentDesc()		{ return "RealArrayAsBitArray_1PointCrossover"; }
					};

					public ref class RealArrayAsBitArray_2PointCrossover : public ICrossoverOperator2To2, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

						virtual	String^		getESOPComponentName() 		{ return "RealArrayAsBitArray_2PointCrossover"; }
						virtual String^		getESOPComponentDesc()		{ return "RealArrayAsBitArray_2PointCrossover"; }
					};

					public ref class RealArrayAsBitArray_UniformCrossover : public ICrossoverOperator2To2, public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

						virtual	String^		getESOPComponentName() 		{ return "RealArrayAsBitArray_UniformCrossover"; }
						virtual String^		getESOPComponentDesc()		{ return "RealArrayAsBitArray_UniformCrossover"; }
					};
				}

				namespace Mutation
				{
					public ref class RealArrayAsBitArray_FlopBitMutation: public IMutationOp, public IESOPParameterOperator
					{
					public:
						RealArrayAsBitArray_FlopBitMutation();

						virtual void	Mutate(IDesignVariable ^vecChildren);
						void	setMutationProbPerBit(double inProb);

						virtual int				getParamNum() { return 1; }
						virtual String^		getParamName(int ParamInd) { return "Mutation probability per bit"; }
						
						virtual void			setParamValue(int Ind, double inVal) { setMutationProbPerBit(inVal); }
						virtual double		getParamValue(int Ind) { return (double) _MutationProbPerBit; }

						virtual	String^		getESOPComponentName() 		{ return "RealArrayAsBitArray_FlopBitMutation"; }
						virtual String^		getESOPComponentDesc()		{ return "RealArrayAsBitArray_FlopBitMutation"; }

					private:
						double	_MutationProbPerBit;
					};
				}

			}
		}
	}
}
