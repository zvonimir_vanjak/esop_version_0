#include "stdafx.h"

#include "RealOperators.h"
#include "RealArrayOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"
#include "..\..\Components\Representations\RealArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"


using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators;
using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

/*******************************************************************************************/
void RealArray_GaussianDistributionCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealArray ^Parent1 = dynamic_cast<RealArray ^>(p1);
	RealArray ^Parent2 = dynamic_cast<RealArray ^>(p2);

	RealArray ^Child1 = dynamic_cast<RealArray ^>(pChild1);
	RealArray ^Child2 = dynamic_cast<RealArray ^>(pChild2);

	for( int i=0; i<Parent1->getVarNum(); i++ )
	{
	}
}

void RealArray_ArithmeticRecombinationCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

void RealArray_ExchangeCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealArray ^Parent1 = dynamic_cast<RealArray ^>(p1);
	RealArray ^Parent2 = dynamic_cast<RealArray ^>(p2);

	RealArray ^Child1 = dynamic_cast<RealArray ^>(pChild1);
	RealArray ^Child2 = dynamic_cast<RealArray ^>(pChild2);

	for( int i=0; i<Parent1->getVarNum(); i++ )
	{
		float	Rnd = (float) ( rand() % 1000 ) / 10;

		if( Rnd < _Probability )
		{
			// zamjeni vrijednosti me�u roditeljima
			Child1->setValue(i, Parent2->getValue(i));
			Child2->setValue(i, Parent1->getValue(i));
		}
		{
			Child1->setValue(i, Parent1->getValue(i));
			Child2->setValue(i, Parent2->getValue(i));
		}
	}
}
/*******************************************************************************************/
