#include "stdafx.h"

#include "BitArrayOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"
#include "..\..\Components\Representations\Integer.h"

#include "IntegerOperators.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

void Integer_1PointCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	Integer ^Parent1 = dynamic_cast<Integer ^>(p1);
	Integer ^Parent2 = dynamic_cast<Integer ^>(p2);

	Integer ^Child1 = dynamic_cast<Integer ^>(pChild1);
	Integer ^Child2 = dynamic_cast<Integer ^>(pChild2);

	int		DigNum=0, temp = Parent1->_Up;
	while( temp > 1 )
	{
		DigNum++;
		temp /= 2;
	}
/*
	vector<bool>		Bit1, Bit2, c1, c2;
	IntToBitArray(Parent1->getValue(),DigNum, Bit1);
	IntToBitArray(Parent2->getValue(),DigNum, Bit2);

	c1.resize(Bit1.size());
	c2.resize(Bit1.size());
	Perform_BitArray_1Point_Crossover(Bit1, Bit2, c1, c2);

	long		out1, out2;
	BitArrayToInt(c1, out1);
	BitArrayToInt(c2, out2);
	
	if( out1 < Parent1->_Low ) out1 = Parent1->_Low;
	if( out1 > Parent1->_Up ) out1 = Parent1->_Up;

	if( out2 < Parent2->_Low ) out2 = Parent2->_Low;
	if( out2 > Parent2->_Up ) out2 = Parent2->_Up;

	Child1->setValue(out1);
	Child2->setValue(out2);
*/
}

void Integer_GaussianDistributionCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
}

/*******************************************************************************************/

void Integer_CreepMutation::Mutate(IDesignVariable ^Child)
{
	// malo cemo (eventualno) promijeniti vrijednost
	Integer ^Ind = dynamic_cast<Integer ^>(Child);

	int		Len = Ind->_Up - Ind->_Low;
	int		RandWidth = Len / 5;					// mutacija ce mijenjati vrijednost max. za 20 % sirine intervala

	int		RandVal = rand() % RandWidth - RandWidth / 2;

	Ind->setValue(Ind->getValue() + RandVal);
}

void Integer_RandomResseting::Mutate(IDesignVariable ^vecChildren)
{
}