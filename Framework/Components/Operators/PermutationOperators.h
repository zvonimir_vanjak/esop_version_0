// GeneticOperators.h

#pragma once

//#include "Base\BaseOperatorInterfaces.h"

using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					public ref class PermutationPMXCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class PermutationEdgeCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class PermutationOrderCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class PermutationCycleCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
				}

				namespace Mutation
				{
					public ref class PermutationSwapMutation : public IMutationOp
					{
					public:
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
					public ref class PermutationInsertMutation : public IMutationOp
					{
					public:
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
					public ref class PermutationScrambleMutation : public IMutationOp
					{
					public:
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
					public ref class PermutationInversionMutation : public IMutationOp
					{
					public:
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
				}
			}
		}
	}
}
