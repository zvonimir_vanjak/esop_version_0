#include "stdafx.h"

#include "RealAsBitArrayOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"

#include "..\..\Components\Representations\RealAsBitArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System;

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators;
using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

void RealAsBitArray_1PointCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(p1);
	RealAsBitArray ^Parent2 = dynamic_cast<RealAsBitArray ^>(p2);

	RealAsBitArray ^Child1 = dynamic_cast<RealAsBitArray ^>(pChild1);
	RealAsBitArray ^Child2 = dynamic_cast<RealAsBitArray ^>(pChild2);

	vector<bool> *Bit1 = Parent1->getRepr()->getRepr();
	vector<bool> *Bit2 = Parent2->getRepr()->getRepr();

	String ^s = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*Bit1)[i] == false )
			s += "0";
		else
			s += "1";
	}
	String ^s2 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*Bit2)[i] == false )
			s2 += "0";
		else
			s2 += "1";
	}

	vector<bool>	*c1, *c2;
	c1 = new vector<bool>;
	c2 = new vector<bool>;
	c1->resize(Bit1->size());
	c2->resize(Bit1->size());

	Perform_BitArray_1Point_Crossover(Bit1, Bit2, c1, c2);

	vector<bool> *BitC1 = Child1->getRepr()->getRepr();
	vector<bool> *BitC2 = Child2->getRepr()->getRepr();

	String ^s3 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*BitC1)[i] == false )
			s3 += "0";
		else
			s3 += "1";
	}
	String ^s4 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*BitC2)[i] == false )
			s4 += "0";
		else
			s4 += "1";
	}

	Child1->setRepr(c1);
	Child2->setRepr(c2);

	delete c1;
	delete c2;
}

void RealAsBitArray_2PointCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(p1);
	RealAsBitArray ^Parent2 = dynamic_cast<RealAsBitArray ^>(p2);

	RealAsBitArray ^Child1 = dynamic_cast<RealAsBitArray ^>(pChild1);
	RealAsBitArray ^Child2 = dynamic_cast<RealAsBitArray ^>(pChild2);

	// i sada treba na svakom pojedinačnom RealAsBitArray u kolekciji obaviti crossover
	vector<bool> *Bit1 = Parent1->getRepr()->getRepr();
	vector<bool> *Bit2 = Parent2->getRepr()->getRepr();

	vector<bool>	*c1, *c2;
	c1 = new vector<bool>;
	c2 = new vector<bool>;
	c1->resize(Bit1->size());
	c2->resize(Bit1->size());

	Perform_BitArray_2Point_Crossover(Bit1, Bit2, c1, c2);
/*
	CString		out1 = "";
	CString		out2 = "";

	for( int j=0; j<(int) c1.size(); j++ )
		if( c1[j] == true )		out1.Append("1");
		else									out1.Append("0");

	for( int j=0; j<(int) c2.size(); j++ )
		if( c2[j] == true )		out2.Append("1");
		else									out2.Append("0");
*/
	Child1->setRepr(c1);
	Child2->setRepr(c2);

	delete c1;
	delete c2;
}

void RealAsBitArray_UniformCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(p1);
	RealAsBitArray ^Parent2 = dynamic_cast<RealAsBitArray ^>(p2);

	RealAsBitArray ^Child1 = dynamic_cast<RealAsBitArray ^>(pChild1);
	RealAsBitArray ^Child2 = dynamic_cast<RealAsBitArray ^>(pChild2);

	// i sada treba na svakom pojedinačnom RealAsBitArray u kolekciji obaviti crossover
	vector<bool> *Bit1 = Parent1->getRepr()->getRepr();
	vector<bool> *Bit2 = Parent2->getRepr()->getRepr();

	vector<bool>	*c1, *c2;
	c1 = new vector<bool>;
	c2 = new vector<bool>;
	c1->resize(Bit1->size());
	c2->resize(Bit1->size());

	Perform_BitArray_Uniform_Crossover(Bit1, Bit2, c1, c2);

	Child1->setRepr(c1);
	Child2->setRepr(c2);

	delete c1;
	delete c2;
}

/*******************************************************************************************/
RealAsBitArray_FlopBitMutation::RealAsBitArray_FlopBitMutation()
{
	_MutationProbPerBit = 0.01;
}

void RealAsBitArray_FlopBitMutation::Mutate(IDesignVariable ^Child) 
{
	RealAsBitArray ^Ind = dynamic_cast<RealAsBitArray ^>(Child);

	Random ^rnd = gcnew Random();
	for( int j=0; j<Ind->getRepr()->getBitNum(); j++ )
	{
		if( (rnd->Next() % 1000) / 1000. < _MutationProbPerBit )
			Ind->FlopBit(j);
	}
}

void	RealAsBitArray_FlopBitMutation::setMutationprob(double inMutProb)
{
	_MutationProbPerBit = inMutProb;
}

