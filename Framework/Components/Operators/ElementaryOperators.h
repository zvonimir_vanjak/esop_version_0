#include <vector>
#include <deque>

using std::vector;
using std::deque;

void	IntToBitArray(long inVal, int inDigitNum, vector<bool> *outBitArray);
void	BitArrayToInt(const vector<bool> *inBitArray, long &outVal);

void	Perform_BitArray_1Point_Crossover(vector<bool> *p1, vector<bool> *p2, vector<bool> *c1, vector<bool> *c2);
void	Perform_BitArray_2Point_Crossover(vector<bool> *p1, vector<bool> *p2, vector<bool> *c1, vector<bool> *c2);
void	Perform_BitArray_Uniform_Crossover(vector<bool> *p1, vector<bool> *p2, vector<bool> *c1, vector<bool> *c2);