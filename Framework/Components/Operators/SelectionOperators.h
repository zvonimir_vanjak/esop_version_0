// GeneticOperators.h

#pragma once
//#include "Base\DynamicSupport.h"

using namespace System;

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
using namespace ESOP::Framework::DynamicSupport;


namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Selection
				{
					public ref class RouleteWheelSelectionFitness : public IFitnessBasedSelectionOperator , public IESOPOperator
					{
					public:
						virtual	void	Select(array<double> ^vecSolFitness, array<int> ^vecOutSelInd, int inReqNumOfSelind);

						virtual	String^		getESOPComponentName() 		{ return "RouleteWheelSelectionFitness"; }
						virtual String^		getESOPComponentDesc()		{ return "RouleteWheelSelectionFitness"; }
					};
/*
					public ref class RouleteWheelSelection : public ISelectionOperator , public IESOPOperator
					{
					public:
						virtual void	Select(ISolutionContainer^ inPopulation, PopulationSelection^ SelIndividuals, int inReqNumOfSelind);

						virtual String^ getESOPComponentName() { return ""; }
					};

					public ref class StohasticUniversalSamplingSelection : public ISelectionOperator , public IESOPOperator
					{
					public:
						virtual void	Select(ISolutionContainer^ inPopulation, PopulationSelection^ SelIndividuals, int inReqNumOfSelind) 
						{
						}
						virtual	String^		getESOPComponentName() 		{ return "StohasticUniversalSamplingSelection"; }
						virtual String^		getESOPComponentDesc()		{ return "StohasticUniversalSamplingSelection"; }
					};

					public ref class TournamentSelection : public ISelectionOperator , public IESOPOperator
					{
					public:
						TournamentSelection() : _K(2) {}
						TournamentSelection(int inK) : _K(inK) {}

						virtual void	Select(ISolutionContainer^ inPopulation, PopulationSelection^ SelIndividuals, int inReqNumOfSelind);

						virtual	String^		getESOPComponentName() 		{ return "TournamentSelection"; }
						virtual String^		getESOPComponentDesc()		{ return "TournamentSelection"; }
					private:
						int		_K;			// od koliko jedinki se izabire najbolja
					};

					public ref class RankBasedSelection : public ISelectionOperator , public IESOPOperator
					{
					public:
						virtual void	Select(ISolutionContainer^ inPopulation, PopulationSelection^ SelIndividuals, int inReqNumOfSelind)
						{
						}
						virtual	String^		getESOPComponentName() 		{ return "RankBasedSelection"; }
						virtual String^		getESOPComponentDesc()		{ return "RankBasedSelection"; }
					};
*/
				}
			}
		}
	}
}
