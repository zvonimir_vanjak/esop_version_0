// GeneticOperators.h

#pragma once



//#include "Base\BaseOperatorInterfaces.h"

using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					public ref class IntegerArray_1PointCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
					public ref class IntegerArray_ExchangeCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
					public ref class IntegerArray_GaussianDistributionCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};
				}

				namespace Mutation
				{
					public ref class IntegerArray_RandomRessetingMutation : public IMutationOp
					{
					public:
						// slučajno odabire novu vrijednost iz dopuštenog raspona
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
					public ref class IntegerArray_CreepMutation : public GeneticAlgorithms::IMutationOp
					{
					public:
						// nova vrijednost se dobiva dodavanjem male vrijednosti sa vjerojatnošću p
						virtual void Mutate(IDesignVariable ^vecChildren);
					};
				}
			}
		}
	}
}
