// GeneticOperators.h

#pragma once

using namespace System;

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;


namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					////////////////////////////////////////////////////////////////////////////////////////////
					public ref class RealAsBitArray_1PointCrossover : public ICrossoverOperator2To2 , public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

						virtual	String^		getESOPComponentName() 		{ return "RealAsBitArray_1PointCrossover"; }
						virtual String^		getESOPComponentDesc()		{ return "RealAsBitArray_1PointCrossover"; }

						virtual String^		ToString() override { return getESOPComponentName(); }
					};

					public ref class RealAsBitArray_2PointCrossover : public ICrossoverOperator2To2 , public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

						virtual	String^		getESOPComponentName() 		{ return "RealAsBitArray_2PointCrossover"; }
						virtual String^		getESOPComponentDesc()		{ return "RealAsBitArray_2PointCrossover"; }
					};

					public ref class RealAsBitArray_UniformCrossover : public ICrossoverOperator2To2 , public IESOPOperator
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

						virtual	String^		getESOPComponentName() 		{ return "RealAsBitArray_UniformCrossover"; }
						virtual String^		getESOPComponentDesc()		{ return "RealAsBitArray_UniformCrossover"; }
					};
				}

				namespace Mutation
				{
					public ref class RealAsBitArray_FlopBitMutation: public IMutationOp , public IESOPParameterOperator
					{
					public:
						RealAsBitArray_FlopBitMutation();

						virtual void Mutate(IDesignVariable ^Child);
						void		setMutationprob(double inMutProb);

						virtual	String^		getESOPComponentName() 		{ return "RealAsBitArray_FlopBitMutation"; }
						virtual String^		getESOPComponentDesc()		{ return "RealAsBitArray_FlopBitMutation"; }

						virtual int				getParamNum() { return 1; }
						virtual String^		getParamName(int ParamInd) { return "Mutation probability per bit"; }
						
						virtual void			setParamValue(int Ind, double inVal) { setMutationprob(inVal); }
						virtual double		getParamValue(int Ind) { return (double) _MutationProbPerBit; }

					private:
						double		_MutationProbPerBit;
					};
				}
			}
		}
	}
}
