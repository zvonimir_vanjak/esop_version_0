#include "stdafx.h"

#include "RealAsBitArrayOperators.h"
#include "RealArrayAsBitArrayOperators.h"

#include "..\..\Components\Algorithms\GAHelpers.h"
#include "..\..\Components\Representations\RealArrayAsBitArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"


using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators;
using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

void RealArrayAsBitArray_1PointCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealArray_AsBitArray ^Parent1 = dynamic_cast<RealArray_AsBitArray ^>(p1);
	RealArray_AsBitArray ^Parent2 = dynamic_cast<RealArray_AsBitArray ^>(p2);

	RealArray_AsBitArray ^Child1 = dynamic_cast<RealArray_AsBitArray ^>(pChild1);
	RealArray_AsBitArray ^Child2 = dynamic_cast<RealArray_AsBitArray ^>(pChild2);

	// i sada treba na svakom pojedinačnom RealAsBitArray u kolekciji obaviti crossover
	RealAsBitArray_1PointCrossover		^cross = gcnew RealAsBitArray_1PointCrossover();

	for( int i=0; i<Parent1->getVarNum(); i++ )
	{
		RealAsBitArray ^p1 = Parent1->getRepr(i);
		RealAsBitArray ^p2 = Parent2->getRepr(i);

		RealAsBitArray ^c1 = Child1->getRepr(i);
		RealAsBitArray ^c2 = Child2->getRepr(i);

		cross->Recombine(p1, p2, c1, c2);
	}

	delete cross;
}

void RealArrayAsBitArray_2PointCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealArray_AsBitArray ^Parent1 = dynamic_cast<RealArray_AsBitArray ^>(p1);
	RealArray_AsBitArray ^Parent2 = dynamic_cast<RealArray_AsBitArray ^>(p2);

	RealArray_AsBitArray ^Child1 = dynamic_cast<RealArray_AsBitArray ^>(pChild1);
	RealArray_AsBitArray ^Child2 = dynamic_cast<RealArray_AsBitArray ^>(pChild2);

	// i sada treba na svakom pojedinačnom RealAsBitArray u kolekciji obaviti crossover
	RealAsBitArray_2PointCrossover		^cross = gcnew RealAsBitArray_2PointCrossover();

	for( int i=0; i<Parent1->getVarNum(); i++ )
	{
		RealAsBitArray ^p1 = Parent1->getRepr(i);
		RealAsBitArray ^p2 = Parent2->getRepr(i);

		RealAsBitArray ^c1 = Child1->getRepr(i);
		RealAsBitArray ^c2 = Child2->getRepr(i);

		cross->Recombine(p1, p2, c1, c2);
	}

	delete cross;
}

void RealArrayAsBitArray_UniformCrossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealArray_AsBitArray ^Parent1 = dynamic_cast<RealArray_AsBitArray ^>(p1);
	RealArray_AsBitArray ^Parent2 = dynamic_cast<RealArray_AsBitArray ^>(p2);

	RealArray_AsBitArray ^Child1 = dynamic_cast<RealArray_AsBitArray ^>(pChild1);
	RealArray_AsBitArray ^Child2 = dynamic_cast<RealArray_AsBitArray ^>(pChild2);

	// i sada treba na svakom pojedinačnom RealAsBitArray u kolekciji obaviti crossover
	RealAsBitArray_UniformCrossover		^cross = gcnew RealAsBitArray_UniformCrossover();

	for( int i=0; i<Parent1->getVarNum(); i++ )
	{
		RealAsBitArray ^p1 = Parent1->getRepr(i);
		RealAsBitArray ^p2 = Parent2->getRepr(i);

		RealAsBitArray ^c1 = Child1->getRepr(i);
		RealAsBitArray ^c2 = Child2->getRepr(i);

		cross->Recombine(p1, p2, c1, c2);
	}

	delete cross;
}
/*******************************************************************************************/
RealArrayAsBitArray_FlopBitMutation::RealArrayAsBitArray_FlopBitMutation()
{
	_MutationProbPerBit = 0.1f;
}

void RealArrayAsBitArray_FlopBitMutation::setMutationProbPerBit(double inProb)
{
	_MutationProbPerBit = inProb;
}

void RealArrayAsBitArray_FlopBitMutation::Mutate(IDesignVariable ^Child)
{
	RealArray_AsBitArray ^Ind = dynamic_cast<RealArray_AsBitArray ^>(Child);

	// i sada treba na svakom pojedinačnom RealAsBitArray u kolekciji obaviti mutaciju
	RealAsBitArray_FlopBitMutation		^MutationOp = gcnew RealAsBitArray_FlopBitMutation();
	MutationOp->setMutationprob(_MutationProbPerBit);

	for( int i=0; i<Ind->getVarNum(); i++ )
	{
		RealAsBitArray	^Var = Ind->getRepr(i);
		MutationOp->Mutate(Var);
	}

	delete MutationOp;
}
