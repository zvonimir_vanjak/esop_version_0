#include "stdafx.h"

#include "CompositeRepresentationOperators.h"
#include "..\..\Components\Algorithms\GAHelpers.h"
#include "..\..\Components\Representations\StandardCompositeRepresentation.h"
#include "..\..\Components\Operators\ElementaryOperators.h"


using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Operators;
using namespace ESOP::Framework::StandardRepresentations;

using namespace ESOP::Framework::Operators::GeneticAlgorithms::Crossover;
using namespace ESOP::Framework::Operators::GeneticAlgorithms::Mutation;

/*******************************************************************************************/
void CompositeCrossoverOperator::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	GenericDynamicCompositeRepresentation ^Parent1 = dynamic_cast<GenericDynamicCompositeRepresentation ^>(p1);
	GenericDynamicCompositeRepresentation ^Parent2 = dynamic_cast<GenericDynamicCompositeRepresentation ^>(p2);

	GenericDynamicCompositeRepresentation ^Child1 = dynamic_cast<GenericDynamicCompositeRepresentation ^>(pChild1);
	GenericDynamicCompositeRepresentation ^Child2 = dynamic_cast<GenericDynamicCompositeRepresentation ^>(pChild2);
}

void CompositeMutationOperator::Mutate(IDesignVariable ^Child)
{
	GenericDynamicCompositeRepresentation ^Ind = dynamic_cast<GenericDynamicCompositeRepresentation ^>(Child);
}
