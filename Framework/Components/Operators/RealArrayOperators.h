// GeneticOperators.h

#pragma once

//#include "Base\BaseOperatorInterfaces.h"

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

namespace ESOP
{
	namespace Framework
	{
		namespace Operators
		{
			namespace GeneticAlgorithms
			{
				namespace Crossover
				{
					public ref class RealArray_GaussianDistributionCrossover : public ICrossoverOperator2To2
					{
					public:
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);
					};

					public ref class RealArray_ArithmeticRecombinationCrossover : public ICrossoverOperator2To2, public IESOPOperator
					{
					public:
						RealArray_ArithmeticRecombinationCrossover() {}
						RealArray_ArithmeticRecombinationCrossover(float	inProbability ) : _Probability(inProbability) {}
						
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

						virtual	String^		getESOPComponentName() 		{ return "RealArray_ArithmeticRecombinationCrossover"; }
						virtual String^		getESOPComponentDesc()		{ return "RealArray_ArithmeticRecombinationCrossover"; }
					private:
						float		_Probability;
					};

					public ref class RealArray_ExchangeCrossover: public ICrossoverOperator2To2
					{
					public:
						RealArray_ExchangeCrossover(float	inProbability ) : _Probability(inProbability) {}
						virtual void Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

					private:
						float		_Probability;
					};
				}

				namespace Mutation
				{
					public ref class RealArray_RandomMutation: public IMutationOp, public IESOPOperator
					{
					public:
						RealArray_RandomMutation() {}

						virtual void	Mutate(IDesignVariable ^vecChildren) {}
						void	setMutationProbPerBit(double inProb) {}
/*
						virtual int				getParamNum() { return 1; }
						virtual String^		getParamName(int ParamInd) { return "Mutation probability per bit"; }

						virtual void			setParamValue(int Ind, double inVal) { setMutationProbPerBit(inVal); }
						virtual double		getParamValue(int Ind) { return (double) _MutationProbPerBit; }
*/
						virtual	String^		getESOPComponentName() 		{ return "RealArray_RandomMutation"; }
						virtual String^		getESOPComponentDesc()		{ return "RealArray_RandomMutation"; }

					private:
						double	_MutationProbPerBit;
					};
				}

			}
		}
	}
}
