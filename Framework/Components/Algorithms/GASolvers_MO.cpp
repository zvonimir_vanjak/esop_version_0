#include "StdAfx.h"

#include "GASolvers_MO.h"

//#include "Components\Representations\StandardRepresentations.h"
#include "..\..\Components\Operators\RealAsBitArrayOperators.h"
#include "..\..\Components\Operators\SelectionOperators.h"
#include "..\..\Components\Operators\FitnessAssignmentOperators.h"

using namespace System;
using namespace System::Text;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;
//using namespace ESOP::Framework::StandardRepresentations;

/*******************************************************************************************/

BaseMOGA::BaseMOGA()
{
	_PopSize = 30;
	_Population = gcnew Population();
	_ParetoSet = gcnew Population();
}

void	BaseMOGA::setESOPDesignVariable(IESOPDesignVariable ^DesVar)
{
	if( _SolutionInstance == nullptr )
		_SolutionInstance = gcnew IMultiObjectiveSolution();

	_SolutionInstance->setDesignVar(dynamic_cast<IDesignVariable ^>(DesVar));
}
void	BaseMOGA::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	if( _SolutionInstance == nullptr )
		throw 1;
	else
	{
		_SolutionInstance->setMultiObjectiveProblem(dynamic_cast<IMultiObjectiveProblem ^>(ProbInst));
	}

//	setProblemInstance(dynamic_cast<IMultiObjectiveProblem ^>(ProbInst));
}

Population^		BaseMOGA::getPopulation() 
{ 
	return _Population; 
}
array<ISolutionMO^>^		BaseMOGA::getParetoSet()
{
	array<ISolutionMO ^>		^ret = gcnew array<ISolutionMO ^>(_ParetoSet->getSize());

	for( int i=0; i<_ParetoSet->getSize(); i++ )
	{
		ret[i] = dynamic_cast<ISolutionMO ^> (_ParetoSet->getIndividual(i));
	}
	return ret;
}
int		BaseMOGA::Initialize()
{
	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISolution		^SolPop = dynamic_cast<ISolution	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}

	return 0;
}
void	BaseMOGA::setPopulationSize(int inSize)
{
	_PopSize = inSize;
//	_Population->Initialize(inSize, _SolutionInstance);
}
int		BaseMOGA::getPopulationSize()
{
	return _PopSize;
}
void	BaseMOGA::setParetoSetMaxSize(int inSize)
{
	_ParetoSetMaxSize = inSize;
}
int		BaseMOGA::getParetoSetMaxSize()
{
	return _ParetoSetMaxSize;
}
void	BaseMOGA::setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign)
{
	_FitnessAssignment = inFitAssign;
}
void	BaseMOGA::setSelectionOp(IFitnessBasedSelectionOperator	^inSelector)
{
	_Selector = inSelector;
}
void	BaseMOGA::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}
void	BaseMOGA::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}
void	BaseMOGA::Run(OptimizationResult^ outRes)
{
	IterativeAlgorithm::Run(outRes);

	MOResult ^finalRes = gcnew MOResult();
	finalRes->_NumObjectives = _SolutionInstance->getProblem()->getNumObjectives();

	for( int i=0; i<_ParetoSet->getSize(); i++ )
	{
		IMultiObjectiveSolution ^FinalSol = dynamic_cast<IMultiObjectiveSolution ^> (_ParetoSet->getIndividual(i));
		finalRes->setResult(i, FinalSol);
	}
	outRes->SetFinalResult(finalRes);

	outRes->_ProblemName		= _SolutionInstance->getProblem()->ToString();
	outRes->_Representation = _SolutionInstance->getDesignVar()->ToString();
	outRes->_Result					= finalRes->GetResultStringRepresentation();
	outRes->_TotalIterNum		= getCurrIterNum();
	outRes->_DurationMs			= getCurrTimeSpanInMs();
}

/*******************************************************************************************/
SPEA::SPEA()
{
	_PopSize = 30;
	_MatingPool = gcnew Population();
}
array<String^>^		SPEA::getAlgorithmInstanceDesc()
{
	array<String^> ^ret = gcnew array<String^>(4);

	ret[0] = "Pop.size" + Convert::ToString(getPopulationSize());
	ret[1] = "Pareto set max.size" + Convert::ToString(getParetoSetMaxSize());
	ret[2] = "Crossover op.";
	ret[3] = "Mutation op.";

	return ret;
}
int		SPEA::PerformIteration() 
{ 
	UpdateParetoSet();
	if( _ParetoSet->getSize() > _ParetoSetMaxSize )
		ReduceParetoSet();

//	PerformFitnessAssignment();

	SPEAFitnessAssignment ^fit = gcnew SPEAFitnessAssignment();

	array<double>		^arrFitness = gcnew array<double>(_Population->getSize());
	fit->setParetoSet(_ParetoSet);
	fit->Assign(_Population, _SolutionInstance->getProblem(), arrFitness);

//	GenerateMatingPool(arrFitness);

	CreateNewPopulation(arrFitness);

	return 1;
}
int		SPEA::getParamNum() 
{ 
	return 2; 
}
String^		SPEA::getParamName(int ParamInd) 
{
	if( ParamInd == 0 )
		return "Population size"; 
	else
		return "Pareto set max size";
}
void	SPEA::setParamValue(int ParamInd, double inVal) 
{ 
	if( ParamInd == 0 )
		setPopulationSize((int) inVal); 
	else
		setParetoSetMaxSize((int) inVal);
}
double		SPEA::getParamValue(int ParamInd) 
{ 
	if( ParamInd == 0 )
		return getPopulationSize(); 
	else
		return getParetoSetMaxSize();
}
int		SPEA::getOperatorNum() 
{ 
	return 3; 
}
String^	SPEA::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
//	case 0 : return "IFitnessAssignmentOperatorMO";
	case 0 : return "IFitnessBasedSelectionOperator";
	case 1 : return "ICrossoverOperator2To2";
	case 2 : return "IMutationOp";
	}
	return " ";
}
String^	SPEA::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
//	case 0 : return "Fitness assign.op.";
	case 0 : return "Selection op.";
	case 1 : return "Crossover 2 on 2";
	case 2 : return "Mutation op.";
	}
	return " ";
}
void	SPEA::setESOPOperator(int Index, IESOPOperator ^oper) 
{
	if( Index == 0 )
		_Selector = dynamic_cast<IFitnessBasedSelectionOperator ^> (oper);
	else if( Index == 1 )
		_Recombinator = dynamic_cast<ICrossoverOperator2To2 ^> (oper);
	else if( Index == 2 )
		_Mutator = dynamic_cast<IMutationOp ^> (oper);
}
int		SPEA::ESOP_Initialize() 
{ 
	return Initialize();
}
void	SPEA::ESOP_Run(OptimizationResult ^outRes) 
{ 
	Run(outRes);
	outRes->_AlgorithmDesc	= getAlgorithmInstanceDesc();
	outRes->_AlgorithmName  = getESOPComponentName();
}
void	SPEA::UpdateParetoSet() 
{
	// ubacuje u _ParetoSet sve jedinke iz trenutne populacije koje su nedominirane (u odnosu na Populaciju i ParetoSet)
	// izbacuje iz ParetoSet-a sve jedinke koje time postaju dominirane
	for( int i=0; i<_Population->getSize(); i++ )
	{
		bool		bDominated = false;
		IMultiObjectiveSolution		^CurrInd = dynamic_cast<IMultiObjectiveSolution ^> (_Population->getIndividual(i));

		// provjeri za populaciju
		for( int j=0; j<_Population->getSize(); j++ ) {
			if( i != j ) {
				IMultiObjectiveSolution		^PopInd = dynamic_cast<IMultiObjectiveSolution ^> (_Population->getIndividual(j));
				if( CurrInd->isCoveredBy(PopInd) )
				{
					bDominated = true;
					break;
				}
			}
		}
		// provjeri za ParetoSet
		if( bDominated == false ) {
			for( int j=0; j<_ParetoSet->getSize(); j++ ) {
				IMultiObjectiveSolution		^ParetoInd = dynamic_cast<IMultiObjectiveSolution ^> (_ParetoSet->getIndividual(j));
				
				if( CurrInd->isCoveredBy(ParetoInd) ) {
					bDominated = true;
					break;
				}
			}
		}
		if( bDominated == false ) {
			// ubacujemo je u ParetoSet
			ISolution	^NewInd = CurrInd->Clone();
			_ParetoSet->addNewSolution(NewInd);
		}
	}

	// sada treba vidjeti za ParetoSet da li je neko od rje�enja postalo dominirano
	for( int j=0; j<_ParetoSet->getSize(); j++ ) {
		IMultiObjectiveSolution	^ParetoSol = dynamic_cast<IMultiObjectiveSolution ^> (_ParetoSet->getIndividual(j));

		for( int i=0; i<_Population->getSize(); i++ ) {
			IMultiObjectiveSolution	^PopSol = dynamic_cast<IMultiObjectiveSolution ^> (_Population->getIndividual(i));

			if( ParetoSol->isCoveredBy(PopSol) ) {
				// treba izbaciti jedinku iz ParetoSeta
				_ParetoSet->removeIndividual(j);
				break;
			}
		}
	}
}
void	SPEA::ReduceParetoSet() 
{
}

void	SPEA::GenerateMatingPool(array<double> ^arrFit) 
{
	_MatingPool->Clear();

	int	PopSize			= _Population->getSize();
	int	ParetoSize	= _ParetoSet->getSize();
	int	TotalNumInd	= PopSize + ParetoSize;

	for( int i=0; i<PopSize; i++ )
	{
		IMultiObjectiveSolution	^Ind1, ^Ind2;

		int		Sel1 = rand() % TotalNumInd;
		int		Sel2 = rand() % TotalNumInd;

		if( Sel1 < PopSize )
			Ind1 = dynamic_cast<IMultiObjectiveSolution ^>(_Population->getIndividual(Sel1));
		else
			Ind1 = dynamic_cast<IMultiObjectiveSolution ^>(_ParetoSet->getIndividual(Sel1 - PopSize));

		if( Sel2 < PopSize )
			Ind2 = dynamic_cast<IMultiObjectiveSolution ^>(_Population->getIndividual(Sel2));
		else
			Ind2 = dynamic_cast<IMultiObjectiveSolution ^>(_ParetoSet->getIndividual(Sel2 - PopSize));

		// koji je bolji, ide u mating pool
		if( arrFit[Sel1] < arrFit[Sel2] )
			_MatingPool->addNewSolution(Ind1);
		else
			_MatingPool->addNewSolution(Ind2);
	}
}
void	SPEA::CreateNewPopulation(array<double> ^arrFitness) 
{
	// kreira novu populaciju kri�anjem i mutacijom jedinki iz mating pool-a
	//		dok se ne popuni populacija
	//			odaberi dvije jedinke sa selection op.
	//			kri�aj ih operatorom kri�anja
	//			ako su odabrani za mutaciju mutiraj ih op. mutacije
	Population		^NewPopulation = gcnew Population();

	while(NewPopulation->getSize() < _Population->getSize() )
	{
		array<int>		^vecOutSel = gcnew array<int>(2);
		_Selector->Select(arrFitness, vecOutSel, 2);

		PopulationSubset			^NewPopSubset = gcnew PopulationSubset();
		NewPopSubset->createChildren(_Population->getIndividual(vecOutSel[0]), 2);

		_Recombinator->Recombine(	_Population->getIndividual(vecOutSel[0])->getDesignVar(), 
															_Population->getIndividual(vecOutSel[1])->getDesignVar(), 
															NewPopSubset->getChild(0)->getDesignVar(),
															NewPopSubset->getChild(1)->getDesignVar() );

		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
		_Mutator->Mutate(NewPopSubset->getChild(0)->getDesignVar());
		_Mutator->Mutate(NewPopSubset->getChild(1)->getDesignVar());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			IMultiObjectiveSolution		^Ind = dynamic_cast<IMultiObjectiveSolution ^> (NewPopSubset->getChild(i));
			Ind->UpdateObjectiveValue();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}

	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	// i sada nam nova populacija postaje trenutna populacija
	_Population->Destroy();
	for( int i=0; i<NewPopulation->getSize(); i++ )
		_Population->addNewSolution(NewPopulation->getIndividual(i));

	NewPopulation->Clear();

	Console::WriteLine("Nova populacija");
	for( int i=0; i<_ParetoSet->getSize(); i++ )
	{
		IMultiObjectiveSolution ^sol = dynamic_cast<IMultiObjectiveSolution ^> (_ParetoSet->getIndividual(i));

		for( int j=0; j<sol->getNumObjectives(); j++ )
		{
		}
		String^	str = "f = " + sol->getCurrObjectiveValues()->getValue(0).ToString("F10") + "    g = " + sol->getCurrObjectiveValues()->getValue(1).ToString("F10");
		IDesignVariable ^Parent1 = dynamic_cast<IDesignVariable ^>(sol->getDesignVar());
		Console::WriteLine(str + " " + Parent1->GetValueStringRepresentation());
	}
}
