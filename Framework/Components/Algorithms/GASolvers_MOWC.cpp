#include "StdAfx.h"

#include "GASolvers_MOWC.h"

//#include "Components\Representations\StandardRepresentations.h"
#include "..\..\Components\Operators\RealAsBitArrayOperators.h"
#include "..\..\Components\Operators\SelectionOperators.h"
#include "..\..\Components\Operators\FitnessAssignmentOperators.h"

using namespace System;
using namespace System::Text;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//using namespace ESOP::Framework::StandardRepresentations;

/*******************************************************************************************/

BaseMOWCGA::BaseMOWCGA()
{
	_PopSize = 30;
	_Population = gcnew Population();
	_ParetoSet = gcnew Population();

	_ParetoSetMaxSize = 20;
}
void	BaseMOWCGA::setESOPDesignVariable(IESOPDesignVariable ^DesVar)
{
	if( _SolutionInstance == nullptr )
		_SolutionInstance = gcnew IMultiObjectiveSolutionWC();

	_SolutionInstance->setDesignVar(dynamic_cast<IDesignVariable ^>(DesVar));
}
void	BaseMOWCGA::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	if( _SolutionInstance == nullptr )
		throw 1;
	else
	{
		_SolutionInstance->setMultiObjectiveProblemWC(dynamic_cast<IMultiObjectiveProblemWC ^>(ProbInst));
	}

//	setProblemInstance(dynamic_cast<IMultiObjectiveProblemWC ^>(ProbInst));
}
Population^		BaseMOWCGA::getPopulation() 
{ 
	return _Population; 
}
int		BaseMOWCGA::Initialize()
{
	_ParetoSet->Destroy();
	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISolution		^SolPop = dynamic_cast<ISolution	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}
	return 0;
}
void	BaseMOWCGA::setPopulationSize(int inSize)
{
	_PopSize = inSize;
}
int		BaseMOWCGA::getPopulationSize()
{
	return _PopSize;
}
void	BaseMOWCGA::setParetoSetMaxSize(int inSize)
{
	_ParetoSetMaxSize = inSize;
}
int		BaseMOWCGA::getParetoSetMaxSize()
{
	return _ParetoSetMaxSize;
}
void	BaseMOWCGA::setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign)
{
	_FitnessAssignment = inFitAssign;
}
void	BaseMOWCGA::setSelectionOp(IFitnessBasedSelectionOperator	^inSelector)
{
	_Selector = inSelector;
}
void	BaseMOWCGA::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}
void	BaseMOWCGA::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}
void	BaseMOWCGA::Run(OptimizationResult^ outRes)
{
	IterativeAlgorithm::Run(outRes);
/*
	Console::WriteLine("Zavr�na populacija");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString());
	}
*/
	MOWCResult ^finalRes = gcnew MOWCResult();
	finalRes->_NumObjectives = _SolutionInstance->getProblem()->getNumObjectives();
	finalRes->_NumConstraints = _SolutionInstance->getProblem()->getNumConstraints();

	for( int i=0; i<_ParetoSet->getSize(); i++ )
	{
		IMultiObjectiveSolutionWC ^FinalSol = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(i));
		finalRes->setResult(i, FinalSol);
	}
	outRes->SetFinalResult(finalRes);

	outRes->_ProblemName		= _SolutionInstance->getProblem()->ToString();
	outRes->_Representation = _SolutionInstance->getDesignVar()->ToString();
	outRes->_Result					= finalRes->GetResultStringRepresentation();
	outRes->_TotalIterNum		= getCurrIterNum();
	outRes->_DurationMs			= getCurrTimeSpanInMs();
}

/*******************************************************************************************/
array<String^>^		CHNA::getAlgorithmInstanceDesc()
{
	array<String^> ^ret = gcnew array<String^>(4);

	ret[0] = "Pop.size" + Convert::ToString(getPopulationSize());
	ret[1] = "Pareto set max.size" + Convert::ToString(getParetoSetMaxSize());
	ret[2] = "Crossover op.";
	ret[3] = "Mutation op.";

	return ret;
}
int				CHNA::PerformIteration() 
{ 
	UpdateParetoSet();

	CHNAFitnessAssignment ^fit = gcnew CHNAFitnessAssignment();

	array<double>		^arrFitness = gcnew array<double>(_Population->getSize());
	fit->Assign(_Population, _SolutionInstance->getProblem(), arrFitness);

	CreateNewPopulation(arrFitness);

	return 1;
}
int				CHNA::getParamNum() 
{ 
	return 2; 
}
String^		CHNA::getParamName(int ParamInd) 
{
	if( ParamInd == 0 )
		return "Population size"; 
	else
		return "Pareto set max size";
}
void			CHNA::setParamValue(int ParamInd, double inVal) 
{ 
	if( ParamInd == 0 )
		setPopulationSize((int) inVal); 
	else
		setParetoSetMaxSize((int) inVal);
}
double		CHNA::getParamValue(int ParamInd) 
{ 
	if( ParamInd == 0 )
		return getPopulationSize(); 
	else
		return getParetoSetMaxSize();
}
int				CHNA::getOperatorNum() 
{ 
	return 3; 
}
String^	CHNA::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "IFitnessBasedSelectionOperator";
	case 1 : return "ICrossoverOperator2To2";
	case 2 : return "IMutationOp";
	}
	return " ";
}
String^	CHNA::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "Selection op.";
	case 1 : return "Crossover 2 on 2";
	case 2 : return "Mutation op.";
	}
	return " ";
}
void			CHNA::setESOPOperator(int Index, IESOPOperator ^oper) 
{
	if( Index == 0 )
		_Selector = dynamic_cast<IFitnessBasedSelectionOperator ^> (oper);
	else if( Index == 1 )
		_Recombinator = dynamic_cast<ICrossoverOperator2To2 ^> (oper);
	else if( Index == 2 )
		_Mutator = dynamic_cast<IMutationOp ^> (oper);
}
int		CHNA::ESOP_Initialize() 
{ 
	return Initialize();
}
void		CHNA::ESOP_Run(OptimizationResult ^outRes) 
{ 
	Run(outRes);

	outRes->_AlgorithmDesc	= getAlgorithmInstanceDesc();
	outRes->_AlgorithmName  = getESOPComponentName();
}
void	CHNA::UpdateParetoSet() 
{
	// ubacuje u _ParetoSet sve jedinke iz trenutne populacije koje su nedominirane (u odnosu na Populaciju i ParetoSet)
	// izbacuje iz ParetoSet-a sve jedinke koje time postaju dominirane
	for( int i=0; i<_Population->getSize(); i++ )
	{
		bool		bDominated = false;
		IMultiObjectiveSolutionWC		^CurrInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (_Population->getIndividual(i));

		// provjeri za populaciju
		for( int j=0; j<_Population->getSize(); j++ ) {
			if( i != j ) {
				IMultiObjectiveSolutionWC		^PopInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (_Population->getIndividual(j));
				if( CurrInd->isCoveredBy(PopInd) )
				{
					bDominated = true;
					break;
				}
			}
		}
		// provjeri za ParetoSet
		if( bDominated == false ) {
			for( int j=0; j<_ParetoSet->getSize(); j++ ) {
				IMultiObjectiveSolutionWC		^ParetoInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(j));
				
				if( CurrInd->isCoveredBy(ParetoInd) ) {
					bDominated = true;
					break;
				}
			}
		}
		if( bDominated == false ) {
			// ubacujemo je u ParetoSet
			ISolution	^NewInd = CurrInd->Clone();
			_ParetoSet->addNewSolution(NewInd);
		}
	}

	// sada treba vidjeti za ParetoSet da li je neko od rje�enja postalo dominirano
	for( int j=0; j<_ParetoSet->getSize(); j++ ) {
		IMultiObjectiveSolutionWC	^ParetoSol = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(j));

		for( int i=0; i<_Population->getSize(); i++ ) {
			IMultiObjectiveSolutionWC	^PopSol = dynamic_cast<IMultiObjectiveSolutionWC ^> (_Population->getIndividual(i));

			if( ParetoSol->isCoveredBy(PopSol) ) {
				// treba izbaciti jedinku iz ParetoSeta
				_ParetoSet->removeIndividual(j);
				break;
			}
		}
	}
}
void	CHNA::CreateNewPopulation(array<double> ^arrFitness) 
{
	// kreira novu populaciju kri�anjem i mutacijom jedinki iz mating pool-a
	//		dok se ne popuni populacija
	//			odaberi dvije jedinke sa selection op.
	//			kri�aj ih operatorom kri�anja
	//			ako su odabrani za mutaciju mutiraj ih op. mutacije
	Population		^NewPopulation = gcnew Population();

	while(NewPopulation->getSize() < _Population->getSize() )
	{
		array<int>		^vecOutSel = gcnew array<int>(2);
		_Selector->Select(arrFitness, vecOutSel, 2);

		PopulationSubset			^NewPopSubset = gcnew PopulationSubset();
		NewPopSubset->createChildren(_Population->getIndividual(vecOutSel[0]), 2);

		_Recombinator->Recombine(	_Population->getIndividual(vecOutSel[0])->getDesignVar(), 
															_Population->getIndividual(vecOutSel[1])->getDesignVar(), 
															NewPopSubset->getChild(0)->getDesignVar(),
															NewPopSubset->getChild(1)->getDesignVar() );

		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
		_Mutator->Mutate(NewPopSubset->getChild(0)->getDesignVar());
		_Mutator->Mutate(NewPopSubset->getChild(1)->getDesignVar());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			IMultiObjectiveSolutionWC		^Ind = dynamic_cast<IMultiObjectiveSolutionWC ^> (NewPopSubset->getChild(i));
			Ind->UpdateObjectiveValue();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}

	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	// i sada nam nova populacija postaje trenutna populacija
	_Population->Destroy();
	for( int i=0; i<NewPopulation->getSize(); i++ )
		_Population->addNewSolution(NewPopulation->getIndividual(i));

	NewPopulation->Clear();
}
/*******************************************************************************************/
array<String^>^		CH_I1::getAlgorithmInstanceDesc()
{
	array<String^> ^ret = gcnew array<String^>(4);

	ret[0] = "Pop.size" + Convert::ToString(getPopulationSize());
	ret[1] = "Pareto set max.size" + Convert::ToString(getParetoSetMaxSize());
	ret[2] = "Crossover op.";
	ret[3] = "Mutation op.";

	return ret;
}
int		CH_I1::PerformIteration() 
{ 
	return 1;
}
int		CH_I1::getParamNum() 
{ 
	return 1; 
}
String^		CH_I1::getParamName(int ParamInd) 
{
	return "Population size"; 
}
void	CH_I1::setParamValue(int Ind, double inVal) 
{ 
	setPopulationSize((int) inVal); 
}
double		CH_I1::getParamValue(int Ind) 
{ 
	return getPopulationSize(); 
}
int		CH_I1::getOperatorNum() 
{ 
	return 4; 
}
String^	CH_I1::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "IFitnessAssignmentOperatorMO";
	case 1 : return "ISelectionOperator";
	case 2 : return "ICrossoverOperator2To2";
	case 3 : return "IMutationOp";
	}
	return " ";
}
String^	CH_I1::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "Fitness assign.op.";
	case 1 : return "Selection op.";
	case 2 : return "Crossover 2 on 2";
	case 3 : return "Mutation op.";
	}
	return " ";
}
void	CH_I1::setESOPOperator(int Index, IESOPOperator ^oper) 
{
}
int		CH_I1::ESOP_Initialize() 
{ 
	return Initialize();
}
void CH_I1::ESOP_Run(OptimizationResult ^outRes) 
{ 
	Run(outRes);
}
