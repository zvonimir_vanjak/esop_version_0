#pragma once

#include "GASolvers.h"

using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				class CH_Base : public GeneralMOGAWC
				{
				public:
					CH_Base(void);
					~CH_Base(void);

					virtual	void	PerformFitnessAssignment() = 0;

					int		PerformIteration();
					int		Run();

				private:
					void	ReduceParetoSet();

					void	GenerateMatingPool();
					void	CreateNewPopulation();

				private:
					int			_IterNum;
					int			_CurrIterNum;

					Population		*_MatingPool;
				};

				class CHNAAlgorithm : public CH_Base
				{
				public:
					CHNAAlgorithm(void);
					~CHNAAlgorithm(void);

					CString getComponentName() { return "StandardAlgorithms.dll"; }
					CString	getAlgorithmName() { return "CHNA"; }
					
					int			getParamNum()		{ return 2; }
					CString	getParamName(int ParamInd) { return ""; }

					void		setParamValue(float inVal) {}
					float		getParamValue(float inVal) { return 0.0; }

					void	PerformFitnessAssignment();
				};

				class CH_I1Algorithm : public CH_Base
				{
				public:
					CH_I1Algorithm(void);
					~CH_I1Algorithm(void);

					CString getComponentName() { return "StandardAlgorithms.dll"; }
					CString	getAlgorithmName() { return "CH_I1"; }

					int			getParamNum()		{ return 2; }
					CString	getParamName(int ParamInd) { return ""; }

					void		setParamValue(float inVal) {}
					float		getParamValue(float inVal) { return 0.0; }

					void	PerformFitnessAssignment();
				};

				class CH_I2Algorithm : public CH_Base
				{
				public:
					CH_I2Algorithm(void);
					~CH_I2Algorithm(void);

					CString getComponentName() { return "StandardAlgorithms.dll"; }
					CString	getAlgorithmName() { return "SPEA"; }

					int			getParamNum()		{ return 2; }
					CString	getParamName(int ParamInd) { return ""; }

					void		setParamValue(float inVal) {}
					float		getParamValue(float inVal) { return 0.0; }

					void	PerformFitnessAssignment();
				};

				class CH_I3Algorithm : public CH_Base
				{
				public:
					CH_I3Algorithm(void);
					~CH_I3Algorithm(void);

					CString getComponentName() { return "StandardAlgorithms.dll"; }
					CString	getAlgorithmName() { return "SPEA"; }

					int			getParamNum()		{ return 2; }
					CString	getParamName(int ParamInd) { return ""; }

					void		setParamValue(float inVal) {}
					float		getParamValue(float inVal) { return 0.0; }

					void	PerformFitnessAssignment();
				};

				class CH_I4Algorithm : public CH_Base
				{
				public:
					CH_I4Algorithm(void);
					~CH_I4Algorithm(void);

					CString getComponentName() { return "StandardAlgorithms.dll"; }
					CString	getAlgorithmName() { return "SPEA"; }

					int			getParamNum()		{ return 2; }
					CString	getParamName(int ParamInd) { return ""; }

					void		setParamValue(float inVal) {}
					float		getParamValue(float inVal) { return 0.0; }

					void	PerformFitnessAssignment();
				};
			}
		}
	}
}
