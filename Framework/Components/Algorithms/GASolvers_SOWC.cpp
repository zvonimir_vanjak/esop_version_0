#include "StdAfx.h"

#include "GASolvers_SOWC.h"

//#include "Components\Representations\StandardRepresentations.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//using namespace ESOP::Framework::StandardRepresentations;

/*******************************************************************************************/

BaseSOWCGA::BaseSOWCGA()
{
	_PopSize = 30;
	_Population = gcnew Population();
}

void	BaseSOWCGA::setESOPDesignVariable(IESOPDesignVariable ^DesVar)
{
	if( _SolutionInstance == nullptr )
		_SolutionInstance = gcnew ISingleObjectiveSolutionWC();

	_SolutionInstance->setDesignVar(dynamic_cast<IDesignVariable ^>(DesVar));
}
void	BaseSOWCGA::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	if( _SolutionInstance == nullptr )
		throw 1;
	else
	{
		_SolutionInstance->setSingleObjectiveProblemWC(dynamic_cast<ISingleObjectiveProblemWC ^>(ProbInst));
	}


//	setProblemInstance(dynamic_cast<ISingleObjectiveProblemWC ^>(ProbInst));
}

Population^		BaseSOWCGA::getPopulation() 
{ 
	return _Population; 
}

int		BaseSOWCGA::Initialize()
{
/*
	Console::WriteLine("Inicijalna populacija 1");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();

		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		vector<bool> *Bit1 = Parent1->getRepr()->getRepr();
		String ^s1 = gcnew String("");
		for( int i=0; i<10; i++ )
		{
			if( (*Bit1)[i] == false )
				s1 += "0";
			else
				s1 += "1";
		}
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString() + " " + s1);
	}
*/
	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISolution		^SolPop = dynamic_cast<ISolution	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}

	return 0;
}

void	BaseSOWCGA::setPopulationSize(int inSize)
{
	_PopSize = inSize;
	_Population->Initialize(inSize, _SolutionInstance);
}
int		BaseSOWCGA::getPopulationSize()
{
	return _Population->getSize();
}

void	BaseSOWCGA::setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign)
{
	_FitnessAssignment = inFitAssign;
}

void	BaseSOWCGA::setSelectionOp(IFitnessBasedSelectionOperator	^inSelector)
{
	_Selector = inSelector;
}
void	BaseSOWCGA::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}
void	BaseSOWCGA::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}
void	BaseSOWCGA::Run(OptimizationResult^ outRes)
{
	IterativeAlgorithm::Run(outRes);
/*
	Console::WriteLine("Zavr�na populacija");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString());
	}
*/
	// treba na�i najbolje rje�enje
	// ajde sad jo� na�i najbolju jedinku i ispi�i njenu vrijednost
	int			ind = 0;
	double	max = -1000000;
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISingleObjectiveSolutionWC ^sol = dynamic_cast<ISingleObjectiveSolutionWC ^> (_Population->getIndividual(i));

		if( sol->getCurrObjectiveValue() > max)
		{
			max = sol->getCurrObjectiveValue();
			ind = i;
		}
	}

//	SOWCResult ^finalRes = gcnew SOWCResult();

//	finalRes->setResult(dynamic_cast<ISingleObjectiveSolutionWC ^> (_Population->getIndividual(ind)));
//	outRes->SetFinalResult(finalRes);
}

double		BaseSOWCGA::getCurrObjectiveValue() 
{ 
	// treba na�i najbolje rje�enje
	int			ind = 0;
	if( _SolutionInstance->getProblem()->getObjectiveType() == EObjectiveType::MIN_OBJECTIVE )
	{
		double	min = 1000000;
		for( int i=0; i<_Population->getSize(); i++ )
		{
			ISingleObjectiveSolutionWC ^sol = dynamic_cast<ISingleObjectiveSolutionWC ^> (_Population->getIndividual(i));

			if( sol->getCurrObjectiveValue() < min)
			{
				min = sol->getCurrObjectiveValue();
				ind = i;
			}
		}
		return min;
	}
	else
	{
		double	max = -1000000;
		for( int i=0; i<_Population->getSize(); i++ )
		{
			ISingleObjectiveSolutionWC ^sol = dynamic_cast<ISingleObjectiveSolutionWC ^> (_Population->getIndividual(i));

			if( sol->getCurrObjectiveValue() > max)
			{
				max = sol->getCurrObjectiveValue();
				ind = i;
			}
		}
		return max;
	}
}

