#include "StdAfx.h"

#include "GASolvers_SO.h"

//#include "Components\Representations\StandardRepresentations.h"
#include "..\..\Components\Operators\RealAsBitArrayOperators.h"
#include "..\..\Components\Operators\SelectionOperators.h"
#include "..\..\Components\Operators\FitnessAssignmentOperators.h"

using namespace System;
using namespace System::Text;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//using namespace ESOP::Framework::StandardRepresentations;

/*******************************************************************************************/

BaseGA::BaseGA()
{
	_Population = gcnew Population();
	_PopSize = 10;
}

void	BaseGA::setESOPDesignVariable(IESOPDesignVariable ^DesVar)
{
	if( _SolutionInstance == nullptr )
		_SolutionInstance = gcnew ISingleObjectiveSolution();

	_SolutionInstance->setDesignVar(dynamic_cast<IDesignVariable ^>(DesVar));
}
void	BaseGA::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	if( _SolutionInstance == nullptr )
		throw 1;
	else
	{
		_SolutionInstance->setSingleObjectiveProblem(dynamic_cast<ISingleObjectiveProblem ^>(ProbInst));
	}

//	setProblemInstance(dynamic_cast<ISingleObjectiveProblem ^>(ProbInst));
}

Population^		BaseGA::getPopulation() 
{ 
	return _Population; 
}

int		BaseGA::Initialize()
{
/*
	Console::WriteLine("Inicijalna populacija 1");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();

		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		vector<bool> *Bit1 = Parent1->getRepr()->getRepr();
		String ^s1 = gcnew String("");
		for( int i=0; i<10; i++ )
		{
			if( (*Bit1)[i] == false )
				s1 += "0";
			else
				s1 += "1";
		}
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString() + " " + s1);
	}
*/
	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISolution		^SolPop = dynamic_cast<ISolution	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}

	return 0;
}

void	BaseGA::setPopulationSize(int inSize)
{
	_PopSize = inSize;
//	_Population->Initialize(inSize, _SolutionInstance);
}
int		BaseGA::getPopulationSize()
{
	return _PopSize;
}

void	BaseGA::setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign)
{
	_FitnessAssignment = inFitAssign;
}

void	BaseGA::setSelectionOp(IFitnessBasedSelectionOperator	^inSelector)
{
	_Selector = inSelector;
}
void	BaseGA::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}
void	BaseGA::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}
void	BaseGA::Run(OptimizationResult^ outRes)
{
	IterativeAlgorithm::Run(outRes);
/*
	Console::WriteLine("Zavr�na populacija");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString());
	}
*/
	// treba na�i najbolje rje�enje
	// ajde sad jo� na�i najbolju jedinku i ispi�i njenu vrijednost
	int			ind = 0;
	double	max = -1000000;
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

		if( sol->getCurrObjectiveValue() > max)
		{
			max = sol->getCurrObjectiveValue();
			ind = i;
		}
	}

	ISingleObjectiveSolution ^FinalSol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(ind));
	SOResult ^finalRes = gcnew SOResult();

	finalRes->setResult(FinalSol);
	outRes->SetFinalResult(finalRes);

	outRes->_ProblemName		= _SolutionInstance->getProblem()->ToString();
	outRes->_Representation = FinalSol->getDesignVar()->ToString();
	outRes->_Result					= finalRes->GetResultStringRepresentation();
	outRes->_TotalIterNum		= getCurrIterNum();
	outRes->_DurationMs			= getCurrTimeSpanInMs();
}

double		BaseGA::getCurrObjectiveValue() 
{ 
	// treba na�i najbolje rje�enje
	int			ind = 0;
	if( _SolutionInstance->getProblem()->getObjectiveType() == EObjectiveType::MIN_OBJECTIVE )
	{
		double	min = 1000000;
		for( int i=0; i<_Population->getSize(); i++ )
		{
			ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

			if( sol->getCurrObjectiveValue() < min)
			{
				min = sol->getCurrObjectiveValue();
				ind = i;
			}
		}
		return min;
	}
	else
	{
		double	max = -1000000;
		for( int i=0; i<_Population->getSize(); i++ )
		{
			ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

			if( sol->getCurrObjectiveValue() > max)
			{
				max = sol->getCurrObjectiveValue();
				ind = i;
			}
		}
		return max;
	}
}
double		BaseGA::getCurrAverageObjectiveValue()
{
	// treba na�i najbolje rje�enje
	double	Sum = 0;
	int			Size = _Population->getSize();
	for( int i=0; i<Size; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

		Sum += sol->getCurrObjectiveValue();
	}
	return Sum / Size;
}

double		BaseGA::getCurrBestFitnessValue()
{
	double Best = -10e6;
	for( int i=0; i<_arrFitness->Length; i++ )
	{
		if( _arrFitness[i] > Best )
			Best = _arrFitness[i];
	}
	return Best;
}
double		BaseGA::getCurrAverageFitnessValue()
{
	double Sum = 0;
	for( int i=0; i<_arrFitness->Length; i++ )
	{
		Sum += _arrFitness[i];
	}

	return Sum / _arrFitness->Length;
}
double		BaseGA::getCurrWorstFitnessValue()
{
	double Worst = 10e6;
	for( int i=0; i<_arrFitness->Length; i++ )
	{
		if( _arrFitness[i] < Worst )
			Worst = _arrFitness[i];
	}
	return Worst;
}
int				BaseGA::getValue1()
{
	return 1;
}
int				BaseGA::getValue2()
{
	return 10;
}
double		BaseGA::getValue3()
{
	return 100;
}

/*******************************************************************************************/
array<String^>^		GenerationalGA::getAlgorithmInstanceDesc()
{
	array<String^> ^ret = gcnew array<String^>(3);

	ret[0] = "Pop.size" + Convert::ToString(getPopulationSize());
	ret[1] = "Crossover op.";
	ret[2] = "Mutation op.";

	return ret;
}
int			GenerationalGA::getOperatorNum() 
{ 
	return 4; 
}
String^	GenerationalGA::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "IFitnessAssignmentOperator";
	case 1 : return "IFitnessBasedSelectionOperator";
	case 2 : return "ICrossoverOperator2To2";
	case 3 : return "IMutationOp";
	}
	return " ";
}
String^	GenerationalGA::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "Fitness assign.op.";
	case 1 : return "Selection op.";
	case 2 : return "Crossover 2 on 2";
	case 3 : return "Mutation op.";
	}
	return " ";
}
void		GenerationalGA::setESOPOperator(int Index, IESOPOperator ^oper)
{
	if( Index == 0 )
		_FitnessAssignment = dynamic_cast<IFitnessAssignmentOperator ^> (oper);
	else if( Index == 1 )
		_Selector = dynamic_cast<IFitnessBasedSelectionOperator ^> (oper);
	else if( Index == 2 )
		_Recombinator = dynamic_cast<ICrossoverOperator2To2 ^> (oper);
	else if( Index == 3 )
		_Mutator = dynamic_cast<IMutationOp ^> (oper);
}

int			GenerationalGA::PerformIteration()
{
	// kreira novu populaciju kri�anjem i mutacijom jedinki iz mating pool-a
	//		dok se ne popuni populacija
	//			odaberi dvije jedinke sa selection op.
	//			kri�aj ih operatorom kri�anja
	//			ako su odabrani za mutaciju mutiraj ih op. mutacije
	Population		^NewPopulation = gcnew Population();

	int Size = _Population->getSize();

	Console::WriteLine("Populacija na po�etku iteracije");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
//		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
//		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + sol->getDesignVar()->GetValueStringRepresentation());
	}

	_arrFitness = gcnew array<double>(Size);
	_FitnessAssignment->Assign(_Population, _SolutionInstance->getProblem(), _arrFitness);

	while(NewPopulation->getSize() < _Population->getSize() )
	{
//		PopulationSelection		^SelInd = gcnew PopulationSelection();

		array<int>		^vecOutSel = gcnew array<int>(2);
		_Selector->Select(_arrFitness, vecOutSel, 2);

		PopulationSubset			^NewPopSubset = gcnew PopulationSubset();
		NewPopSubset->createChildren(_Population->getIndividual(vecOutSel[0]), 2);

		_Recombinator->Recombine(	_Population->getIndividual(vecOutSel[0])->getDesignVar(), 
															_Population->getIndividual(vecOutSel[1])->getDesignVar(), 
															NewPopSubset->getChild(0)->getDesignVar(),
															NewPopSubset->getChild(1)->getDesignVar() );
/*
		ISingleObjectiveSolution ^sol1 = dynamic_cast<ISingleObjectiveSolution ^> (SelInd->getSelIndividual(0));
		ISingleObjectiveSolution ^sol2 = dynamic_cast<ISingleObjectiveSolution ^> (SelInd->getSelIndividual(1));

		ISingleObjectiveSolution ^sol3 = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(0));
		ISingleObjectiveSolution ^sol4 = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(1));
*/
		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
//		_Mutator->Mutate(NewPopSubset->getChild(0)->getDesignVar());
//		_Mutator->Mutate(NewPopSubset->getChild(1)->getDesignVar());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			ISingleObjectiveSolution		^Ind = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(i));
			Ind->UpdateObjectiveValue();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}
/*
	Console::WriteLine("Kreirana populacija");
	for( int i=0; i<NewPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (NewPopulation->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString());
	}
*/
	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	if( NewPopulation->getSize() > _Population->getSize() )
		NewPopulation->pruneExcessIndividuals(_Population->getSize());

	// i sada nam nova populacija postaje trenutna populacija
	_Population->Destroy();
	for( int i=0; i<NewPopulation->getSize(); i++ )
		_Population->addNewSolution(NewPopulation->getIndividual(i));

	int s = _Population->getSize();
	int g = NewPopulation->getSize();

	NewPopulation->Clear();
/*
	Console::WriteLine("Nova populacija");
	for( int i=0; i<NewPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

		String^	str = "Func.val.= " + sol->getCurrObjectiveValue().ToString("F10");
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		Console::WriteLine(str + " " + Parent1->GetValueStringRepresentation());
	}
	*/

	return 0;
}

int			GenerationalGA::ESOP_Initialize()
{
	return Initialize();
}
void GenerationalGA::ESOP_Run(OptimizationResult ^outRes)
{
	Run(outRes);

	outRes->_AlgorithmDesc	= getAlgorithmInstanceDesc();
	outRes->_AlgorithmName  = getESOPComponentName();
}

/*******************************************************************************************/
array<String^>^		SteadyStateGA::getAlgorithmInstanceDesc()
{
	array<String^> ^ret = gcnew array<String^>(3);

	ret[0] = "Pop.size" + Convert::ToString(getPopulationSize());
	ret[1] = "Crossover op.";
	ret[2] = "Mutation op.";

	return ret;
}
int			SteadyStateGA::getOperatorNum() 
{ 
	return 4; 
}
String^	SteadyStateGA::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "IFitnessAssignmentOperator";
	case 1 : return "IFitnessBasedSelectionOperator";
	case 2 : return "ICrossoverOperator2To2";
	case 3 : return "IMutationOp";
	}
	return " ";
}
String^	SteadyStateGA::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "Fitness assign.op.";
	case 1 : return "Selection op.";
	case 2 : return "Crossover 2 on 2";
	case 3 : return "Mutation op.";
	}
	return " ";
}
void		SteadyStateGA::setESOPOperator(int Index, IESOPOperator ^oper)
{
	if( Index == 0 )
		_FitnessAssignment = dynamic_cast<IFitnessAssignmentOperator ^> (oper);
	else if( Index == 1 )
		_Selector = dynamic_cast<IFitnessBasedSelectionOperator ^> (oper);
	else if( Index == 2 )
		_Recombinator = dynamic_cast<ICrossoverOperator2To2 ^> (oper);
	else if( Index == 3 )
		_Mutator = dynamic_cast<IMutationOp ^> (oper);
}

int			SteadyStateGA::PerformIteration()
{
	int a = 2;
	int b = 3;
	int c;

	c = a + b;
	return 0;
}

int			SteadyStateGA::ESOP_Initialize()
{
	return Initialize();
}
void SteadyStateGA::ESOP_Run(OptimizationResult ^outRes)
{
	Run(outRes);
	outRes->_AlgorithmDesc	= getAlgorithmInstanceDesc();
	outRes->_AlgorithmName  = getESOPComponentName();
}


/*******************************************************************************************/
array<String^>^		CompleteFuncOptimizatorGA::getAlgorithmInstanceDesc()
{
	array<String^> ^ret = gcnew array<String^>(5);

	ret[0] = "Pop.size" + Convert::ToString(getPopulationSize());
	ret[1] = "Fitness assign. op.- ScaledFitnessAssignment";
	ret[2] = "Selection op.- RouleteWheelSelectionFitness";
	ret[3] = "Crossover op.-RealAsBitArray_FlopBitMutation";
	ret[4] = "Mutation  op.- RealAsBitArray_FlopBitMutation";

	return ret;
}
int			CompleteFuncOptimizatorGA::PerformIteration()
{
	Population		^NewPopulation = gcnew Population();

	int Size = _Population->getSize();

	Console::WriteLine("Populacija na po�etku iteracije");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + sol->getDesignVar()->GetValueStringRepresentation());
	}

	array<double>		^arrFitness = gcnew array<double>(Size);
	_FitnessAssignment->Assign(_Population, _SolutionInstance->getProblem(), arrFitness);

	while(NewPopulation->getSize() < _Population->getSize() )
	{
		array<int>		^vecOutSel = gcnew array<int>(2);
		_Selector->Select(arrFitness, vecOutSel, 2);

		PopulationSubset			^NewPopSubset = gcnew PopulationSubset();
		NewPopSubset->createChildren(_Population->getIndividual(vecOutSel[0]), 2);

		_Recombinator->Recombine(	_Population->getIndividual(vecOutSel[0])->getDesignVar(), 
															_Population->getIndividual(vecOutSel[1])->getDesignVar(), 
															NewPopSubset->getChild(0)->getDesignVar(),
															NewPopSubset->getChild(1)->getDesignVar() );

		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
		_Mutator->Mutate(NewPopSubset->getChild(0)->getDesignVar());
		_Mutator->Mutate(NewPopSubset->getChild(1)->getDesignVar());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			ISingleObjectiveSolution		^Ind = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(i));
			Ind->UpdateObjectiveValue();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}

	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	if( NewPopulation->getSize() > _Population->getSize() )
		NewPopulation->pruneExcessIndividuals(_Population->getSize());

	// i sada nam nova populacija postaje trenutna populacija
	_Population->Destroy();
	for( int i=0; i<NewPopulation->getSize(); i++ )
		_Population->addNewSolution(NewPopulation->getIndividual(i));

	NewPopulation->Clear();

	return 0;
}

int			CompleteFuncOptimizatorGA::ESOP_Initialize()
{
	_FitnessAssignment = gcnew ESOP::Framework::Operators::GeneticAlgorithms::ScaledFitnessAssignment();
	_Selector					 = gcnew ESOP::Framework::Operators::GeneticAlgorithms::Selection::RouleteWheelSelectionFitness();
	_Recombinator			 = gcnew ESOP::Framework::Operators::GeneticAlgorithms::Crossover::RealAsBitArray_1PointCrossover();
	_Mutator					 = gcnew ESOP::Framework::Operators::GeneticAlgorithms::Mutation::RealAsBitArray_FlopBitMutation();

	_PopSize = 10;

	return Initialize();
}
void CompleteFuncOptimizatorGA::ESOP_Run(OptimizationResult ^outRes)
{
	Run(outRes);
	outRes->_AlgorithmDesc	= getAlgorithmInstanceDesc();
	outRes->_AlgorithmName  = getESOPComponentName();
}

