#pragma once

#include "GAHelpers.h"
//#include "Base\BaseOperatorInterfaces.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				/*
					- implementira zajedničke dijelov eza standardni genetski algoritma
				*/
				public ref class BaseGA abstract : public SingleObjectiveIterativeAlgorithm
				{
				public:
					BaseGA();

					virtual	String^	getComponentName() 		{ return "Prdo"; }
					virtual	String^	getAlgorithmName() 		{ return "Prdo"; }

					virtual	int		Initialize() override;
					virtual	int		Run() override;

					void	setPopulationSize(int inSize);
					int		getPopulationSize();

					virtual void		setESOPDesignVariable(IESOPDesignVariable ^DesVar)
					{
						_SolutionInstance->setCurrSol(dynamic_cast<IDesignVariable ^>(DesVar));
					}
					virtual void		setESOPProblemInstance(IESOPProblem ^ProbInst)
					{
						setProblemInstance(dynamic_cast<ISingleObjectiveProblem ^>(ProbInst));
					}

					void	setSelectionOp(ISelectionOperator	^inSelector);
					void	setRecombinationOp(ICrossoverOperator2To2	^inRecombinator);
					void	setMutationOp(IMutationOp	^inMutator);

					Population^		getPopulation() { return _Population; }

				protected:
					int						_PopSize;
					Population		^_Population;

					ISelectionOperator				^_Selector;
					ICrossoverOperator2To2		^_Recombinator;
					IMutationOp								^_Mutator;

//					ISingleObjectiveProblem		^_Problem;
//					ISingleObjectiveSolution	^_SolObject;
				};
				/*
					- operatorom selekcije se izabire MatingPool od _PopSize jedinki
					- zatim se nad jedinkama iz MatingPool-a krizanjem i mutacijom stvara novih _PopSize jedinki
					- novokreirane jedinke postaju nova populacija
				 */
				public ref class GenerationalGA : public BaseGA, public ITemplateAlgorithm
				{
				public:
					virtual	String^	getComponentName() override		{ return "Prdo"; }
					virtual	String^	getAlgorithmName() override		{ return "Prdo"; }

					virtual int				getParamNum() { return 1; }
					virtual String^		getParamName(int ParamInd) {return "Population size"; }
					
					virtual void			setParamValue(int Ind, double inVal) { setPopulationSize((int) inVal); }
					virtual double		getParamValue(int Ind) { return getPopulationSize(); }

					virtual int				getOperatorNum() { return 3; }
					virtual String^		getOperatorType(int ParamInd)
					{
						switch(ParamInd)
						{
						case 0 : return "ISelectionOperator";
						case 1 : return "ICrossoverOperator2To2";
						case 2 : return "IMutationOp";
						}
						return " ";
					}
					virtual String^		getOperatorID(int ParamInd)
					{
						switch(ParamInd)
						{
						case 0 : return "Selection op.";
						case 1 : return "Crossover 2 on 2";
						case 2 : return "Mutation op.";
						}
						return " ";
					}
					virtual void	setESOPOperator(int Index, IESOPOperator ^oper)
					{
						if( Index == 0 )
							_Selector = dynamic_cast<ISelectionOperator ^> (oper);
						else if( Index == 1 )
							_Recombinator = dynamic_cast<ICrossoverOperator2To2 ^> (oper);
						else if( Index == 2 )
							_Mutator = dynamic_cast<IMutationOp ^> (oper);
					}

					virtual	int		ESOP_Initialize()
					{
						return Initialize();
					}
					virtual	int		ESOP_Run()
					{
						return Run();
					}

					virtual	int		PerformIteration() override;
				};

				public ref class SteadyStateGA : public BaseGA, public ITemplateAlgorithm
				{
				public:
					virtual	String^	getComponentName() 	override	{ return "Prdo"; }
					virtual	String^	getAlgorithmName() 	override	{ return "Prdo"; }

					virtual int				getParamNum() { return 1; }
					virtual String^		getParamName(int ParamInd) {return "Population size"; }
					
					virtual void			setParamValue(int Ind, double inVal) { setPopulationSize((int) inVal); }
					virtual double			getParamValue(int Ind) { return getPopulationSize(); }

					virtual int				getOperatorNum() { return 3; }
					virtual String^		getOperatorType(int ParamInd)
					{
						switch(ParamInd)
						{
						case 0 : return "ISelectionOperator";
						case 1 : return "ICrossoverOperator2To2";
						case 2 : return "IMutationOp";
						}
						return " ";
					}
					virtual String^		getOperatorID(int ParamInd)
					{
						switch(ParamInd)
						{
						case 0 : return "Selection op.";
						case 1 : return "Crossover 2 on 2";
						case 2 : return "Mutation op.";
						}
						return " ";
					}
					virtual void	setESOPOperator(int Index, IESOPOperator ^oper)
					{
					}

					virtual	int		ESOP_Initialize()
					{
						return Initialize();
					}
					virtual	int		ESOP_Run()
					{
						return Run();
					}

					virtual	int		PerformIteration() override;
				};

				public ref class GeneralMOGAWC : public MultiObjectiveIterativeAlgorithmWC, public ITemplateAlgorithm
				{
				public:
					GeneralMOGAWC();
					~GeneralMOGAWC();

					virtual	String^	getComponentName() 		{ return "Prdo"; }
					virtual	String^	getAlgorithmName() 		{ return "Prdo"; }

					virtual int				getParamNum()  { return 1; }
					virtual String^		getParamName(int ParamInd) { return " "; }
					
					virtual void			setParamValue(int Ind, double inVal) {  }
					virtual double			getParamValue(int Ind) { return 0.0;  }

					virtual int				getOperatorNum() { return 3; }
					virtual String^		getOperatorType(int ParamInd)
					{
						switch(ParamInd)
						{
						case 0 : return "ISelectionOperator";
						case 1 : return "ICrossoverOperator2To2";
						case 2 : return "IMutationOp";
						}
						return " ";
					}
					virtual String^		getOperatorID(int ParamInd)
					{
						switch(ParamInd)
						{
						case 0 : return "Selection op.";
						case 1 : return "Crossover 2 on 2";
						case 2 : return "Mutation op.";
						}
						return " ";
					}
					virtual void	setESOPOperator(int Index, IESOPOperator ^oper)
					{
					}

					virtual	int		ESOP_Initialize()
					{
						return Initialize();
					}
					virtual	int		ESOP_Run()
					{
						return Run();
					}

					virtual	int		Initialize() override;
					virtual	int		PerformIteration() override;
					virtual	int		Run() override { return 1;}

					void	setIterationNum(int inIterNum);
					int		getIterationNum();

					void	setPopulationSize(int inSize);
					int		getPopulationSize();

					virtual void		setESOPDesignVariable(IESOPDesignVariable ^SolInst)
					{
						_SolObject = dynamic_cast<IMultiObjectiveSolutionWC ^>(SolInst);
					}
					virtual void		setESOPProblemInstance(IESOPProblem ^ProbInst)
					{
						_Problem = dynamic_cast<IMultiObjectiveProblemWC ^>(ProbInst);
					}

					void	setSelectionOp(ISelectionOperator	^inSelector);
					void	setRecombinationOp(ICrossoverOperator2To2	^inRecombinator);
					void	setMutationOp(IMutationOp	^inMutator);

					Population^		getPopulation() { return _Population; }
					Population^		getParetoSet()	{ return _ParetoSet; }

				protected:
					int		_IterNum;

					int						_PopSize;
					Population		^_Population;

					int						_ParetoSetMaxSize;
					Population		^_ParetoSet;
					
					ISelectionOperator				^_Selector;
					ICrossoverOperator2To2		^_Recombinator;
					IMutationOp								^_Mutator;

					IMultiObjectiveProblemWC		^_Problem;
					IMultiObjectiveSolutionWC		^_SolObject;
				};
			}
		}
	}
}
