#include "StdAfx.h"
#include ".\chalgorithms.h"
#include "MOHelpers.h"

//#include "..\StandardProblems\RealFunction_SolutionObjects.h"


using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//using namespace ESOP::Framework::StandardProblems::RealFunctionOptimization;

/**************************************************************************************************/
CH_Base::CH_Base(void)
{
	_MatingPool = new Population();;
}
CH_Base::~CH_Base(void)
{
	delete _MatingPool;
}
int		CH_Base::Run()
{
	Initialize();

	for(_CurrIterNum=0; _CurrIterNum<_IterNum; _CurrIterNum++ )
	{
		PerformIteration();
	}

	return 0;
}

int		CH_Base::PerformIteration()
{
	ParetoSetOperations::UpdateParetoSet(_ParetoSet, _Population);

	if( _ParetoSet->getSize() > _ParetoSetMaxSize )
		ParetoSetOperations::ReduceParetoSet(&_ParetoSet, _ParetoSetMaxSize);

	PerformFitnessAssignment();

	GenerateMatingPool();

	CreateNewPopulation();

	PerformFitnessAssignment();

	return 0;
}


void	CH_Base::GenerateMatingPool()
{
	// generira novu Populaciju jedinki koje �e se "pariti"
	//		odr�avaju se binary trounaments me�u jedinkama iz Populacije i ParetoSet-a sve dok se ne popuni mating pool
	//		vjerojatnost selekcije je povezana sa fitnessom jedinke
	_MatingPool->Clear();

	int	PopSize			= _Population->getSize();
	int	ParetoSize	= _ParetoSet->getSize();
	int	TotalNumInd	= PopSize + ParetoSize;

	for( int i=0; i<PopSize; i++ )
	{
		IFitnessProvider	*Ind1, *Ind2;

		int		Sel1 = rand() % TotalNumInd;
		int		Sel2 = rand() % TotalNumInd;

		if( Sel1 < PopSize )
			Ind1 = dynamic_cast<IFitnessProvider *>(_Population->getIndividual(Sel1));
		else
			Ind1 = dynamic_cast<IFitnessProvider *>(_ParetoSet->getIndividual(Sel1 - PopSize));

		if( Sel2 < PopSize )
			Ind2 = dynamic_cast<IFitnessProvider *>(_Population->getIndividual(Sel2));
		else
			Ind2 = dynamic_cast<IFitnessProvider *>(_ParetoSet->getIndividual(Sel2 - PopSize));

		// koji je bolji, ide u mating pool
		if( Ind1->getFitness() == Ind2->getFitness() )
		{
			// ako su jednaki treba vidjeti ko nad kime dominira
			IMultiObjectiveSolutionWC *Sol1 = dynamic_cast<IMultiObjectiveSolutionWC *> (Ind1);
			IMultiObjectiveSolutionWC *Sol2 = dynamic_cast<IMultiObjectiveSolutionWC *> (Ind2);

			int		Sel = rand() % 2;
			if( Sol1->isDominatedBy(Sol2) )
				Sel = 1;
			else if( Sol2->isDominatedBy(Sol1) )
				Sel = 0;

			if( Sel == 0 )		_MatingPool->addNewSolution(Sol1->Clone());
			else							_MatingPool->addNewSolution(Sol2->Clone());
		}
		else
		{
			IMultiObjectiveSolutionWC *Sol1 = dynamic_cast<IMultiObjectiveSolutionWC *> (Ind1);
			IMultiObjectiveSolutionWC *Sol2 = dynamic_cast<IMultiObjectiveSolutionWC *> (Ind2);

			if( Ind1->getFitness() < Ind2->getFitness() )
				_MatingPool->addNewSolution(Sol1->Clone());
			else
				_MatingPool->addNewSolution(Sol2->Clone());
		}
	}
}
void	CH_Base::CreateNewPopulation()
{
	// kreira novu populaciju kri�anjem i mutacijom jedinki iz mating pool-a
	//		dok se ne popuni populacija
	//			odaberi dvije jedinke sa selection op.
	//			kri�aj ih operatorom kri�anja
	//			ako su odabrani za mutaciju mutiraj ih op. mutacije
	Population		*NewPopulation = new Population();

	// da vidimo sadr�aj mating poola
	for( int k=0; k<_MatingPool->getSize(); k++ )
	{
		CString		kr;
		(dynamic_cast<IMultiObjectiveSolutionWC*> (_MatingPool->getIndividual(k)))->getStringRepr(kr);

		int a, b=0;
		a = b+1;
	}

	while(NewPopulation->getSize() < _MatingPool->getSize() )
	{
		PopulationSelection		*SelInd = new PopulationSelection();

		_Selector->Select(_MatingPool, SelInd, 2);

		// da vidimo �to smo selektirali
		CString		p1;		(dynamic_cast<IMultiObjectiveSolutionWC*> (SelInd->getSelIndividual(0)))->getStringRepr(p1);
		CString		p2;		(dynamic_cast<IMultiObjectiveSolutionWC*> (SelInd->getSelIndividual(1)))->getStringRepr(p2);

		PopulationSubset			*NewPopSubset = new PopulationSubset();
		NewPopSubset->createChildren(SelInd->getSelIndividual(0), 2);

		_Recombinator->Recombine(	SelInd->getSelIndividual(0)->GetRepresentation(), 
															SelInd->getSelIndividual(1)->GetRepresentation(), 
															NewPopSubset->getChild(0)->GetRepresentation(),
															NewPopSubset->getChild(1)->GetRepresentation() );

		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
		_Mutator->Mutate(NewPopSubset->getChild(0)->GetRepresentation());
		_Mutator->Mutate(NewPopSubset->getChild(1)->GetRepresentation());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			IMultiObjectiveSolutionWC		*Ind = dynamic_cast<IMultiObjectiveSolutionWC *> (NewPopSubset->getChild(i));
			Ind->updateObjectiveValues();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}

	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	if( NewPopulation->getSize() > _Population->getSize() )
		NewPopulation->pruneExcessIndividuals(_Population->getSize());

	// i sada nam nova populacija postaje trenutna populacija
	_Population->Destroy();
	for( int i=0; i<NewPopulation->getSize(); i++ )
		_Population->addNewSolution(NewPopulation->getIndividual(i));

	int s = _Population->getSize();
	int g = NewPopulation->getSize();

	NewPopulation->Clear();
//	_Population->operator = (*NewPopulation);
}
/**************************************************************************************************/
CHNAAlgorithm::CHNAAlgorithm(void)
{
}

CHNAAlgorithm::~CHNAAlgorithm(void)
{
}

void	CHNAAlgorithm::PerformFitnessAssignment()
{
	vector<bool>	vecIsDominated;
	vector<bool>	vecIsFeasible;
	vector<float>	vecRank;

	vecIsDominated.resize(_Population->getSize());
	vecRank.resize(_Population->getSize());
	vecIsFeasible.resize(_Population->getSize());

	// naci sva nedominirana rjesenja
	for( int i=0; i<_Population->getSize(); i++ ) {
		bool		bDominated = false;
		IMultiObjectiveSolutionWC		*CurrInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(i));

		// provjeri za populaciju
		for( int j=0; j<_Population->getSize(); j++ ) {
			if( i != j ) {
				IMultiObjectiveSolutionWC		*PopInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(j));

				if( CurrInd->isCoveredBy(PopInd) ) {
					bDominated = true;
					break;
				}
			}
		}
		vecIsDominated[i] = bDominated;
	}

	for( int i=0; i<_Population->getSize(); i++ ) {
		IMultiObjectiveSolutionWC		*SolPop = dynamic_cast<IMultiObjectiveSolutionWC	*>(_Population->getIndividual(i));

		if( SolPop->isFeasible() == false) {
			vecIsFeasible[i] = false;
			vecRank[i] = 10 / (SolPop->getNumViolatedConstraints()+1) / (1 + SolPop->getAmountViolatedConstraints());
		}
		else {
			vecIsFeasible[i] = true;
			if( vecIsDominated[i] == false )
				vecRank[i] = 500;
			else
				vecRank[i] = 100;
		}
	}
	// treba pridru�iti fitness i jedinkama u ParetoSetu
	// jednostavno - svim jedinkama se pridruzuje maksimalni fitness
	for( int i=0; i<_ParetoSet->getSize(); i++ ) {
		IFitnessProvider		*CurrInd = dynamic_cast<IFitnessProvider *> (_ParetoSet->getIndividual(i));
		CurrInd->setFitness(50);
	}

	for( int i=0; i<_Population->getSize(); i++ ) {
		IFitnessProvider		*CurrInd = dynamic_cast<IFitnessProvider *> (_Population->getIndividual(i));
		float	a;
		CurrInd->setFitness(a = vecRank[i]);
		float b = a;
	}
}

/**************************************************************************************************/
CH_I1Algorithm::CH_I1Algorithm(void)
{
}

CH_I1Algorithm::~CH_I1Algorithm(void)
{
}

void	CH_I1Algorithm::PerformFitnessAssignment()
{
	vector<bool>	vecIsDominated;
	vector<bool>	vecIsFeasible;
	vector<double>	vecRank;

	vecIsDominated.resize(_Population->getSize());
	vecRank.resize(_Population->getSize());
	vecIsFeasible.resize(_Population->getSize());

	// naci sva nedominirana rjesenja
	for( int i=0; i<_Population->getSize(); i++ ) {
		bool		bDominated = false;
		IMultiObjectiveSolutionWC		*CurrInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(i));

		// provjeri za populaciju
		for( int j=0; j<_Population->getSize(); j++ ) {
			if( i != j ) {
				IMultiObjectiveSolutionWC		*PopInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(j));

				if( CurrInd->isCoveredBy(PopInd) ) {
					bDominated = true;
					break;
				}
			}
		}
		vecIsDominated[i] = bDominated;
	}

	for( int i=0; i<_Population->getSize(); i++ ) {
		IMultiObjectiveSolutionWC		*SolPop = dynamic_cast<IMultiObjectiveSolutionWC	*>(_Population->getIndividual(i));

		if( SolPop->isFeasible() == false) {
			// treba uklju�iti i ovisnost o broju prekr�enih ograni�enja
			int		Num = SolPop->getNumViolatedConstraints();
			vecIsFeasible[i] = false;
			vecRank[i] = 0.95 * getPopulationSize() / (Num+1) / (1 + SolPop->getAmountViolatedConstraints());;
		}
		else {
			vecIsFeasible[i] = true;
			if( vecIsDominated[i] == false )
				vecRank[i] = 10;
			else
				vecRank[i] = 0.5 * getPopulationSize();
		}
	}
	double	Cmax = 1.2;
	double	Cmin = 0.8;
	int			M = getPopulationSize();

	for( int i=0; i<_Population->getSize(); i++ ) {
		IFitnessProvider		*CurrInd = dynamic_cast<IFitnessProvider *> (_Population->getIndividual(i));

		CurrInd->setFitness(Cmax - (Cmax - Cmin) * (vecRank[i] - 1) / (M - 1));
	}
	// treba pridru�iti fitness i jedinkama u ParetoSetu
	// jednostavno - svim jedinkama se pridruzuje maksimalni fitness
	for( int i=0; i<_ParetoSet->getSize(); i++ ) {
		IFitnessProvider		*CurrInd = dynamic_cast<IFitnessProvider *> (_ParetoSet->getIndividual(i));
		CurrInd->setFitness(50);
	}
}

/**************************************************************************************************/
CH_I2Algorithm::CH_I2Algorithm(void)
{
}

CH_I2Algorithm::~CH_I2Algorithm(void)
{
}

void	CH_I2Algorithm::PerformFitnessAssignment()
{
}

/**************************************************************************************************/
CH_I3Algorithm::CH_I3Algorithm(void)
{
}

CH_I3Algorithm::~CH_I3Algorithm(void)
{
}

void	CH_I3Algorithm::PerformFitnessAssignment()
{
}

/**************************************************************************************************/
CH_I4Algorithm::CH_I4Algorithm(void)
{
}

CH_I4Algorithm::~CH_I4Algorithm(void)
{
}

void	CH_I4Algorithm::PerformFitnessAssignment()
{
}
