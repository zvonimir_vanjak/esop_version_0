#include "StdAfx.h"

#include "SPEA.h"
//#include "..\StandardProblems\RealFunction_SolutionObjects.h"


using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//using namespace ESOP::Framework::StandardProblems::RealFunctionOptimization;

/********************************************************************************************************************/
SPEAWC::SPEAWC()
{
	_IterNum = 10;
	_ParetoSetMaxSize = 100;
}
void	SPEAWC::Reset()
{
	_ParetoSet.Destroy();
	_MatingPool.Clear();
}
void	SPEAWC::setIterationNumber(int inIterNum)
{
	_IterNum = inIterNum;
}
int		SPEAWC::getCurrIterNum()
{
	return _CurrIterNum;
}
void		SPEAWC::setFitness(IMultiObjectiveSolutionWC *Ind, double Fitness)
{
	IFitnessProvider		*Sol = dynamic_cast<IFitnessProvider *> (Ind);
	Sol->setFitness(Fitness);
}
double	SPEAWC::getFitness(IMultiObjectiveSolutionWC *Ind)
{
	IFitnessProvider		*Sol = dynamic_cast<IFitnessProvider *> (Ind);
	return Sol->getFitness();
}
void	SPEAWC::setParetoSetMaxSize(int inMaxSize)
{
	_ParetoSetMaxSize = inMaxSize;
}
Population*		SPEAWC::getParetoSet()
{
	return &_ParetoSet;
}
int		SPEAWC::Run()
{
	Initialize();

	for(_CurrIterNum=0; _CurrIterNum<_IterNum; _CurrIterNum++ )
	{
		PerformIteration();
	}

	return 0;
}
int		SPEAWC::PerformIteration()
{
	UpdateParetoSet();
	if( _ParetoSet.getSize() > _ParetoSetMaxSize )
		ReduceParetoSet();

	PerformFitnessAssignment();

	GenerateMatingPool();

	CreateNewPopulation();

	return 0;
}

void	SPEAWC::UpdateParetoSet()
{
	// ubacuje u _ParetoSet sve jedinke iz trenutne populacije koje su nedominirane (u odnosu na Populaciju i ParetoSet)
	// izbacuje iz ParetoSet-a sve jedinke koje time postaju dominirane
	for( int i=0; i<_Population->getSize(); i++ )
	{
		bool		bDominated = false;
		IMultiObjectiveSolutionWC		*CurrInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(i));

		// provjeri za populaciju
		for( int j=0; j<_Population->getSize(); j++ ) {
			if( i != j ) {
				IMultiObjectiveSolutionWC		*PopInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(j));
				if( CurrInd->isCoveredBy(PopInd) )
				{
					bDominated = true;
					break;
				}
			}
		}
		// provjeri za ParetoSet
		if( bDominated == false ) {
			for( int j=0; j<_ParetoSet.getSize(); j++ ) {
				IMultiObjectiveSolutionWC		*ParetoInd = dynamic_cast<IMultiObjectiveSolutionWC *> (_ParetoSet.getIndividual(j));
				
				if( CurrInd->isCoveredBy(ParetoInd) ) {
					bDominated = true;
					break;
				}
			}
		}
		if( bDominated == false ) {
			// ubacujemo je u ParetoSet
			ISolution	*NewInd = CurrInd->Clone();
			_ParetoSet.addNewSolution(NewInd);
		}
	}

	// sada treba vidjeti za ParetoSet da li je neko od rje�enja postalo dominirano
	for( int j=0; j<_ParetoSet.getSize(); j++ ) {
		IMultiObjectiveSolutionWC		*ParetoSol = dynamic_cast<IMultiObjectiveSolutionWC *> (_ParetoSet.getIndividual(j));

		for( int i=0; i<_Population->getSize(); i++ ) {
			IMultiObjectiveSolutionWC		*PopSol = dynamic_cast<IMultiObjectiveSolutionWC *> (_Population->getIndividual(i));

			if( ParetoSol->isCoveredBy(PopSol) ) {
				// treba izbaciti jedinku iz ParetoSeta
				_ParetoSet.removeIndividual(j);
				break;
			}
		}
	}
}

void	SPEAWC::ReduceParetoSet()
{
	if( _ParetoSet.getSize() < _ParetoSetMaxSize )
		return;

	ClusterSet		ClSet;

	// na po�etku svako rje�enje ide u svoj cluster
	for( int j=0; j<_ParetoSet.getSize(); j++ )  {
		Cluster										*pCl = new Cluster();
		IMultiObjectiveSolutionWC		*ParetoSol = dynamic_cast<IMultiObjectiveSolutionWC *> (_ParetoSet.getIndividual(j));

		pCl->AddSolToCluster(ParetoSol);
		ClSet.AddCluster(pCl);
	}

	while(ClSet.GetSize() > _ParetoSetMaxSize ) {
		// tra�imo dva me�usobno najbli�a clustera
		float			MinDist = ClSet.GetCluster(0)->GetDistanceToCluster(ClSet.GetCluster(1));
		Cluster		*c1 = ClSet.GetCluster(0);
		Cluster		*c2 = ClSet.GetCluster(1);

		for( int i=0; i<ClSet.GetSize()-1; i++ ) {
			for( int j=i+1; j<ClSet.GetSize(); j++ ) {
				float		NewDist = ClSet.GetCluster(i)->GetDistanceToCluster(ClSet.GetCluster(j)) ;
				
				if( NewDist < MinDist ) {
					c1	= ClSet.GetCluster(i);
					c2	= ClSet.GetCluster(j);
					MinDist = NewDist;
				}
			}
		}

		// spajamo dva na�ena clustera (odnosno, sva rje�enja koja su bila dio ta dva clustera postaju dio novog clustera)
		Cluster		*pNewCluster = new Cluster();

		for(int i=0; i<c1->GetSize(); i++ )
			pNewCluster->AddSolToCluster(c1->GetSol(i));
		for(int i=0; i<c2->GetSize(); i++ )
			pNewCluster->AddSolToCluster(c2->GetSol(i));

		// a c1 i c2 treba izbaciti iz ClSet-a
		ClSet.RemoveCluster(c1);
		ClSet.RemoveCluster(c2);
	}

	// sada iz svakog clustera treba u ParetoSet staviti po 1 predstavnika (centroid)
	Population		NewParetoSet;

	for( int i=0; i<ClSet.GetSize(); i++ ) {
		IMultiObjectiveSolutionWC		*p = dynamic_cast<IMultiObjectiveSolutionWC *> (ClSet.GetCluster(i)->GetCentroid()->Clone());
		NewParetoSet.addNewSolution(p);
	}

	_ParetoSet = NewParetoSet;
}

void	SPEAWC::PerformFitnessAssignment()
{
	for( int j=0; j<_Population->getSize(); j++ )
	{
//		MultiObjectiveRealFunc_SingleVar_SolObject		*SolPop = dynamic_cast<MultiObjectiveRealFunc_SingleVar_SolObject	*>(_Population->getIndividual(j));
	}

	// ra�unamo Pareto strengths
	//	Console::Write("\nPareto set");
	int		Count;
	for( int i=0; i<_ParetoSet.getSize(); i++ )
	{
		Count = 0;
		IMultiObjectiveSolutionWC		*SolPareto = dynamic_cast<IMultiObjectiveSolutionWC	*>(_ParetoSet.getIndividual(i));

		for( int j=0; j<_Population->getSize(); j++ )
		{
			IMultiObjectiveSolutionWC		*SolPop = dynamic_cast<IMultiObjectiveSolutionWC	*>(_Population->getIndividual(j));

			if( SolPop->isCoveredBy(SolPareto) )
				Count++;
		}
		double	Strength = Count / ((double) _Population->getSize() + 1);
		setFitness(SolPareto, Strength);

		//		SolPareto->Print();
	}

	// a onda odre�ujemo fitness za jedinke iz populacije
	//	printf("\n Population");
	double		Sum;
	for( int j=0; j<_Population->getSize(); j++ )
	{
		Sum = 0;
		IMultiObjectiveSolutionWC		*SolPop = dynamic_cast<IMultiObjectiveSolutionWC	*>(_Population->getIndividual(j));

		for( int i=0; i<_ParetoSet.getSize(); i++ )
		{
			IMultiObjectiveSolutionWC		*SolPareto = dynamic_cast<IMultiObjectiveSolutionWC	*>(_ParetoSet.getIndividual(i));

			if( SolPop->isCoveredBy(SolPareto) )
				Sum += getFitness(SolPareto);
		}
		setFitness(SolPop, Sum + 1);
	}
}

void	SPEAWC::GenerateMatingPool()
{
	// generira novu Populaciju jedinki koje �e se "pariti"
	//		odr�avaju se binary trounaments me�u jedinkama iz Populacije i ParetoSet-a sve dok se ne popuni mating pool
	//		vjerojatnost selekcije je povezana sa fitnessom jedinke
	_MatingPool.Clear();

	int	PopSize			= _Population->getSize();
	int	ParetoSize	= _ParetoSet.getSize();
	int	TotalNumInd	= PopSize + ParetoSize;

	for( int i=0; i<PopSize; i++ )
	{
		IMultiObjectiveSolutionWC	*Ind1, *Ind2;

		int		Sel1 = rand() % TotalNumInd;
		int		Sel2 = rand() % TotalNumInd;

		if( Sel1 < PopSize )
			Ind1 = dynamic_cast<IMultiObjectiveSolutionWC *>(_Population->getIndividual(Sel1));
		else
			Ind1 = dynamic_cast<IMultiObjectiveSolutionWC *>(_ParetoSet.getIndividual(Sel1 - PopSize));

		if( Sel2 < PopSize )
			Ind2 = dynamic_cast<IMultiObjectiveSolutionWC *>(_Population->getIndividual(Sel2));
		else
			Ind2 = dynamic_cast<IMultiObjectiveSolutionWC *>(_ParetoSet.getIndividual(Sel2 - PopSize));

		// koji je bolji, ide u mating pool
		if( getFitness(Ind1) < getFitness(Ind2) )
			_MatingPool.addNewSolution(Ind1);
		else
			_MatingPool.addNewSolution(Ind2);
	}
}
void	SPEAWC::CreateNewPopulation()
{
	// kreira novu populaciju kri�anjem i mutacijom jedinki iz mating pool-a
	//		dok se ne popuni populacija
	//			odaberi dvije jedinke sa selection op.
	//			kri�aj ih operatorom kri�anja
	//			ako su odabrani za mutaciju mutiraj ih op. mutacije
	Population		*NewPopulation = new Population();

	while(NewPopulation->getSize() < _MatingPool.getSize() )
	{
		PopulationSelection		*SelInd = new PopulationSelection();

		_Selector->Select(&_MatingPool, SelInd, 1);

		PopulationSubset			*NewPopSubset = new PopulationSubset();
		NewPopSubset->createChildren(SelInd->getSelIndividual(0), 2);

		_Recombinator->Recombine(	SelInd->getSelIndividual(0)->GetRepresentation(), 
															SelInd->getSelIndividual(1)->GetRepresentation(), 
															NewPopSubset->getChild(0)->GetRepresentation(),
															NewPopSubset->getChild(1)->GetRepresentation() );

		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
		_Mutator->Mutate(NewPopSubset->getChild(0)->GetRepresentation());
		_Mutator->Mutate(NewPopSubset->getChild(1)->GetRepresentation());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			IMultiObjectiveSolutionWC		*Ind = dynamic_cast<IMultiObjectiveSolutionWC *> (NewPopSubset->getChild(i));
			Ind->updateObjectiveValues();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}

	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	if( NewPopulation->getSize() > _Population->getSize() )
		NewPopulation->pruneExcessIndividuals(_Population->getSize());

	// i sada nam nova populacija postaje trenutna populacija
	_Population = NewPopulation;
}