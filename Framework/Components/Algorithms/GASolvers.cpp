#include "StdAfx.h"

#include "GASolvers.h"

#include "Components\Representations\StandardRepresentations.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

using namespace ESOP::Framework::StandardRepresentations;

/*******************************************************************************************/

BaseGA::BaseGA()
{
	_PopSize = 0;
	_Population = gcnew Population();
}

void	BaseGA::setESOPDesignVariable(IESOPDesignVariable ^DesVar)
{
	_SolutionInstance->setDesignVar(dynamic_cast<IDesignVariable ^>(DesVar));
}
void	BaseGA::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	setProblemInstance(dynamic_cast<ISingleObjectiveProblem ^>(ProbInst));
}

Population^		BaseGA::getPopulation() 
{ 
	return _Population; 
}

int		BaseGA::Initialize()
{
/*
	Console::WriteLine("Inicijalna populacija 1");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();

		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		vector<bool> *Bit1 = Parent1->getRepr()->getRepr();
		String ^s1 = gcnew String("");
		for( int i=0; i<10; i++ )
		{
			if( (*Bit1)[i] == false )
				s1 += "0";
			else
				s1 += "1";
		}
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString() + " " + s1);
	}
*/
	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISolution		^SolPop = dynamic_cast<ISolution	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}

	return 0;
}

void	BaseGA::setPopulationSize(int inSize)
{
	_PopSize = inSize;
	_Population->Initialize(inSize, _SolutionInstance);
}
int		BaseGA::getPopulationSize()
{
	return _Population->getSize();
}

void	BaseGA::setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign)
{
	_FitnessAssignment = inFitAssign;
}

void	BaseGA::setSelectionOp(IFitnessBasedSelectionOperator	^inSelector)
{
	_Selector = inSelector;
}
void	BaseGA::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}
void	BaseGA::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}
OptimizationResult^	BaseGA::Run()
{
	IterativeAlgorithm::Run();
/*
	Console::WriteLine("Zavr�na populacija");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString());
	}
*/
	// treba na�i najbolje rje�enje
	// ajde sad jo� na�i najbolju jedinku i ispi�i njenu vrijednost
	int			ind = 0;
	double	max = -1000000;
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

		if( sol->getCurrObjectiveValue() > max)
		{
			max = sol->getCurrObjectiveValue();
			ind = i;
		}
	}

	SOResult ^finalRes = gcnew SOResult();

	finalRes->setResult(dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(ind)));
	_FinalOptResult->SetFinalResult(finalRes);

	return _FinalOptResult;
}

double		BaseGA::getCurrObjectiveValue() 
{ 
	// treba na�i najbolje rje�enje
	int			ind = 0;
	if( _Problem->getObjectiveType() == EObjectiveType::MIN_OBJECTIVE )
	{
		double	min = 1000000;
		for( int i=0; i<_Population->getSize(); i++ )
		{
			ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

			if( sol->getCurrObjectiveValue() < min)
			{
				min = sol->getCurrObjectiveValue();
				ind = i;
			}
		}
		return min;
	}
	else
	{
		double	max = -1000000;
		for( int i=0; i<_Population->getSize(); i++ )
		{
			ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

			if( sol->getCurrObjectiveValue() > max)
			{
				max = sol->getCurrObjectiveValue();
				ind = i;
			}
		}
		return max;
	}
}


/*******************************************************************************************/
int			GenerationalGA::getOperatorNum() 
{ 
	return 3; 
}
String^	GenerationalGA::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "ISelectionOperator";
	case 1 : return "ICrossoverOperator2To2";
	case 2 : return "IMutationOp";
	}
	return " ";
}
String^	GenerationalGA::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "Selection op.";
	case 1 : return "Crossover 2 on 2";
	case 2 : return "Mutation op.";
	}
	return " ";
}
void		GenerationalGA::setESOPOperator(int Index, IESOPOperator ^oper)
{
	if( Index == 0 )
		_Selector = dynamic_cast<IFitnessBasedSelectionOperator ^> (oper);
	else if( Index == 1 )
		_Recombinator = dynamic_cast<ICrossoverOperator2To2 ^> (oper);
	else if( Index == 2 )
		_Mutator = dynamic_cast<IMutationOp ^> (oper);
}

int			GenerationalGA::PerformIteration()
{
	// kreira novu populaciju kri�anjem i mutacijom jedinki iz mating pool-a
	//		dok se ne popuni populacija
	//			odaberi dvije jedinke sa selection op.
	//			kri�aj ih operatorom kri�anja
	//			ako su odabrani za mutaciju mutiraj ih op. mutacije
	Population		^NewPopulation = gcnew Population();

	int Size = _Population->getSize();

	Console::WriteLine("Populacija na po�etku iteracije");
	for( int i=0; i<_PopSize; i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
//		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
//		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + sol->GetStringRepr());
	}


	array<double>		^arrFitness = gcnew array<double>(Size);
	_FitnessAssignment->Assign(_Population, arrFitness, _Problem->getObjectiveType());

	while(NewPopulation->getSize() < _Population->getSize() )
	{
//		PopulationSelection		^SelInd = gcnew PopulationSelection();

		array<int>		^vecOutSel = gcnew array<int>(2);
		_Selector->Select(arrFitness, vecOutSel, 2);

		PopulationSubset			^NewPopSubset = gcnew PopulationSubset();
		NewPopSubset->createChildren(_Population->getIndividual(vecOutSel[0]), 2);

		_Recombinator->Recombine(	_Population->getIndividual(vecOutSel[0])->GetRepresentation(), 
															_Population->getIndividual(vecOutSel[1])->GetRepresentation(), 
															NewPopSubset->getChild(0)->GetRepresentation(),
															NewPopSubset->getChild(1)->GetRepresentation() );
/*
		ISingleObjectiveSolution ^sol1 = dynamic_cast<ISingleObjectiveSolution ^> (SelInd->getSelIndividual(0));
		ISingleObjectiveSolution ^sol2 = dynamic_cast<ISingleObjectiveSolution ^> (SelInd->getSelIndividual(1));

		ISingleObjectiveSolution ^sol3 = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(0));
		ISingleObjectiveSolution ^sol4 = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(1));
*/
		// na svako kreirano dijete primjenjujemo (mo�da) mutaciju
		_Mutator->Mutate(NewPopSubset->getChild(0)->GetRepresentation());
		_Mutator->Mutate(NewPopSubset->getChild(1)->GetRepresentation());

		// za novokreirane jedinke treba izracunati vrijednosti funkcija cilja
		for( int i=0; i<NewPopSubset->getNumCreated(); i++ )
		{
			ISingleObjectiveSolution		^Ind = dynamic_cast<ISingleObjectiveSolution ^> (NewPopSubset->getChild(i));
			Ind->UpdateObjectiveValue();
		}

		// ubacujemo kreirane jedinke u novu populaciju
		NewPopSubset->copyToPopulation(NewPopulation);
	}
/*
	Console::WriteLine("Kreirana populacija");
	for( int i=0; i<NewPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (NewPopulation->getIndividual(i));
		double	val = sol->getCurrObjectiveValue();
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		double x = Parent1->getValue();
		Console::WriteLine("Func .val = " + val.ToString() + " x = " + x.ToString());
	}
*/
	// provjeri da li smo ih kreirali vi�e, i ako da odbaci vi�ak
	if( NewPopulation->getSize() > _Population->getSize() )
		NewPopulation->pruneExcessIndividuals(_Population->getSize());

	// i sada nam nova populacija postaje trenutna populacija
	_Population->Destroy();
	for( int i=0; i<NewPopulation->getSize(); i++ )
		_Population->addNewSolution(NewPopulation->getIndividual(i));

	int s = _Population->getSize();
	int g = NewPopulation->getSize();

	NewPopulation->Clear();
/*
	Console::WriteLine("Nova populacija");
	for( int i=0; i<NewPopulation->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

		String^	str = "Func.val.= " + sol->getCurrObjectiveValue().ToString("F10");
		RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(sol->GetRepresentation());
		Console::WriteLine(str + " " + Parent1->GetStringRepresentation());
	}
	*/

	return 0;
}

int			GenerationalGA::ESOP_Initialize()
{
	return Initialize();
}
int		GenerationalGA::ESOP_Run()
{
	Run();
	return 0;
}

/*******************************************************************************************/
int			SteadyStateGA::getOperatorNum() 
{ 
	return 3; 
}
String^	SteadyStateGA::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "ISelectionOperator";
	case 1 : return "ICrossoverOperator2To2";
	case 2 : return "IMutationOp";
	}
	return " ";
}
String^	SteadyStateGA::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
	case 0 : return "Selection op.";
	case 1 : return "Crossover 2 on 2";
	case 2 : return "Mutation op.";
	}
	return " ";
}
void		SteadyStateGA::setESOPOperator(int Index, IESOPOperator ^oper)
{
}

int			SteadyStateGA::PerformIteration()
{
	int a = 2;
	int b = 3;
	int c;

	c = a + b;
	return 0;
}


int			SteadyStateGA::ESOP_Initialize()
{
	return Initialize();
}
int		SteadyStateGA::ESOP_Run()
{
	Run();
	return 0;
}


/*******************************************************************************************/
GeneralMOGAWC::GeneralMOGAWC()
{
	_PopSize = 0;
	_Population = gcnew Population();
	_ParetoSet = gcnew Population();

	_ParetoSetMaxSize = 20;
//	_Selector					= 0;
//	_Recombinator			= 0;
//	_Mutator					= 0;
}
GeneralMOGAWC::~GeneralMOGAWC()
{
//	_Population = 0;
//	_ParetoSet = 0;
}

int			GeneralMOGAWC::Initialize()
{
	_ParetoSet->Destroy();
	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		IMultiObjectiveSolutionWC		^SolPop = dynamic_cast<IMultiObjectiveSolutionWC	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}

	return 0;
}

void		GeneralMOGAWC::setPopulationSize(int inSize)
{
	_PopSize = inSize;
	_Population->Initialize(inSize, _SolutionInstance);
}
int			GeneralMOGAWC::getPopulationSize()
{
	return _Population->getSize();
}

void		GeneralMOGAWC::setSelectionOp(ISelectionOperator	^inSelector)
{
	_Selector = inSelector;
}

void		GeneralMOGAWC::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}

void		GeneralMOGAWC::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}

int			GeneralMOGAWC::PerformIteration() 
{
	return 1;
}
OptimizationResult^		GeneralMOGAWC::Run() 
{
	OptimizationResult^  OptRes = gcnew OptimizationResult();

	return OptRes;
}

int			GeneralMOGAWC::getOperatorNum() 
{ 
	return 3; 
}
String^	GeneralMOGAWC::getOperatorType(int ParamInd)
{
	switch(ParamInd)
	{
		case 0 : return "ISelectionOperator";
		case 1 : return "ICrossoverOperator2To2";
		case 2 : return "IMutationOp";
	}
	return " ";
}
String^	GeneralMOGAWC::getOperatorID(int ParamInd)
{
	switch(ParamInd)
	{
		case 0 : return "Selection op.";
		case 1 : return "Crossover 2 on 2";
		case 2 : return "Mutation op.";
	}
	return " ";
}
void		GeneralMOGAWC::setESOPOperator(int Index, IESOPOperator ^oper)
{
}

int			GeneralMOGAWC::ESOP_Initialize()
{
	return Initialize();
}
void		GeneralMOGAWC::setESOPDesignVariable(IESOPDesignVariable ^SolInst)
{
	_SolObject = dynamic_cast<IMultiObjectiveSolutionWC ^>(SolInst);
}
void		GeneralMOGAWC::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	_Problem = dynamic_cast<IMultiObjectiveProblemWC ^>(ProbInst);
}

int		GeneralMOGAWC::ESOP_Run()
{
	Run();
	return 0;
}
