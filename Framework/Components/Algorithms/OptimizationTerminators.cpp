#include "stdafx.h"
#include "OptimizationTerminators.h"

TerminateOnIterationNum::TerminateOnIterationNum()
{
	_TotalIterNum = 10;
}

void		TerminateOnIterationNum::SetParameter(int par)
{
	_TotalIterNum = par;
}

String^	TerminateOnIterationNum::GetRequestedValueProviderClassName()
{
	return "IOptTermIterNumProvider";
}

String^	TerminateOnIterationNum::GetTerminatorDesc()
{
	return "Terminate on iteration num.";
}

bool	TerminateOnIterationNum::IsTermConditionSatisfied(IterativeAlgorithm ^CurrContext)
{
	IOptTermIterNumProvider		^term = dynamic_cast<IOptTermIterNumProvider ^>(CurrContext);
	return (term->getCurrIterNum() == _TotalIterNum);
}

//////////////////////////////////////////////////////////////////////
TerminateOnDuration::TerminateOnDuration()
{
	_TotalDuration = 5000;
}

void		TerminateOnDuration::SetParameter(int par)
{
	_TotalDuration = par;
}

String^	TerminateOnDuration::GetRequestedValueProviderClassName()
{
	return "IOptTermDurationProvider";
}

String^	TerminateOnDuration::GetTerminatorDesc()
{
	return "Terminate on time passed";
}

bool	TerminateOnDuration::IsTermConditionSatisfied(IterativeAlgorithm ^CurrContext)
{
	IOptTermDurationProvider		^term = dynamic_cast<IOptTermDurationProvider ^>(CurrContext);
	return (term->getCurrTimeSpanInMs() >= _TotalDuration);
}
