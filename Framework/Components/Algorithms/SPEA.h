#pragma once

#include "GASolvers.h"
#include "MOHelpers.h"

using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				class SPEAWC : public GeneralMOGAWC
				{
				public:
					SPEAWC();

					CString getComponentName() { return "StandardAlgorithms.dll"; }
					CString	getAlgorithmName() { return "SPEA"; }

					int			getParamNum()		{ return 2; }
					CString	getParamName(int ParamInd) { return ""; }

					void		setParamValue(float inVal) {}
					float		getParamValue(float inVal) { return 0.0; }

					void	Reset();

					int		PerformIteration();
					int		Run();

					void	setIterationNumber(int inIterNum);
					void	setParetoSetMaxSize(int inMaxSize);

					int		getCurrIterNum();

					Population*		getParetoSet();

					void		setFitness(IMultiObjectiveSolutionWC *Ind, double Fitness);
					double	getFitness(IMultiObjectiveSolutionWC *Ind);

				private:
					void	UpdateParetoSet();
					void	ReduceParetoSet();
					void	PerformFitnessAssignment();

					void	GenerateMatingPool();
					void	CreateNewPopulation();

				private:
					int			_IterNum;
					int			_CurrIterNum;

					int						_ParetoSetMaxSize;
					Population		_MatingPool;
					Population		_ParetoSet;
				};
			}
		}
	}
}
