#pragma once

#include "GAHelpers.h"
#include "IntermediateResultsProviderInterfaces.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				/*
					- implementira zajedničke dijelove za standardni genetski algoritma
				*/
				public ref class FitnessBasedGA abstract : public IterativeAlgorithm,
																										public IIntermedValueProvider_CurrentObjectiveValue,
																										public IIntermedValueProvider_BestFitnessInCurrPopulation,
																										public IIntermedValueProvider_AverageFitnessInCurrPopulation,
																										public IIntermedValueProvider_WorstFitnessInCurrPopulation 
				{
				public:
					FitnessBasedGA();

					// Base related
					void		setProblemInstance(IProblem ^inProblem);
					void		setSolutionInstance(ISolution ^inSolutionInstance);

					void		setPopulationSize(int inSize);
					int			getPopulationSize();

					void		setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign);
					void		setSelectionOp(IFitnessBasedSelectionOperator	^inSelector);
					void		setRecombinationOp(ICrossoverOperator2To2	^inRecombinator);
					void		setMutationOp(IMutationOp	^inMutator);

					Population^		getPopulation();

					virtual	int			Initialize() override;
					virtual	void		Run(OptimizationResult^	outRes) override;

					virtual	double		getCurrObjectiveValue();
					virtual	double		getCurrBestFitnessValue();
					virtual	double		getCurrAverageFitnessValue();
					virtual	double		getCurrWorstFitnessValue();

					// ESOP related
					virtual void		setESOPDesignVariable(IESOPDesignVariable ^DesVar);
					virtual void		setESOPProblemInstance(IESOPProblem ^ProbInst);

				protected:
					IProblem		^_Problem;
					ISolution		^_SolutionInstance;

					int						_PopSize;
					Population		^_Population;

					array<double>	^_arrFitness;

					IFitnessAssignmentOperator			^_FitnessAssignment;
					IFitnessBasedSelectionOperator	^_Selector;
					ICrossoverOperator2To2					^_Recombinator;
					IMutationOp											^_Mutator;
				};
			}
		}
	}
}
