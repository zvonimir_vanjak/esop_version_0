#pragma once

#include "GAHelpers.h"
#include "IntermediateResultsProviderInterfaces.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				/*
					- implementira zajedničke dijelove za standardni genetski algoritma
				*/
				public ref class BaseGA abstract : public SingleObjectiveIterativeAlgorithm,
																					 public IIntermedValueProvider_CurrentObjectiveValue,
																					 public IIntermedValueProvider_CurrentAverageObjective,
																					 public IIntermedValueProvider_BestFitnessInCurrPopulation,
																					 public IIntermedValueProvider_AverageFitnessInCurrPopulation,
																					 public IIntermedValueProvider_WorstFitnessInCurrPopulation, 
																					 public IIntermedValueProvider_WholePopulation,
																					 public ITestDoubleCollProvider
				{
				public:
					BaseGA();

					// Base related
					void		setPopulationSize(int inSize);
					int			getPopulationSize();

					void		setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign);
					void		setSelectionOp(IFitnessBasedSelectionOperator	^inSelector);
					void		setRecombinationOp(ICrossoverOperator2To2	^inRecombinator);
					void		setMutationOp(IMutationOp	^inMutator);

					virtual Population^		getPopulation();

					virtual	int			Initialize() override;
					virtual	void		Run(OptimizationResult^	outRes) override;

 					virtual	double		getCurrObjectiveValue();
					virtual	double		getCurrAverageObjectiveValue();
					virtual	double		getCurrBestFitnessValue();
					virtual	double		getCurrAverageFitnessValue();
					virtual	double		getCurrWorstFitnessValue();
					virtual	int				getValue1();
					virtual	int				getValue2();
					virtual	double		getValue3();

					// ESOP related
					virtual void		setESOPDesignVariable(IESOPDesignVariable ^DesVar);
					virtual void		setESOPProblemInstance(IESOPProblem ^ProbInst);

				protected:
					int						_PopSize;
					Population		^_Population;

					array<double>	^_arrFitness;

					IFitnessAssignmentOperator			^_FitnessAssignment;
					IFitnessBasedSelectionOperator	^_Selector;
					ICrossoverOperator2To2					^_Recombinator;
					IMutationOp											^_Mutator;
				};
				/*
					- operatorom selekcije se izabire MatingPool od _PopSize jedinki
					- zatim se nad jedinkama iz MatingPool-a krizanjem i mutacijom stvara novih _PopSize jedinki
					- novokreirane jedinke postaju nova populacija
				 */
				public ref class GenerationalGA : public BaseGA, public IESOPTemplateAlgorithm
				{
				public:
					virtual	int				PerformIteration() override;

					// ESOP related
					virtual	String^		getESOPComponentName() 		{ return "GenerationalGA"; }
					virtual String^		getESOPComponentDesc()		{ return "Desc"; }
					virtual	array<String^>^		getAlgorithmInstanceDesc();

					virtual int				getParamNum() { return 1; }
					virtual String^		getParamName(int ParamInd) {return "Population size"; }
					
					virtual void			setParamValue(int Ind, double inVal) { setPopulationSize((int) inVal); }
					virtual double		getParamValue(int Ind) { return getPopulationSize(); }

					virtual int				getOperatorNum();
					virtual String^		getOperatorType(int ParamInd);
					virtual String^		getOperatorID(int ParamInd);
					virtual void			setESOPOperator(int Index, IESOPOperator ^oper);

					virtual	int				ESOP_Initialize();
					virtual	void			ESOP_Run(OptimizationResult ^outRes);
				};

				public ref class SteadyStateGA : public BaseGA, public IESOPTemplateAlgorithm
				{
				public:
					// Base related
					virtual	int				PerformIteration() override;

					// ESOP related
					virtual	String^		getESOPComponentName() 		{ return "SteadyStateGA"; }
					virtual String^		getESOPComponentDesc()		{ return "Desc"; }
					virtual	array<String^>^		getAlgorithmInstanceDesc();

					virtual int				getParamNum() { return 1; }
					virtual String^		getParamName(int ParamInd) {return "Population size"; }
					
					virtual void			setParamValue(int Ind, double inVal) { setPopulationSize((int) inVal); }
					virtual double		getParamValue(int Ind) { return getPopulationSize(); }

					virtual int				getOperatorNum();
					virtual String^		getOperatorType(int ParamInd);
					virtual String^		getOperatorID(int ParamInd);
					virtual void			setESOPOperator(int Index, IESOPOperator ^oper);

					virtual	int				ESOP_Initialize();
					virtual	void			ESOP_Run(OptimizationResult ^outRes);
				};

				public ref class CompleteFuncOptimizatorGA : public BaseGA, public IESOPCompleteAlgorithm
				{
				public:
					// Base related
					virtual	int				PerformIteration() override;

					// ESOP related
					virtual	String^		getESOPComponentName() 		{ return "CompleteFuncOptimizatorGA"; }
					virtual String^		getESOPComponentDesc()		{ return "Generational genetic algorithm for\r\nsingle-objective function optimization\r\nwith use of 1-point crossover and flop-bit mutation"; }
					virtual	array<String^>^		getAlgorithmInstanceDesc();

					virtual int				getParamNum() { return 1; }
					virtual String^		getParamName(int ParamInd) {return "Population size"; }
					
					virtual void			setParamValue(int Ind, double inVal) { setPopulationSize((int) inVal); }
					virtual double		getParamValue(int Ind) { return getPopulationSize(); }

					virtual	int				ESOP_Initialize();
					virtual	void			ESOP_Run(OptimizationResult ^outRes);
				};

			}
		}
	}
}
