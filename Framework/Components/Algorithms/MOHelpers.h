#pragma once

#include <math.h>
#include <assert.h>
#include <vector>


using std::vector;
using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				public ref class Cluster
				{
				public:
					int			GetSize()														{ return (int) _vecSol->size(); }
					void		AddSolToCluster(IMultiObjectiveSolutionWC ^in)	{ _vecSol->push_back(in); }
					IMultiObjectiveSolutionWC^	GetSol(int Ind)		{ return (*_vecSol)[Ind]; }
					float		GetDistanceToCluster(Cluster ^Cl2)
					{
						// treba izra�unati udaljenosti izme�u svih parova i onda odrediti prosjek
						int			DistNum = 0;
						float		TotalDist = 0;

						for( int i=0; i<GetSize(); i++ )
						{
							for( int j=0; j<Cl2->GetSize(); j++ )
							{
								float	PairDist = GetDistBetweenSol(GetSol(i), Cl2->GetSol(j));

								TotalDist += PairDist;
								DistNum++;
							}
						}
						return TotalDist / DistNum;
					}

					float		GetDistBetweenSol(IMultiObjectiveSolutionWC ^Sol1, IMultiObjectiveSolutionWC ^Sol2)
					{
						int			NumObj = Sol1->getNumObjectives();
						int			DistNum = 0;
						vector<double>		vec1, vec2;

						vec1.resize(NumObj);
						vec2.resize(NumObj);

						// uzimamo vrijednosti funkcije cilja prvog rje�enja
						for( int k=0; k<NumObj; k++ )
							vec1[k] = 1;

//						VectorDouble ^r = Sol1->getCurrObjectiveValues();
						// vrijednosti funkcije cilja drugog rje�enja
						for( int k=0; k<NumObj; k++ )
							vec2[k] = 1; //Sol2->getCurrObjectiveValues()[k];

						// ra�unamo Euklidsku udaljenost me�u rje�enjima
						double		PairDist = 0;
						for( int k=0; k<NumObj; k++ )
							PairDist += pow(vec1[k] - vec2[k], 2);

						return (float) sqrt(PairDist);
					}

					ISolution^	GetCentroid()
					{
						// tra�imo rje�enje koje ima minimalnu prosje�nu udaljenost do svih drugih rje�enja u clusteru
						int			SelInd = -1;
						float		MinAvgDist = 1e10;

						for( int i=0; i<GetSize(); i++ )
						{
							// ra�unamo udaljenost do svih drugih rje�enja
							int		j;
							float	MyDist = 0;
							for( j=0; j<GetSize(); j++ )
								if( i != j )
									MyDist += GetDistBetweenSol(GetSol(i), GetSol(j));			// ra�unamo udaljenost

							if( MyDist / j < MinAvgDist )
							{
								MinAvgDist = MyDist;
								SelInd = i;
							}
						}
						assert(SelInd != -1);

						return (*_vecSol)[SelInd];
					}

				private:
					vector<gcroot<IMultiObjectiveSolutionWC ^> >		*_vecSol;
				};

				public ref class ClusterSet
				{
				public:
					~ClusterSet()
					{
						for( int i=0; i<GetSize(); i++ )
							(*_vecClusters)[i] = 0;
					}

					int				GetSize()						{ return (int) _vecClusters->size(); }
					Cluster^	GetCluster(int i)		{ return (*_vecClusters)[i]; }

					void			AddCluster(Cluster ^in)		{ _vecClusters->push_back(in); }
					void			RemoveCluster(Cluster ^p)	{ 
						for( int i=0; i<GetSize(); i++ )
							if( (*_vecClusters)[i]->Equals(p) )
							{
								_vecClusters->erase(_vecClusters->begin() + i);
								break;
							}
					}

				private:
					vector<gcroot<Cluster ^> >		*_vecClusters;
				};
				
				public ref class ParetoSetOperations
				{
				public:
					static	void	UpdateParetoSet(Population ^_ParetoSet, Population ^_Population)
					{
						// ubacuje u _ParetoSet sve jedinke iz trenutne populacije koje su nedominirane (u odnosu na Populaciju i ParetoSet)
						// izbacuje iz ParetoSet-a sve jedinke koje time postaju dominirane
						for( int i=0; i<_Population->getSize(); i++ ) {
							bool		bDominated = false;
							IMultiObjectiveSolutionWC		^CurrInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (_Population->getIndividual(i));

							// najprije provjeri da li je feasible

							if( CurrInd->isFeasible() == false ) {
								bDominated = true;
								break;
							}

							// provjeri za populaciju
							for( int j=0; j<_Population->getSize(); j++ ) {
								if( i != j ) {
									IMultiObjectiveSolutionWC		^PopInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (_Population->getIndividual(j));

									if( CurrInd->isCoveredByFeasible(PopInd) ) {
										bDominated = true;
										break;
									}
								}
							}
							// provjeri za ParetoSet
							if( bDominated == false ) {
								for( int j=0; j<_ParetoSet->getSize(); j++ ) {
									IMultiObjectiveSolutionWC		^ParetoInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(j));

									if( CurrInd->isCoveredByFeasible(ParetoInd) ) {
										bDominated = true;
										break;
									}
								}
							}
							if( bDominated == false && CurrInd->isFeasible() == true ) {
								// ubacujemo je u ParetoSet
								IMultiObjectiveSolutionWC	^NewInd = dynamic_cast<IMultiObjectiveSolutionWC ^> (CurrInd->Clone());

								int		Num1 = CurrInd->getNumViolatedConstraints();
								int		Num2 = NewInd->getNumViolatedConstraints();

								if( Num1 > 0 || Num2 > 0 )
								{
//									CString		prdo;
//									prdo.Format("Num1 = %d, Num2 = %d", Num1, Num2);
//									AfxMessageBox(prdo);
								}
								//			CString		ptr;
								//			ptr.Format("Dodano %p", NewInd);
								//			AfxMessageBox(ptr);
								_ParetoSet->addNewSolution(NewInd);

								//			AfxMessageBox("Dodano\n"+NewInd->getStringRepr());
							}
						}

						// sada treba vidjeti za ParetoSet da li je neko od rje�enja postalo dominirano
						for( int j=0; j<_ParetoSet->getSize(); j++ ) {
							IMultiObjectiveSolutionWC		^ParetoSol = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(j));

							for( int i=0; i<_ParetoSet->getSize(); i++ ) {
								IMultiObjectiveSolutionWC		^PopSol = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(i));

								if( ParetoSol->isDominatedByFeasible(PopSol) ){
									_ParetoSet->removeIndividual(j);				// treba izbaciti jedinku iz ParetoSeta
									//				CString		ptr;
									//				ptr.Format("Vadim %p", PopSol);
									//				AfxMessageBox(ptr);
									break;
								}
							}
						}
					}
					static	void	ReduceParetoSet(Population ^inParetoSet, int _ParetoSetMaxSize)
					{
						Population ^_ParetoSet = inParetoSet;

						if( _ParetoSet->getSize() < _ParetoSetMaxSize )
							return;

						ClusterSet		ClSet;

						// na po�etku svako rje�enje ide u svoj cluster
						for( int j=0; j<_ParetoSet->getSize(); j++ ) 
						{
							Cluster										^pCl = gcnew Cluster();
							IMultiObjectiveSolutionWC		^ParetoSol = dynamic_cast<IMultiObjectiveSolutionWC ^> (_ParetoSet->getIndividual(j));

							pCl->AddSolToCluster(ParetoSol);
							ClSet.AddCluster(pCl);
						}

						while(ClSet.GetSize() > _ParetoSetMaxSize )
						{
							// tra�imo dva me�usobno najbli�a clustera
							float			MinDist = ClSet.GetCluster(0)->GetDistanceToCluster(ClSet.GetCluster(1));
							Cluster		^c1 = ClSet.GetCluster(0);
							Cluster		^c2 = ClSet.GetCluster(1);

							for( int i=0; i<ClSet.GetSize()-1; i++ )
							{
								for( int j=i+1; j<ClSet.GetSize(); j++ )
								{
									float		NewDist = ClSet.GetCluster(i)->GetDistanceToCluster(ClSet.GetCluster(j)) ;
									if( NewDist < MinDist )
									{
										c1	= ClSet.GetCluster(i);
										c2	= ClSet.GetCluster(j);
										MinDist = NewDist;
									}
								}
							}

							// spajamo dva na�ena clustera (odnosno, sva rje�enja koja su bila dio ta dva clustera postaju dio novog clustera)
							Cluster		^pNewCluster = gcnew Cluster();

							for(int i=0; i<c1->GetSize(); i++ )
								pNewCluster->AddSolToCluster(c1->GetSol(i));
							for(int i=0; i<c2->GetSize(); i++ )
								pNewCluster->AddSolToCluster(c2->GetSol(i));

							// a c1 i c2 treba izbaciti iz ClSet-a
							ClSet.RemoveCluster(c1);
							ClSet.RemoveCluster(c2);
						}

						// sada iz svakog clustera treba u ParetoSet staviti po 1 predstavnika (centroid)
						Population		^NewParetoSet = gcnew Population();

						for( int i=0; i<ClSet.GetSize(); i++ )
						{
							IMultiObjectiveSolutionWC		^p = dynamic_cast<IMultiObjectiveSolutionWC ^> (ClSet.GetCluster(i)->GetCentroid()->Clone());
							NewParetoSet->addNewSolution(p);
						}

						_ParetoSet->Destroy();
//						*inParetoSet = NewParetoSet;
					}
				};

			}
		}
	}
}