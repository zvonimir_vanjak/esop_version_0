#pragma once

#include "GAHelpers.h"
#include "IntermediateResultsProviderInterfaces.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				/*
					- implementira zajedničke dijelove za standardni genetski algoritma
				*/
				public ref class BaseSOWCGA abstract : public SingleObjectiveIterativeAlgorithmWC,
																							 public IIntermedValueProvider_CurrentObjectiveValue
				{
				public:
					BaseSOWCGA();

					// Base related
					void		setPopulationSize(int inSize);
					int			getPopulationSize();

					void		setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign);
					void		setSelectionOp(IFitnessBasedSelectionOperator	^inSelector);
					void		setRecombinationOp(ICrossoverOperator2To2	^inRecombinator);
					void		setMutationOp(IMutationOp	^inMutator);

					Population^		getPopulation();

					virtual	int			Initialize() override;
					virtual	void		Run(OptimizationResult^	outRes) override;

					virtual	double		getCurrObjectiveValue();

					// ESOP related
					virtual void		setESOPDesignVariable(IESOPDesignVariable ^DesVar);
					virtual void		setESOPProblemInstance(IESOPProblem ^ProbInst);

				protected:
					int						_PopSize;
					Population		^_Population;

					IFitnessAssignmentOperator			^_FitnessAssignment;
					IFitnessBasedSelectionOperator	^_Selector;
					ICrossoverOperator2To2					^_Recombinator;
					IMutationOp											^_Mutator;
				};
			}
		}
	}
}