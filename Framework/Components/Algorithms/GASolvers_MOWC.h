#pragma once

#include "GAHelpers.h"
#include "IntermediateResultsProviderInterfaces.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				/*
					- implementira zajedničke dijelove za standardni genetski algoritma
				*/
				public ref class BaseMOWCGA abstract : public MultiObjectiveIterativeAlgorithmWC
				{
				public:
					BaseMOWCGA();

					// Base related
					void		setPopulationSize(int inSize);
					int			getPopulationSize();
					void		setParetoSetMaxSize(int inSize);
					int			getParetoSetMaxSize();

					void		setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign);
					void		setSelectionOp(IFitnessBasedSelectionOperator	^inSelector);
					void		setRecombinationOp(ICrossoverOperator2To2	^inRecombinator);
					void		setMutationOp(IMutationOp	^inMutator);

					Population^		getPopulation();
					Population^		getParetoSet()	{ return _ParetoSet; }

					virtual	int			Initialize() override;
					virtual	void		Run(OptimizationResult^	outRes) override;

					// ESOP related
					virtual void		setESOPDesignVariable(IESOPDesignVariable ^DesVar);
					virtual void		setESOPProblemInstance(IESOPProblem ^ProbInst);

				protected:
					int						_PopSize;
					Population		^_Population;

					int						_ParetoSetMaxSize;
					Population		^_ParetoSet;
					
					IFitnessAssignmentOperator			^_FitnessAssignment;
					IFitnessBasedSelectionOperator	^_Selector;
					ICrossoverOperator2To2					^_Recombinator;
					IMutationOp											^_Mutator;
				};

				//////////////////////////////////////////////////////////////////////////////////////
				public ref class CHNA : public BaseMOWCGA, public IESOPTemplateAlgorithm
				{
				public:
					// Base related
					virtual	int				PerformIteration() override;

					// ESOP related
					virtual	String^		getESOPComponentName() 		{ return "CHNA"; }
					virtual String^		getESOPComponentDesc()		{ return "Standard CHNA algorithm"; }
					virtual	array<String^>^		getAlgorithmInstanceDesc();

					virtual int				getParamNum() ;
					virtual String^		getParamName(int ParamInd) ;
					
					virtual void			setParamValue(int Ind, double inVal);
					virtual double		getParamValue(int Ind);
					virtual int				getOperatorNum();
					virtual String^		getOperatorType(int ParamInd);
					virtual String^		getOperatorID(int ParamInd);
					virtual void			setESOPOperator(int Index, IESOPOperator ^oper);

					virtual	int				ESOP_Initialize();
					virtual	void			ESOP_Run(OptimizationResult ^outRes);

				private:
					void	UpdateParetoSet();
					void	CreateNewPopulation(array<double> ^arrFitness);
				};

				//////////////////////////////////////////////////////////////////////////////////////
				public ref class CH_I1 : public BaseMOWCGA, public IESOPTemplateAlgorithm
				{
				public:
					// Base related
					virtual	int				PerformIteration() override;

					// ESOP related
					virtual	String^		getESOPComponentName() 		{ return "CH_I1"; }
					virtual String^		getESOPComponentDesc()		{ return "Standard CH_I1 algorithm"; }
					virtual	array<String^>^		getAlgorithmInstanceDesc();

					virtual int				getParamNum() ;
					virtual String^		getParamName(int ParamInd) ;
					
					virtual void			setParamValue(int Ind, double inVal);
					virtual double		getParamValue(int Ind);

					virtual int				getOperatorNum();
					virtual String^		getOperatorType(int ParamInd);
					virtual String^		getOperatorID(int ParamInd);
					virtual void			setESOPOperator(int Index, IESOPOperator ^oper);

					virtual	int				ESOP_Initialize();
					virtual	void			ESOP_Run(OptimizationResult ^outRes);
				};

			}
		}
	}
}