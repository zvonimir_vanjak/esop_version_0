#include "stdafx.h"
#include "IntermediateResultsObservers.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
IntermedTimerOnEveryIterNum::IntermedTimerOnEveryIterNum()
{
	_SaveEveryIterNum = 1;
	_LastIterNumSaved = 0;
}
bool	IntermedTimerOnEveryIterNum::IsTimeToSave(ObservedValueTag ^inTag)
{
	if( _LastIterNumSaved + _SaveEveryIterNum == inTag->_IterNum )
	{
		_LastIterNumSaved = inTag->_IterNum;
		return true;
	}
	else
		return false;
}
void	IntermedTimerOnEveryIterNum::SetParameter(int inPar)
{
	_SaveEveryIterNum = inPar;
}

IntermedTimerOnTimeInterval::IntermedTimerOnTimeInterval()
{
	_SaveEveryMs = 100;
	_LastTimeSaved = 0;
}
bool	IntermedTimerOnTimeInterval::IsTimeToSave(ObservedValueTag ^inTag)
{
	if( inTag->_TimeMs >= _LastTimeSaved + _SaveEveryMs )
	{
		_LastTimeSaved = (int) inTag->_TimeMs;
		return true;
	}
	else
		return false;
}

void	IntermedTimerOnTimeInterval::SetParameter(int inPar)
{
	_SaveEveryMs = inPar;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
CurrObjectiveValueSaver::CurrObjectiveValueSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_CurrentObjectiveValue ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		CurrObjectiveValueSaver::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_CurrentObjectiveValue";
}
String^		CurrObjectiveValueSaver::getObserverDesc()
{
	return "Current best objective value";
}

void	CurrObjectiveValueSaver::Save(ObservedValueTag ^inTag)
{
	double a = _Provider->getCurrObjectiveValue();

	_ValCollection->Add(a, inTag); 
}

void	CurrObjectiveValueSaver::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	CurrObjectiveValueSaver::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_CurrentObjectiveValue ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
AverageObjectiveValueInPopulationSaver::AverageObjectiveValueInPopulationSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_CurrentAverageObjective ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		AverageObjectiveValueInPopulationSaver::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_CurrentAverageObjective";
}
String^		AverageObjectiveValueInPopulationSaver::getObserverDesc()
{
	return "Current average objective value in population";
}

void	AverageObjectiveValueInPopulationSaver::Save(ObservedValueTag ^inTag)
{
	double a = _Provider->getCurrAverageObjectiveValue();

	_ValCollection->Add(a, inTag); 
}

void	AverageObjectiveValueInPopulationSaver::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	AverageObjectiveValueInPopulationSaver::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_CurrentAverageObjective ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
BestFitnessValueInPopulation::BestFitnessValueInPopulation(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_BestFitnessInCurrPopulation ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		BestFitnessValueInPopulation::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_BestFitnessInCurrPopulation";
}
String^		BestFitnessValueInPopulation::getObserverDesc()
{
	return "Best fitness in population";
}

void	BestFitnessValueInPopulation::Save(ObservedValueTag ^inTag)
{
	double a = _Provider->getCurrBestFitnessValue();

	_ValCollection->Add(a, inTag); 
}

void	BestFitnessValueInPopulation::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	BestFitnessValueInPopulation::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_BestFitnessInCurrPopulation ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
WorstFitnessValueInPopulation::WorstFitnessValueInPopulation(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_WorstFitnessInCurrPopulation ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		WorstFitnessValueInPopulation::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_WorstFitnessInCurrPopulation";
}
String^		WorstFitnessValueInPopulation::getObserverDesc()
{
	return "Worst fitness in population";
}

void	WorstFitnessValueInPopulation::Save(ObservedValueTag ^inTag)
{
	double a = _Provider->getCurrWorstFitnessValue();

	_ValCollection->Add(a, inTag); 
}

void	WorstFitnessValueInPopulation::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	WorstFitnessValueInPopulation::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_WorstFitnessInCurrPopulation ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
AverageFitnessValueInPopulation::AverageFitnessValueInPopulation(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_AverageFitnessInCurrPopulation ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		AverageFitnessValueInPopulation::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_AverageFitnessInCurrPopulation";
}
String^		AverageFitnessValueInPopulation::getObserverDesc()
{
	return "Average fitness in population";
}

void	AverageFitnessValueInPopulation::Save(ObservedValueTag ^inTag)
{
	double a = _Provider->getCurrAverageFitnessValue();

	_ValCollection->Add(a, inTag); 
}

void	AverageFitnessValueInPopulation::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	AverageFitnessValueInPopulation::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_AverageFitnessInCurrPopulation ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
PopulationConvergenceMeasure::PopulationConvergenceMeasure(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_PopulationConvergenceMeasure ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		PopulationConvergenceMeasure::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_PopulationConvergenceMeasure";
}
String^		PopulationConvergenceMeasure::getObserverDesc()
{
	return "Population convergence by measure ... observer";
}

void	PopulationConvergenceMeasure::Save(ObservedValueTag ^inTag)
{
	double a = _Provider->getPopulation();

	_ValCollection->Add(a, inTag); 
}

void	PopulationConvergenceMeasure::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	PopulationConvergenceMeasure::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_PopulationConvergenceMeasure ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
WholePopulationSaver::WholePopulationSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_WholePopulation ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		WholePopulationSaver::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_WholePopulation";
}
String^		WholePopulationSaver::getObserverDesc()
{
	return "Whole population saver";
}

void	WholePopulationSaver::Save(ObservedValueTag ^inTag)
{
	_ValCollection->Add(_Provider->getPopulation(), inTag); 
}

void	WholePopulationSaver::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	WholePopulationSaver::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_WholePopulation ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
WholeParetoSetSaver::WholeParetoSetSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_WholeParetoSet ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}

String^		WholeParetoSetSaver::getRequestedValueProviderClassName()
{
	return "IIntermedValueProvider_WholeParetoSet";
}
String^		WholeParetoSetSaver::getObserverDesc()
{
	return "WholeParetoSetSaver";
}

void	WholeParetoSetSaver::Save(ObservedValueTag ^inTag)
{
	_ValCollection->Add(_Provider->getParetoSet(), inTag); 
}

void	WholeParetoSetSaver::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	WholeParetoSetSaver::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<IIntermedValueProvider_WholeParetoSet ^> (provider);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
TestDoubleCollValues::TestDoubleCollValues(ObservedValuesCollection ^inCollToSave, ITestDoubleCollProvider ^provider)
{
	_ValCollection = inCollToSave;
	_Provider = provider;
}
String^		TestDoubleCollValues::getRequestedValueProviderClassName()
{
	return "ITestDoubleCollProvider";
}
String^		TestDoubleCollValues::getObserverDesc()
{
	return "TestDoubleCollValues";
}
void	TestDoubleCollValues::Save(ObservedValueTag ^inTag)
{
	array<double>	 ^a = gcnew array<double>(3);
	a[0] = _Provider->getValue1();
	a[1] = _Provider->getValue2();
	a[2] = _Provider->getValue3();

	_ValCollection->Add(a, inTag); 
}
void	TestDoubleCollValues::setCollection(ObservedValuesCollection ^inCollToSave)
{
	_ValCollection = inCollToSave;
}
void	TestDoubleCollValues::setProvider(IIntermediateValueProvider ^provider)
{
	_Provider = dynamic_cast<ITestDoubleCollProvider ^> (provider);
}


