#include "StdAfx.h"
#include "GAHelpers.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//////////////////////////////////////////////////////////////////////////
Population::Population() : ISolutionContainer()
{
}

Population^		Population::operator=(Population ^b)
{
	// this populacija se bri�e
	Destroy();

	setSize(b->getSize());
	for( int i=0; i<b->getSize(); i++ )
	{
		ISolution^	newInd = b->getIndividual(i)->Clone();
		setIndividual(i, newInd);
	}

	return this;
}

Population^		Population::Clone()
{
	return gcnew Population();
}
void	Population::pruneExcessIndividuals(int inRequiredPopSize)
{
}

//////////////////////////////////////////////////////////////////////////
PopulationSubset::PopulationSubset()
{
	_vecChildren = new vector<gcroot<ISolution^> >	();
}

ISolution^	 PopulationSubset::getChild(int Index)
{
	return (*_vecChildren)[Index];
}
void	PopulationSubset::createChildren(ISolution^ Prototype, int Num)
{
	_NumChildren = Num;
	_vecChildren->resize(Num);

	for( int i=0; i<Num; i++ )
		(*_vecChildren)[i] = Prototype->Clone();
}
int		PopulationSubset::getNumCreated()
{
	return _NumChildren;
}
void	PopulationSubset::copyToPopulation(Population ^Pop)
{
	for( int i=0; i<getNumCreated(); i++ )
	{
		Pop->addNewSolution(getChild(i));
		(*_vecChildren)[i] = 0;										// poni�tavamo referencu
	}
}

void		PopulationSubset::transferToPopulation(int ChildInd, Population ^Pop)
{
	Pop->addNewSolution(getChild(ChildInd));
	(*_vecChildren)[ChildInd] = 0;
}
