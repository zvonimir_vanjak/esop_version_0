#pragma once

#include "IntermediateResultsProviderInterfaces.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			/*******************************************************************************/
			public ref class IntermedTimerOnEveryIterNum : public IIntermediateTimer
			{
			public:
				IntermedTimerOnEveryIterNum();

				virtual void	SetParameter(int par);
				virtual	bool	IsTimeToSave(ObservedValueTag ^inTag);

			private:
				int		_SaveEveryIterNum;
				int		_LastIterNumSaved;
			};

			public ref class IntermedTimerOnTimeInterval : public IIntermediateTimer
			{
			public:
				IntermedTimerOnTimeInterval();

				virtual void	SetParameter(int par);
				virtual	bool	IsTimeToSave(ObservedValueTag ^inTag);

			private:
				int		_SaveEveryMs;
				int		_LastTimeSaved;
			};

			/*******************************************************************************/
			public ref class CurrObjectiveValueSaver : public IObservedValueSaver
			{
			public:
				CurrObjectiveValueSaver() {}
				CurrObjectiveValueSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_CurrentObjectiveValue ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection					^_ValCollection;
				IIntermedValueProvider_CurrentObjectiveValue		^_Provider;
			};

			public ref class AverageObjectiveValueInPopulationSaver : public IObservedValueSaver
			{
			public:
				AverageObjectiveValueInPopulationSaver() {}
				AverageObjectiveValueInPopulationSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_CurrentAverageObjective ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection							^_ValCollection;
				IIntermedValueProvider_CurrentAverageObjective		^_Provider;
			};

			public ref class BestFitnessValueInPopulation : public IObservedValueSaver
			{
			public:
				BestFitnessValueInPopulation() {}
				BestFitnessValueInPopulation(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_BestFitnessInCurrPopulation ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection					^_ValCollection;
				IIntermedValueProvider_BestFitnessInCurrPopulation		^_Provider;
			};

			public ref class WorstFitnessValueInPopulation : public IObservedValueSaver
			{
			public:
				WorstFitnessValueInPopulation() {}
				WorstFitnessValueInPopulation(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_WorstFitnessInCurrPopulation ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection					^_ValCollection;
				IIntermedValueProvider_WorstFitnessInCurrPopulation		^_Provider;
			};

			public ref class AverageFitnessValueInPopulation : public IObservedValueSaver
			{
			public:
				AverageFitnessValueInPopulation() {}
				AverageFitnessValueInPopulation(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_AverageFitnessInCurrPopulation ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection					^_ValCollection;
				IIntermedValueProvider_AverageFitnessInCurrPopulation		^_Provider;
			};

			public ref class TestDoubleCollValues : public IObservedValueSaver
			{
			public:
				TestDoubleCollValues() {}
				TestDoubleCollValues(ObservedValuesCollection ^inCollToSave, ITestDoubleCollProvider ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection		^_ValCollection;
				ITestDoubleCollProvider			^_Provider;
			};

			public ref class WholePopulationSaver : public IObservedValueSaver
			{
			public:
				WholePopulationSaver() {}
				WholePopulationSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_WholePopulation ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection		^_ValCollection;
				IIntermedValueProvider_WholePopulation					^_Provider;
			};

			public ref class PopulationConvergenceMeasure : public IObservedValueSaver
			{
			public:
				PopulationConvergenceMeasure() {}
				PopulationConvergenceMeasure(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_PopulationConvergenceMeasure ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection					^_ValCollection;
				IIntermedValueProvider_PopulationConvergenceMeasure		^_Provider;
			};

			public ref class WholeParetoSetSaver : public IObservedValueSaver
			{
			public:
				WholeParetoSetSaver() {}
				WholeParetoSetSaver(ObservedValuesCollection ^inCollToSave, IIntermedValueProvider_WholeParetoSet ^provider);

				virtual String^		getRequestedValueProviderClassName();
				virtual String^		getObserverDesc();
				virtual	void			Save(ObservedValueTag ^inTag);

				virtual void	setCollection(ObservedValuesCollection ^inCollToSave);
				virtual void	setProvider(IIntermediateValueProvider ^provider);

			private:
				ObservedValuesCollection		^_ValCollection;
				IIntermedValueProvider_WholeParetoSet					^_Provider;
			};
		}
	}
}