#pragma once

#include <math.h>
#include <assert.h>
#include <vector>
#include <vcclr.h>

//#include "..\..\Base\BaseSolutions.h"

using std::vector;
using namespace ESOP::Framework::Base;

namespace ESOP
{
	namespace Framework
	{
		namespace Algorithms
		{
			namespace GeneticAlgorithms
			{
				public ref class Population : public ISolutionContainer
				{
				public:
					Population();

					Population^		operator=(Population ^b);

					void					TransferSolutions(Population ^otherPopulation) {}

					Population^		Clone();
					void					pruneExcessIndividuals(int inRequiredPopSize);
				};

				public ref class PopulationSubset
				{
				public:
					PopulationSubset();

					void				createChildren(ISolution^ Prototype, int Num);
					int					getNumCreated();
					ISolution^	getChild(int Index);
					void				copyToPopulation(Population ^Pop);
					void				transferToPopulation(int ChildInd, Population ^Pop);

				private:
					int			_NumChildren;
					vector<gcroot<ISolution^> >	*_vecChildren;
				};
			}
		}
	}
}