#pragma once

#include "GAHelpers.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			public interface class IIntermedValueProvider_CurrentObjectiveValue : IIntermediateValueProvider
			{
			public:
				virtual	double		getCurrObjectiveValue() = 0;
			};

			public interface class IIntermedValueProvider_CurrentAverageObjective : IIntermediateValueProvider
			{
			public:
				virtual	double		getCurrAverageObjectiveValue() = 0;
			};

			public interface class IIntermedValueProvider_BestFitnessInCurrPopulation : IIntermediateValueProvider
			{
			public:
				virtual	double		getCurrBestFitnessValue() = 0;
			};

			public interface class IIntermedValueProvider_AverageFitnessInCurrPopulation : IIntermediateValueProvider
			{
			public:
				virtual	double		getCurrAverageFitnessValue() = 0;
			};

			public interface class IIntermedValueProvider_WorstFitnessInCurrPopulation : IIntermediateValueProvider
			{
			public:
				virtual	double		getCurrWorstFitnessValue() = 0;
			};

			public interface class IIntermedValueProvider_CurrentConstraintViolation : IIntermediateValueProvider
			{
			public:
				virtual	int			getNumConstraints() = 0;
				virtual	int			getNumViolatedConstraints() = 0;
				virtual	double	getAmountViolatedConstraints() = 0;
			};

			public interface class IIntermedValueProvider_PopulationConvergenceMeasure : IIntermediateValueProvider
			{
			public:
				virtual	double		getPopulation() = 0;
			};

			// daje cijelu populaciju
			public interface class IIntermedValueProvider_WholePopulation : IIntermediateValueProvider
			{
			public:
				virtual	Population^		getPopulation() = 0;
			};

			public interface class IIntermedValueProvider_WholeParetoSet : IIntermediateValueProvider
			{
			public:
				virtual	array<ISolutionMO^>^	getParetoSet() = 0;
			};

			public interface class ITestDoubleCollProvider : IIntermediateValueProvider
			{
			public:
				virtual	int			getValue1() = 0;
				virtual	int			getValue2() = 0;
				virtual	double	getValue3() = 0;
			};
		}
	}
}