#pragma once

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			/*******************************************************************************/
			public ref class TerminateOnIterationNum : public IOptimizationTerminator
			{
			public:
				TerminateOnIterationNum();

				virtual String^	GetRequestedValueProviderClassName();
				virtual String^	GetTerminatorDesc();
				virtual void		SetParameter(int par);

				virtual	bool		IsTermConditionSatisfied(IterativeAlgorithm ^CurrContext);

			private:
				int		_TotalIterNum;
			};

			public ref class TerminateOnDuration : public IOptimizationTerminator
			{
			public:
				TerminateOnDuration();

				virtual String^	GetRequestedValueProviderClassName();
				virtual String^	GetTerminatorDesc();
				virtual void		SetParameter(int par);

				virtual	bool		IsTermConditionSatisfied(IterativeAlgorithm ^CurrContext);

			private:
				int		_TotalDuration;
			};
		}
	}
}