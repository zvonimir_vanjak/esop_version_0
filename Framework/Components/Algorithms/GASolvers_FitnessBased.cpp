#include "StdAfx.h"

#include "GASolvers_FitnessBased.h"

//#include "Components\Representations\StandardRepresentations.h"
#include "..\..\Components\Operators\RealAsBitArrayOperators.h"
#include "..\..\Components\Operators\SelectionOperators.h"
#include "..\..\Components\Operators\FitnessAssignmentOperators.h"

using namespace System;
using namespace System::Text;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

//using namespace ESOP::Framework::StandardRepresentations;

/*******************************************************************************************/

FitnessBasedGA::FitnessBasedGA()
{
	_PopSize = 10;

	_Population = gcnew Population();
	_arrFitness = gcnew array<double>(_PopSize);
}
void		FitnessBasedGA::setProblemInstance(IProblem ^inProblem)
{
	_Problem = inProblem;
}
void		FitnessBasedGA::setSolutionInstance(ISolution ^inSolutionInstance)
{
	_SolutionInstance = inSolutionInstance;
}
void	FitnessBasedGA::setESOPDesignVariable(IESOPDesignVariable ^DesVar)
{
	if( _SolutionInstance == nullptr )
		_SolutionInstance = gcnew ISingleObjectiveSolution();

	_SolutionInstance->setDesignVar(dynamic_cast<IDesignVariable ^>(DesVar));
}
void	FitnessBasedGA::setESOPProblemInstance(IESOPProblem ^ProbInst)
{
	setProblemInstance(dynamic_cast<ISingleObjectiveProblem ^>(ProbInst));
}

Population^		FitnessBasedGA::getPopulation() 
{ 
	return _Population; 
}

int		FitnessBasedGA::Initialize()
{

	_Population->Initialize(_PopSize, _SolutionInstance);		

	// i sada treba svakoj kreiranoj jedinki postaviti vrijednosti funkcije cilja
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISolution		^SolPop = dynamic_cast<ISolution	^>(_Population->getIndividual(i));
		SolPop->UpdateObjectiveValue();
	}

	return 0;
}

void	FitnessBasedGA::setPopulationSize(int inSize)
{
	_PopSize = inSize;
	_arrFitness = gcnew array<double>(_PopSize);
}
int		FitnessBasedGA::getPopulationSize()
{
	return _PopSize;
}

void	FitnessBasedGA::setFitnessAssignmentOp(IFitnessAssignmentOperator	^inFitAssign)
{
	_FitnessAssignment = inFitAssign;
}

void	FitnessBasedGA::setSelectionOp(IFitnessBasedSelectionOperator	^inSelector)
{
	_Selector = inSelector;
}
void	FitnessBasedGA::setRecombinationOp(ICrossoverOperator2To2	^inRecombinator)
{
	_Recombinator = inRecombinator;
}
void	FitnessBasedGA::setMutationOp(IMutationOp	^inMutator)
{
	_Mutator = inMutator;
}
void	FitnessBasedGA::Run(OptimizationResult^ outRes)
{
	IterativeAlgorithm::Run(outRes);

	// treba na�i najbolje rje�enje
	// ajde sad jo� na�i najbolju jedinku i ispi�i njenu vrijednost
	int			ind = 0;
	double	max = -1000000;
	for( int i=0; i<_Population->getSize(); i++ )
	{
		ISingleObjectiveSolution ^sol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(i));

		if( sol->getCurrObjectiveValue() > max)
		{
			max = sol->getCurrObjectiveValue();
			ind = i;
		}
	}

	ISingleObjectiveSolution ^FinalSol = dynamic_cast<ISingleObjectiveSolution ^> (_Population->getIndividual(ind));
	SOResult ^finalRes = gcnew SOResult();

	finalRes->setResult(FinalSol);
	outRes->SetFinalResult(finalRes);

	outRes->_ProblemName		= _Problem->ToString();
	outRes->_Representation = FinalSol->getDesignVar()->ToString();
	outRes->_Result					= finalRes->GetResultStringRepresentation();
	outRes->_TotalIterNum		= getCurrIterNum();
	outRes->_DurationMs			= getCurrTimeSpanInMs();
}

double		FitnessBasedGA::getCurrObjectiveValue() 
{ 
	return 0;
}

double		FitnessBasedGA::getCurrBestFitnessValue()
{
	return 0;
}
double		FitnessBasedGA::getCurrAverageFitnessValue()
{
	return 0;
}
double		FitnessBasedGA::getCurrWorstFitnessValue()
{
	return 0;
}