#include "stdafx.h"
#include "RealFunction_SolutionObjects.h"

using namespace ESOP::Framework::StandardProblems::RealFunctionOptimization;

/********************************************************************************************************************/
/*
RealFunction_SingleRealVar_SolObject::RealFunction_SingleRealVar_SolObject(double inLow, double	inUp)  
{
	_Low = inLow;
	_Upp = inUp;
}
RealFunction_SingleRealVar_SolObject::RealFunction_SingleRealVar_SolObject(RealFunction_SingleRealVar_SolObject &copy) 
	: ISingleObjectiveSolution(*(static_cast<ISingleObjectiveSolution *> (&copy)))
{
	_CurrObjectiveValue = copy._CurrObjectiveValue;
	_Problem = copy._Problem;
	_CurrSol = copy._CurrSol->Clone();

	_Low = copy._Low;
	_Upp = copy._Upp;
}
CString* RealFunction_SingleRealVar_SolObject::getComponentName() 
{ 
	return "RealFunctionOpt"; 
}
ISolution*	RealFunction_SingleRealVar_SolObject::Clone()
{
	return new RealFunction_SingleRealVar_SolObject(*this);
}
void		RealFunction_SingleRealVar_SolObject::setRepresentationObject(IDesignVariable *inRepr)
{
	_CurrSol = inRepr;
}
*/
/********************************************************************************************************************/
MultiObjectiveRealFunc_SingleVar_SolObject::MultiObjectiveRealFunc_SingleVar_SolObject()
{
//	_pOptObject = 0;
}

MultiObjectiveRealFunc_SingleVar_SolObject::MultiObjectiveRealFunc_SingleVar_SolObject(MultiObjectiveRealFunc_SingleVar_SolObject ^copy) : IMultiObjectiveSolution(copy)
{
//	_pOptObject  = copy._pOptObject;
	_CurrFitness = copy->_CurrFitness;

	_pRepresentation = dynamic_cast<RealAsBitArray ^> (copy->Clone());

	this->GenerateRandom();
/*
	_vecCurrObjectiveVal->resize(copy->_vecCurrObjectiveVal->size());
	for( int i=0; i<(int) copy->_vecCurrObjectiveVal->size(); i++ )
		_vecCurrObjectiveVal[i] = copy->_vecCurrObjectiveVal[i];
*/
}

IRepresentation^	MultiObjectiveRealFunc_SingleVar_SolObject::GetRepresentation()
{
	return _pRepresentation;
}
double		MultiObjectiveRealFunc_SingleVar_SolObject::getFitness() 
{ 
	return _CurrFitness; 
}

void	MultiObjectiveRealFunc_SingleVar_SolObject::setFitness(double inFitness)
{
	_CurrFitness = inFitness;
}

/********************************************************************************************************************/
MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr()
{
}

MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr(MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr ^copy) : IMultiObjectiveSolution(copy)
{
	_pRepresentation = dynamic_cast<RealArray_AsBitArray ^> (copy->Clone());
/*
	_vecCurrObjectiveVal.resize(copy->_vecCurrObjectiveVal.size());
	for( int i=0; i<(int) copy->_vecCurrObjectiveVal.size(); i++ )
		_vecCurrObjectiveVal[i] = copy->_vecCurrObjectiveVal[i];
*/
}
/*
void	MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::updateObjectiveValues()
{
	_Problem->calcObjective(this, _vecCurrObjectiveVal);
}

vector<double> &	MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::getCurrObjectiveValues()
{
	return *_vecCurrObjectiveVal;
}
*/
int		MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::getNumObjectives()
{
	return _Problem->getNumObjectives();
}
/*
void	MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::setMultiObjectiveProblem(IMultiObjectiveProblem ^inProblem)
{
	_Problem = inProblem;
	_vecCurrObjectiveVal->resize(_Problem->getNumObjectives());
}
*/
IRepresentation^	MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr::GetRepresentation()
{
	return _pRepresentation;
}
