// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class BitArray : public IRepresentation
			{
			public:
				BitArray();
				BitArray(const BitArray ^copy);

				BitArray^	operator=(const BitArray ^b);

				virtual String^		getComponentName();
				virtual void			GenerateRandom();
				virtual IRepresentation^	Clone();

				void		setRepr(const vector<bool> *inRepr) { *_vecBitsRepr = *inRepr; }
				vector<bool>*		getRepr() ;
				
				bool		getReprBit(int BitNum);
				void		setReprBit(int Ind, bool Val);

				void		FlopBit(int BitNum);

				void		setBitNum(int inBitNum);
				int			getBitNum();

				String^	getBits();

			public:
				vector<bool>		*_vecBitsRepr;
			};

			public ref class Real : public IReal  , public IRepresentation, public IESOPDesignVariable
			{
			public:
				Real();
				Real(double inLow, double inUp);
				Real(const Real ^copy);
				
				virtual String^		getComponentName();
				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IRepresentation^	GetRepresentation();

				virtual int				getParamNum() { return 2; }
				virtual String^		getParamName(int ParamInd) {
					if( ParamInd == 0 ) return "Low bound";
					else return "Upp bound";
				}
				virtual void			setParamValue(int Ind, double inVal) {
					if( Ind == 0 ) _Low = inVal;
					else _Up = inVal;
				}
				virtual double		getParamValue(int Ind) {
					if( Ind == 0 ) return _Low;
					else return _Up;
				}

				virtual	double		getValue();
				virtual	void			setValue(double inVal);

				void			setBoundary(double Low, double Up);
				double		getUp()		{ return _Up; }
				double		getLow()	{ return _Low; }

			protected:
				double	_Value;
				double	_Low, _Up;
			};
			
			public ref class RealAsBitArray : public Real
			{
			public:
				RealAsBitArray();
				RealAsBitArray(double inLow, double inUp);
				RealAsBitArray(const RealAsBitArray ^copy);

				RealAsBitArray^	operator=(const RealAsBitArray ^b);

				virtual		String^		getComponentName() override;
				virtual		void			GenerateRandom() override;
				virtual		IDesignVariable^	Clone() override;
				virtual		IRepresentation^	GetRepresentation() override;

				void		setBitNum(int inBitNum);
				int			getBitNum();
				void		setVarMinResolution	(double inMinRes);

				void		setVarBounds(double inLow, double inUpp);
				double	getLowBound(); 
				double	getUppBound();

				virtual void		setValue(double inVal) override;

				BitArray^		getRepr();
				void				setRepr(const vector<bool> *inRepr);
				bool				getReprBit(int BitNum);
				void				setReprBit(int Ind, bool Val);

				void		FlopBit(int BitNum);

//				friend class RealArray_AsBitArray;

			public:
				void		adjustValueToBitRepr();			// kad se promijeni bit reprezentacija, poziva se da podesi realna vrijednost
				void		adjustReprToValue();				// kad se promijeni vrijednost, poziva se da se podesi reprezentacija

				BitArray	^_BitsRepr;
			};

			public ref class RealArray : public IRepresentation , public IRealArray, public IESOPDesignVariable
			{
			public:
				RealArray(int VarNum);

				virtual	String^		getComponentName() ;
				virtual	void			GenerateRandom() ;
				virtual	IDesignVariable^	Clone() ;
				virtual	IRepresentation^	GetRepresentation() ;

				virtual int				getParamNum() { return 3; }
				virtual String^		getParamName(int ParamInd) {
					if( ParamInd == 0 ) return "Num var";
					else if( ParamInd == 1 ) return "Low bound";
					else return "Upp bound";
				}
				virtual void			setParamValue(int Ind, double inVal) {
					if( Ind == 0 ) setVarNum((int) inVal);
				}
				virtual double		getParamValue(int Ind) {
					if( Ind == 0 ) return getVarNum();
					else return 0;
				}

				int			getVarNum();
				void		setVarNum(int inVarNum);

				virtual double	getValue(int VarInd) ;
				virtual void		setValue(int VarInd, double inVal) ;

				void		setVarBounds(int VarInd, double inLow, double inUpp);
				double	getLowBound(int VarInd) ;
				double	getUppBound(int VarInd) ;

			public:
				vector<double>	*_vecValues;
				vector<double>	*_vecLow, *_vecUp;
			};

			public ref class RealArray_AsBitArray : public IRepresentation , public IRealArray, public IESOPDesignVariable
			{
			public:
				RealArray_AsBitArray(int VarNum);
				RealArray_AsBitArray(RealArray_AsBitArray ^copy);

				virtual	String^		getComponentName();
				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IRepresentation^	GetRepresentation();

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return " ";}
				virtual void			setParamValue(int Ind, double inVal) { }
				virtual double		getParamValue(int Ind) { return 0; }

				int			getVarNum() ;

				virtual	double	getValue(int VarInd);
				virtual	void		setValue(int VarInd, double inVal);

				void		setVarBounds(int VarInd, double inLow, double inUpp);
				double	getLowBound(int VarInd) ;
				double	getUppBound(int VarInd) ;

				RealAsBitArray^		getRepr(int Ind);
				bool		getReprBit(int VarInd, int BitNum);
				void		setReprBit(int VarInd, int Ind, bool Val);

				void		FlopBit(int VarInd, int BitNum);

				void		setBitNum(int VarInd, int inBitNum);
				int			getBitNum(int VarInd);
				void		setVarMinResolution(int VarInd, double inMinRes);

//				String^	getBits();
			private:
				void		adjustValueToBitRepr();			// kad se promijeni bit reprezentacija, poziva se da podesi realna vrijednost
				void		adjustReprToValue();				// kad se promijeni vrijednost, poziva se da se podesi reprezentacija

				vector<gcroot<RealAsBitArray ^> > *_vecRepr;
			};

			public ref class Integer : public IRepresentation, public IInteger, public IESOPDesignVariable
			{
			public:
				Integer();
				Integer(int inLow, int inUp);
				Integer(Integer ^copy);

				virtual	String^		getComponentName();
				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IRepresentation^	GetRepresentation();

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return " ";}
				virtual void			setParamValue(int Ind, double inVal) { }
				virtual double		getParamValue(int Ind) { return 0; }

				virtual int		getValue();
				virtual void	setValue(int inVal);

			public:
				int		_Value;
				int		_Low, _Up;
			};

			public ref class IntegerArray : public IRepresentation, public IIntegerArray, public IESOPDesignVariable
			{
			public:
				IntegerArray(int VarNum);
				IntegerArray(int VarNum, int AllLow, int AllUp);
				IntegerArray(IntegerArray ^copy);

				virtual	String^		getComponentName();
				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IRepresentation^	GetRepresentation();

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return " ";}
				virtual void			setParamValue(int Ind, double inVal) { }
				virtual double		getParamValue(int Ind) { return 0; }

				Integer^		getVar(int Ind);

				virtual int		getVarNum();
				virtual int		getValue(int Ind);
				virtual void	setValue(int Ind, int inVal);

			public:
				vector<gcroot<Integer ^> >		*_vecValues;
			};

			public ref class Permutation : public IRepresentation, public IPermutation, public IESOPDesignVariable
			{
			public:
				Permutation();
				Permutation(Permutation ^copy);

				virtual	String^		getComponentName();
				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IRepresentation^	GetRepresentation();

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return " ";}
				virtual void			setParamValue(int Ind, double inVal) { }
				virtual double		getParamValue(int Ind) { return 0; }

			};
		}
	}
}

