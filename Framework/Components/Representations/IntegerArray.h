// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

#include "Integer.h"

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class IntegerArray : public IIntegerArray, public IESOPDesignVariable
			{
			public:
				IntegerArray() { setVarNum(1); }
				IntegerArray(int VarNum);
				IntegerArray(int VarNum, int AllLow, int AllUp);
				IntegerArray(IntegerArray ^copy);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int ParamInd, double inVal);
				virtual double		getParamValue(int ParamInd);

				Integer^			getVar(int Ind);

				virtual int		getVarNum();
				virtual void	setVarNum(int inVarNum);
				virtual int		getValue(int Ind);
				virtual void	setValue(int Ind, int inVal);

				void		setVarBounds(int VarInd, int inLow, int inUpp);
				int			getLowBound(int VarInd) ;
				int			getUppBound(int VarInd) ;
				void		setLowBound(int VarInd, int inLow) ;
				void		setUppBound(int VarInd, int inUpp) ;

			public:
				vector<gcroot<Integer ^> >		*_vecRepr;
			};

		}
	}
}

