// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class RealArray : public IRealArray, public IESOPConstructedDesignVariable
			{
			public:
				RealArray();
				RealArray(int VarNum);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int ParamInd, double inVal);
				virtual double		getParamValue(int ParamInd);

				virtual int				getConstructorParamNum();
				virtual String^		getConstructorParamName(int ParamInd);
				virtual void			setConstructorParamValue(int ParamInd, double inVal);
				virtual double		getConstructorParamValue(int ParamInd);

				int			getVarNum();
				void		setVarNum(int inVarNum);

				virtual double	getValue(int VarInd) ;
				virtual void		setValue(int VarInd, double inVal) ;

				void		setVarBounds(int VarInd, double inLow, double inUpp);
				double	getLowBound(int VarInd) ;
				double	getUppBound(int VarInd) ;
				void		setLowBound(int VarInd, double inLow) ;
				void		setUppBound(int VarInd, double inUpp) ;

			public:
				vector<double>	*_vecValues;
				vector<double>	*_vecLow, *_vecUp;
			};

		}
	}
}

