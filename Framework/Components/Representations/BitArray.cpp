// This is the main DLL file.

#include "stdafx.h"

#include "BitArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
BitArray::BitArray()
{
	_vecBitsRepr = new vector<bool>();
}
BitArray::BitArray(const BitArray ^copy)
{
	_vecBitsRepr = new vector<bool>();
	_vecBitsRepr->resize(copy->_vecBitsRepr->size());

	for( int i=0; i<(int) copy->_vecBitsRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*copy->_vecBitsRepr)[i];
}
BitArray^	BitArray::operator=(const BitArray ^copy)
{
//	*_vecBitsRepr = *(copy->_vecBitsRepr);
	_vecBitsRepr->resize(copy->_vecBitsRepr->size());

	for( int i=0; i<(int) copy->_vecBitsRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*copy->_vecBitsRepr)[i];
	return this;
}
void	BitArray::GenerateRandom()
{
	for( int i=0; i<(int) _vecBitsRepr->size(); i++ )
	{
		if( rand() % 2 == 0 )
			(*_vecBitsRepr)[i] = true;
		else
			(*_vecBitsRepr)[i] = false;
	}
}
IDesignVariable^	BitArray::Clone()
{
	return gcnew BitArray(this);
}
String^	BitArray::GetValueStringRepresentation()
{ 
	return getBits(); 
}
void		BitArray::setBitNum(int inBitNum)
{
	_vecBitsRepr->resize(inBitNum);
}
int			BitArray::getBitNum()
{
	return (int) _vecBitsRepr->size();
}
void		BitArray::setReprBit(int Ind, bool Val)
{
	(*_vecBitsRepr)[Ind] = Val;
}
bool		BitArray::getReprBit(int BitNum)
{
	return (*_vecBitsRepr)[BitNum];
}
vector<bool>*	BitArray::getRepr()
{
	return _vecBitsRepr;
}
void		BitArray::setRepr(const vector<bool> *inRepr) 
{ 
	_vecBitsRepr->resize(inRepr->size());

	for( int i=0; i<(int) inRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*inRepr)[i];

//	*_vecBitsRepr = *inRepr; 
}
void		BitArray::FlopBit(int BitNum)
{
	if( (*_vecBitsRepr)[BitNum] == false )
		(*_vecBitsRepr)[BitNum] = true;
	else
		(*_vecBitsRepr)[BitNum] = false;
}
String^		BitArray::getBits()
{
	String^		out = "";

	for( int j=0; j<(int) _vecBitsRepr->size(); j++ )
		if( (*_vecBitsRepr)[j] == true )
			out += "1";
		else
			out += "0";

	return out;
}

