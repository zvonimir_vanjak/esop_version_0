// This is the main DLL file.

#include "stdafx.h"

#include "RealArrayAsBitArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/

RealArray_AsBitArray::RealArray_AsBitArray()
{
	setVarNum(5);
}
RealArray_AsBitArray::RealArray_AsBitArray(int VarNum) 
{
	setVarNum(VarNum);
}
RealArray_AsBitArray::RealArray_AsBitArray(RealArray_AsBitArray ^copy) 
{
	_vecRepr = new vector<gcroot<RealAsBitArray ^> >();
	_vecRepr->resize(copy->getVarNum());
	for( int i=0; i<copy->getVarNum(); i++ )
	{
		(*_vecRepr)[i] = (RealAsBitArray ^) (*copy->_vecRepr)[i]->Clone();
	}
}
RealArray_AsBitArray::~RealArray_AsBitArray()
{
	delete _vecRepr;
}

IDesignVariable^	RealArray_AsBitArray::Clone()
{
	return gcnew RealArray_AsBitArray(this);
}
void	RealArray_AsBitArray::GenerateRandom()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
		(*_vecRepr)[i]->GenerateRandom();
}

String^		RealArray_AsBitArray::GetStringDesc()
{
	return "RealArray_AsBitArray";
}
String^		RealArray_AsBitArray::GetValueStringRepresentation()
{
	StringBuilder ^ret = gcnew StringBuilder();

	for( int i=0; i<getVarNum(); i++ )
	{
		ret->Append("x[");
		ret->Append(i);
		ret->Append("] = ");
		ret->Append(getRepr(i)->getValue());
		ret->Append(" ;  ");
	}
	return ret->ToString();
}

int				RealArray_AsBitArray::getParamNum() 
{ 
	return getVarNum() * 2; 
}
String^		RealArray_AsBitArray::getParamName(int ParamInd) 
{
	String ^strInd = gcnew String("");
	strInd->Format("%d", ParamInd / 2);
	
	if( ParamInd % 2 == 0 ) return "Low bound " + strInd;
	else return "Upp.bound " + strInd;
}
void			RealArray_AsBitArray::setParamValue(int ParamInd, double inVal) 
{
	if( ParamInd % 2 == 0 ) setLowBound(ParamInd / 2, inVal);
	else setUppBound(ParamInd / 2, inVal);
}
double		RealArray_AsBitArray::getParamValue(int ParamInd) 
{
	if( ParamInd % 2 == 0 ) return getLowBound(ParamInd / 2);
	else return getUppBound(ParamInd / 2);
}

int				RealArray_AsBitArray::getConstructorParamNum() { return 1; }
String^		RealArray_AsBitArray::getConstructorParamName(int ParamInd) {	return "Num var";}
void			RealArray_AsBitArray::setConstructorParamValue(int ParamInd, double inVal) {	setVarNum((int) inVal);}
double		RealArray_AsBitArray::getConstructorParamValue(int ParamInd) {	return getVarNum();}

void		RealArray_AsBitArray::setBitNum(int VarInd, int inBitNum)
{
	(*_vecRepr)[VarInd]->getRepr()->setBitNum(inBitNum);
}
int			RealArray_AsBitArray::getBitNum(int VarInd )
{
	return (int) (*_vecRepr)[VarInd]->getRepr()->getBitNum();
}
void		RealArray_AsBitArray::setVarMinResolution(int VarInd, double inMinRes)
{
	double		IntervalWidth = (*_vecRepr)[VarInd]->getUp() - (*_vecRepr)[VarInd]->getLow();
	double		NumDiff = IntervalWidth / inMinRes;

	int		BitNum = 1;
	while( NumDiff > 1 )
	{
		NumDiff /= 2.0;
		BitNum++;
	}

	(*_vecRepr)[VarInd]->getRepr()->setBitNum(BitNum);
}

int	RealArray_AsBitArray::getVarNum() 
{
	return (int) _vecRepr->size();
}
void	RealArray_AsBitArray::setVarNum(int inVarNum) 
{
	_vecRepr = new vector<gcroot<RealAsBitArray ^> >();
	_vecRepr->resize(inVarNum);

	for(int i=0; i<inVarNum; i++ )
		(*_vecRepr)[i] = gcnew RealAsBitArray();
}

double	RealArray_AsBitArray::getValue(int VarInd) 
{ 
	return (*_vecRepr)[VarInd]->getValue(); 
}
void		RealArray_AsBitArray::setValue(int VarInd, double	inVal)
{
	(*_vecRepr)[VarInd]->setValue(inVal); 
//	adjustReprToValue();
}

void		RealArray_AsBitArray::setVarBounds(int VarInd, double inLow, double inUpp)	{	(*_vecRepr)[VarInd]->setVarBounds(inLow, inUpp);}
double	RealArray_AsBitArray::getLowBound(int VarInd)		{	return (*_vecRepr)[VarInd]->getLowBound();}
double	RealArray_AsBitArray::getUppBound(int VarInd)		{	return (*_vecRepr)[VarInd]->getUppBound();}
void		RealArray_AsBitArray::setLowBound(int VarInd, double inLow)	{	(*_vecRepr)[VarInd]->setLowBound(inLow);}
void		RealArray_AsBitArray::setUppBound(int VarInd, double inUpp)	{	(*_vecRepr)[VarInd]->setUppBound(inUpp);}

RealAsBitArray^	RealArray_AsBitArray::getRepr(int VarInd )
{
	return (*_vecRepr)[VarInd];
}
void		RealArray_AsBitArray::setReprBit(int VarInd, int Ind, bool Val)
{
	(*_vecRepr)[VarInd]->getRepr()->setReprBit(Ind,Val);

	adjustValueToBitRepr();
}
bool		RealArray_AsBitArray::getReprBit(int VarInd, int BitNum)
{
	return (*_vecRepr)[VarInd]->getRepr()->getReprBit(BitNum);
}

void		RealArray_AsBitArray::FlopBit(int VarInd, int BitNum)
{
	if( (*_vecRepr)[VarInd]->getRepr()->getReprBit(BitNum) == false )
		(*_vecRepr)[VarInd]->getRepr()->setReprBit(BitNum,true);
	else
		(*_vecRepr)[VarInd]->getRepr()->setReprBit(BitNum,false);
}

void		RealArray_AsBitArray::adjustValueToBitRepr()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
	{
		// iz BitArraya idemo dobiti x
		long	NewIntValue = 0;
		BitArrayToInt((*_vecRepr)[i]->getRepr()->getRepr(), NewIntValue);

		// sada to treba pretvoriti u double unutar danog intervala
		long	MaxIntValue = (1 << (*_vecRepr)[i]->getRepr()->getBitNum()) - 1;
		(*_vecRepr)[i]->setValue( ((*_vecRepr)[i]->getUp() - (*_vecRepr)[i]->getLow()) * NewIntValue / MaxIntValue  + (*_vecRepr)[i]->getLow() );
	}
}
void		RealArray_AsBitArray::adjustReprToValue()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
	{
		// konvertiramo x u BitArray
		int		BitNum = (int) (*_vecRepr)[i]->getRepr()->getBitNum();
		long	MaxIntValue = (1 << BitNum) - 1;

		long	IntX = (long) (MaxIntValue * ((*_vecRepr)[i]->getValue() - (*_vecRepr)[i]->getLow()) / ((*_vecRepr)[i]->getUp() - (*_vecRepr)[i]->getLow()) );

		vector<bool>	*newRepr = new vector<bool>;
		IntToBitArray(IntX, BitNum, newRepr);

		// TODO
	}
}
