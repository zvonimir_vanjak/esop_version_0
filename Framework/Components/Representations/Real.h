// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class Real : public IReal, public IESOPDesignVariable
			{
			public:
				Real();
				Real(double inLow, double inUp);
				Real(const Real ^copy);
				
				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc() ;
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int Ind, double inVal);
				virtual double		getParamValue(int Ind);

				virtual	double		getValue();
				virtual	void			setValue(double inVal);

				void			setBoundary(double Low, double Up);
				double		getUp();
				double		getLow();

			protected:
				double	_Value;
				double	_Low, _Up;
			};
		}
	}
}

