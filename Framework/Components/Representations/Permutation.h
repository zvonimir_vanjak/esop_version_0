// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class Permutation : public IPermutation, public IESOPDesignVariable
			{
			public:
				Permutation();
				Permutation(Permutation ^copy);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IDesignVariable^	GetRepresentation();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int Ind, double inVal);
				virtual double		getParamValue(int Ind);

				void						setNum(int inNum);
				int							getNum();
				virtual int			getNth(int i);

			private:
				int						_Num;
				array<int, 1> ^_arrOrder;
			};
		}
	}
}

