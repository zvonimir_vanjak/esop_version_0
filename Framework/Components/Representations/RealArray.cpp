// This is the main DLL file.

#include "stdafx.h"

#include "RealArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
RealArray::RealArray() 
{ 
	_vecValues = new vector<double>();
	_vecLow		= new vector<double>();
	_vecUp		= new vector<double>();

	setVarNum(1); 
}
RealArray::RealArray(int VarNum) 
{
	_vecValues = new vector<double>();
	_vecLow  = new vector<double>();
	_vecUp		= new vector<double>();

	setVarNum(VarNum);
}
IDesignVariable^	RealArray::Clone()
{
	RealArray ^copy = gcnew RealArray(getVarNum());

	for( int i=0; i<getVarNum(); i++ )
	{
		copy->_vecValues[i] = _vecValues[i];
		copy->_vecLow[i]		= _vecLow[i];
		copy->_vecUp[i]			= _vecUp[i];
	}

	return copy;
}
void		RealArray::GenerateRandom()
{
	for( int i=0; i<getVarNum(); i++ )
		(*_vecValues)[i] = rand() % 10000 * 3.141f / 100000 - 3.141f / 2;
}

String^	RealArray::GetStringDesc()
{
	return "RealArray";
}
String^	RealArray::GetValueStringRepresentation()
{
	StringBuilder ^ret = gcnew StringBuilder();

	for( int i=0; i<getVarNum(); i++ )
	{
		ret->Append("x[");
		ret->Append(i);
		ret->Append("] = ");
		ret->Append(getValue(i));
	}
	return ret->ToString();
}

int			RealArray::getParamNum() 
{ 
	return getVarNum() * 2; 
}
String^	RealArray::getParamName(int ParamInd) 
{
	String ^strInd = gcnew String("");
	strInd->Format("%d", ParamInd / 2);
	
	if( ParamInd % 2 == 0 ) return "Low bound " + strInd;
	else return "Upp.bound " + strInd;
}
void		RealArray::setParamValue(int ParamInd, double inVal) 
{
	if( ParamInd % 2 == 0 ) setLowBound(ParamInd / 2, inVal);
	else setUppBound(ParamInd / 2, inVal);
}
double	RealArray::getParamValue(int ParamInd) 
{
	if( ParamInd % 2 == 0 ) return getLowBound(ParamInd / 2);
	else return getUppBound(ParamInd / 2);
}
int		RealArray::getConstructorParamNum() 
{ 
	return 1; 
}
String^	RealArray::getConstructorParamName(int ParamInd) 
{
	return "Num var";
}
void	RealArray::setConstructorParamValue(int ParamInd, double inVal) 
{
	setVarNum((int) inVal);
}
double	RealArray::getConstructorParamValue(int ParamInd) 
{
	return getVarNum();
}

int			RealArray::getVarNum()
{
	return (int) _vecValues->size();
}
void		RealArray::setVarNum(int inVarNum) 
{
	_vecValues->resize(inVarNum);
	_vecLow->resize(inVarNum);
	_vecUp->resize(inVarNum);
}

double	RealArray::getValue(int VarInd) 
{ 
	return (*_vecValues)[VarInd]; 
}
void		RealArray::setValue(int VarInd, double inVal) 
{ 
	(*_vecValues)[VarInd] = inVal; 
}

void		RealArray::setVarBounds(int VarInd, double inLow, double inUpp)
{
	(*_vecLow)[VarInd] = inLow; 
	(*_vecUp)[VarInd]	= inUpp;
}
double	RealArray::getLowBound(int VarInd) {	return (*_vecLow)[VarInd];}
double	RealArray::getUppBound(int VarInd) {	return (*_vecUp)[VarInd];}
void		RealArray::setLowBound(int VarInd, double inLow)	{	(*_vecLow)[VarInd] = inLow; }
void		RealArray::setUppBound(int VarInd, double inUpp)	{	(*_vecUp)[VarInd]	= inUpp; }

