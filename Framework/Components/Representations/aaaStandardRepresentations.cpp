// This is the main DLL file.

#include "stdafx.h"

#include "StandardRepresentations.h"
#include "Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
BitArray::BitArray()
{
	_vecBitsRepr = new vector<bool>();
}
BitArray::BitArray(const BitArray ^copy)
{
	_vecBitsRepr = new vector<bool>();
	_vecBitsRepr->resize(copy->_vecBitsRepr->size());

	for( int i=0; i<(int) copy->_vecBitsRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*copy->_vecBitsRepr)[i];
}
BitArray^	BitArray::operator=(const BitArray ^copy)
{
//	*_vecBitsRepr = *(copy->_vecBitsRepr);
	_vecBitsRepr->resize(copy->_vecBitsRepr->size());

	for( int i=0; i<(int) copy->_vecBitsRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*copy->_vecBitsRepr)[i];
	return this;
}
void	BitArray::GenerateRandom()
{
	for( int i=0; i<(int) _vecBitsRepr->size(); i++ )
	{
		if( rand() % 2 == 0 )
			(*_vecBitsRepr)[i] = true;
		else
			(*_vecBitsRepr)[i] = false;
	}
}
IDesignVariable^	BitArray::Clone()
{
	return gcnew BitArray(this);
}
String^	BitArray::GetValueStringRepresentation()
{ 
	return getBits(); 
}
void		BitArray::setBitNum(int inBitNum)
{
	_vecBitsRepr->resize(inBitNum);
}
int			BitArray::getBitNum()
{
	return (int) _vecBitsRepr->size();
}
void		BitArray::setReprBit(int Ind, bool Val)
{
	(*_vecBitsRepr)[Ind] = Val;
}
bool		BitArray::getReprBit(int BitNum)
{
	return (*_vecBitsRepr)[BitNum];
}
vector<bool>*	BitArray::getRepr()
{
	return _vecBitsRepr;
}
void		BitArray::setRepr(const vector<bool> *inRepr) 
{ 
	*_vecBitsRepr = *inRepr; 
}
void		BitArray::FlopBit(int BitNum)
{
	if( (*_vecBitsRepr)[BitNum] == false )
		(*_vecBitsRepr)[BitNum] = true;
	else
		(*_vecBitsRepr)[BitNum] = false;
}
String^		BitArray::getBits()
{
	String^		out = "";

	for( int j=0; j<(int) _vecBitsRepr->size(); j++ )
		if( (*_vecBitsRepr)[j] == true )
			out += "1";
		else
			out += "0";

	return out;
}

/********************************************************************************************************************/
Real::Real()
{
	setBoundary(0,1	);
}
Real::Real(double inLow, double inUp) 
{
	setBoundary(inLow, inUp);
}
Real::Real(const Real ^copy)
{
	_Value = copy->_Value;
	_Low = copy->_Low;
	_Up = copy->_Up;
}

IDesignVariable^	Real::Clone()
{
	Real ^copy = gcnew Real(_Low, _Up);
	copy->_Value = _Value;

	return copy;
}
void		Real::GenerateRandom()
{
	_Value = rand() % 10000 * (_Up - _Low) / 10000 + _Low;
}

String^		Real::GetStringDesc()
{
	return "Real";
}
String^		Real::GetValueStringRepresentation()
{
	StringBuilder ^str = gcnew StringBuilder();
	str->Append("X = ");
	str->Append(_Value);

	return str->ToString();
}

int				Real::getParamNum() 
{ 
	return 2; 
}
String^		Real::getParamName(int ParamInd) 
{
	if( ParamInd == 0 ) 
		return "Low bound";
	else 
		return "Upp bound";
}
void			Real::setParamValue(int Ind, double inVal) 
{
	if( Ind == 0 ) 
		_Low = inVal;
	else 
		_Up = inVal;
}
double		Real::getParamValue(int Ind) 
{
	if( Ind == 0 ) 
		return _Low;
	else 
		return _Up;
}

double		Real::getValue()							{ 	return _Value; }
void			Real::setValue(double inVal)	{ 	_Value = inVal; }
double		Real::getUp()									{ return _Up; }
double		Real::getLow()								{ return _Low; }
void			Real::setBoundary(double Low, double Up)	{	_Low = Low; 	_Up = Up;}

/********************************************************************************************************************/
RealAsBitArray::RealAsBitArray()
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(10);
	setVarBounds(0,1);
}
RealAsBitArray::RealAsBitArray(double inLow, double inUp) : Real(inLow, inUp)
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(10);
	setVarBounds(inLow, inUp);
}
RealAsBitArray::RealAsBitArray(double inLow, double inUp, int inBitNum) : Real(inLow, inUp)
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(inBitNum);
	setVarBounds(inLow, inUp);
}
RealAsBitArray::RealAsBitArray(const RealAsBitArray ^copy) : Real(copy), _BitsRepr(copy->_BitsRepr)
{
//	_BitsRepr = (BitArray ^) copy->_BitsRepr->Clone();
}
RealAsBitArray^	RealAsBitArray::operator=(const RealAsBitArray ^b)
{
	_Value = b->_Value;
	_Low = b->_Low;
	_Up = b->_Up;
	_BitsRepr = b->_BitsRepr;
	return this;
}

IDesignVariable^	RealAsBitArray::Clone()
{
	return gcnew RealAsBitArray(this);
}
void		RealAsBitArray::GenerateRandom()
{
//	assert(_Up > _Low);

	int			RandomNumber	= rand() % 10000;
	double	IntervalWidth = _Up - _Low;

	setValue(RandomNumber * (IntervalWidth / 10000) + _Low);
}

String^		RealAsBitArray::GetStringDesc()
{
	return "RealAsBitArray";
}
String^		RealAsBitArray::GetValueStringRepresentation()
{
	StringBuilder	^ret = gcnew StringBuilder();

	ret->Append("x = ");
	ret->Append(getValue());
	ret->Append("  Bit repr. ");

	vector<bool> *Bit1 = getRepr()->getRepr();
	for( int i=0; i<(int) Bit1->size(); i++ )
	{
		if( (*Bit1)[i] == false )
			ret->Append("0");
		else
			ret->Append("1");
	}

	return ret->ToString();
}

int			RealAsBitArray::getParamNum()  
{ 
	return 3; 
}
String^	RealAsBitArray::getParamName(int ParamInd)  
{
	if( ParamInd == 0 ) 
		return "Low bound";
	else if( ParamInd == 1 ) 
		return "Upp bound";
	else return 
		"Bit number";
}
void		RealAsBitArray::setParamValue(int Ind, double inVal) 
{
	if (Ind == 0) 
		_Low = inVal;
	else if(Ind == 1)
		_Up = inVal;
	else 
		setBitNum((int) inVal);

}
double	RealAsBitArray::getParamValue(int Ind) 
{
	if( Ind == 0 ) 
		return _Low;
	else if( Ind == 1) 
		return _Up;
	else 
		return getBitNum();
}

void		RealAsBitArray::setBitNum(int inBitNum)
{
	_BitsRepr->setBitNum(inBitNum);
}
int			RealAsBitArray::getBitNum()
{
	return _BitsRepr->getBitNum();
}
void		RealAsBitArray::setVarMinResolution(double inMinRes)
{
	double		IntervalWidth = _Up - _Low;
	double		NumDiff = IntervalWidth / inMinRes;

	int		BitNum = 1;
	while( NumDiff > 1 )
	{
		NumDiff /= 2.0;
		BitNum++;
	}

	_BitsRepr->setBitNum(BitNum);
}

void		RealAsBitArray::setVarBounds(double inLow, double inUpp)	{	_Low = inLow;	_Up = inUpp;}
double	RealAsBitArray::getLowBound()							{	return _Low;	}
double	RealAsBitArray::getUppBound()							{	return _Up;	}
void		RealAsBitArray::setLowBound(double inLow)	{	_Low = inLow; }
void		RealAsBitArray::setUppBound(double inUpp)	{	_Up = inUpp; }

void		RealAsBitArray::setValue(double	inVal)
{
	_Value = inVal;
	adjustReprToValue();
}

BitArray^		RealAsBitArray::getRepr()
{
	return _BitsRepr;
}
void		RealAsBitArray::setRepr(const vector<bool> *inRepr)
{
	_BitsRepr->setRepr(inRepr);
	adjustValueToBitRepr();
}
void		RealAsBitArray::setReprBit(int Ind, bool Val)
{
	_BitsRepr->setReprBit(Ind, Val);

	adjustValueToBitRepr();
}
bool		RealAsBitArray::getReprBit(int BitNum)
{
	return _BitsRepr->getReprBit(BitNum);
}

void		RealAsBitArray::FlopBit(int BitNum)
{
	if( _BitsRepr->getReprBit(BitNum) == false )
		_BitsRepr->setReprBit(BitNum, true);
	else
		_BitsRepr->setReprBit(BitNum, false);

	adjustValueToBitRepr();
}

void		RealAsBitArray::adjustValueToBitRepr()
{
	// iz BitArraya idemo dobiti x
	long	NewIntValue = 0;
	BitArrayToInt(_BitsRepr->getRepr(), NewIntValue);

	// sada to treba pretvoriti u double unutar danog intervala
	long	MaxIntValue = (1 << _BitsRepr->getBitNum()) - 1;
	_Value = (_Up - _Low) * NewIntValue / MaxIntValue  + _Low;
}
void		RealAsBitArray::adjustReprToValue()
{
	// konvertiramo x u BitArray
	int		BitNum = (int) _BitsRepr->getBitNum();
	long	MaxIntValue = (1 << BitNum) - 1;

	long	IntX = (long) (MaxIntValue * (_Value - _Low) / (_Up - _Low));

	vector<bool>	*newRepr = new vector<bool>;
	IntToBitArray(IntX, BitNum, newRepr);

	// i sada to treba ubaciti u internu reprezentaciju RealAsBitArray
	setRepr(newRepr);

	String ^s = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*newRepr)[i] == false )
			s += "0";
		else
			s += "1";
	}

	String ^s2 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*_BitsRepr->_vecBitsRepr)[i] == false )
			s2 += "0";
		else
			s2 += "1";
	}

	delete newRepr;
}
/********************************************************************************************************************/
RealArray::RealArray() 
{ 
	_vecValues = new vector<double>();
	_vecLow		= new vector<double>();
	_vecUp		= new vector<double>();

	setVarNum(1); 
}
RealArray::RealArray(int VarNum) 
{
	_vecValues = new vector<double>();
	_vecLow  = new vector<double>();
	_vecUp		= new vector<double>();

	setVarNum(VarNum);
}
IDesignVariable^	RealArray::Clone()
{
	RealArray ^copy = gcnew RealArray(getVarNum());

	for( int i=0; i<getVarNum(); i++ )
	{
		copy->_vecValues[i] = _vecValues[i];
		copy->_vecLow[i]		= _vecLow[i];
		copy->_vecUp[i]			= _vecUp[i];
	}

	return copy;
}
void		RealArray::GenerateRandom()
{
	for( int i=0; i<getVarNum(); i++ )
		(*_vecValues)[i] = rand() % 10000 * 3.141f / 100000 - 3.141f / 2;
}

String^	RealArray::GetStringDesc()
{
	return "RealArray";
}
String^	RealArray::GetValueStringRepresentation()
{
	StringBuilder ^ret = gcnew StringBuilder();

	for( int i=0; i<getVarNum(); i++ )
	{
		ret->Append("x[");
		ret->Append(i);
		ret->Append("] = ");
		ret->Append(getValue(i));
	}
	return ret->ToString();
}

int			RealArray::getParamNum() 
{ 
	return getVarNum() * 2; 
}
String^	RealArray::getParamName(int ParamInd) 
{
	String ^strInd = gcnew String("");
	strInd->Format("%d", ParamInd / 2);
	
	if( ParamInd % 2 == 0 ) return "Low bound " + strInd;
	else return "Upp.bound " + strInd;
}
void		RealArray::setParamValue(int ParamInd, double inVal) 
{
	if( ParamInd % 2 == 0 ) setLowBound(ParamInd / 2, inVal);
	else setUppBound(ParamInd / 2, inVal);
}
double	RealArray::getParamValue(int ParamInd) 
{
	if( ParamInd % 2 == 0 ) return getLowBound(ParamInd / 2);
	else return getUppBound(ParamInd / 2);
}
int		RealArray::getConstructorParamNum() 
{ 
	return 1; 
}
String^	RealArray::getConstructorParamName(int ParamInd) 
{
	return "Num var";
}
void	RealArray::setConstructorParamValue(int ParamInd, double inVal) 
{
	setVarNum((int) inVal);
}
double	RealArray::getConstructorParamValue(int ParamInd) 
{
	return getVarNum();
}

int			RealArray::getVarNum()
{
	return (int) _vecValues->size();
}
void		RealArray::setVarNum(int inVarNum) 
{
	_vecValues->resize(inVarNum);
	_vecLow->resize(inVarNum);
	_vecUp->resize(inVarNum);
}

double	RealArray::getValue(int VarInd) 
{ 
	return (*_vecValues)[VarInd]; 
}
void		RealArray::setValue(int VarInd, double inVal) 
{ 
	(*_vecValues)[VarInd] = inVal; 
}

void		RealArray::setVarBounds(int VarInd, double inLow, double inUpp)
{
	(*_vecLow)[VarInd] = inLow; 
	(*_vecUp)[VarInd]	= inUpp;
}
double	RealArray::getLowBound(int VarInd) {	return (*_vecLow)[VarInd];}
double	RealArray::getUppBound(int VarInd) {	return (*_vecUp)[VarInd];}
void		RealArray::setLowBound(int VarInd, double inLow)	{	(*_vecLow)[VarInd] = inLow; }
void		RealArray::setUppBound(int VarInd, double inUpp)	{	(*_vecUp)[VarInd]	= inUpp; }

/********************************************************************************************************************/
RealArray_AsBitArray::RealArray_AsBitArray()
{
	setVarNum(5);
}
RealArray_AsBitArray::RealArray_AsBitArray(int VarNum) 
{
	setVarNum(VarNum);
}
RealArray_AsBitArray::RealArray_AsBitArray(RealArray_AsBitArray ^copy) 
{
	_vecRepr = new vector<gcroot<RealAsBitArray ^> >();
	_vecRepr->resize(copy->getVarNum());
	for( int i=0; i<copy->getVarNum(); i++ )
	{
		(*_vecRepr)[i] = (RealAsBitArray ^) (*copy->_vecRepr)[i]->Clone();
	}
}
RealArray_AsBitArray::~RealArray_AsBitArray()
{
	delete _vecRepr;
}

IDesignVariable^	RealArray_AsBitArray::Clone()
{
	return gcnew RealArray_AsBitArray(this);
}
void	RealArray_AsBitArray::GenerateRandom()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
		(*_vecRepr)[i]->GenerateRandom();
}

String^		RealArray_AsBitArray::GetStringDesc()
{
	return "RealArray_AsBitArray";
}
String^		RealArray_AsBitArray::GetValueStringRepresentation()
{
	StringBuilder ^ret = gcnew StringBuilder();

	for( int i=0; i<getVarNum(); i++ )
	{
		ret->Append("x[");
		ret->Append(i);
		ret->Append("] = ");
		ret->Append(getRepr(i)->getValue());
		ret->Append(" ;  ");
	}
	return ret->ToString();
}

int				RealArray_AsBitArray::getParamNum() 
{ 
	return getVarNum() * 2; 
}
String^		RealArray_AsBitArray::getParamName(int ParamInd) 
{
	String ^strInd = gcnew String("");
	strInd->Format("%d", ParamInd / 2);
	
	if( ParamInd % 2 == 0 ) return "Low bound " + strInd;
	else return "Upp.bound " + strInd;
}
void			RealArray_AsBitArray::setParamValue(int ParamInd, double inVal) 
{
	if( ParamInd % 2 == 0 ) setLowBound(ParamInd / 2, inVal);
	else setUppBound(ParamInd / 2, inVal);
}
double		RealArray_AsBitArray::getParamValue(int ParamInd) 
{
	if( ParamInd % 2 == 0 ) return getLowBound(ParamInd / 2);
	else return getUppBound(ParamInd / 2);
}

int				RealArray_AsBitArray::getConstructorParamNum() { return 1; }
String^		RealArray_AsBitArray::getConstructorParamName(int ParamInd) {	return "Num var";}
void			RealArray_AsBitArray::setConstructorParamValue(int ParamInd, double inVal) {	setVarNum((int) inVal);}
double		RealArray_AsBitArray::getConstructorParamValue(int ParamInd) {	return getVarNum();}

void		RealArray_AsBitArray::setBitNum(int VarInd, int inBitNum)
{
	(*_vecRepr)[VarInd]->getRepr()->setBitNum(inBitNum);
}
int			RealArray_AsBitArray::getBitNum(int VarInd )
{
	return (int) (*_vecRepr)[VarInd]->getRepr()->getBitNum();
}
void		RealArray_AsBitArray::setVarMinResolution(int VarInd, double inMinRes)
{
	double		IntervalWidth = (*_vecRepr)[VarInd]->getUp() - (*_vecRepr)[VarInd]->getLow();
	double		NumDiff = IntervalWidth / inMinRes;

	int		BitNum = 1;
	while( NumDiff > 1 )
	{
		NumDiff /= 2.0;
		BitNum++;
	}

	(*_vecRepr)[VarInd]->getRepr()->setBitNum(BitNum);
}

int	RealArray_AsBitArray::getVarNum() 
{
	return (int) _vecRepr->size();
}
void	RealArray_AsBitArray::setVarNum(int inVarNum) 
{
	_vecRepr = new vector<gcroot<RealAsBitArray ^> >();
	_vecRepr->resize(inVarNum);

	for(int i=0; i<inVarNum; i++ )
		(*_vecRepr)[i] = gcnew RealAsBitArray();
}

double	RealArray_AsBitArray::getValue(int VarInd) 
{ 
	return (*_vecRepr)[VarInd]->getValue(); 
}
void		RealArray_AsBitArray::setValue(int VarInd, double	inVal)
{
	(*_vecRepr)[VarInd]->setValue(inVal); 
//	adjustReprToValue();
}

void		RealArray_AsBitArray::setVarBounds(int VarInd, double inLow, double inUpp)	{	(*_vecRepr)[VarInd]->setVarBounds(inLow, inUpp);}
double	RealArray_AsBitArray::getLowBound(int VarInd)		{	return (*_vecRepr)[VarInd]->getLowBound();}
double	RealArray_AsBitArray::getUppBound(int VarInd)		{	return (*_vecRepr)[VarInd]->getUppBound();}
void		RealArray_AsBitArray::setLowBound(int VarInd, double inLow)	{	(*_vecRepr)[VarInd]->setLowBound(inLow);}
void		RealArray_AsBitArray::setUppBound(int VarInd, double inUpp)	{	(*_vecRepr)[VarInd]->setUppBound(inUpp);}

RealAsBitArray^	RealArray_AsBitArray::getRepr(int VarInd )
{
	return (*_vecRepr)[VarInd];
}
void		RealArray_AsBitArray::setReprBit(int VarInd, int Ind, bool Val)
{
	(*_vecRepr)[VarInd]->getRepr()->setReprBit(Ind,Val);

	adjustValueToBitRepr();
}
bool		RealArray_AsBitArray::getReprBit(int VarInd, int BitNum)
{
	return (*_vecRepr)[VarInd]->getRepr()->getReprBit(BitNum);
}

void		RealArray_AsBitArray::FlopBit(int VarInd, int BitNum)
{
	if( (*_vecRepr)[VarInd]->getRepr()->getReprBit(BitNum) == false )
		(*_vecRepr)[VarInd]->getRepr()->setReprBit(BitNum,true);
	else
		(*_vecRepr)[VarInd]->getRepr()->setReprBit(BitNum,false);
}

void		RealArray_AsBitArray::adjustValueToBitRepr()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
	{
		// iz BitArraya idemo dobiti x
		long	NewIntValue = 0;
		BitArrayToInt((*_vecRepr)[i]->getRepr()->getRepr(), NewIntValue);

		// sada to treba pretvoriti u double unutar danog intervala
		long	MaxIntValue = (1 << (*_vecRepr)[i]->getRepr()->getBitNum()) - 1;
		(*_vecRepr)[i]->setValue( ((*_vecRepr)[i]->getUp() - (*_vecRepr)[i]->getLow()) * NewIntValue / MaxIntValue  + (*_vecRepr)[i]->getLow() );
	}
}
void		RealArray_AsBitArray::adjustReprToValue()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
	{
		// konvertiramo x u BitArray
		int		BitNum = (int) (*_vecRepr)[i]->getRepr()->getBitNum();
		long	MaxIntValue = (1 << BitNum) - 1;

		long	IntX = (long) (MaxIntValue * ((*_vecRepr)[i]->getValue() - (*_vecRepr)[i]->getLow()) / ((*_vecRepr)[i]->getUp() - (*_vecRepr)[i]->getLow()) );

		vector<bool>	*newRepr = new vector<bool>;
		IntToBitArray(IntX, BitNum, newRepr);

		// TODO
	}
}
/********************************************************************************************************************/
Integer::Integer()
{
}
Integer::Integer(int inLow, int inUp)
{
}
Integer::Integer(Integer ^copy)
{
}
void		Integer::GenerateRandom()
{
}
IDesignVariable^	Integer::Clone()
{
	return gcnew Integer(this);
}

String^		Integer::GetStringDesc()
{
	return "Integer";
}
String^		Integer::GetValueStringRepresentation()
{
	return gcnew String(" ");
}

int				Integer::getParamNum() 
{ 
	return 2; 
}
String^		Integer::getParamName(int ParamInd) 
{
	if( ParamInd == 0 ) return "Low bound";
	else return "Upp bound";
}
void			Integer::setParamValue(int Ind, double inVal) 
{
	if( Ind == 0 ) _Low = (int) inVal;
	else _Up = (int) inVal;
}
double		Integer::getParamValue(int Ind) 
{
	if( Ind == 0 ) return _Low;
	else return _Up;
}

int		Integer::getValue()	{	return _Value;}
void	Integer::setValue(int inVal)	{	_Value = inVal;}

void	Integer::setVarBounds(int inLow, int inUpp)	{	setLowBound(inLow);	setUppBound(inUpp);	}
int		Integer::getLowBound()	{	return _Low;	}
int		Integer::getUppBound()	{	return _Up;	}
void	Integer::setLowBound(int inLow)	{	_Low = inLow;	}
void	Integer::setUppBound(int inUpp)	{	_Up = inUpp;	}

/********************************************************************************************************************/
IntegerArray::IntegerArray(int VarNum)
{
}
IntegerArray::IntegerArray(int VarNum, int AllLow, int AllUp)
{
}
IntegerArray::IntegerArray(IntegerArray ^copy)
{
}

void			IntegerArray::GenerateRandom()
{
}
IDesignVariable^	IntegerArray::Clone()
{
	return gcnew IntegerArray(this);
}

String^		IntegerArray::GetStringDesc()
{
	return "IntegerArray";
}
String^		IntegerArray::GetValueStringRepresentation()
{
	return gcnew String(" ");
}

int				IntegerArray::getParamNum() 
{ 
	return 1 + getVarNum() * 2; 
}
String^		IntegerArray::getParamName(int ParamInd) 
{
	if( ParamInd == 0 ) return "Num var";
	else 
	{
		String ^strInd = gcnew String("");
		strInd->Format("%d", (ParamInd - 1) / 2);
		
		if( ParamInd % 2 == 1 ) return "Low bound " + strInd;
		else return "Upp.bound " + strInd;
	}
}
void			IntegerArray::setParamValue(int ParamInd, double inVal) 
{
	if( ParamInd == 0 )	
		setVarNum((int) inVal);
	else 
	{
		if( ParamInd % 2 == 1 ) setLowBound((ParamInd-1) / 2, (int) inVal);
		else setUppBound((ParamInd-1) / 2, (int) inVal);
	}
}
double		IntegerArray::getParamValue(int ParamInd) 
{
	if( ParamInd == 0 ) return getVarNum();
	else 
	{
		if( ParamInd % 2 == 1 ) return getLowBound((ParamInd-1) / 2);
		else return getUppBound((ParamInd-1) / 2);
	}
}

Integer^		IntegerArray::getVar(int Ind)
{
	return (*_vecRepr)[Ind];
}

int		IntegerArray::getVarNum()
{
	return _vecRepr->size();
}
void	IntegerArray::setVarNum(int inVarNum)
{
	_vecRepr = new vector<gcroot<Integer ^> >();
	_vecRepr->resize(inVarNum);

	for(int i=0; i<inVarNum; i++ )
		(*_vecRepr)[i] = gcnew Integer();
}
int		IntegerArray::getValue(int Ind)
{
	return 0;
}
void	IntegerArray::setValue(int Ind, int inVal)
{
}

void		IntegerArray::setVarBounds(int VarInd, int inLow, int inUpp)	
{	
	getVar(VarInd)->setLowBound(inLow);	
	getVar(VarInd)->setUppBound(inUpp);
}
int			IntegerArray::getLowBound(int VarInd)							{	return getVar(VarInd)->getLowBound();}
int			IntegerArray::getUppBound(int VarInd)							{	return getVar(VarInd)->getUppBound();}
void		IntegerArray::setLowBound(int VarInd, int inLow)	{	getVar(VarInd)->setLowBound(inLow);}
void		IntegerArray::setUppBound(int VarInd, int inUpp)	{	getVar(VarInd)->setUppBound(inUpp);}

/********************************************************************************************************************/
Permutation::Permutation()
{
	_Num = 0;
}

Permutation::Permutation(Permutation ^copy)
{
	setNum(copy->_Num);

	for( int i=0; i<_Num; i++ )
		_arrOrder[i] = copy->_arrOrder[i];
}
void			Permutation::GenerateRandom()
{
	if( _Num <= 0 ) return;

	for( int i=0; i<_Num; i++ )
		_arrOrder[i] = i;

	for( int i=0; i<_Num; i++ )
	{
		int	 r1 = rand() % _Num;
		int  r2 = rand() % _Num;

		int	 temp = _arrOrder[r1];
		_arrOrder[r1] = _arrOrder[r2];
		_arrOrder[r2] = temp;
	}
}
IDesignVariable^	Permutation::Clone()
{
	return gcnew Permutation(this);
}
IDesignVariable^	Permutation::GetRepresentation()
{
	return this;
}
String^		Permutation::GetStringDesc()
{
	return "";
}
String^		Permutation::GetValueStringRepresentation()
{
	String ^ret = gcnew String("");

	for(int i=0; i<_Num; i++ )
		ret += (_arrOrder[i].ToString() + " ");

	return ret;
}
int				Permutation::getParamNum() 
{ 
	return 1; 
}
String^		Permutation::getParamName(int ParamInd) 
{ 
	return "Num items";
}
void			Permutation::setParamValue(int Ind, double inVal) 
{ 
	setNum((int) inVal);
}
double		Permutation::getParamValue(int Ind) 
{ 
	return _Num; 
}
void			Permutation::setNum(int inNum)
{
	_Num = inNum;
	_arrOrder = gcnew array<int,1>(_Num);
}
int		Permutation::getNum()
{
	return _Num;
}
int				Permutation::getNth(int i) 
{ 
	return _arrOrder[i]; 
}
