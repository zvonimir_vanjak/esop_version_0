// This is the main DLL file.

#include "stdafx.h"

#include "Real.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
Real::Real()
{
	setBoundary(0,1	);
}
Real::Real(double inLow, double inUp) 
{
	setBoundary(inLow, inUp);
}
Real::Real(const Real ^copy)
{
	_Value = copy->_Value;
	_Low = copy->_Low;
	_Up = copy->_Up;
}

IDesignVariable^	Real::Clone()
{
	Real ^copy = gcnew Real(_Low, _Up);
	copy->_Value = _Value;

	return copy;
}
void		Real::GenerateRandom()
{
	_Value = rand() % 10000 * (_Up - _Low) / 10000 + _Low;
}

String^		Real::GetStringDesc()
{
	return "Real";
}
String^		Real::GetValueStringRepresentation()
{
	StringBuilder ^str = gcnew StringBuilder();
	str->Append("X = ");
	str->Append(_Value);

	return str->ToString();
}

int				Real::getParamNum() 
{ 
	return 2; 
}
String^		Real::getParamName(int ParamInd) 
{
	if( ParamInd == 0 ) 
		return "Low bound";
	else 
		return "Upp bound";
}
void			Real::setParamValue(int Ind, double inVal) 
{
	if( Ind == 0 ) 
		_Low = inVal;
	else 
		_Up = inVal;
}
double		Real::getParamValue(int Ind) 
{
	if( Ind == 0 ) 
		return _Low;
	else 
		return _Up;
}

double		Real::getValue()							{ 	return _Value; }
void			Real::setValue(double inVal)	{ 	_Value = inVal; }
double		Real::getUp()									{ return _Up; }
double		Real::getLow()								{ return _Low; }
void			Real::setBoundary(double Low, double Up)	{	_Low = Low; 	_Up = Up;}

