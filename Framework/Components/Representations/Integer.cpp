// This is the main DLL file.

#include "stdafx.h"

#include "Integer.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/

Integer::Integer()
{
}
Integer::Integer(int inLow, int inUp)
{
}
Integer::Integer(Integer ^copy)
{
}
void		Integer::GenerateRandom()
{
}
IDesignVariable^	Integer::Clone()
{
	return gcnew Integer(this);
}

String^		Integer::GetStringDesc()
{
	return "Integer";
}
String^		Integer::GetValueStringRepresentation()
{
	return gcnew String(" ");
}

int				Integer::getParamNum() 
{ 
	return 2; 
}
String^		Integer::getParamName(int ParamInd) 
{
	if( ParamInd == 0 ) return "Low bound";
	else return "Upp bound";
}
void			Integer::setParamValue(int Ind, double inVal) 
{
	if( Ind == 0 ) _Low = (int) inVal;
	else _Up = (int) inVal;
}
double		Integer::getParamValue(int Ind) 
{
	if( Ind == 0 ) return _Low;
	else return _Up;
}

int		Integer::getValue()	{	return _Value;}
void	Integer::setValue(int inVal)	{	_Value = inVal;}

void	Integer::setVarBounds(int inLow, int inUpp)	{	setLowBound(inLow);	setUppBound(inUpp);	}
int		Integer::getLowBound()	{	return _Low;	}
int		Integer::getUppBound()	{	return _Up;	}
void	Integer::setLowBound(int inLow)	{	_Low = inLow;	}
void	Integer::setUppBound(int inUpp)	{	_Up = inUpp;	}

