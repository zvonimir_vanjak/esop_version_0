// This is the main DLL file.

#include "stdafx.h"

#include "StandardRepresentations.h"
#include "ElementaryOperators.h"

using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
BitArray::BitArray()
{
	_vecBitsRepr = new vector<bool>();
}
BitArray::BitArray(const BitArray ^copy)
{
	_vecBitsRepr = new vector<bool>();
	_vecBitsRepr->resize(copy->_vecBitsRepr->size());

	for( int i=0; i<(int) copy->_vecBitsRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*copy->_vecBitsRepr)[i];
}
BitArray^	BitArray::operator=(const BitArray ^copy)
{
//	*_vecBitsRepr = *(copy->_vecBitsRepr);
	for( int i=0; i<(int) copy->_vecBitsRepr->size(); i++ )
		(*_vecBitsRepr)[i] = (*copy->_vecBitsRepr)[i];
	return this;
}
String^	BitArray::getComponentName() 
{ 
	return gcnew String("BitArray"); 
}
IRepresentation^	BitArray::Clone()
{
	return gcnew BitArray(this);
}
void	BitArray::GenerateRandom()
{
}
vector<bool>*	BitArray::getRepr()
{
	return _vecBitsRepr;
}
void		BitArray::setReprBit(int Ind, bool Val)
{
	(*_vecBitsRepr)[Ind] = Val;
}
bool		BitArray::getReprBit(int BitNum)
{
	return (*_vecBitsRepr)[BitNum];
}
void		BitArray::FlopBit(int BitNum)
{
	if( (*_vecBitsRepr)[BitNum] == false )
		(*_vecBitsRepr)[BitNum] = true;
	else
		(*_vecBitsRepr)[BitNum] = false;
}
void		BitArray::setBitNum(int inBitNum)
{
	_vecBitsRepr->resize(inBitNum);
}
int			BitArray::getBitNum()
{
	return (int) _vecBitsRepr->size();
}
String^		BitArray::getBits()
{
	String^		out = "";

	for( int j=0; j<(int) _vecBitsRepr->size(); j++ )
		if( (*_vecBitsRepr)[j] == true )
			out += "1";
		else
			out += "0";

	return out;
}

/********************************************************************************************************************/

Real::Real()
{
	setBoundary(0,0);
}
Real::Real(double inLow, double inUp) 
{
	setBoundary(inLow, inUp);
}
Real::Real(const Real ^copy)
{
	_Value = copy->_Value;
	_Low = copy->_Low;
	_Up = copy->_Up;
}
String^	Real::getComponentName() 
{ 
	return "Real"; 
}
IDesignVariable^	Real::Clone()
{
	Real ^copy = gcnew Real(_Low, _Up);
	copy->_Value = _Value;

	return copy;
}
IRepresentation^	Real::GetRepresentation()
{
	return this;
}
double	Real::getValue() 
{ 
	return _Value; 
}
void		Real::setValue(double inVal) 
{ 
	_Value = inVal; 
}
void		Real::setBoundary(double Low, double Up)
{
	_Low = Low; 
	_Up = Up;
}
void		Real::GenerateRandom()
{
	_Value = rand() % 10000 * (_Up - _Low) / 10000 + _Low;
}

/********************************************************************************************************************/
RealAsBitArray::RealAsBitArray()
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(10);
	setVarBounds(0,1);
}
RealAsBitArray::RealAsBitArray(double inLow, double inUp) : Real(inLow, inUp)
{
	_BitsRepr->setBitNum(10);
	setVarBounds(inLow, inUp);
}
RealAsBitArray::RealAsBitArray(const RealAsBitArray ^copy) : Real(copy)
{
	_BitsRepr = (BitArray ^) copy->_BitsRepr->Clone();
}
RealAsBitArray^	RealAsBitArray::operator=(const RealAsBitArray ^b)
{
	_Value = b->_Value;
	_Low = b->_Low;
	_Up = b->_Up;
	_BitsRepr = b->_BitsRepr;
	return this;
}
IDesignVariable^	RealAsBitArray::Clone()
{
	return gcnew RealAsBitArray(this);
}
IRepresentation^	RealAsBitArray::GetRepresentation()
{
	return this;
}
String^	RealAsBitArray::getComponentName() 
{ 
	return "RealAsBitArray"; 
}
void		RealAsBitArray::GenerateRandom()
{
//	assert(_Up > _Low);

	int			RandomNumber	= rand() % 10000;
	double		IntervalWidth = _Up - _Low;

	setValue(RandomNumber * (IntervalWidth / 10000) + _Low);

	vector<bool> *Bit1 = getRepr()->getRepr();
	String ^s1 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*Bit1)[i] == false )
			s1 += "0";
		else
			s1 += "1";
	}
	double x = getValue();
	Console::WriteLine("Rand x = " + x.ToString() + " " + s1);

}
void		RealAsBitArray::setValue(double	inVal)
{
	_Value = inVal;
	adjustReprToValue();
}
BitArray^		RealAsBitArray::getRepr()
{
	return _BitsRepr;
}
void		RealAsBitArray::setRepr(const vector<bool> *inRepr)
{
	_BitsRepr->setRepr(inRepr);
	adjustValueToBitRepr();
}

void		RealAsBitArray::setReprBit(int Ind, bool Val)
{
	_BitsRepr->setReprBit(Ind, Val);

	adjustValueToBitRepr();
}
bool		RealAsBitArray::getReprBit(int BitNum)
{
	return _BitsRepr->getReprBit(BitNum);
}
void		RealAsBitArray::FlopBit(int BitNum)
{
	if( _BitsRepr->getReprBit(BitNum) == false )
		_BitsRepr->setReprBit(BitNum, true);
	else
		_BitsRepr->setReprBit(BitNum, false);

	adjustValueToBitRepr();
}
void		RealAsBitArray::setBitNum(int inBitNum)
{
	_BitsRepr->setBitNum(inBitNum);
}
int			RealAsBitArray::getBitNum()
{
	return _BitsRepr->getBitNum();
}
void		RealAsBitArray::setVarMinResolution(double inMinRes)
{
	double		IntervalWidth = _Up - _Low;
	double		NumDiff = IntervalWidth / inMinRes;

	int		BitNum = 1;
	while( NumDiff > 1 )
	{
		NumDiff /= 2.0;
		BitNum++;
	}

	_BitsRepr->setBitNum(BitNum);
}
void		RealAsBitArray::setVarBounds(double inLow, double inUpp)
{
	_Low = inLow;
	_Up = inUpp;
}
double	RealAsBitArray::getLowBound() 
{
	return _Low;
}
double	RealAsBitArray::getUppBound() 
{
	return _Up;
}
void		RealAsBitArray::adjustValueToBitRepr()
{
	// iz BitArraya idemo dobiti x
	long	NewIntValue = 0;
	BitArrayToInt(_BitsRepr->getRepr(), NewIntValue);

	// sada to treba pretvoriti u double unutar danog intervala
	long	MaxIntValue = 1 << _BitsRepr->getBitNum();
	_Value = (_Up - _Low) * NewIntValue / MaxIntValue  + _Low;
}
void		RealAsBitArray::adjustReprToValue()
{
	// konvertiramo x u BitArray
	int		BitNum = (int) _BitsRepr->getBitNum();
	long	MaxIntValue = 1 << BitNum;

	long	IntX = (long) (MaxIntValue * (_Value - _Low) / (_Up - _Low));

	vector<bool>	*newRepr = new vector<bool>;
	IntToBitArray(IntX, BitNum, newRepr);

	// i sada to treba ubaciti u internu reprezentaciju RealAsBitArray
	setRepr(newRepr);

	String ^s = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*newRepr)[i] == false )
			s += "0";
		else
			s += "1";
	}

	String ^s2 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*_BitsRepr->_vecBitsRepr)[i] == false )
			s2 += "0";
		else
			s2 += "1";
	}

	delete newRepr;
}

/********************************************************************************************************************/
Integer::Integer()
{
}
Integer::Integer(int inLow, int inUp)
{
}
Integer::Integer(Integer ^copy)
{
}

String^		Integer::getComponentName()
{
	return "Integer";
}
void			Integer::GenerateRandom()
{
}
IDesignVariable^	Integer::Clone()
{
	return gcnew Integer(this);
}
IRepresentation^	Integer::GetRepresentation()
{
	return this;
}

int		Integer::getValue()
{
	return _Value;
}
void	Integer::setValue(int inVal)
{
	_Value = inVal;
}
/********************************************************************************************************************/
IntegerArray::IntegerArray(int VarNum)
{
}
IntegerArray::IntegerArray(int VarNum, int AllLow, int AllUp)
{
}
IntegerArray::IntegerArray(IntegerArray ^copy)
{
}

String^		IntegerArray::getComponentName()
{
	return "IntegerArray";
}
void			IntegerArray::GenerateRandom()
{
}
IDesignVariable^	IntegerArray::Clone()
{
	return gcnew IntegerArray(this);
}
IRepresentation^	IntegerArray::GetRepresentation()
{
	return this;
}

Integer^		IntegerArray::getVar(int Ind)
{
	return (*_vecValues)[Ind];
}

int		IntegerArray::getVarNum()
{
	return _vecValues->size();
}
int		IntegerArray::getValue(int Ind)
{
	return 0;
}
void	IntegerArray::setValue(int Ind, int inVal)
{
}

/********************************************************************************************************************/
RealArray::RealArray(int VarNum) 
{
	setVarNum(VarNum);
}

String^	RealArray::getComponentName() 
{ 
	return "RealArray"; 
}
IDesignVariable^	RealArray::Clone()
{
	RealArray ^copy = gcnew RealArray(getVarNum());

	for( int i=0; i<getVarNum(); i++ )
	{
		copy->_vecValues[i] = _vecValues[i];
		copy->_vecLow[i]		= _vecLow[i];
		copy->_vecUp[i]			= _vecUp[i];
	}

	return copy;
}
IRepresentation^	RealArray::GetRepresentation()
{
	return this;
}

void	RealArray::setVarNum(int inVarNum) 
{
	_vecValues->resize(inVarNum);
	_vecLow->resize(inVarNum);
	_vecUp->resize(inVarNum);
}

int	RealArray::getVarNum()
{
	return (int) _vecValues->size();
}
double	RealArray::getValue(int VarInd) 
{ 
	return (*_vecValues)[VarInd]; 
}
void		RealArray::setValue(int VarInd, double inVal) 
{ 
	(*_vecValues)[VarInd] = inVal; 
}
void		RealArray::setVarBounds(int VarInd, double inLow, double inUpp)
{
	(*_vecLow)[VarInd] = inLow; 
	(*_vecUp)[VarInd]	= inUpp;
}
double	RealArray::getLowBound(int VarInd) 
{
	return (*_vecLow)[VarInd];
}
double	RealArray::getUppBound(int VarInd) 
{
	return (*_vecUp)[VarInd];
}
void	RealArray::GenerateRandom()
{
	for( int i=0; i<getVarNum(); i++ )
		(*_vecValues)[i] = rand() % 10000 * 3.141f / 100000 - 3.141f / 2;
}
/********************************************************************************************************************/

RealArray_AsBitArray::RealArray_AsBitArray(int VarNum) 
{
	_vecRepr->resize(VarNum);
}
RealArray_AsBitArray::RealArray_AsBitArray(RealArray_AsBitArray ^copy) 
{
	_vecRepr->resize(copy->getVarNum());
	for( int i=0; i<copy->getVarNum(); i++ )
	{
		(*_vecRepr)[i] = (*copy->_vecRepr)[i];
	}
}
IDesignVariable^	RealArray_AsBitArray::Clone()
{
	return gcnew RealArray_AsBitArray(this);
}
IRepresentation^	RealArray_AsBitArray::GetRepresentation()
{
	return this;
}
void	RealArray_AsBitArray::GenerateRandom()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
		(*_vecRepr)[i]->GenerateRandom();
}
String^	RealArray_AsBitArray::getComponentName() 
{ 
	return "RealArray_AsBitArray"; 
}
int	RealArray_AsBitArray::getVarNum() {
	return (int) _vecRepr->size();
}
double	RealArray_AsBitArray::getValue(int VarInd) 
{ 
	return (*_vecRepr)[VarInd]->getValue(); 
}
void		RealArray_AsBitArray::setVarBounds(int VarInd, double inLow, double inUpp)
{
	(*_vecRepr)[VarInd]->setVarBounds(inLow, inUpp);
}
double	RealArray_AsBitArray::getLowBound(int VarInd) 
{
	return (*_vecRepr)[VarInd]->getLowBound();
}
double	RealArray_AsBitArray::getUppBound(int VarInd) 
{
	return (*_vecRepr)[VarInd]->getUppBound();
}
void		RealArray_AsBitArray::setValue(int VarInd, double	inVal)
{
	(*_vecRepr)[VarInd]->setValue(inVal); 
//	adjustReprToValue();
}
RealAsBitArray^	RealArray_AsBitArray::getRepr(int VarInd )
{
	return (*_vecRepr)[VarInd];
}
void		RealArray_AsBitArray::setReprBit(int VarInd, int Ind, bool Val)
{
	(*_vecRepr)[VarInd]->getRepr()->setReprBit(Ind,Val);

	adjustValueToBitRepr();
}
bool		RealArray_AsBitArray::getReprBit(int VarInd, int BitNum)
{
	return (*_vecRepr)[VarInd]->getRepr()->getReprBit(BitNum);
}
void		RealArray_AsBitArray::FlopBit(int VarInd, int BitNum)
{
	if( (*_vecRepr)[VarInd]->getRepr()->getReprBit(BitNum) == false )
		(*_vecRepr)[VarInd]->getRepr()->setReprBit(BitNum,true);
	else
		(*_vecRepr)[VarInd]->getRepr()->setReprBit(BitNum,false);
}
void		RealArray_AsBitArray::setBitNum(int VarInd, int inBitNum)
{
	(*_vecRepr)[VarInd]->getRepr()->setBitNum(inBitNum);
}
int			RealArray_AsBitArray::getBitNum(int VarInd )
{
	return (int) (*_vecRepr)[VarInd]->getRepr()->getBitNum();
}
void		RealArray_AsBitArray::setVarMinResolution(int VarInd, double inMinRes)
{
	double		IntervalWidth = (*_vecRepr)[VarInd]->getUp() - (*_vecRepr)[VarInd]->getLow();
	double		NumDiff = IntervalWidth / inMinRes;

	int		BitNum = 1;
	while( NumDiff > 1 )
	{
		NumDiff /= 2.0;
		BitNum++;
	}

	(*_vecRepr)[VarInd]->getRepr()->setBitNum(BitNum);
}

void		RealArray_AsBitArray::adjustValueToBitRepr()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
	{
		// iz BitArraya idemo dobiti x
		long	NewIntValue = 0;
		BitArrayToInt((*_vecRepr)[i]->getRepr()->getRepr(), NewIntValue);

		// sada to treba pretvoriti u double unutar danog intervala
		long	MaxIntValue = 1 << (*_vecRepr)[i]->getRepr()->getBitNum();
		(*_vecRepr)[i]->setValue( ((*_vecRepr)[i]->getUp() - (*_vecRepr)[i]->getLow()) * NewIntValue / MaxIntValue  + (*_vecRepr)[i]->getLow() );
	}
}
void		RealArray_AsBitArray::adjustReprToValue()
{
	for( int i=0; i<(int) _vecRepr->size(); i++ )
	{
		// konvertiramo x u BitArray
		int		BitNum = (int) (*_vecRepr)[i]->getRepr()->getBitNum();
		long	MaxIntValue = 1 << BitNum;

		long	IntX = (long) (MaxIntValue * ((*_vecRepr)[i]->getValue() - (*_vecRepr)[i]->getLow()) / ((*_vecRepr)[i]->getUp() - (*_vecRepr)[i]->getLow()) );

		vector<bool>	*newRepr = new vector<bool>;
		IntToBitArray(IntX, BitNum, newRepr);

		// TODO
	}
}

/********************************************************************************************************************/
Permutation::Permutation()
{
}

Permutation::Permutation(Permutation ^copy)
{
}

String^		Permutation::getComponentName()
{
	return "Permutation";
}
void			Permutation::GenerateRandom()
{
}
IDesignVariable^	Permutation::Clone()
{
	return gcnew Permutation(this);
}
IRepresentation^	Permutation::GetRepresentation()
{
	return this;
}

