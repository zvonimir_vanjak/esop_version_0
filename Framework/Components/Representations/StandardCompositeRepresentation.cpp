// This is the main DLL file.

#include "stdafx.h"

#include "StandardCompositeRepresentation.h"
//#include "ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
GenericDynamicCompositeRepresentation::GenericDynamicCompositeRepresentation() 
{
	_arrParts = gcnew ArrayList();
}

GenericDynamicCompositeRepresentation::GenericDynamicCompositeRepresentation(GenericDynamicCompositeRepresentation ^copy)
{
	for( int i=0; i<(int)copy->_arrParts->Count; i++ )
		AddPart( (dynamic_cast<IDesignVariable ^>(copy->_arrParts[i]))->Clone());
}

IDesignVariable^	GenericDynamicCompositeRepresentation::Clone() 
{ 
	GenericDynamicCompositeRepresentation	^newObj = gcnew GenericDynamicCompositeRepresentation();

	for( int i=0; i<(int)_arrParts->Count; i++ )
		newObj->AddPart( (dynamic_cast<IDesignVariable ^>(_arrParts[i]))->Clone() );

	return newObj;
}
IDesignVariable^	GenericDynamicCompositeRepresentation::GetRepresentation()  
{ 
	return this; 
}

void	GenericDynamicCompositeRepresentation::GenerateRandom()
{
	// proci kroz sve dijelove i pozvati GenerateRandom
	for( int i=0; i<(int) _arrParts->Count; i++ )
		(dynamic_cast<IDesignVariable ^>(_arrParts[i]))->GenerateRandom();
}

IDesignVariable^	GenericDynamicCompositeRepresentation::GetPart(int PartIndex) 	
{	
	return dynamic_cast<IDesignVariable ^>(_arrParts[PartIndex]);	
}

int			GenericDynamicCompositeRepresentation::AddPart(IDesignVariable ^ inVar)  
{ 
	_arrParts->Add(inVar); 
	return (int) _arrParts->Count - 1;	
}

String^		GenericDynamicCompositeRepresentation::GetStringDesc()
{
	return gcnew String("DynamicCompositeRepr");
}
String^		GenericDynamicCompositeRepresentation::GetValueStringRepresentation() 
{
	return gcnew String(" ");
}
