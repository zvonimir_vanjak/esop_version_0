// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

#include "RealAsBitArray.h"

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class RealArray_AsBitArray : public IRealArray, public IESOPConstructedDesignVariable
			{
			public:
				RealArray_AsBitArray();
				RealArray_AsBitArray(int VarNum);
				RealArray_AsBitArray(RealArray_AsBitArray ^copy);
				~RealArray_AsBitArray();

				// IRealArray related
				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual	double		getValue(int VarInd);
				virtual	void			setValue(int VarInd, double inVal);

				// IESOPConstructedDesignVariable related
				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int ParamInd, double inVal);
				virtual double		getParamValue(int ParamInd);

				virtual int				getConstructorParamNum();
				virtual String^		getConstructorParamName(int ParamInd);
				virtual void			setConstructorParamValue(int ParamInd, double inVal);
				virtual double		getConstructorParamValue(int ParamInd);

				// Implementation
				int			getVarNum() ;
				void		setVarNum(int inVarNum);

				void		setBitNum(int VarInd, int inBitNum);
				int			getBitNum(int VarInd);
				void		setVarMinResolution(int VarInd, double inMinRes);

				void		setVarBounds(int VarInd, double inLow, double inUpp);
				double	getLowBound(int VarInd) ;
				double	getUppBound(int VarInd) ;
				void		setLowBound(int VarInd, double inLow) ;
				void		setUppBound(int VarInd, double inUpp) ;

				RealAsBitArray^		getRepr(int Ind);
				bool		getReprBit(int VarInd, int BitNum);
				void		setReprBit(int VarInd, int Ind, bool Val);

				void		FlopBit(int VarInd, int BitNum);

			private:
				void		adjustValueToBitRepr();			// kad se promijeni bit reprezentacija, poziva se da podesi realna vrijednost
				void		adjustReprToValue();				// kad se promijeni vrijednost, poziva se da se podesi reprezentacija

				vector<gcroot<RealAsBitArray ^> > *_vecRepr;
			};

		}
	}
}

