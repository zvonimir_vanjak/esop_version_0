#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace ESOP::Framework::Base;
//using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class GenericDynamicCompositeRepresentation : public ICompositeVariable
			{
			public:
				GenericDynamicCompositeRepresentation();
				GenericDynamicCompositeRepresentation(GenericDynamicCompositeRepresentation ^copy);
				virtual IDesignVariable^	Clone();
				virtual IDesignVariable^	GetRepresentation();

				virtual void		GenerateRandom();

				virtual IDesignVariable^	GetPart(int PartIndex);
				virtual int								AddPart(IDesignVariable ^ inVar);

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation();

			protected:
				ArrayList  ^_arrParts;
			};
		}
	}
}

