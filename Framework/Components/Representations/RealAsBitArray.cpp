// This is the main DLL file.

#include "stdafx.h"

#include "RealAsBitArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
RealAsBitArray::RealAsBitArray()
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(10);
	setVarBounds(0,1);
}
RealAsBitArray::RealAsBitArray(double inLow, double inUp) : Real(inLow, inUp)
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(10);
	setVarBounds(inLow, inUp);
}
RealAsBitArray::RealAsBitArray(double inLow, double inUp, int inBitNum) : Real(inLow, inUp)
{
	_BitsRepr = gcnew BitArray();

	_BitsRepr->setBitNum(inBitNum);
	setVarBounds(inLow, inUp);
}
RealAsBitArray::RealAsBitArray(const RealAsBitArray ^copy) : Real(copy) //, _BitsRepr(copy->_BitsRepr)
{
	_BitsRepr = (BitArray ^) copy->_BitsRepr->Clone();
}
RealAsBitArray^	RealAsBitArray::operator=(const RealAsBitArray ^b)
{
	_Value = b->_Value;
	_Low = b->_Low;
	_Up = b->_Up;
	_BitsRepr = b->_BitsRepr;
	return this;
}

IDesignVariable^	RealAsBitArray::Clone()
{
	return gcnew RealAsBitArray(this);
}
void		RealAsBitArray::GenerateRandom()
{
//	assert(_Up > _Low);

	int			RandomNumber	= rand() % 10000;
	double	IntervalWidth = _Up - _Low;

	setValue(RandomNumber * (IntervalWidth / 10000) + _Low);
}

String^		RealAsBitArray::GetStringDesc()
{
	return "RealAsBitArray";
}
String^		RealAsBitArray::GetValueStringRepresentation()
{
	StringBuilder	^ret = gcnew StringBuilder();

	ret->Append("x = ");
	ret->Append(getValue());
	ret->Append("  Bit repr. ");

	vector<bool> *Bit1 = getRepr()->getRepr();
	for( int i=0; i<(int) Bit1->size(); i++ )
	{
		if( (*Bit1)[i] == false )
			ret->Append("0");
		else
			ret->Append("1");
	}

	return ret->ToString();
}

int			RealAsBitArray::getParamNum()  
{ 
	return 3; 
}
String^	RealAsBitArray::getParamName(int ParamInd)  
{
	if( ParamInd == 0 ) 
		return "Low bound";
	else if( ParamInd == 1 ) 
		return "Upp bound";
	else return 
		"Bit number";
}
void		RealAsBitArray::setParamValue(int Ind, double inVal) 
{
	if (Ind == 0) 
		_Low = inVal;
	else if(Ind == 1)
		_Up = inVal;
	else 
		setBitNum((int) inVal);

}
double	RealAsBitArray::getParamValue(int Ind) 
{
	if( Ind == 0 ) 
		return _Low;
	else if( Ind == 1) 
		return _Up;
	else 
		return getBitNum();
}

void		RealAsBitArray::setBitNum(int inBitNum)
{
	_BitsRepr->setBitNum(inBitNum);
}
int			RealAsBitArray::getBitNum()
{
	return _BitsRepr->getBitNum();
}
void		RealAsBitArray::setVarMinResolution(double inMinRes)
{
	double		IntervalWidth = _Up - _Low;
	double		NumDiff = IntervalWidth / inMinRes;

	int		BitNum = 1;
	while( NumDiff > 1 )
	{
		NumDiff /= 2.0;
		BitNum++;
	}

	_BitsRepr->setBitNum(BitNum);
}

void		RealAsBitArray::setVarBounds(double inLow, double inUpp)	{	_Low = inLow;	_Up = inUpp;}
double	RealAsBitArray::getLowBound()							{	return _Low;	}
double	RealAsBitArray::getUppBound()							{	return _Up;	}
void		RealAsBitArray::setLowBound(double inLow)	{	_Low = inLow; }
void		RealAsBitArray::setUppBound(double inUpp)	{	_Up = inUpp; }

void		RealAsBitArray::setValue(double	inVal)
{
	_Value = inVal;
	adjustReprToValue();
}

BitArray^		RealAsBitArray::getRepr()
{
	return _BitsRepr;
}
void		RealAsBitArray::setRepr(const vector<bool> *inRepr)
{
	_BitsRepr->setRepr(inRepr);
	adjustValueToBitRepr();
}
void		RealAsBitArray::setReprBit(int Ind, bool Val)
{
	_BitsRepr->setReprBit(Ind, Val);

	adjustValueToBitRepr();
}
bool		RealAsBitArray::getReprBit(int BitNum)
{
	return _BitsRepr->getReprBit(BitNum);
}

void		RealAsBitArray::FlopBit(int BitNum)
{
	if( _BitsRepr->getReprBit(BitNum) == false )
		_BitsRepr->setReprBit(BitNum, true);
	else
		_BitsRepr->setReprBit(BitNum, false);

	adjustValueToBitRepr();
}

void		RealAsBitArray::adjustValueToBitRepr()
{
	// iz BitArraya idemo dobiti x
	long	NewIntValue = 0;
	BitArrayToInt(_BitsRepr->getRepr(), NewIntValue);

	// sada to treba pretvoriti u double unutar danog intervala
	long	MaxIntValue = (1 << _BitsRepr->getBitNum()) - 1;
	_Value = (_Up - _Low) * NewIntValue / MaxIntValue  + _Low;
}
void		RealAsBitArray::adjustReprToValue()
{
	// konvertiramo x u BitArray
	int		BitNum = (int) _BitsRepr->getBitNum();
	long	MaxIntValue = (1 << BitNum) - 1;

	long	IntX = (long) (MaxIntValue * (_Value - _Low) / (_Up - _Low));

	vector<bool>	*newRepr = new vector<bool>;
	IntToBitArray(IntX, BitNum, newRepr);

	// i sada to treba ubaciti u internu reprezentaciju RealAsBitArray
	setRepr(newRepr);

	String ^s = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*newRepr)[i] == false )
			s += "0";
		else
			s += "1";
	}

	String ^s2 = gcnew String("");
	for( int i=0; i<10; i++ )
	{
		if( (*_BitsRepr->_vecBitsRepr)[i] == false )
			s2 += "0";
		else
			s2 += "1";
	}

	delete newRepr;
}
