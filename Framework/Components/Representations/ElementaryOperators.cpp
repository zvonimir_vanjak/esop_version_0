#include "stdafx.h"

#include "ElementaryOperators.h"


void	IntToBitArray(long inVal, int inDigitNum, vector<bool> *outBitArray)
{
	// pretvaranje iz int u binarni prikaz s danom to�no��u
	long	x = inVal;
	deque<bool>		deqRes;

	int		digCount = 0;
	while( x > 0 )
	{
		int	bit = x % 2;
		if( bit == 0 )
			deqRes.push_front(false);
		else
			deqRes.push_front(true);

		x = x / 2;
		digCount++;
	}
	while( digCount++ < inDigitNum )
		deqRes.push_front(false);

	outBitArray->resize(inDigitNum);
	copy(deqRes.begin(), deqRes.end(), outBitArray->begin());
}

void	BitArrayToInt(const vector<bool> *inBitArray, long &outVal)
{
	outVal = 0;

	long		basePow = 1;
	for( long i=inBitArray->size()-1; i>=0; --i )
	{
		if( (*inBitArray)[i] == true )
			outVal += basePow;

		basePow *= 2;
	}
}

void	Perform_BitArray_1Point_Crossover(vector<bool> *p1, vector<bool> *p2, vector<bool> *c1, vector<bool> *c2)
{
	// odredi tocku crossovera
	int		CrossPoint = rand() % p1->size();

	// kopiramo bitove iz roditelja u djecu
	for( int i=0; i<(int) p1->size(); i++ )
	{
		if( i < CrossPoint ) {
			(*c1)[i] = (*p1)[i];
			(*c2)[i] = (*p2)[i];
		}
		else {
			(*c1)[i] = (*p2)[i];
			(*c2)[i] = (*p1)[i];
		}
	}
}

void	Perform_BitArray_2Point_Crossover(vector<bool> *p1, vector<bool> *p2, vector<bool> *c1, vector<bool> *c2)
{
	// odredi dvije tocke crossovera
	int		CrossPoint1;
	int		CrossPoint2;

	do {
		CrossPoint1 = rand() % p1->size();
		CrossPoint2 = rand() % p1->size();
	} while(CrossPoint1 == CrossPoint2 );

	if( CrossPoint2 < CrossPoint1 )
	{
		int		temp = CrossPoint1;
		CrossPoint1 = CrossPoint2;
		CrossPoint2 = temp;
	}
	// kopiramo bitove iz roditelja u djecu
	for( int i=0; i<(int) p1->size(); i++ )
	{
		if( i < CrossPoint1 || i > CrossPoint2 ) {
			(*c1)[i] = (*p1)[i];
			(*c2)[i] = (*p2)[i];
		}
		else {
			(*c1)[i] = (*p2)[i];
			(*c2)[i] = (*p1)[i];
		}
	}
}

void	Perform_BitArray_Uniform_Crossover(vector<bool> *p1, vector<bool> *p2, vector<bool> *c1, vector<bool> *c2)
{
	for( int i=0; i<(int) p1->size(); i++ )
	{
		if( rand() % 2 ) {
			(*c1)[i] = (*p1)[i];
			(*c2)[i] = (*p2)[i];
		}
		else {
			(*c1)[i] = (*p2)[i];
			(*c2)[i] = (*p1)[i];
		}
	}
}