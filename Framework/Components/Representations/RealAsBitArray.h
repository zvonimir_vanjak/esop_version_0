// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

#include "Real.h"
#include "BitArray.h"

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class RealAsBitArray : public Real
			{
			public:
				RealAsBitArray();
				RealAsBitArray(double inLow, double inUp);
				RealAsBitArray(double inLow, double inUp, int BitNum);
				RealAsBitArray(const RealAsBitArray ^copy);

				RealAsBitArray^	operator=(const RealAsBitArray ^b);

				virtual	String^		getESOPComponentName() override	{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc() override	{ return "Desc"; }

				virtual		void							GenerateRandom() override;
				virtual		IDesignVariable^	Clone() override;

				virtual String^		GetStringDesc() override;
				virtual String^		GetValueStringRepresentation() override;

				virtual int				getParamNum() override; 
				virtual String^		getParamName(int ParamInd) override ;
				virtual void			setParamValue(int Ind, double inVal) override;
				virtual double		getParamValue(int Ind) override;

				void		setBitNum(int inBitNum);
				int			getBitNum();
				void		setVarMinResolution	(double inMinRes);

				void		setVarBounds(double inLow, double inUpp);
				double	getLowBound(); 
				double	getUppBound();
				void		setLowBound(double inLow); 
				void		setUppBound(double inUpp);

				virtual void		setValue(double inVal) override;

				BitArray^		getRepr();
				void				setRepr(const vector<bool> *inRepr);
				bool				getReprBit(int BitNum);
				void				setReprBit(int Ind, bool Val);

				void				FlopBit(int BitNum);

			public:
				void		adjustValueToBitRepr();			// kad se promijeni bit reprezentacija, poziva se da podesi realna vrijednost
				void		adjustReprToValue();				// kad se promijeni vrijednost, poziva se da se podesi reprezentacija

				BitArray	^_BitsRepr;
			};

		}
	}
}

