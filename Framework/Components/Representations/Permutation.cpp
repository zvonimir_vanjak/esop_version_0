// This is the main DLL file.

#include "stdafx.h"

#include "Permutation.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
Permutation::Permutation()
{
	_Num = 0;
}

Permutation::Permutation(Permutation ^copy)
{
	setNum(copy->_Num);

	for( int i=0; i<_Num; i++ )
		_arrOrder[i] = copy->_arrOrder[i];
}
void			Permutation::GenerateRandom()
{
	if( _Num <= 0 ) return;

	for( int i=0; i<_Num; i++ )
		_arrOrder[i] = i;

	for( int i=0; i<_Num; i++ )
	{
		int	 r1 = rand() % _Num;
		int  r2 = rand() % _Num;

		int	 temp = _arrOrder[r1];
		_arrOrder[r1] = _arrOrder[r2];
		_arrOrder[r2] = temp;
	}
}
IDesignVariable^	Permutation::Clone()
{
	return gcnew Permutation(this);
}
IDesignVariable^	Permutation::GetRepresentation()
{
	return this;
}
String^		Permutation::GetStringDesc()
{
	return "";
}
String^		Permutation::GetValueStringRepresentation()
{
	String ^ret = gcnew String("");

	for(int i=0; i<_Num; i++ )
		ret += (_arrOrder[i].ToString() + " ");

	return ret;
}
int				Permutation::getParamNum() 
{ 
	return 1; 
}
String^		Permutation::getParamName(int ParamInd) 
{ 
	return "Num items";
}
void			Permutation::setParamValue(int Ind, double inVal) 
{ 
	setNum((int) inVal);
}
double		Permutation::getParamValue(int Ind) 
{ 
	return _Num; 
}
void			Permutation::setNum(int inNum)
{
	_Num = inNum;
	_arrOrder = gcnew array<int,1>(_Num);
}
int		Permutation::getNum()
{
	return _Num;
}
int				Permutation::getNth(int i) 
{ 
	return _arrOrder[i]; 
}
