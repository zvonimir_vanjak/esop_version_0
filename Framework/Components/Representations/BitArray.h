// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class BitArray : public IDesignVariable
			{
			public:
				BitArray();
				BitArray(const BitArray ^copy);

				BitArray^	operator=(const BitArray ^b);

				virtual void							GenerateRandom();
				virtual IDesignVariable^	Clone();

				virtual String^	GetValueStringRepresentation();

				void		setBitNum(int inBitNum);
				int			getBitNum();

				bool						getReprBit(int BitNum);
				void						setReprBit(int Ind, bool Val);
				void						setRepr(const vector<bool> *inRepr);
				vector<bool>*		getRepr() ;

				void		FlopBit(int BitNum);

				String^	getBits();

			public:
				vector<bool>		*_vecBitsRepr;
			};

		}
	}
}

