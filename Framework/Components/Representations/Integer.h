// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class Integer : public IInteger, public IESOPDesignVariable
			{
			public:
				Integer();
				Integer(int inLow, int inUp);
				Integer(Integer ^copy);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int Ind, double inVal);
				virtual double		getParamValue(int Ind);

				virtual int		getValue();
				virtual void	setValue(int inVal);

				void		setVarBounds(int inLow, int inUpp);
				int			getLowBound(); 
				int			getUppBound();
				void		setLowBound(int inLow); 
				void		setUppBound(int inUpp);

			public:
				int		_Value;
				int		_Low, _Up;
			};

		}
	}
}

