// This is the main DLL file.

#include "stdafx.h"

#include "IntegerArray.h"
#include "..\..\Components\Operators\ElementaryOperators.h"

using namespace System::Text;
using namespace ESOP::Framework::StandardRepresentations;

/********************************************************************************************************************/
IntegerArray::IntegerArray(int VarNum)
{
}
IntegerArray::IntegerArray(int VarNum, int AllLow, int AllUp)
{
}
IntegerArray::IntegerArray(IntegerArray ^copy)
{
}

void			IntegerArray::GenerateRandom()
{
}
IDesignVariable^	IntegerArray::Clone()
{
	return gcnew IntegerArray(this);
}

String^		IntegerArray::GetStringDesc()
{
	return "IntegerArray";
}
String^		IntegerArray::GetValueStringRepresentation()
{
	return gcnew String(" ");
}

int				IntegerArray::getParamNum() 
{ 
	return 1 + getVarNum() * 2; 
}
String^		IntegerArray::getParamName(int ParamInd) 
{
	if( ParamInd == 0 ) return "Num var";
	else 
	{
		String ^strInd = gcnew String("");
		strInd->Format("%d", (ParamInd - 1) / 2);
		
		if( ParamInd % 2 == 1 ) return "Low bound " + strInd;
		else return "Upp.bound " + strInd;
	}
}
void			IntegerArray::setParamValue(int ParamInd, double inVal) 
{
	if( ParamInd == 0 )	
		setVarNum((int) inVal);
	else 
	{
		if( ParamInd % 2 == 1 ) setLowBound((ParamInd-1) / 2, (int) inVal);
		else setUppBound((ParamInd-1) / 2, (int) inVal);
	}
}
double		IntegerArray::getParamValue(int ParamInd) 
{
	if( ParamInd == 0 ) return getVarNum();
	else 
	{
		if( ParamInd % 2 == 1 ) return getLowBound((ParamInd-1) / 2);
		else return getUppBound((ParamInd-1) / 2);
	}
}

Integer^		IntegerArray::getVar(int Ind)
{
	return (*_vecRepr)[Ind];
}

int		IntegerArray::getVarNum()
{
	return _vecRepr->size();
}
void	IntegerArray::setVarNum(int inVarNum)
{
	_vecRepr = new vector<gcroot<Integer ^> >();
	_vecRepr->resize(inVarNum);

	for(int i=0; i<inVarNum; i++ )
		(*_vecRepr)[i] = gcnew Integer();
}
int		IntegerArray::getValue(int Ind)
{
	return 0;
}
void	IntegerArray::setValue(int Ind, int inVal)
{
}

void		IntegerArray::setVarBounds(int VarInd, int inLow, int inUpp)	
{	
	getVar(VarInd)->setLowBound(inLow);	
	getVar(VarInd)->setUppBound(inUpp);
}
int			IntegerArray::getLowBound(int VarInd)							{	return getVar(VarInd)->getLowBound();}
int			IntegerArray::getUppBound(int VarInd)							{	return getVar(VarInd)->getUppBound();}
void		IntegerArray::setLowBound(int VarInd, int inLow)	{	getVar(VarInd)->setLowBound(inLow);}
void		IntegerArray::setUppBound(int VarInd, int inUpp)	{	getVar(VarInd)->setUppBound(inUpp);}

