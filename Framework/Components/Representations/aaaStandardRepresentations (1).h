// BasicRepresentations.h

#pragma once

#include <vector>
#include <vcclr.h>

using std::vector;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;

using namespace System;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardRepresentations
		{
			public ref class BitArray : public IDesignVariable
			{
			public:
				BitArray();
				BitArray(const BitArray ^copy);

				BitArray^	operator=(const BitArray ^b);

				virtual void							GenerateRandom();
				virtual IDesignVariable^	Clone();

				virtual String^	GetValueStringRepresentation();

				void		setBitNum(int inBitNum);
				int			getBitNum();

				bool						getReprBit(int BitNum);
				void						setReprBit(int Ind, bool Val);
				void						setRepr(const vector<bool> *inRepr);
				vector<bool>*		getRepr() ;

				void		FlopBit(int BitNum);

				String^	getBits();

			public:
				vector<bool>		*_vecBitsRepr;
			};

			public ref class Real : public IReal, public IESOPDesignVariable
			{
			public:
				Real();
				Real(double inLow, double inUp);
				Real(const Real ^copy);
				
				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc() ;
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int Ind, double inVal);
				virtual double		getParamValue(int Ind);

				virtual	double		getValue();
				virtual	void			setValue(double inVal);

				void			setBoundary(double Low, double Up);
				double		getUp();
				double		getLow();

			protected:
				double	_Value;
				double	_Low, _Up;
			};
			
			public ref class RealAsBitArray : public Real
			{
			public:
				RealAsBitArray();
				RealAsBitArray(double inLow, double inUp);
				RealAsBitArray(double inLow, double inUp, int BitNum);
				RealAsBitArray(const RealAsBitArray ^copy);

				RealAsBitArray^	operator=(const RealAsBitArray ^b);

				virtual	String^		getESOPComponentName() override	{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc() override	{ return "Desc"; }

				virtual		void							GenerateRandom() override;
				virtual		IDesignVariable^	Clone() override;

				virtual String^		GetStringDesc() override;
				virtual String^		GetValueStringRepresentation() override;

				virtual int				getParamNum() override; 
				virtual String^		getParamName(int ParamInd) override ;
				virtual void			setParamValue(int Ind, double inVal) override;
				virtual double		getParamValue(int Ind) override;

				void		setBitNum(int inBitNum);
				int			getBitNum();
				void		setVarMinResolution	(double inMinRes);

				void		setVarBounds(double inLow, double inUpp);
				double	getLowBound(); 
				double	getUppBound();
				void		setLowBound(double inLow); 
				void		setUppBound(double inUpp);

				virtual void		setValue(double inVal) override;

				BitArray^		getRepr();
				void				setRepr(const vector<bool> *inRepr);
				bool				getReprBit(int BitNum);
				void				setReprBit(int Ind, bool Val);

				void				FlopBit(int BitNum);

			public:
				void		adjustValueToBitRepr();			// kad se promijeni bit reprezentacija, poziva se da podesi realna vrijednost
				void		adjustReprToValue();				// kad se promijeni vrijednost, poziva se da se podesi reprezentacija

				BitArray	^_BitsRepr;
			};

			public ref class RealArray : public IRealArray, public IESOPConstructedDesignVariable
			{
			public:
				RealArray();
				RealArray(int VarNum);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int ParamInd, double inVal);
				virtual double		getParamValue(int ParamInd);

				virtual int				getConstructorParamNum();
				virtual String^		getConstructorParamName(int ParamInd);
				virtual void			setConstructorParamValue(int ParamInd, double inVal);
				virtual double		getConstructorParamValue(int ParamInd);

				int			getVarNum();
				void		setVarNum(int inVarNum);

				virtual double	getValue(int VarInd) ;
				virtual void		setValue(int VarInd, double inVal) ;

				void		setVarBounds(int VarInd, double inLow, double inUpp);
				double	getLowBound(int VarInd) ;
				double	getUppBound(int VarInd) ;
				void		setLowBound(int VarInd, double inLow) ;
				void		setUppBound(int VarInd, double inUpp) ;

			public:
				vector<double>	*_vecValues;
				vector<double>	*_vecLow, *_vecUp;
			};

			public ref class RealArray_AsBitArray : public IRealArray, public IESOPConstructedDesignVariable
			{
			public:
				RealArray_AsBitArray();
				RealArray_AsBitArray(int VarNum);
				RealArray_AsBitArray(RealArray_AsBitArray ^copy);
				~RealArray_AsBitArray();

				// IRealArray related
				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual	double		getValue(int VarInd);
				virtual	void			setValue(int VarInd, double inVal);

				// IESOPConstructedDesignVariable related
				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int ParamInd, double inVal);
				virtual double		getParamValue(int ParamInd);

				virtual int				getConstructorParamNum();
				virtual String^		getConstructorParamName(int ParamInd);
				virtual void			setConstructorParamValue(int ParamInd, double inVal);
				virtual double		getConstructorParamValue(int ParamInd);

				// Implementation
				int			getVarNum() ;
				void		setVarNum(int inVarNum);

				void		setBitNum(int VarInd, int inBitNum);
				int			getBitNum(int VarInd);
				void		setVarMinResolution(int VarInd, double inMinRes);

				void		setVarBounds(int VarInd, double inLow, double inUpp);
				double	getLowBound(int VarInd) ;
				double	getUppBound(int VarInd) ;
				void		setLowBound(int VarInd, double inLow) ;
				void		setUppBound(int VarInd, double inUpp) ;

				RealAsBitArray^		getRepr(int Ind);
				bool		getReprBit(int VarInd, int BitNum);
				void		setReprBit(int VarInd, int Ind, bool Val);

				void		FlopBit(int VarInd, int BitNum);

			private:
				void		adjustValueToBitRepr();			// kad se promijeni bit reprezentacija, poziva se da podesi realna vrijednost
				void		adjustReprToValue();				// kad se promijeni vrijednost, poziva se da se podesi reprezentacija

				vector<gcroot<RealAsBitArray ^> > *_vecRepr;
			};

			public ref class Integer : public IInteger, public IESOPDesignVariable
			{
			public:
				Integer();
				Integer(int inLow, int inUp);
				Integer(Integer ^copy);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int Ind, double inVal);
				virtual double		getParamValue(int Ind);

				virtual int		getValue();
				virtual void	setValue(int inVal);

				void		setVarBounds(int inLow, int inUpp);
				int			getLowBound(); 
				int			getUppBound();
				void		setLowBound(int inLow); 
				void		setUppBound(int inUpp);

			public:
				int		_Value;
				int		_Low, _Up;
			};

			public ref class IntegerArray : public IIntegerArray, public IESOPDesignVariable
			{
			public:
				IntegerArray() { setVarNum(1); }
				IntegerArray(int VarNum);
				IntegerArray(int VarNum, int AllLow, int AllUp);
				IntegerArray(IntegerArray ^copy);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void							GenerateRandom();
				virtual	IDesignVariable^	Clone();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int ParamInd, double inVal);
				virtual double		getParamValue(int ParamInd);

				Integer^			getVar(int Ind);

				virtual int		getVarNum();
				virtual void	setVarNum(int inVarNum);
				virtual int		getValue(int Ind);
				virtual void	setValue(int Ind, int inVal);

				void		setVarBounds(int VarInd, int inLow, int inUpp);
				int			getLowBound(int VarInd) ;
				int			getUppBound(int VarInd) ;
				void		setLowBound(int VarInd, int inLow) ;
				void		setUppBound(int VarInd, int inUpp) ;

			public:
				vector<gcroot<Integer ^> >		*_vecRepr;
			};

			public ref class Permutation : public IPermutation, public IESOPDesignVariable
			{
			public:
				Permutation();
				Permutation(Permutation ^copy);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	void			GenerateRandom();
				virtual	IDesignVariable^	Clone();
				virtual	IDesignVariable^	GetRepresentation();

				virtual String^		GetStringDesc();
				virtual String^		GetValueStringRepresentation() ;

				virtual int				getParamNum();
				virtual String^		getParamName(int ParamInd);
				virtual void			setParamValue(int Ind, double inVal);
				virtual double		getParamValue(int Ind);

				void						setNum(int inNum);
				int							getNum();
				virtual int			getNth(int i);

			private:
				int						_Num;
				array<int, 1> ^_arrOrder;
			};
		}
	}
}

