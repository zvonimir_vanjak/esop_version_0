#pragma once

#include "..\Algorithms\GAHelpers.h"

using namespace System;

using namespace ESOP::Framework::Algorithms::GeneticAlgorithms;

namespace ESOP
{
	namespace Framework
	{
		namespace Base
		{
			public interface class IPopulationConvergenceProvider
			{
			public:
				virtual	double		calcConvergenceMeasure(Population ^inPop) = 0;
			};
		}
	}
}