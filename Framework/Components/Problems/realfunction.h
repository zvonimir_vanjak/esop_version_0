#pragma once

//#include "Components\Representations\StandardRepresentations.h"
#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;
//using namespace ESOP::Framework::StandardRepresentations;

namespace ESOP
{
	namespace Framework
	{
		namespace Problems
		{
			namespace RealFunctionOptimization
			{
				public ref class ISOFunctionOptimization abstract : public ISingleObjectiveProblem
				{
				public:
//					virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance) = 0; // FROM Base class

					virtual int			getNumVar() = 0;
					virtual String^	getVarName(int i) = 0;
					virtual void		getVarBounds(int i, double ^outLower, double ^outUpper) = 0;
				};

				public ref class IMOFunctionOptimization abstract : public IMultiObjectiveProblem
				{
				public:
//					virtual void		calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^val) = 0;

//					virtual int			getNumObjectives() = 0;
//					virtual String^	getObjectiveName(int i) = 0;

					virtual int			getNumVar() = 0;
					virtual String^	getVarName(int i) = 0;
					virtual void		getVarBounds(int i, double &outLower, double &outUpper) = 0;
				};

				public ref class  ISOWCFunctionOptimization abstract : public ISingleObjectiveProblemWC
				{
				public:
//					virtual double	calcObjective(ISingleObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcConstrValues) =0;

//					virtual int			getNumConstraints() = 0;
//					virtual String^	getConstraintName(int i) = 0;
					virtual int			getNumVar() = 0;
					virtual String^	getVarName(int i) = 0;
					virtual void		getVarBounds(int i, double &outLower, double &outUpper) = 0;
				};

				public ref class  IMOWCFunctionOptimization abstract : public IMultiObjectiveProblemWC
				{
				public:
//					virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) = 0;
	
//					virtual int			getNumObjectives() = 0;
//					virtual int			getNumConstraints() = 0;

//					virtual String^	getObjectiveName(int i) = 0;
//					virtual String^	getConstraintName(int i) = 0;
					virtual int			getNumVar() = 0;
					virtual String^	getVarName(int i) = 0;
					virtual void		getVarBounds(int i, double &outLower, double &outUpper) = 0;
				};
			}
		}
	}
}
