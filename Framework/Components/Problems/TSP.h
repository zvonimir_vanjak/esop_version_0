#pragma once

//#include "Components\Representations\StandardRepresentations.h"

//#include "Base\DynamicSupport.h"

using namespace System;

using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::DynamicSupport;


namespace ESOP
{
	namespace Framework
	{
		namespace Problems
		{
			namespace TSP
			{
				public ref class ITravellingSalesmanProblem abstract : public ISingleObjectiveProblem
				{
				public:
					virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance) override 
					{ 
						IPermutation		^sol = dynamic_cast<IPermutation ^>(inSolInstance->getDesignVar());
						double		TotalTripLen = 0;

						int		City1 = sol->getNth(0);
						int		City2;
						for( int i=1; i<getNumCities(); i++ )
						{
							City2 = sol->getNth(i);
							TotalTripLen += getDistBetweenCities(City1, City2);
							City1 = City2;
						}

						return TotalTripLen;
					}

					virtual	int			getNumCities() = 0;
					virtual double	getDistBetweenCities(int CityInd1, int CityInd2) = 0;
				};

				public ref class TSP_Complete abstract : public ITravellingSalesmanProblem, public IESOPCompleteProblem
				{
				public:
					TSP_Complete();
					TSP_Complete(int inNumCities);

					virtual	String^		getESOPComponentName() 		{ return "TSP_Complete"; }
					virtual String^		getESOPComponentDesc()		{ return "TSP_Complete"; }
	
					virtual	String^		getRequestedDesignVar()  { return "IPermutation"; }

					virtual int			getNumCities() override;
					virtual double	getDistBetweenCities(int CityInd1, int CityInd2) override;

					void		setNumCities(int inCityNum);

				protected:
					int			_NumCities;
					array<double, 2> ^_arrDist;
				};

				public ref class TSP_FromFile : public ITravellingSalesmanProblem, public IESOPFileInitProblem
				{
				public:
					virtual	String^		getESOPComponentName() 		{ return "TSP_FromFile"; }
					virtual String^		getESOPComponentDesc()		{ return "TSP_FromFile"; }

					virtual	String^		getRequestedDesignVar()  { return "IPermutation"; }

					virtual int			getNumCities() override;
					virtual double	getDistBetweenCities(int CityInd1, int CityInd2) override;

					virtual void	InitFromFile(String ^FileName);
				};
			}
		}
	}
}
