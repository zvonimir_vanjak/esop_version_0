#include "stdafx.h"
#include "TSP.h"

using namespace ESOP::Framework::Problems::TSP;

int			TSP_FromFile::getNumCities() 
{ 
	return 0; 
}
double	TSP_FromFile::getDistBetweenCities(int CityInd1, int CityInd2)
{
	return 0;
}

void	TSP_FromFile::InitFromFile(String ^FileName) 
{ 
}


TSP_Complete::TSP_Complete()
{
	setNumCities(10);
}
TSP_Complete::TSP_Complete(int inNumCities)
{
	setNumCities(inNumCities);
}
int			TSP_Complete::getNumCities() 
{ 
	return _NumCities; 
}
double	TSP_Complete::getDistBetweenCities(int CityInd1, int CityInd2)
{
	return _arrDist[CityInd1,CityInd2];
}

void		TSP_Complete::setNumCities(int inCityNum)
{
	_NumCities = inCityNum;
	_arrDist = gcnew array<double, 2>(inCityNum, inCityNum);
}
