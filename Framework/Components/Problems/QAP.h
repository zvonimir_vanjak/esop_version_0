#pragma once

#include "..\..\Components\Representations\Permutation.h"


using namespace ESOP::Framework;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;

namespace ESOP
{
	namespace Framework
	{
		namespace Problems
		{
			namespace QAP
			{
				public ref class IQuadraticAssignmentProblem abstract: public ISingleObjectiveProblem
				{
				public:
					virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance) override
					{ 
						int		i, j, pi, pj, N;
						double	f=0.0;

						IPermutation		^sol = dynamic_cast<IPermutation ^>(inSolInstance->getDesignVar());

						N = getFacilityNum();

						for( i=0; i<N; i++ ) {
							for( j=0; j<N; j++ ) {
								pi = sol->getNth(i);
								pj = sol->getNth(j);

								f += getA(i,j) * getB(pi, pj);
							}
						}
						return f;
					}

					virtual	int			getFacilityNum() = 0;
					virtual	double	getA(int i, int j) = 0;
					virtual	double	getB(int i, int j) = 0;
				};

				public ref class QAP_Complete : public IQuadraticAssignmentProblem, public IESOPCompleteProblem
				{
				public:
					QAP_Complete();
					QAP_Complete(int inNumFacilities);

					virtual	String^		getESOPComponentName() 		{ return "QAP_Complete"; }
					virtual String^		getESOPComponentDesc()		{ return "QAP_Complete"; }

					virtual	String^		getRequestedDesignVar()  { return "IPermutation"; }

					void		setFacilityNum(int inFacNum)
					{
					}
					virtual	int			getFacilityNum() override
					{
						return _NumFacilities;
					}
					virtual	double	getA(int i, int j) override
					{
						return _arrA[i,j];
					}
					virtual	double	getB(int i, int j) override
					{
						return _arrB[i,j];
					}

				protected:
					int			_NumFacilities;
					array<double, 2> ^_arrA;
					array<double, 2> ^_arrB;
				};

				public ref class QAP_FromFile : public IQuadraticAssignmentProblem, public IESOPFileInitProblem
				{
				public:
					virtual	String^		getESOPComponentName() 		{ return "QAP_FromFile"; }
					virtual String^		getESOPComponentDesc()		{ return "QAP_FromFile"; }

					virtual	String^		getRequestedDesignVar()  { return "IPermutation"; }

					virtual	int			getFacilityNum() override
					{
						return _NumFacilities;
					}
					virtual	double	getA(int i, int j) override
					{
						return _arrA[i,j];
					}
					virtual	double	getB(int i, int j) override
					{
						return _arrB[i,j];
					}

					virtual void	InitFromFile(String ^FileName)
					{
					}

				protected:
					int			_NumFacilities;
					array<double, 2> ^_arrA;
					array<double, 2> ^_arrB;
				};
			}
		}
	}
}
