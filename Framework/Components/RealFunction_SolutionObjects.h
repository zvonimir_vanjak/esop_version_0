#include "RealFunction.h"
#include "..\Components\Representations\StandardRepresentations.h"

using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::StandardRepresentations;

namespace ESOP
{
	namespace Framework
	{
		namespace StandardProblems
		{
			namespace RealFunctionOptimization
			{
				
				/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				class RealFunction_SingleRealVar_SolObject : public ISingleObjectiveSolution
				{
				public:
					RealFunction_SingleRealVar_SolObject(double inLow, double	inUp);
					RealFunction_SingleRealVar_SolObject(RealFunction_SingleRealVar_SolObject &copy);

					CString*			getComponentName();
					ISolution*	Clone();
					void				setRepresentationObject(IDesignVariable *inRepr);

				private:
					double		_Low, _Upp;
				};
				*/
				public ref class MultiObjectiveRealFunc_SingleVar_SolObject : public IMultiObjectiveSolution
				{
				public:					// from interfaces
					MultiObjectiveRealFunc_SingleVar_SolObject();
					MultiObjectiveRealFunc_SingleVar_SolObject(MultiObjectiveRealFunc_SingleVar_SolObject ^copy);

					virtual	IRepresentation^	GetRepresentation() override;

					void		setFitness(double inFitness);
					double	getFitness();

				protected:
					RealAsBitArray		^_pRepresentation;

					double	_CurrFitness;
				};

				public ref class MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr : public IMultiObjectiveSolution
				{
				public:
					MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr();
					MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr(MultiObjectiveRealFunc_MultiVar_RealArrayAsBitArrayRepr ^copy);

					virtual IRepresentation^	GetRepresentation() override;

					int			getNumObjectives();
//					virtual void		setMultiObjectiveProblem(IMultiObjectiveProblem ^inProblem) override;

				protected:
					RealArray_AsBitArray		^_pRepresentation;
				};
			}
		}
	}
}
