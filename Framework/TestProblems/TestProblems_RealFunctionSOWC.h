#pragma once

//#include "RealFunction.h"
#using "E:\Projects\ESOP_version_0\Debug\Components.dll"

#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Problems::RealFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;

namespace ESOP
{
	namespace TestProblems
	{
		namespace SOFunctionOptimization
		{
			/*************************************************************************************/
			public ref class SOWC1_TestProblem : public ISOWCFunctionOptimization
			{
			public:
				SOWC1_TestProblem(int inNumVar);

				virtual	String^		getESOPComponentName() 		{ return "Components.dll"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual String^	getProblemDesc() { return "SOWC1_TestProblem";	}

				virtual double	calcObjective(ISingleObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcConstrValues) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			private:
				int		_NumVar;
			};
		}
	}
}