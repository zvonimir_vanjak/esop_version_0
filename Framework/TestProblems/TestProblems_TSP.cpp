#include "stdafx.h"
#include "TestProblems_TSP.h"

#include <math.h>

using namespace ESOP::TestProblems::TSP;
using namespace ESOP::Framework::StandardRepresentations;

TSPComplete10_TestProblem::TSPComplete10_TestProblem() : TSP_Complete(10)
{
	double polje[10][10] = 
		{ 
			0,  5,  14, 3,  25, 13, 7,  8,  35, 18, 
			5,  0,  7,  77, 34, 12, 5,  9,  17, 25, 
			14, 7,  0,  54, 14, 11, 23, 3,  5,  9, 
			3,  77, 54, 0,  12, 19, 8,  25, 35, 22, 
			25, 34, 14, 12, 0,  14, 87, 34, 67, 28, 
			13, 12, 11, 19, 14, 0,  12, 4,  5,  7, 
			7,  5,  23, 8,  87, 12, 0,  27, 13, 2, 
			8,  9,  3,  25, 34, 4,  27, 0,  18, 25, 
			35, 17, 5,  35, 67, 5,  13, 18, 0,  13, 
			18, 25, 9,  22, 28, 7,  2,  25, 12, 0
		};

	for(int i=0; i<10; i++ )
		for( int j=0; j<10; j++ )
		{
			_arrDist[i,j] = polje[i][j];
		}
}
