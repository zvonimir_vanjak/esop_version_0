#pragma once

//#include "RealFunction.h"
#using "E:\Projects\ESOP_version_0\Debug\Components.dll"

#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Problems::RealFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::DynamicSupport;

namespace ESOP
{
	namespace TestProblems
	{
		namespace SOFunctionOptimization
		{
			/*************************************************************************************/
			public ref class SinFunc_TestProblem : public ISOFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				SinFunc_TestProblem()
				{
					setObjectiveType(EObjectiveType::MAX_OBJECTIVE);
				}
				virtual	String^		getESOPComponentName() 		{ return "Sin.func"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual double		calcObjective(ISingleObjectiveSolution ^inSolInstance) override;

				virtual	String^		getRequestedDesignVar()  { return "IReal"; }

				virtual int				getNumVar() override;
				virtual String^		getVarName(int i) override;
				virtual void			getVarBounds(int i, double ^outLower, double ^outUpper) override;
			};

			public ref class DeJong_F1_TestProblem : public ISOFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				DeJong_F1_TestProblem() : _NumVar(5) 
				{ 
					_enProblemType = EObjectiveType::MIN_OBJECTIVE; 
				}
				DeJong_F1_TestProblem(int inNumVar) : _NumVar(inNumVar) {}

				virtual	String^		getESOPComponentName() 		{ return "DeJong F1 function"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual double		calcObjective(ISingleObjectiveSolution ^inSolInstance) override;

				virtual	String^		getRequestedDesignVar()  { return "IRealArray"; }

				virtual int				getNumVar() override;
				virtual String^		getVarName(int i) override;
				virtual void			getVarBounds(int i, double ^outLower, double ^outUpper) override;

				void setNumVar(int inVarNum)
				{
					_NumVar = inVarNum;
				}
			private:
				int		_NumVar;
			};
			public ref class AckleyFunction_TestProblem : public ISOFunctionOptimization, public IESOPParametrizedProblem
			{
			public:
				AckleyFunction_TestProblem() : _NumVar(5) 
				{ 
					_enProblemType = EObjectiveType::MIN_OBJECTIVE; 
				}
				AckleyFunction_TestProblem(int inNumVar) : _NumVar(inNumVar) {}

				virtual	String^		getESOPComponentName() 		{ return "Ackley function"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return "Number of variables"; }
				
				virtual void			setParamValue(int Ind, double inVal) { setNumVar((int) inVal); }
				virtual double		getParamValue(int Ind) { return _NumVar; }

				virtual double		calcObjective(ISingleObjectiveSolution ^inSolInstance) override;

				virtual	String^		getRequestedDesignVar()  { return "IRealArray"; }

				virtual int				getNumVar() override;
				virtual String^		getVarName(int i) override;
				virtual void			getVarBounds(int i, double ^outLower, double ^outUpper) override;

				void setNumVar(int inVarNum)
				{
					_NumVar = inVarNum;
				}
			private:
				int		_NumVar;
			};

			public ref class SchwefelFunction_TestProblem : public ISOFunctionOptimization, public IESOPParametrizedProblem
			{
			public:
				SchwefelFunction_TestProblem() : _NumVar(5) 
				{ 
					_enProblemType = EObjectiveType::MIN_OBJECTIVE; 
				}

				SchwefelFunction_TestProblem(int inNumVar) : _NumVar(inNumVar) {}

				virtual	String^		getESOPComponentName() 		{ return "Schwefel function"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return "Number of variables"; }
				
				virtual void			setParamValue(int Ind, double inVal) { setNumVar((int) inVal); }
				virtual double		getParamValue(int Ind) { return _NumVar; }

				virtual double		calcObjective(ISingleObjectiveSolution ^inSolInstance) override;

				virtual	String^		getRequestedDesignVar()  { return "IRealArray"; }

				virtual int				getNumVar() override;
				virtual String^		getVarName(int i) override;
				virtual void			getVarBounds(int i, double ^outLower, double ^outUpper) override;

				void setNumVar(int inVarNum)
				{
					_NumVar = inVarNum;
				}
			private:
				int		_NumVar;
			};
		}
	}
}
