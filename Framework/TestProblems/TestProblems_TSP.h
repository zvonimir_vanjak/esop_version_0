#pragma once

#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Problems::TSP;
using namespace ESOP::Framework::StandardRepresentations;

namespace ESOP
{
	namespace TestProblems
	{
		namespace TSP
		{
			public ref class TSPComplete10_TestProblem : public TSP_Complete
			{
			public:
				TSPComplete10_TestProblem();

				virtual	String^		getESOPComponentName() 	override	{ return "TSP_Complete10"; }
				virtual String^		getESOPComponentDesc()	override	{ return "TSP_Complete10"; }
			};
		}
	}
}
