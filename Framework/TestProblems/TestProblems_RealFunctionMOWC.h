#pragma once

//#include "RealFunction.h"
#using "E:\Projects\ESOP_version_0\Debug\Components.dll"

#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Problems::RealFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::DynamicSupport;

using namespace ESOP::Framework::Operators::GeneticAlgorithms;

namespace ESOP
{
	namespace TestProblems
	{
		namespace SOFunctionOptimization
		{
			/*************************************************************************************/
			public ref class BNH_TestProblem : public IMOWCFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				BNH_TestProblem();
				
				virtual	String^		getESOPComponentName() 		{ return "BNH"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual	String^	getRequestedDesignVar()  { return "IRealArray"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};
			public ref class SRN_TestProblem : public IMOWCFunctionOptimization
			{
			public:
				SRN_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "SRN"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class TNK_TestProblem : public IMOWCFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				TNK_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "TNK"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual	String^	getRequestedDesignVar()  { return "IRealArray"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class OSY_TestProblem : public IMOWCFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				OSY_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "OSY"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual	String^	getRequestedDesignVar()  { return "IRealArray"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class TwoBarTrussDesign_TestProblem : public IMOWCFunctionOptimization
			{
			public:
				TwoBarTrussDesign_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Two-Bar truss design"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class WeldedBeamDesign_TestProblem : public IMOWCFunctionOptimization
			{
			public:
				WeldedBeamDesign_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Welded beam design"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumConstraints() override;
				virtual String^	getConstraintName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class SpeedReducerDesignVariable : public IESOPDesignVariable, public GenericDynamicCompositeRepresentation
			{
			public:
				SpeedReducerDesignVariable();

				virtual String^		getESOPComponentName() { return "SpeedReducerDesignVariable"; }
				virtual String^		getESOPComponentDesc() { return "SpeedReducerDesignVariable"; }

				virtual int				getParamNum() { return 0; }
				virtual String^		getParamName(int ParamInd) { return ""; }
				
				virtual void			setParamValue(int Ind, double inVal) {}
				virtual double		getParamValue(int Ind) { return 0; }
			};

			public ref class SpeedReducer_Crossover : public ICrossoverOperator2To2 , public IESOPOperator
			{
			public:
				virtual void			Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2);

				virtual	String^		getESOPComponentName() 		{ return "SpeedReducer_Crossover"; }
				virtual String^		getESOPComponentDesc()		{ return "SpeedReducer_Crossover"; }

				virtual String^		ToString() override { return getESOPComponentName(); }
			};

			public ref class SpeedReducer_Mutation : public IMutationOp , public IESOPParameterOperator
			{
			public:
				SpeedReducer_Mutation();

				void							setMutationprob(double inMutProb);

				virtual void			Mutate(IDesignVariable ^Child);

				virtual	String^		getESOPComponentName() 		{ return "SpeedReducer_Mutation"; }
				virtual String^		getESOPComponentDesc()		{ return "SpeedReducer_Mutation"; }

				virtual int				getParamNum() { return 1; }
				virtual String^		getParamName(int ParamInd) { return "Mutation probability per bit"; }
				
				virtual void			setParamValue(int Ind, double inVal) { setMutationprob(inVal); }
				virtual double		getParamValue(int Ind) { return (double) _MutationProbPerBit; }

			private:
				double		_MutationProbPerBit;
			};

			public ref class SpeedReducerDesignTestProblem : public IMOWCFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				SpeedReducerDesignTestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Speed reducer design"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual	String^		getRequestedDesignVar()  { return "SpeedReducerDesignVariable"; }

				virtual void			calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) override;

				virtual int				getNumObjectives() override;
				virtual String^		getObjectiveName(int i) override;

				virtual int				getNumConstraints() override;
				virtual String^		getConstraintName(int i) override;

				virtual int				getNumVar() override;
				virtual String^		getVarName(int i) override;
				virtual void			getVarBounds(int i, double &outLower, double &outUpper) override;
			};
		}
	}
}