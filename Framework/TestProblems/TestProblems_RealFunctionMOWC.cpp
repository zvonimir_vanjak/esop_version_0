#include "stdafx.h"
#include "TestProblems_RealFunctionMOWC.h"

#include <math.h>

using namespace ESOP::TestProblems::SOFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;

/***********************************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////
BNH_TestProblem::BNH_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void	BNH_TestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) 
{
	IMultiObjectiveSolutionWC	^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	IRealArray								^Repr		= dynamic_cast<IRealArray ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	double	x1, x2;

	x1 = Repr->getValue(0);
	x2 = Repr->getValue(1);

	outCalcObjValues->setValue(0, 4 * x1 * x2 + 4 * x2 * x2);
	outCalcObjValues->setValue(1, (x1 - 5) * (x1 - 5) + (x2 - 5) * (x2 - 5));

	outCalcConstrValues->setValue(0, (x1 - 5) * (x1 - 5) + x2 * x2 - 25);
	outCalcConstrValues->setValue(1, 7.7 - (x1 - 8) * (x1 - 8) - (x2 + 3) * (x2 + 3));
}
int		BNH_TestProblem::getNumObjectives()
{
	return 2;
}
String^	BNH_TestProblem::getObjectiveName(int i)
{
	return "";
}
int		BNH_TestProblem::getNumConstraints()
{
	return 2;
}
String^	BNH_TestProblem::getConstraintName(int i)
{
	return "";
}
int			BNH_TestProblem::getNumVar()
{
	return 2;
}
String^	BNH_TestProblem::getVarName(int i)
{
	return "x";
}
void	BNH_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	if( i == 0 ) {
		outLower = 0;
		outUpper = 5;
	}
	else	{
		outLower = 0;
		outUpper = 3;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////
SRN_TestProblem::SRN_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void	SRN_TestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) 
{
	IMultiObjectiveSolutionWC	^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	IRealArray								^Repr		= dynamic_cast<IRealArray ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	double	x1, x2;

	x1 = Repr->getValue(0);
	x2 = Repr->getValue(1);

	outCalcObjValues->setValue(0, 2 + pow(x1-2, 2) + pow(x2-2,2));
	outCalcObjValues->setValue(1, 9 * x1 - pow(x2-1,2));

	outCalcConstrValues->setValue(0, x1*x1 + x2*x2 - 225);
	outCalcConstrValues->setValue(1, x1 - 3 * x2 + 10);
}
int		SRN_TestProblem::getNumObjectives()
{
	return 2;
}
String^	SRN_TestProblem::getObjectiveName(int i)
{
	return "";
}
int		SRN_TestProblem::getNumConstraints()
{
	return 2;
}
String^	SRN_TestProblem::getConstraintName(int i)
{
	return "";
}
int			SRN_TestProblem::getNumVar()
{
	return 2;
}
String^	SRN_TestProblem::getVarName(int i)
{
	return "x";
}
void	SRN_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = -20;
	outUpper = 20;
}
//////////////////////////////////////////////////////////////////////////////////////////////
TNK_TestProblem::TNK_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void	TNK_TestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) 
{
	IMultiObjectiveSolutionWC	^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	IRealArray								^Repr		= dynamic_cast<IRealArray ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	double	x1, x2;

	x1 = Repr->getValue(0);
	x2 = Repr->getValue(1);

	outCalcObjValues->setValue(0, x1);
	outCalcObjValues->setValue(1, x2);

	outCalcConstrValues->setValue(0, -(x1*x1 + x2*x2 - 1 - 0.1 * cos(16 * atan2(x1,x2))));
	outCalcConstrValues->setValue(1, pow(x1-0.5,2) + pow(x2-0.5, 2) - 0.5);
}
int		TNK_TestProblem::getNumObjectives()
{
	return 2;
}
String^	TNK_TestProblem::getObjectiveName(int i)
{
	return "";
}
int		TNK_TestProblem::getNumConstraints()
{
	return 2;
}
String^	TNK_TestProblem::getConstraintName(int i)
{
	return "";
}
int			TNK_TestProblem::getNumVar()
{
	return 2;
}
String^	TNK_TestProblem::getVarName(int i)
{
	return "x";
}
void	TNK_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = 0;
	outUpper = 3.14153;
}

//////////////////////////////////////////////////////////////////////////////////////////////
OSY_TestProblem::OSY_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void	OSY_TestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) 
{
	IMultiObjectiveSolutionWC	^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	IRealArray								^Repr		= dynamic_cast<IRealArray ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	double	x1, x2, x3, x4, x5, x6;

	x1 = Repr->getValue(0);
	x2 = Repr->getValue(1);
	x3 = Repr->getValue(2);
	x4 = Repr->getValue(3);
	x5 = Repr->getValue(4);
	x6 = Repr->getValue(5);

	outCalcObjValues->setValue(0, -(25 * pow(x1-2,2) + pow(x2-2,2) + pow(x3-1,2) + pow(x4-4,2) + pow(x5-1,2)));
	outCalcObjValues->setValue(1, x1*x1 + x2*x2 + x3*x3 + x4*x4 + x5*x5 + x6*x6);

	outCalcConstrValues->setValue(0, -(x1 + x2 - 2));
	outCalcConstrValues->setValue(1, -(6 - x1 - x2));
	outCalcConstrValues->setValue(2, -(2 - x2 + x1));
	outCalcConstrValues->setValue(3, -(2 - x1 + 3 * x2));
	outCalcConstrValues->setValue(4, -(4 - pow(x3-3,2)));
	outCalcConstrValues->setValue(5, -(pow(x5-3,2) + x6 - 4));
}
int		OSY_TestProblem::getNumObjectives()
{
	return 2;
}
String^	OSY_TestProblem::getObjectiveName(int i)
{
	return "";
}
int		OSY_TestProblem::getNumConstraints()
{
	return 6;
}
String^	OSY_TestProblem::getConstraintName(int i)
{
	return "";
}
int			OSY_TestProblem::getNumVar()
{
	return 6;
}
String^	OSY_TestProblem::getVarName(int i)
{
	return "x";
}
void	OSY_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	if( i == 0 || i == 1 )	{
		outLower = 0;
		outUpper = 10;
	}
	else if ( i == 2 ) {
		outLower = 1;
		outUpper = 5;
	}
	else if ( i == 3 ) {
		outLower = 0;
		outUpper = 6;
	}
	else if ( i == 4 ) {
		outLower = 1;
		outUpper = 5;
	}
	else if ( i == 5 ) {
		outLower = 0;
		outUpper = 10;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////
TwoBarTrussDesign_TestProblem::TwoBarTrussDesign_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void	TwoBarTrussDesign_TestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) 
{
	IMultiObjectiveSolutionWC	^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	IRealArray								^Repr		= dynamic_cast<IRealArray ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	double	x1, x2, x3;

	x1 = Repr->getValue(0);
	x2 = Repr->getValue(1);
	x3 = Repr->getValue(2);

	double	sigma1 = 20 * sqrt(16 + x3*x3) / (x1*x3);
	double	sigma2 = 80 * sqrt(1 + x3*x3) / (x2*x3);

	outCalcObjValues->setValue(0,x1 * sqrt(16 + x3*x3) + x2 * sqrt(1 + x3*x3));
	if( sigma1 > sigma2)
		outCalcObjValues->setValue(1, sigma1);
	else
		outCalcObjValues->setValue(1, sigma2);

	outCalcConstrValues->setValue(0, outCalcObjValues->getValue(1) - 1e5);
}
int		TwoBarTrussDesign_TestProblem::getNumObjectives()
{
	return 2;
}
String^	TwoBarTrussDesign_TestProblem::getObjectiveName(int i)
{
	return "";
}
int		TwoBarTrussDesign_TestProblem::getNumConstraints()
{
	return 1;
}
String^	TwoBarTrussDesign_TestProblem::getConstraintName(int i)
{
	return "";
}
int			TwoBarTrussDesign_TestProblem::getNumVar()
{
	return 1;
}
String^	TwoBarTrussDesign_TestProblem::getVarName(int i)
{
	return "x";
}
void	TwoBarTrussDesign_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	if(i == 0 || i == 1) {
		outLower = 0;
		outUpper = 0.01;
	}
	else {
		outLower = 1;
		outUpper = 3;
	}
}
/************************************************************************/
WeldedBeamDesign_TestProblem::WeldedBeamDesign_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void	WeldedBeamDesign_TestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues)
{
	IMultiObjectiveSolutionWC	^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	IRealArray								^Repr		= dynamic_cast<IRealArray ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	double	h, b, l, t;

	h = Repr->getValue(0);
	b = Repr->getValue(1);
	l = Repr->getValue(2);
	t = Repr->getValue(3);

	outCalcObjValues->setValue(0, 1.10471 * h * h * l + 0.048 * t * b * (14 + l));
	outCalcObjValues->setValue(1, 2.1952 / (t * t * t * b));

	double	tau, taudot, tauddot, sigma, Pc;

	sigma = 504000 / (t * t * b);
	Pc		= 64746.022 * (1 - 0.0282346 * t) * t * b * b * b;

	taudot	= 6000 / (sqrt(2.0) * h * l);
	tauddot = (6000 * (14 + 0.5 * l) * sqrt(0.25 * (l * l + pow(h + t,2)))) / (2 * sqrt(2.0) * h * l * (l * l / 12 + 0.25 * pow(h + t,2)));
	tau			= sqrt(taudot * taudot + tauddot * tauddot + l * taudot * tauddot / sqrt(0.25 * ( l * l + pow(h + t,2))));

	outCalcConstrValues->setValue(0, -(13600 - tau));
	outCalcConstrValues->setValue(1, -(30000 - sigma));
	outCalcConstrValues->setValue(2, -(b - h));
	outCalcConstrValues->setValue(3, -(Pc - 6000));
}
int		WeldedBeamDesign_TestProblem::getNumObjectives()
{
	return 2;
}
String^	WeldedBeamDesign_TestProblem::getObjectiveName(int i)
{
	return "";
}
int		WeldedBeamDesign_TestProblem::getNumConstraints()
{
	return 4;
}
String^	WeldedBeamDesign_TestProblem::getConstraintName(int i)
{
	return "";
}
int			WeldedBeamDesign_TestProblem::getNumVar()
{
	return 1;
}
String^	WeldedBeamDesign_TestProblem::getVarName(int i)
{
	return "x";
}
void	WeldedBeamDesign_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	if( i == 0 || i == 1 ) {
		outLower = 0.125;
		outUpper = 5;
	}
	else {
		outLower = 0.1;
		outUpper = 10;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////
SpeedReducerDesignVariable::SpeedReducerDesignVariable()
{
		AddPart(gcnew RealAsBitArray(3.0, 3.6));			// x1 - gear face width
		AddPart(gcnew RealAsBitArray(0.7, 0.8));			// x2
		AddPart(gcnew Integer(17, 28));								// x3 
		AddPart(gcnew RealAsBitArray(7.3, 8.3));			// x4 
		AddPart(gcnew RealAsBitArray(7.3, 8.3));			// x5 
		AddPart(gcnew RealAsBitArray(2.9, 3.9));			// x6 
		AddPart(gcnew RealAsBitArray(5.0, 5.5));			// x7 
}

void SpeedReducer_Crossover::Recombine(IDesignVariable ^p1, IDesignVariable ^p2, IDesignVariable ^pChild1, IDesignVariable ^pChild2)
{
	RealAsBitArray ^Parent1 = dynamic_cast<RealAsBitArray ^>(p1);
	RealAsBitArray ^Parent2 = dynamic_cast<RealAsBitArray ^>(p2);

	RealAsBitArray ^Child1 = dynamic_cast<RealAsBitArray ^>(pChild1);
	RealAsBitArray ^Child2 = dynamic_cast<RealAsBitArray ^>(pChild2);
}

SpeedReducer_Mutation::SpeedReducer_Mutation()
{
	_MutationProbPerBit = 0.01;
}

void SpeedReducer_Mutation::Mutate(IDesignVariable ^Child) 
{
	RealAsBitArray ^Ind = dynamic_cast<RealAsBitArray ^>(Child);

	Random ^rnd = gcnew Random();
	for( int j=0; j<Ind->getRepr()->getBitNum(); j++ )
	{
		if( (rnd->Next() % 1000) / 1000. < _MutationProbPerBit )
			Ind->FlopBit(j);
	}
}

void	SpeedReducer_Mutation::setMutationprob(double inMutProb)
{
	_MutationProbPerBit = inMutProb;
}

SpeedReducerDesignTestProblem::SpeedReducerDesignTestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void		SpeedReducerDesignTestProblem::calcObjective(IMultiObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcObjValues, VectorDouble ^outCalcConstrValues) 
{
	IMultiObjectiveSolutionWC							^SolInst = dynamic_cast<IMultiObjectiveSolutionWC ^> (inSolInstance);
	GenericDynamicCompositeRepresentation	^Repr		 = dynamic_cast<GenericDynamicCompositeRepresentation ^> (SolInst->getDesignVar());

	// sada vadimo vrijednosti varijabli jednu po jednu
	IReal			^Obj1;
	IInteger	^Obj2;

	double	x1, x2, x4, x5, x6, x7;
	int			x3;

	Obj1 = dynamic_cast<IReal ^>		(Repr->GetPart(0));		x1 = Obj1->getValue();
	Obj1 = dynamic_cast<IReal ^>		(Repr->GetPart(1));		x2 = Obj1->getValue();
	Obj2 = dynamic_cast<IInteger ^> (Repr->GetPart(2));	x3 = Obj2->getValue();
	Obj1 = dynamic_cast<IReal ^>		(Repr->GetPart(3));		x4 = Obj1->getValue();
	Obj1 = dynamic_cast<IReal ^>		(Repr->GetPart(4));		x5 = Obj1->getValue();
	Obj1 = dynamic_cast<IReal ^>		(Repr->GetPart(5));		x6 = Obj1->getValue();
	Obj1 = dynamic_cast<IReal ^>		(Repr->GetPart(6));		x7 = Obj1->getValue();

	outCalcObjValues->setValue(0, 0.7854 * x1 * x2 * x2 * (10 * x3 * x3 / 3 + 14.933 * x3 - 43.0934) 
																- 1.508 * x1 * (x6 * x6 + x7 * x7) + 7.477 * (pow(x6,3) + pow(x7,3)) + 0.7854 * (x4 * x6 * x6 + x5 * x7 * x7));

	outCalcObjValues->setValue(1, sqrt( pow((745 * x4 / x2 / x3), 2) + 1.69e7 ) / 0.1 / pow(x6, 3));

	outCalcConstrValues->setValue(0, 1 / (x1 * x2 * x2 * x3) - 1 / 27.);
	outCalcConstrValues->setValue(1, 1 / (x1 * x2 * x2 * x3 * x3) - 1 / 397.5);
	outCalcConstrValues->setValue(2, pow(x4,3) / (x2 * x3 * pow(x6,4)) - 1 / 1.93);
	outCalcConstrValues->setValue(3, pow(x5,3) / (x2 * x3 * pow(x7,4)) - 1 / 1.93);
	outCalcConstrValues->setValue(4, x2 * x3 / 40 - 1);
	outCalcConstrValues->setValue(5, x1 / x2 - 12);
	outCalcConstrValues->setValue(6, 5 - x1 / x2);
	outCalcConstrValues->setValue(7, 1.9 - x4 + 1.5 * x6);
	outCalcConstrValues->setValue(8, 1.9 - x5 + 1.1 * x7);
	outCalcConstrValues->setValue(9, (outCalcObjValues->getValue(1) - 1300) / 100);
	outCalcConstrValues->setValue(10, sqrt( pow(745 * x5 / (x2 * x3), 2) + 1.575e8 ) / 0.1 / pow(x7, 3) - 1100);
}
int			SpeedReducerDesignTestProblem::getNumObjectives() 
{
	return 2;
}
String^	SpeedReducerDesignTestProblem::getObjectiveName(int i) 
{
	return "f";
}
int			SpeedReducerDesignTestProblem::getNumConstraints() 
{
	return 11;
}
String^	SpeedReducerDesignTestProblem::getConstraintName(int i) 
{
	return "x";
}
int			SpeedReducerDesignTestProblem::getNumVar()
{
	return 1;
}
String^	SpeedReducerDesignTestProblem::getVarName(int i)
{
	return "x";
}
void	SpeedReducerDesignTestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	/*
		AddPart(new RealAsBitArray(3.0, 3.6));			// x1 - gear face width
		AddPart(new RealAsBitArray(0.7, 0.8));			// x2
		AddPart(new Integer(17, 28));								// x3 
		AddPart(new RealAsBitArray(7.3, 8.3));			// x4 
		AddPart(new RealAsBitArray(7.3, 8.3));			// x5 
		AddPart(new RealAsBitArray(2.9, 3.9));			// x6 
		AddPart(new RealAsBitArray(5.0, 5.5));			// x7 
*/
	outLower = 0;
	outUpper = 5;
}

