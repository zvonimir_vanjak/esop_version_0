#pragma once

//#include "RealFunction.h"
#using "E:\Projects\ESOP_version_0\Debug\Components.dll"

#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Problems::RealFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;
using namespace ESOP::Framework::DynamicSupport;

namespace ESOP
{
	namespace TestProblems
	{
		namespace MOFunctionOptimization
		{
			/*************************************************************************************/
			public ref class Schaffer_MO_TestProblem : public IMOFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				Schaffer_MO_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Schaffer MO"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^val) override;

				virtual	String^	getRequestedDesignVar()  { return "IReal"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class SchafferDisconnected_MO_TestProblem : public IMOFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				SchafferDisconnected_MO_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Schaffer disconnected MO"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }
	
				virtual void		calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^val) override;

				virtual	String^	getRequestedDesignVar()  { return "IReal"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class ValenzuelaRendon_MO_TestProblem : public IMOFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				ValenzuelaRendon_MO_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Valenzuela-Rendon MO"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^val) override;

				virtual	String^	getRequestedDesignVar()  { return "IRealArray"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};

			public ref class ValenzuelaRendon_As_Composite_MO_TestProblem : public IMOFunctionOptimization, public IESOPCompleteProblem
			{
			public:
				ValenzuelaRendon_As_Composite_MO_TestProblem();

				virtual	String^		getESOPComponentName() 		{ return "Valenzuela-Rendon MO composite"; }
				virtual String^		getESOPComponentDesc()		{ return "Desc"; }

				virtual void		calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^val) override;

				virtual	String^	getRequestedDesignVar()  { return "ICompositeVariable"; }

				virtual int			getNumObjectives() override;
				virtual String^	getObjectiveName(int i) override;

				virtual int			getNumVar() override;
				virtual String^	getVarName(int i) override;
				virtual void		getVarBounds(int i, double &outLower, double &outUpper) override;
			};
		}
	}
}