#include "stdafx.h"
#include "TestProblems_RealFunctionSOWC.h"

#include <math.h>

using namespace ESOP::Framework::Base;
using namespace ESOP::TestProblems::SOFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;

/***********************************************************************************/
SOWC1_TestProblem::SOWC1_TestProblem(int inNumVar)
{
	_NumVar = inNumVar;
}

double	SOWC1_TestProblem::calcObjective(ISingleObjectiveSolutionWC ^inSolInstance, VectorDouble ^outCalcConstrValues) 
{
	ISingleObjectiveSolutionWC	^SolObj = dynamic_cast<ISingleObjectiveSolutionWC ^> (inSolInstance);
	IRealArray									^x			= dynamic_cast<IRealArray ^>(SolObj->getDesignVar());
	
	// vrijednost funkcije cilja
	double sumCos4 = 0;
	double mulCos2 = 1;
	double sumX2 = 0;

	for( int i=1; i<=_NumVar; i++ )
	{
		sumCos4 += pow(cos(x->getValue(i)), 4);
		mulCos2 *= pow(cos(x->getValue(i)), 2);
		sumX2 += i * x->getValue(i) * x->getValue(i);
	}

	// ograničenja
	double		constr1 = 1;
	double		constr2 = 0;
	for( int i=1; i<=_NumVar; i++ )
	{
		constr1 *= x->getValue(i);
		constr2 += x->getValue(i);
	}
	outCalcConstrValues->setValue(0, constr1);
	outCalcConstrValues->setValue(1, constr2);
	
	return fabs((sumCos4 - 2 * mulCos2) / sqrt(sumX2));
}

int			SOWC1_TestProblem::getNumConstraints() 
{
	return 2;
}
String^	SOWC1_TestProblem::getConstraintName(int i) 
{
	if( i== 0 )
		return "Nonlinear cnstr.";
	else
		return "Linear constr.";
}

int			SOWC1_TestProblem::getNumVar()
{
	return _NumVar;
}

String^	SOWC1_TestProblem::getVarName(int i)
{
	return "x";
}

void	SOWC1_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = 0;
	outUpper = 10;
}

//////////////////////////////////////////////////////////////////////////////////////////////
