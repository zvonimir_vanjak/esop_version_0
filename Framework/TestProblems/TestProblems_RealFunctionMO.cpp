#include "stdafx.h"
#include "TestProblems_RealFunctionMO.h"

#include <math.h>

using namespace ESOP::TestProblems::MOFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;

/***********************************************************************************/
Schaffer_MO_TestProblem::Schaffer_MO_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void		Schaffer_MO_TestProblem::calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^outFuncVal) 
{
	IMultiObjectiveSolution 	^SolObj = dynamic_cast<IMultiObjectiveSolution  ^> (inSolInstance);
	IReal											^var		= dynamic_cast<IReal ^>(SolObj->getDesignVar());

	double	x = var->getValue();

	outFuncVal->setValue(0, x * x);
	outFuncVal->setValue(1, (x-2) * (x-2));
}

int			Schaffer_MO_TestProblem::getNumObjectives() 
{
	return 2;
}
String^	Schaffer_MO_TestProblem::getObjectiveName(int i) 
{
	return "f";
}

int			Schaffer_MO_TestProblem::getNumVar()
{
	return 1;
}

String^	Schaffer_MO_TestProblem::getVarName(int i)
{
	return "x";
}

void	Schaffer_MO_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = -5;
	outUpper = 5;
}

//////////////////////////////////////////////////////////////////////////////////////////////
SchafferDisconnected_MO_TestProblem::SchafferDisconnected_MO_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void		SchafferDisconnected_MO_TestProblem::calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^outFuncVal) 
{
	IMultiObjectiveSolution 	^SolObj = dynamic_cast<IMultiObjectiveSolution  ^> (inSolInstance);
	IReal											^var		= dynamic_cast<IReal ^>(SolObj->getDesignVar());

	double	x = var->getValue();
	double	ret;

	if( x <= 1 ) ret = -x;
	else if( x <=3 ) ret = -2 + x;
	else if( x <= 4 ) ret = 4 - x;
	else ret = -4 + x;

	outFuncVal->setValue(0, ret);
	outFuncVal->setValue(1, (x-5) * (x-5));
}

int			SchafferDisconnected_MO_TestProblem::getNumObjectives() 
{
	return 2;
}
String^	SchafferDisconnected_MO_TestProblem::getObjectiveName(int i) 
{
	return "f";
}

int			SchafferDisconnected_MO_TestProblem::getNumVar()
{
	return 1;
}

String^	SchafferDisconnected_MO_TestProblem::getVarName(int i)
{
	return "x";
}

void	SchafferDisconnected_MO_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = -5;
	outUpper = 10;
}
//////////////////////////////////////////////////////////////////////////////////////////////
ValenzuelaRendon_MO_TestProblem::ValenzuelaRendon_MO_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void		ValenzuelaRendon_MO_TestProblem::calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^outFuncVal) 
{
	IMultiObjectiveSolution 	^SolObj = dynamic_cast<IMultiObjectiveSolution  ^> (inSolInstance);
	IRealArray								^var		= dynamic_cast<IRealArray ^>(SolObj->getDesignVar());

	double	x1 = var->getValue(0);
	double	x2 = var->getValue(1);

	outFuncVal->setValue(0, x1 + x2 + 1);
	outFuncVal->setValue(1, x1*x1 + 2*x2 - 1);
}

int			ValenzuelaRendon_MO_TestProblem::getNumObjectives() 
{
	return 2;
}
String^	ValenzuelaRendon_MO_TestProblem::getObjectiveName(int i) 
{
	return "f";
}
int			ValenzuelaRendon_MO_TestProblem::getNumVar()
{
	return 2;
}
String^	ValenzuelaRendon_MO_TestProblem::getVarName(int i)
{
	return "x";
}
void	ValenzuelaRendon_MO_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = -3;
	outUpper = 3;
}
//////////////////////////////////////////////////////////////////////////////////////////////
ValenzuelaRendon_As_Composite_MO_TestProblem::ValenzuelaRendon_As_Composite_MO_TestProblem()
{
	setObjectiveType(0, EObjectiveType::MIN_OBJECTIVE);
	setObjectiveType(1, EObjectiveType::MIN_OBJECTIVE);
}
void		ValenzuelaRendon_As_Composite_MO_TestProblem::calcObjective(IMultiObjectiveSolution ^inSolInstance, VectorDouble ^outFuncVal) 
{
	IMultiObjectiveSolution 	^SolObj = dynamic_cast<IMultiObjectiveSolution  ^> (inSolInstance);
	ICompositeVariable				^var		= dynamic_cast<ICompositeVariable ^>(SolObj->getDesignVar());

	IReal ^ref1 = dynamic_cast<IReal ^> (var->GetPart(0));
	IReal ^ref2 = dynamic_cast<IReal ^> (var->GetPart(1));

	double	x1 = ref1->getValue();
	double	x2 = ref2->getValue();

	outFuncVal->setValue(0, x1 + x2 + 1);
	outFuncVal->setValue(1, x1*x1 + 2*x2 - 1);
}

int			ValenzuelaRendon_As_Composite_MO_TestProblem::getNumObjectives() 
{
	return 2;
}
String^	ValenzuelaRendon_As_Composite_MO_TestProblem::getObjectiveName(int i) 
{
	return "f";
}
int			ValenzuelaRendon_As_Composite_MO_TestProblem::getNumVar()
{
	return 2;
}
String^	ValenzuelaRendon_As_Composite_MO_TestProblem::getVarName(int i)
{
	return "x";
}
void	ValenzuelaRendon_As_Composite_MO_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = -3;
	outUpper = 3;
}
