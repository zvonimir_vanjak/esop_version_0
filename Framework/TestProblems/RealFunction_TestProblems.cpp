#include "stdafx.h"
#include "RealFunction_TestProblems.h"

#include <math.h>

using namespace ESOP::TestProblems::SOFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;

/***********************************************************************************/
double	SinFunc_TestProblem::calcObjective(ISingleObjectiveSolution ^inSolInstance)
{
	ISingleObjectiveSolution				^SolObj = dynamic_cast<ISingleObjectiveSolution ^> (inSolInstance);
	IReal	^x			= dynamic_cast<IReal ^>(SolObj->getCurrSol());
	
	return sin(x->getValue());
}

int			SinFunc_TestProblem::getNumVar()
{
	return 1;
}

String^	SinFunc_TestProblem::getVarName(int i)
{
	return "x";
}

void	SinFunc_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = 0;
	outUpper = 5;
}

//////////////////////////////////////////////////////////////////////////////////////////////
double	AckleyFunction_TestProblem::calcObjective(ISingleObjectiveSolution ^inSolInstance)
{
	ISingleObjectiveSolution	^SolObj = dynamic_cast<ISingleObjectiveSolution ^> (inSolInstance);
	IRealArray								^x			= dynamic_cast<IRealArray ^>(SolObj->getCurrSol());

	double sumSquare = 0;
	double sumCos = 0;

	for( int i=0; i<_NumVar; i++ )
	{
		sumSquare += pow(x->getValue(i), 2);
		sumCos		+= cos(2 * 3.14153 * x->getValue(i));
	}

	return -20 * exp( -0.2 * sqrt(sumSquare / _NumVar) ) - exp(sumCos / _NumVar) + 20 + 2.71; 
}

int			AckleyFunction_TestProblem::getNumVar()
{
	return _NumVar;
}

String^	AckleyFunction_TestProblem::getVarName(int i)
{
	return "x";
}

void	AckleyFunction_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = 0;
	outUpper = 5;
}

//////////////////////////////////////////////////////////////////////////////////////////////
double	SchwefelFunction_TestProblem::calcObjective(ISingleObjectiveSolution ^inSolInstance)
{
	ISingleObjectiveSolution	^SolObj = dynamic_cast<ISingleObjectiveSolution ^> (inSolInstance);
	IRealArray								^x			= dynamic_cast<IRealArray ^>(SolObj->getCurrSol());

	double retVal = 0;
	double sumJ = 0;
	for( int  i=0; i<_NumVar; i++ )
	{
		sumJ = 0;
		for( int j=0; j<i; j++ )
			sumJ += x->getValue(j);

		retVal += sumJ * sumJ;
	}
	return retVal;
}

int			SchwefelFunction_TestProblem::getNumVar()
{
	return _NumVar;
}

String^	SchwefelFunction_TestProblem::getVarName(int i)
{
	return "x";
}

void	SchwefelFunction_TestProblem::getVarBounds(int i, double &outLower, double &outUpper)
{
	outLower = -65.536; 
	outUpper =  65.536;
}
