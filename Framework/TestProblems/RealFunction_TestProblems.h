#pragma once

//#include "RealFunction.h"
#using "D:\Doktorat\ESOP\debug\Components.dll"

#include <vector>
using std::vector;

using namespace System;
using namespace ESOP::Framework::Base;
using namespace ESOP::Framework::Problems::RealFunctionOptimization;
using namespace ESOP::Framework::StandardRepresentations;

namespace ESOP
{
	namespace TestProblems
	{
		namespace SOFunctionOptimization
		{
			/*************************************************************************************/
			public ref class SinFunc_TestProblem : public ISOFunctionOptimization
			{
			public:
				virtual String^	getComponentName() { return ""; }
				virtual String^	getProblemName() { return "SinFunc_TestProblem";	}

				virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance);

				virtual int			getNumVar();
				virtual String^	getVarName(int i);
				virtual void		getVarBounds(int i, double &outLower, double &outUpper);
			};

			public ref class AckleyFunction_TestProblem : public ISOFunctionOptimization
			{
			public:
				AckleyFunction_TestProblem(int inNumVar) : _NumVar(inNumVar) {}

				virtual String^	getComponentName() { return ""; }
				virtual String^	getProblemName() { return "AckleyFunction";	}

				virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance);

				virtual int			getNumVar();
				virtual String^	getVarName(int i);
				virtual void		getVarBounds(int i, double &outLower, double &outUpper);

			private:
				int		_NumVar;
			};

			public ref class SchwefelFunction_TestProblem : public ISOFunctionOptimization
			{
			public:
				SchwefelFunction_TestProblem(int inNumVar) : _NumVar(inNumVar) {}

				virtual String^	getComponentName() { return ""; }
				virtual String^	getProblemName() { return "SchwefelFunction";	}

				virtual double	calcObjective(ISingleObjectiveSolution ^inSolInstance);

				virtual int			getNumVar();
				virtual String^	getVarName(int i);
				virtual void		getVarBounds(int i, double &outLower, double &outUpper);

			private:
				int		_NumVar;
			};
		}
	}
}
