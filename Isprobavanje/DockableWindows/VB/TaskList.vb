Public Class TaskList
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents listView1 As System.Windows.Forms.ListView
    Friend WithEvents columnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents columnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents columnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents columnHeader4 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.listView1 = New System.Windows.Forms.ListView()
        Me.columnHeader1 = New System.Windows.Forms.ColumnHeader()
        Me.columnHeader2 = New System.Windows.Forms.ColumnHeader()
        Me.columnHeader3 = New System.Windows.Forms.ColumnHeader()
        Me.columnHeader4 = New System.Windows.Forms.ColumnHeader()
        Me.SuspendLayout()
        '
        'listView1
        '
        Me.listView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnHeader1, Me.columnHeader2, Me.columnHeader3, Me.columnHeader4})
        Me.listView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.listView1.Name = "listView1"
        Me.listView1.Size = New System.Drawing.Size(194, 83)
        Me.listView1.TabIndex = 1
        Me.listView1.View = System.Windows.Forms.View.Details
        '
        'columnHeader1
        '
        Me.columnHeader1.Text = "!"
        Me.columnHeader1.Width = 18
        '
        'columnHeader2
        '
        Me.columnHeader2.Text = ""
        Me.columnHeader2.Width = 18
        '
        'columnHeader3
        '
        Me.columnHeader3.Text = "Description"
        Me.columnHeader3.Width = 128
        '
        'columnHeader4
        '
        Me.columnHeader4.Text = "File"
        Me.columnHeader4.Width = 256
        '
        'TaskList
        '
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.listView1})
        Me.Name = "TaskList"
        Me.Size = New System.Drawing.Size(194, 83)
        Me.ResumeLayout(False)

    End Sub

#End Region

End Class
