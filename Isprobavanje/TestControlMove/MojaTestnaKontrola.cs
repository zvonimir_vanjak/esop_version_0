using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TestControlMove
{
	public partial class MojaTestnaKontrola : UserControl
	{
		public delegate void ClickEventHandler(object sender, int OperIndex, int inX, int inY);
		public delegate void MoveEventHandler(object sender, int inX, int inY);

		ClickEventHandler _clickHandler;
		MoveEventHandler _moveHandler;

		public MojaTestnaKontrola()
		{
			InitializeComponent();
		}

		public void RegisterClickEvent(ClickEventHandler handler)
		{
			_clickHandler += handler;
		}
		public void RegisterMoveEvent(MoveEventHandler handler)
		{
			_moveHandler += handler;
		}

		private void MojaTestnaKontrola_MouseDown(object sender, MouseEventArgs e)
		{
			_clickHandler(this, 0, e.X, e.Y);
		}

		private void MojaTestnaKontrola_MouseMove(object sender, MouseEventArgs e)
		{
			_moveHandler(this, e.X, e.Y);
		}

		private void MojaTestnaKontrola_MouseUp(object sender, MouseEventArgs e)
		{
			Form1 frm = this.Parent as Form1;

			frm._bControlSelected = false;
//			frm.AutoScroll = true;
		}
	}
}
