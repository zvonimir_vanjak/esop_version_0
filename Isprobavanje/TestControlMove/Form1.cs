using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace TestControlMove
{
	public partial class Form1 : Form
	{
		public bool _bControlSelected;
		private UserControl _SelCtrl;
		private int _StartX, _StartY;
		private int _DX, _DY;

		MojaTestnaKontrola _Ctrl1;

		public Form1()
		{
			_bControlSelected = false;
			InitializeComponent();

			_Ctrl1 = new MojaTestnaKontrola();

			_Ctrl1.Location = new System.Drawing.Point(100, 100);
			_Ctrl1.Name = "solobject";
			_Ctrl1.TabIndex = 0;
			_Ctrl1.RegisterClickEvent(OnClickedControl);
			_Ctrl1.RegisterMoveEvent(OnMoveControl);
			this.Controls.Add(_Ctrl1);

			MojaTestnaKontrola newCtrl2 = new MojaTestnaKontrola();

			newCtrl2.Location = new System.Drawing.Point(10, 100);
			newCtrl2.Name = "solobject";
			newCtrl2.TabIndex = 0;
			newCtrl2.RegisterClickEvent(OnClickedControl);
			newCtrl2.RegisterMoveEvent(OnMoveControl);
			this.Controls.Add(newCtrl2);
		}
		public void OnClickedControl(object sender, int OperIndex, int inX, int inY)
		{
			Debug.Print("OnClickedControl");

//			Debug.Print(this.AutoScrollPosition.X.ToString() + " " + AutoScrollPosition.Y.ToString());
			_bControlSelected = true;
			_SelCtrl = sender as UserControl;

			_StartX = _SelCtrl.Location.X; // -AutoScrollPosition.X; // +inX;
			_StartY = _SelCtrl.Location.Y; // -AutoScrollPosition.Y; // +inY;
			_DX = inX;
			_DY = inY;
		}
		public void OnMoveControl(object sender, int inX, int inY)
		{
			if (_bControlSelected == true && !(inX==_DX && inY==_DY))
			{
//				this.AutoScroll = false;

				Debug.Print("OnMouseMove");
				_SelCtrl.Location = new Point(_StartX + (inX - _DX), _StartY + (inY - _DY));

				_StartX += (inX - _DX);
				_StartY += (inY - _DY);

				this.Refresh();
			}
		}
		private void Form1_MouseMove(object sender, MouseEventArgs e)
		{
		}
		private void Form1_MouseUp(object sender, MouseEventArgs e)
		{
			Debug.Print("OnMouseUp");
			_bControlSelected = false;
		}
		private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
		{
			Point oldLoc = _Ctrl1.Location;

			_Ctrl1.Location = new Point(oldLoc.X, oldLoc.Y - (e.NewValue - e.OldValue));
			Refresh();
		}

	
	}
}