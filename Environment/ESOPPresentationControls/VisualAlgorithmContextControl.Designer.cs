namespace ESOPPresentationControls
{
	partial class VisualAlgorithmContextControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.MenuItem_setalgorithmParameters = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(92, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "Algorithm parameters";
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Enabled = true;
			this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_setalgorithmParameters});
			this.contextMenuStrip1.Location = new System.Drawing.Point(21, 48);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.contextMenuStrip1.Size = new System.Drawing.Size(138, 26);
			// 
			// MenuItem_setalgorithmParameters
			// 
			this.MenuItem_setalgorithmParameters.Name = "MenuItem_setalgorithmParameters";
			this.MenuItem_setalgorithmParameters.Text = "Set parameters";
			this.MenuItem_setalgorithmParameters.Click += new System.EventHandler(this.MenuItem_setalgorithmParameters_Click);
			// 
			// VisualAlgorithmContextControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.Controls.Add(this.label1);
			this.Name = "VisualAlgorithmContextControl";
			this.Size = new System.Drawing.Size(148, 148);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.VisualAlgorithmContextControl_Paint);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.VisualAlgorithmContextControl_MouseUp);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem MenuItem_setalgorithmParameters;
	}
}
