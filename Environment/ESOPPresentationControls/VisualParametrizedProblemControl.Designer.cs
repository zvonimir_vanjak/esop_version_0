namespace ESOPPresentationControls
{
	partial class VisualParametrizedProblemControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this.label1 = new System.Windows.Forms.Label();
      this.labelProbName = new System.Windows.Forms.Label();
      this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.menuItem_SetParameters = new System.Windows.Forms.ToolStripMenuItem();
      this.contextMenuStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(108, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Parametrized problem";
      // 
      // labelProbName
      // 
      this.labelProbName.AutoSize = true;
      this.labelProbName.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelProbName.Location = new System.Drawing.Point(0, 13);
      this.labelProbName.Name = "labelProbName";
      this.labelProbName.Size = new System.Drawing.Size(35, 13);
      this.labelProbName.TabIndex = 1;
      this.labelProbName.Text = "label2";
      // 
      // contextMenuStrip1
      // 
      this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_SetParameters});
      this.contextMenuStrip1.Name = "contextMenuStrip1";
      this.contextMenuStrip1.Size = new System.Drawing.Size(160, 26);
      // 
      // menuItem_SetParameters
      // 
      this.menuItem_SetParameters.Name = "menuItem_SetParameters";
      this.menuItem_SetParameters.Size = new System.Drawing.Size(159, 22);
      this.menuItem_SetParameters.Text = "Set parameters";
      this.menuItem_SetParameters.Click += new System.EventHandler(this.menuItem_SetParameters_Click);
      // 
      // VisualParametrizedProblemControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.ContextMenuStrip = this.contextMenuStrip1;
      this.Controls.Add(this.labelProbName);
      this.Controls.Add(this.label1);
      this.Name = "VisualParametrizedProblemControl";
      this.Size = new System.Drawing.Size(148, 148);
      this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VisualParametrizedProblemControl_MouseDown);
      this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.VisualParametrizedProblemControl_MouseMove);
      this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.VisualParametrizedProblemControl_MouseUp);
      this.contextMenuStrip1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelProbName;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menuItem_SetParameters;
	}
}
