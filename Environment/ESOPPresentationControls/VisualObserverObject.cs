using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	public partial class VisualObserverObject : UserControl
	{
		public delegate void DeleteThisObjectEventHandler(object sender);
		DeleteThisObjectEventHandler _deleteHandler;

		private String _obsDesc = "";
		public OptPlanObserver _OptPlanObs;

		public VisualObserverObject(string inObsDesc)
		{
			_obsDesc = inObsDesc;

			InitializeComponent();

			labObsDesc.Text = _obsDesc;

			comboBox1.Items.Add("Save every Nth iteration");
			comboBox1.Items.Add("Save every Nth ms");
		}
		public void SetObserver(OptPlanObserver inOptPlanObs)
		{
			_OptPlanObs = inOptPlanObs;

			if( _OptPlanObs._SelTimerType == "IntermedTimerOnEveryIterNum" )
				comboBox1.SelectedIndex = 0;
			else
				comboBox1.SelectedIndex = 1;

			textBox1.Text = _OptPlanObs._TimerParameter.ToString();
			_OptPlanObs._TimerParameter = 1;
			_OptPlanObs._SelTimerType = "IntermedTimerOnEveryIterNum";
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			if( textBox1.Text == "")
				_OptPlanObs._TimerParameter = 0;
			else
				_OptPlanObs._TimerParameter = Convert.ToInt32(textBox1.Text);
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (comboBox1.SelectedIndex == 0)
				_OptPlanObs._SelTimerType = "IntermedTimerOnEveryIterNum";
			else
				_OptPlanObs._SelTimerType = "IntermedTimerOnTimeInterval";
		}

		public void RegisterDeleteObject(DeleteThisObjectEventHandler handler)
		{
			_deleteHandler += handler;
		}

		private void menuItem_cmdDeleteThisObject_Click(object sender, EventArgs e)
		{
			_deleteHandler(this);
		}
	}
}
