using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;
using ESOP.Framework.DynamicSupport;
using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	public partial class VisualOptimizationTerminator : UserControl
	{
		OptComponentsManager _CompManager;

		OptPlanTerminator _OptTerminator;
		private ArrayList _arlAddedTerminators = new ArrayList();

		public VisualOptimizationTerminator(LoadedAlgorithm selAlg, OptComponentsManager inCompManager)
		{
			_CompManager = inCompManager;

			InitializeComponent();

			// u listbox treba staviti samo one koje algoritam podr�ava !!!
			foreach (LoadedOptTerminator optTerm in _CompManager._listAvailableOptTerm)
			{
				string ReqInterface = optTerm._RequestedIntermediateProvider;

				// treba provjeriti da li algoritam to podr�ava
				if (OptComponentsLoader.DoesTypeSupportInterface(selAlg._Type, "ESOP.Framework.Base." + ReqInterface))
				{
					_arlAddedTerminators.Add(optTerm);
					_cmdSelector.Items.Add(optTerm._OptTermDesc);
				}
			}
		}

		public void SetOptTerminator(OptPlanTerminator inTerm)
		{
			_OptTerminator = inTerm;
			_txtParameterValue.Text = _OptTerminator._SetParamValue.ToString();

			bool bAlreadySelected = false;

			// a ako je ve� selektirana neka reprezentacija u OptSol objektu, odaberi je u combo-box-u (bitno kod u�itavanja postoje�eg optplana)
			if (_OptTerminator._OptTerminator != null)
			{
				int i = 0;
				foreach (LoadedOptTerminator optTerm in _arlAddedTerminators)
				{
					if (_OptTerminator._OptTerminator.ComponentName == optTerm.ComponentName)
					{
						_cmdSelector.SelectedIndex = i;
						bAlreadySelected = true;
						break;
					}
					i++;
				}
			}

			if( bAlreadySelected == false )
				_cmdSelector.SelectedIndex = 0;

			_OptTerminator._OptTerminator = (LoadedOptTerminator)_arlAddedTerminators[_cmdSelector.SelectedIndex];
		}

		private void _txtParameterValue_TextChanged(object sender, EventArgs e)
		{
			try
			{
				_OptTerminator._SetParamValue = Convert.ToInt32(_txtParameterValue.Text);
			}
			catch(Exception)
			{
				_OptTerminator._SetParamValue = 1;
			}
		}

		private void _cmdSelector_SelectedIndexChanged(object sender, EventArgs e)
		{
			_OptTerminator._OptTerminator = (LoadedOptTerminator) _arlAddedTerminators[_cmdSelector.SelectedIndex];
		}
	}
}
