using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	public partial class VisualIntermediateResultsControl : UserControl
	{
		public delegate void AddNewObserverHandle(OptPlanIntermediateResultsObserver obs, LoadedObserverSaver selObs, int x, int y);

		AddNewObserverHandle _newObserverHandler;

		public OptPlanIntermediateResultsObserver _ObsMaster;
		OptComponentsManager _CompManager;

		public VisualIntermediateResultsControl(OptComponentsManager inCompManager)
		{
			_CompManager = inCompManager;

			InitializeComponent();
		}

		public void RegisterAddNewObserverHandler(AddNewObserverHandle handler)
		{
			_newObserverHandler += handler;
		}

		private void cmdAddObserver_Click(object sender, EventArgs e)
		{
			// treba nam LoadedAlgorithm objekt kako bi znali koje IIntermediateValueProvider podr�ava
			frmSelectFromAvailableObservers frm = new frmSelectFromAvailableObservers(_ObsMaster._SelAlg, _CompManager._listAvailableObservers);

			if( frm.ShowDialog() == DialogResult.OK )
			{
				if( frm.getSelObserver() != null )
					_newObserverHandler(_ObsMaster, frm.getSelObserver(), this.Location.X, this.Location.Y);
//				_ObsMaster.addNewObserver(newObs);
			}
		}

		public void setObserverMaster(OptPlanIntermediateResultsObserver inObs)
		{
			_ObsMaster = inObs;
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}
	}
}
