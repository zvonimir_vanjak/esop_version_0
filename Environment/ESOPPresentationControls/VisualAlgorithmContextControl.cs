using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;
using ESOPPresentationControls;

using ESOP.Framework.DynamicSupport;

namespace ESOPPresentationControls
{
	public partial class VisualAlgorithmContextControl : UserControl
	{
		public delegate void ClickEventHandler(OptPlanAlgorithmContext context, int OperIndex, int inX, int inY);

		ClickEventHandler _clickHandler;

		public OptPlanAlgorithmContext _AlgContext;

		public VisualAlgorithmContextControl()
		{
			InitializeComponent();
		}

		public void RegisterClickEvent(ClickEventHandler handler)
		{
			_clickHandler += handler;
		}

		public int getOperIndex(int X, int Y)
		{
			// gledamo po y
			return (Y - 18) / 15;
		}
		public int getOperConnectorYCoord(int OperIndex)
		{
			return OperIndex * 15 + 25;
		}

		private void VisualAlgorithmContextControl_Paint(object sender, PaintEventArgs e)
		{
//			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			this.Size = new System.Drawing.Size(140, 20 * _AlgContext._AlgOper.getOperNum() + 20);

			Graphics g = e.Graphics;
			for (int i = 0; i < _AlgContext._AlgOper.getOperNum(); i++)
			{
				g.DrawLine(new Pen(Color.Black, 2), 100, 23 + i * 15, 120, 23 + i * 15);
				g.FillEllipse(Brushes.Black, 110, 18 + i * 15, 10, 10);
			}
			g.Dispose();
		}

		public void SetAlgContext(OptPlanAlgorithmContext inAlgContext)
		{
			_AlgContext = inAlgContext;

			// ovdje se sada mo�e postaviti vizualne oznake za tra�ene operator
			for (int i = 0; i < _AlgContext._AlgOper.getOperNum(); i++)
			{
				Label label1 = new Label();

				label1.AutoSize = true;
				label1.Location = new System.Drawing.Point(1, 15 + i * 15);
				label1.Name = "label1";
				label1.TabIndex = 0;
				label1.Text = _AlgContext._AlgOper.getOperID(i);

				this.Controls.Add(label1);
			}
			this.Refresh();
		}
		private void MenuItem_setalgorithmParameters_Click(object sender, EventArgs e)
		{
			if (_AlgContext._AlgParam._NumParam > 0)
			{
				frmAlgorithmParameters frm = new frmAlgorithmParameters(_AlgContext._AlgParam);

				frm.ShowDialog();
			}
			else
				MessageBox.Show("No parameters available !");
		}

		private void VisualAlgorithmContextControl_MouseUp(object sender, MouseEventArgs e)
		{
			_clickHandler(_AlgContext, getOperIndex(e.X, e.Y), this.Location.X, this.Location.Y);
		}
	}
}
