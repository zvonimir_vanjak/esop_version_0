namespace ESOPPresentationControls
{
	partial class VisualAlgorithmControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.labelAlgName = new System.Windows.Forms.Label();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.menutItem_cmdDeleteThisObject = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(46, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Algorithm";
			// 
			// labelAlgName
			// 
			this.labelAlgName.AutoEllipsis = true;
			this.labelAlgName.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelAlgName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelAlgName.ForeColor = System.Drawing.Color.White;
			this.labelAlgName.Location = new System.Drawing.Point(0, 13);
			this.labelAlgName.Name = "labelAlgName";
			this.labelAlgName.Size = new System.Drawing.Size(105, 23);
			this.labelAlgName.TabIndex = 1;
			this.labelAlgName.Text = "Naziv algoritma";
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Enabled = true;
			this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menutItem_cmdDeleteThisObject});
			this.contextMenuStrip1.Location = new System.Drawing.Point(128, 98);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.contextMenuStrip1.Size = new System.Drawing.Size(148, 26);
			// 
			// menutItem_cmdDeleteThisObject
			// 
			this.menutItem_cmdDeleteThisObject.Name = "menutItem_cmdDeleteThisObject";
			this.menutItem_cmdDeleteThisObject.Text = "Delete this object";
			this.menutItem_cmdDeleteThisObject.Click += new System.EventHandler(this.menutItem_cmdDeleteThisObject_Click);
			// 
			// VisualAlgorithmControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.Controls.Add(this.labelAlgName);
			this.Controls.Add(this.label1);
			this.Name = "VisualAlgorithmControl";
			this.Size = new System.Drawing.Size(105, 89);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.VisualAlgorithmControl_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.VisualAlgorithmControl_MouseUp);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VisualAlgorithmControl_MouseDown);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelAlgName;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menutItem_cmdDeleteThisObject;
	}
}
