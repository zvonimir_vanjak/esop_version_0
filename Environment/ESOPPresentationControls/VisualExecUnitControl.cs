using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	public partial class VisualExecUnitControl : UserControl
	{
		public delegate void MouseUpEventHandler(object sender, int x, int y);
		public delegate void MouseDownEventHandler(object sender, int x, int y);
		public delegate void MouseMoveEventHandler(object sender, int x, int y);

		MouseUpEventHandler _mouseUpHandler;
		MouseDownEventHandler _mouseDownHandler;
		MouseMoveEventHandler _mouseMoveHandler;

		public delegate void DeleteThisObjectEventHandler(object sender);
		DeleteThisObjectEventHandler _deleteHandler;
		
		int _StartX, _StartY;
		bool _bMovementStarted;

		Point _AlgContexOldLoc;
		Point _SolObjectOldLoc;
		Point _OptTerminatorOldLoc;
		Point _IntermedSaverOldLoc;

		ExecutionUnit _ExecUnit;

		public VisualExecUnitControl()
		{
			InitializeComponent();

			_bMovementStarted = false;
		}

		public void SetExecUnit(ExecutionUnit inUnit)
		{
			_ExecUnit = inUnit;
		}
		private void menuItem_Execute_Click(object sender, EventArgs e)
		{
			_ExecUnit.Execute();
		}
		private void cmdMultiExecute_Click(object sender, EventArgs e)
		{
		}
		private void cmdMultiExecuteAllComb_Click(object sender, EventArgs e)
		{
		}

		public void RegisterMouseUp(MouseUpEventHandler handler)
		{
			_mouseUpHandler += handler;
		}
		public void RegisterMouseDown(MouseDownEventHandler handler)
		{
			_mouseDownHandler += handler;
		}
		public void RegisterMouseMove(MouseMoveEventHandler handler)
		{
			_mouseMoveHandler += handler;
		}
		public void RegisterDeleteObject(DeleteThisObjectEventHandler handler)
		{
			_deleteHandler += handler;
		}

		private void VisualExecUnitControl_MouseUp(object sender, MouseEventArgs e)
		{
			if( _mouseUpHandler != null )
				_mouseUpHandler(this, e.X, e.Y);

			_bMovementStarted = false;
		}
		private void VisualExecUnitControl_MouseMove(object sender, MouseEventArgs e)
		{
			if (_mouseMoveHandler != null && _bMovementStarted == true)
			{
				int Dx = e.X - _StartX;
				int Dy = e.Y - _StartY;

				_ExecUnit._AlgContext._VisCtrl.Location = new Point(_AlgContexOldLoc.X + Dx, _AlgContexOldLoc.Y + Dy);
				_ExecUnit._SolObject._VisCtrl.Location = new Point(_SolObjectOldLoc.X + Dx, _SolObjectOldLoc.Y + Dy);
				_ExecUnit._OptTerminator._VisCtrl.Location = new Point(_OptTerminatorOldLoc.X + Dx, _OptTerminatorOldLoc.Y + Dy);
				_ExecUnit._IntermedRes.MoveControl(Dx, Dy);

				_mouseMoveHandler(this, e.X, e.Y);

				_AlgContexOldLoc = _ExecUnit._AlgContext._VisCtrl.Location;
				_SolObjectOldLoc = _ExecUnit._SolObject._VisCtrl.Location;
				_OptTerminatorOldLoc = _ExecUnit._OptTerminator._VisCtrl.Location;
				_IntermedSaverOldLoc = _ExecUnit._IntermedRes._VisCtrl.Location;
			}
		}
		private void VisualExecUnitControl_MouseDown(object sender, MouseEventArgs e)
		{
			if (_mouseDownHandler != null)
			{
				_bMovementStarted = true;
				_StartX = e.X;
				_StartY = e.Y;
				
				_AlgContexOldLoc			= _ExecUnit._AlgContext._VisCtrl.Location;
				_SolObjectOldLoc			= _ExecUnit._SolObject._VisCtrl.Location;
				_OptTerminatorOldLoc	= _ExecUnit._OptTerminator._VisCtrl.Location;
				_IntermedSaverOldLoc	= _ExecUnit._IntermedRes._VisCtrl.Location;

				_mouseDownHandler(this, e.X, e.Y);
			}
		}

		private void menuItem_cmdDeleteThisObject_Click(object sender, EventArgs e)
		{
			_deleteHandler(this);
		}
	}
}
