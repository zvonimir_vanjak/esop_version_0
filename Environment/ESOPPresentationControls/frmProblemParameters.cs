using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	public partial class frmProblemParameters : Form
	{
		private int _NumParam;

		private ArrayList _listLabels = new ArrayList();
		private ArrayList _listTextBox = new ArrayList();

		ProblemParametersInitializer _probInit;

		public frmProblemParameters(ProblemParametersInitializer probInit)
		{
			_NumParam = probInit._NumParam;
			_probInit = probInit;

			InitializeComponent();

			for (int i = 0; i < _NumParam; i++)
			{
				Label label1 = new Label();
				TextBox textBox1 = new TextBox();

				label1.AutoSize = true;
				label1.Location = new System.Drawing.Point(1, 48 + i * 32);
				label1.Name = "label1";
				//				label1.Size = new System.Drawing.Size(31, 13);
				label1.TabIndex = 0;
				label1.Text = _probInit._arrName[i];
				// 
				// textBox1
				// 
				textBox1.Location = new System.Drawing.Point(90, 45 + i * 32);
				textBox1.Name = "textBox1";
				textBox1.Size = new System.Drawing.Size(66, 20);
				textBox1.TabIndex = 1;
				textBox1.Text = _probInit._arrVal[i].ToString();

				this.Controls.Add(textBox1);	
				this.Controls.Add(label1);

				_listLabels.Add(label1);
				_listTextBox.Add(textBox1);

				this.Size = new System.Drawing.Size(170, (i + 1) * 32 + 70);
				this.Refresh();
			}
		}
		private void button1_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < _NumParam; i++)
			{
				_probInit._arrVal[i] = Convert.ToDouble(((TextBox)_listTextBox[i]).Text.ToString());
			}
		}
	}
}