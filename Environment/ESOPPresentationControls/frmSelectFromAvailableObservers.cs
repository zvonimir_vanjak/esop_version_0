using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;
using ESOP.Framework.DynamicSupport;
using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	/// <summary>
	/// Na osnovu odabranog algoritma, kreira dostupnu listu observera i omogu�ava selektiranje jednog od njih
	/// </summary>
	public partial class frmSelectFromAvailableObservers : Form
	{
		private ArrayList _ListAvailableObservers = new ArrayList();
		private ArrayList _ListAllAvailableObservers;

		public frmSelectFromAvailableObservers(LoadedAlgorithm selAlg, ArrayList inListAvailableObservers)
		{
			_ListAllAvailableObservers = inListAvailableObservers;
			InitializeComponent();

			// u listbox treba staviti samo one koje algoritam podr�ava !!!
			IterativeAlgorithm alg = (IterativeAlgorithm)Activator.CreateInstance(selAlg._Type);

			foreach (LoadedObserverSaver obsSaver in _ListAllAvailableObservers)
			{
				string ReqInterface = obsSaver.RequestedIntermediateProvider;

				// treba provjeriti da li algoritam to podr�ava
				if (OptComponentsLoader.DoesTypeSupportInterface(selAlg._Type, "ESOP.Framework.Base." + ReqInterface))
				{
					listBoxAvailableObservers.Items.Add(obsSaver.ObserverDesc);
					_ListAvailableObservers.Add(obsSaver);
				}
			}
		}

		public LoadedObserverSaver getSelObserver()
		{
			if (listBoxAvailableObservers.SelectedIndex > -1)
				return _ListAvailableObservers[listBoxAvailableObservers.SelectedIndex] as LoadedObserverSaver;
			else
				return null;
		}

		private void cmdOK_Click(object sender, EventArgs e)
		{
		}
	}
}