using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;
using ESOPPresentationControls;

using ESOP.Framework.DynamicSupport;

namespace ESOPPresentationControls
{
	public partial class VisualSolObjectControl : UserControl
	{
		private ArrayList _listPossibleRepr;

		OptPlanSolObject _SolObject;
		public VisualSolObjectControl()
		{
			InitializeComponent();
		}

		public void RefreshComboList(ArrayList inList)
		{
			_listPossibleRepr = inList;

			_cmbReprList.Items.Clear();
			foreach(LoadedRepresentation repr in inList)
				_cmbReprList.Items.Add(repr.ComponentName);

			_cmbReprList.Refresh();
		}
		public bool IsCompSelected()
		{
			if (_cmbReprList.SelectedIndex >= 0)
				return true;
			else
				return false;
		}
		public void SetSolObject(OptPlanSolObject inSol)
		{
			_SolObject = inSol;
			RefreshComboList(inSol.GetPossibleRepresentations());

			// ako je ve� selektirana neka reprezentacija u OptSol objektu, odaberi je u combo-box-u (bitno kod u�itavanja postoje�eg optplana)
			if (inSol.getSelLoadedRepr() != null)
			{
				int i = 0;
				foreach (LoadedRepresentation repr in _listPossibleRepr)
				{
					if (inSol.getSelLoadedRepr() == repr)
						_cmbReprList.SelectedIndex = i;
					i++;
				}
			}
		}
		public LoadedRepresentation getSelRepresentation()
		{
			return _SolObject.getSelLoadedRepr();
		}

		private void _cmbReprList_SelectedIndexChanged(object sender, EventArgs e)
		{
			LoadedRepresentation selRepr = (LoadedRepresentation)_listPossibleRepr[_cmbReprList.SelectedIndex];
			_SolObject.SetSelRepr(selRepr);

			// TODO - podesiti vidljivost pojeidnih pop-up menu-itema ovisno o vrsti odabrane reprezentacije
			this.menuItem_SetRepresentationParameters.Visible = true;
			if( selRepr is LoadedConstructedRepresentation )
				this.cmdSetConstructorParameters.Visible = true;
		}

		private void menuItem_SetRepresentationParameters_Click(object sender, EventArgs e)
		{
			frmRepresentationParameters frm = new frmRepresentationParameters(_SolObject._ReprInit);

			frm.ShowDialog();
		}

		private void cmdSetConstructorParameters_Click(object sender, EventArgs e)
		{
			frmRepresentationConstructorParameters frm = new frmRepresentationConstructorParameters(_SolObject._ReprInit);

			frm.ShowDialog();

			// svaka promjena konstruktorskih varijabli zna�i potrebu za ponovnom inicijalizacijom 
			// TODO - ovdje sada treba ponovno kreirati varijablu i popuniti obi�ne paramtre
			_SolObject.ReCheckSelRepr();
		}
	}
}
