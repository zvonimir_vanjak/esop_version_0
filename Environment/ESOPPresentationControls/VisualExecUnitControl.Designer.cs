namespace ESOPPresentationControls
{
	partial class VisualExecUnitControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.menuItem_Execute = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdMultiExecute = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdMultiExecuteAllComb = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.menuItem_cmdDeleteThisObject = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Execution unit";
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Enabled = true;
			this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_Execute,
            this.cmdMultiExecute,
            this.cmdMultiExecuteAllComb,
            this.toolStripSeparator1,
            this.menuItem_cmdDeleteThisObject});
			this.contextMenuStrip1.Location = new System.Drawing.Point(22, 50);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.contextMenuStrip1.Size = new System.Drawing.Size(210, 117);
			this.contextMenuStrip1.Visible = true;
			// 
			// menuItem_Execute
			// 
			this.menuItem_Execute.Name = "menuItem_Execute";
			this.menuItem_Execute.Text = "Execute";
			this.menuItem_Execute.Click += new System.EventHandler(this.menuItem_Execute_Click);
			// 
			// cmdMultiExecute
			// 
			this.cmdMultiExecute.Name = "cmdMultiExecute";
			this.cmdMultiExecute.Text = "Multiexecute";
			this.cmdMultiExecute.Click += new System.EventHandler(this.cmdMultiExecute_Click);
			// 
			// cmdMultiExecuteAllComb
			// 
			this.cmdMultiExecuteAllComb.Name = "cmdMultiExecuteAllComb";
			this.cmdMultiExecuteAllComb.Text = "Multiexecute - all combinations";
			this.cmdMultiExecuteAllComb.Click += new System.EventHandler(this.cmdMultiExecuteAllComb_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			// 
			// menuItem_cmdDeleteThisObject
			// 
			this.menuItem_cmdDeleteThisObject.Name = "menuItem_cmdDeleteThisObject";
			this.menuItem_cmdDeleteThisObject.Text = "Delete this object";
			this.menuItem_cmdDeleteThisObject.Click += new System.EventHandler(this.menuItem_cmdDeleteThisObject_Click);
			// 
			// VisualExecUnitControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.Controls.Add(this.label1);
			this.Name = "VisualExecUnitControl";
			this.Size = new System.Drawing.Size(148, 148);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.VisualExecUnitControl_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.VisualExecUnitControl_MouseUp);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VisualExecUnitControl_MouseDown);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menuItem_Execute;
		private System.Windows.Forms.ToolStripMenuItem cmdMultiExecute;
		private System.Windows.Forms.ToolStripMenuItem cmdMultiExecuteAllComb;
		private System.Windows.Forms.ToolStripMenuItem menuItem_cmdDeleteThisObject;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
	}
}
