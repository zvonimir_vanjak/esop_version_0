namespace ESOPPresentationControls
{
	partial class VisualSolObjectControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this._cmbReprList = new System.Windows.Forms.ComboBox();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.menuItem_SetRepresentationParameters = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdSetConstructorParameters = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdDefineCompositeRepresentation = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(114, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Selected representation";
			// 
			// _cmbReprList
			// 
			this._cmbReprList.DropDownWidth = 200;
			this._cmbReprList.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this._cmbReprList.FormattingEnabled = true;
			this._cmbReprList.Location = new System.Drawing.Point(3, 16);
			this._cmbReprList.Name = "_cmbReprList";
			this._cmbReprList.Size = new System.Drawing.Size(125, 21);
			this._cmbReprList.TabIndex = 1;
			this._cmbReprList.SelectedIndexChanged += new System.EventHandler(this._cmbReprList_SelectedIndexChanged);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Enabled = true;
			this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSetConstructorParameters,
            this.menuItem_SetRepresentationParameters,
            this.cmdDefineCompositeRepresentation});
			this.contextMenuStrip1.Location = new System.Drawing.Point(22, 50);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.contextMenuStrip1.Size = new System.Drawing.Size(220, 89);
			this.contextMenuStrip1.Visible = true;
			// 
			// menuItem_SetRepresentationParameters
			// 
			this.menuItem_SetRepresentationParameters.Name = "menuItem_SetRepresentationParameters";
			this.menuItem_SetRepresentationParameters.Text = "Set parameters";
			this.menuItem_SetRepresentationParameters.Visible = false;
			this.menuItem_SetRepresentationParameters.Click += new System.EventHandler(this.menuItem_SetRepresentationParameters_Click);
			// 
			// cmdSetConstructorParameters
			// 
			this.cmdSetConstructorParameters.Name = "cmdSetConstructorParameters";
			this.cmdSetConstructorParameters.Text = "Set constructor parameters";
			this.cmdSetConstructorParameters.Visible = false;
			this.cmdSetConstructorParameters.Click += new System.EventHandler(this.cmdSetConstructorParameters_Click);
			// 
			// cmdDefineCompositeRepresentation
			// 
			this.cmdDefineCompositeRepresentation.Name = "cmdDefineCompositeRepresentation";
			this.cmdDefineCompositeRepresentation.Text = "Define composite representation";
			this.cmdDefineCompositeRepresentation.Visible = false;
			// 
			// VisualSolObjectControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.Controls.Add(this.label1);
			this.Controls.Add(this._cmbReprList);
			this.Name = "VisualSolObjectControl";
			this.Size = new System.Drawing.Size(148, 148);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox _cmbReprList;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menuItem_SetRepresentationParameters;
		private System.Windows.Forms.ToolStripMenuItem cmdSetConstructorParameters;
		private System.Windows.Forms.ToolStripMenuItem cmdDefineCompositeRepresentation;
	}
}
