namespace ESOPPresentationControls
{
	partial class VisualOperatorControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.labelOperName = new System.Windows.Forms.Label();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.cmdSetParameters = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(44, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Operator";
			// 
			// labelOperName
			// 
			this.labelOperName.AutoEllipsis = true;
			this.labelOperName.AutoSize = true;
			this.labelOperName.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelOperName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelOperName.Location = new System.Drawing.Point(0, 13);
			this.labelOperName.Name = "labelOperName";
			this.labelOperName.Size = new System.Drawing.Size(31, 13);
			this.labelOperName.TabIndex = 1;
			this.labelOperName.Text = "label2";
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Enabled = true;
			this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSetParameters});
			this.contextMenuStrip1.Location = new System.Drawing.Point(22, 63);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.contextMenuStrip1.Size = new System.Drawing.Size(138, 26);
			// 
			// cmdSetParameters
			// 
			this.cmdSetParameters.Name = "cmdSetParameters";
			this.cmdSetParameters.Text = "Set parameters";
			this.cmdSetParameters.Click += new System.EventHandler(this.cmdSetParameters_Click);
			// 
			// VisualOperatorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Lime;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.Controls.Add(this.labelOperName);
			this.Controls.Add(this.label1);
			this.Name = "VisualOperatorControl";
			this.Size = new System.Drawing.Size(148, 148);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.VisualOperatorControl_MouseClick);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelOperName;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem cmdSetParameters;
	}
}
