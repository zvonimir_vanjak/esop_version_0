using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace ESOPPresentationControls
{
	public partial class VisualAlgorithmControl : UserControl
	{
		public delegate void MouseUpEventHandler(object sender, int x, int y);
		public delegate void MouseDownEventHandler(object sender, int x, int y);
		public delegate void MouseMoveEventHandler(object sender, int x, int y);

		MouseUpEventHandler _mouseUpHandler;
		MouseDownEventHandler _mouseDownHandler;
		MouseMoveEventHandler _mouseMoveHandler;

		public delegate void DeleteThisObjectEventHandler(object sender);
		DeleteThisObjectEventHandler _deleteHandler;

		public VisualAlgorithmControl(string inAlgName)
		{
			InitializeComponent();

			this.labelAlgName.Text = inAlgName;
		}
		public void RegisterMouseUp(MouseUpEventHandler handler)
		{
			_mouseUpHandler += handler;
		}
		public void RegisterMouseDown(MouseDownEventHandler handler)
		{
			_mouseDownHandler += handler;
		}
		public void RegisterMouseMove(MouseMoveEventHandler handler)
		{
			_mouseMoveHandler += handler;
		}
		public void RegisterDeleteObject(DeleteThisObjectEventHandler handler)
		{
			_deleteHandler += handler;
		}

		private void VisualAlgorithmControl_MouseUp(object sender, MouseEventArgs e)
		{
			_mouseUpHandler(this, this.Location.X + e.X, this.Location.Y + e.Y);
		}
		private void VisualAlgorithmControl_MouseDown(object sender, MouseEventArgs e)
		{
			_mouseDownHandler(this, e.X, e.Y);
		}
		private void VisualAlgorithmControl_MouseMove(object sender, MouseEventArgs e)
		{
			_mouseMoveHandler(this, e.X, e.Y);
		}

		private void menutItem_cmdDeleteThisObject_Click(object sender, EventArgs e)
		{
			_deleteHandler(this);
		}
	}
}
