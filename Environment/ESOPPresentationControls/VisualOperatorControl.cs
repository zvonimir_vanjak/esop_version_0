using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;

namespace ESOPPresentationControls
{
	public partial class VisualOperatorControl : UserControl
	{
		public OptPlanOperator _OperatorWrapper;

		public VisualOperatorControl(string inOperName)
		{
			InitializeComponent();

			this.labelOperName.Text = inOperName;
		}

		public void VisualOperatorControl_MouseClick(object sender, MouseEventArgs e)
		{
		}
		public void SetOperator(OptPlanOperator inOperator)
		{
			_OperatorWrapper = inOperator;
		}
		private void cmdSetParameters_Click(object sender, EventArgs e)
		{
			if (_OperatorWrapper._OperInit._NumParam > 0)
			{
				frmOperatorParameters frm = new frmOperatorParameters(_OperatorWrapper._OperInit);

				frm.ShowDialog();
			}
			else
				MessageBox.Show("No parameters available !");
		}
	}
}
