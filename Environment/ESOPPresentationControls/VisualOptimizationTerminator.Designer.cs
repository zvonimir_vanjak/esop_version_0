namespace ESOPPresentationControls
{
	partial class VisualOptimizationTerminator
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this._cmdSelector = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this._txtParameterValue = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Opt. terminator";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label2.Location = new System.Drawing.Point(3, 22);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(29, 12);
			this.label2.TabIndex = 1;
			this.label2.Text = "Select";
			// 
			// _cmdSelector
			// 
			this._cmdSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this._cmdSelector.FormattingEnabled = true;
			this._cmdSelector.Location = new System.Drawing.Point(42, 19);
			this._cmdSelector.Name = "_cmdSelector";
			this._cmdSelector.Size = new System.Drawing.Size(95, 20);
			this._cmdSelector.TabIndex = 2;
			this._cmdSelector.SelectedIndexChanged += new System.EventHandler(this._cmdSelector_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label3.Location = new System.Drawing.Point(3, 42);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 12);
			this.label3.TabIndex = 3;
			this.label3.Text = "Parameter";
			// 
			// _txtParameterValue
			// 
			this._txtParameterValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this._txtParameterValue.Location = new System.Drawing.Point(62, 40);
			this._txtParameterValue.Name = "_txtParameterValue";
			this._txtParameterValue.Size = new System.Drawing.Size(75, 18);
			this._txtParameterValue.TabIndex = 4;
			this._txtParameterValue.TextChanged += new System.EventHandler(this._txtParameterValue_TextChanged);
			// 
			// VisualOptimizationTerminator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this._txtParameterValue);
			this.Controls.Add(this.label3);
			this.Controls.Add(this._cmdSelector);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "VisualOptimizationTerminator";
			this.Size = new System.Drawing.Size(148, 60);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox _cmdSelector;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox _txtParameterValue;
	}
}
