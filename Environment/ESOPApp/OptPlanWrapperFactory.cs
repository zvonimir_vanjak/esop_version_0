using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;
using ESOPPresentationControls;

namespace ESOPApp
{
	public class OptPlanWrapperFactory
	{
		public UserControl CreateWrapperObjectUserControl(frmOptPlan inFrmOptPlan, ILoadedComponent inComp, int x, int y)
		{
			if (inComp is LoadedCompleteAlgorithm)
			{
				VisualAlgorithmControl newCtrl = new VisualAlgorithmControl(inComp.ComponentName);

				newCtrl.Location = new System.Drawing.Point(x, y);
				newCtrl.Name = "Algorithm1";
				newCtrl.Size = new System.Drawing.Size(100, 100);
				newCtrl.TabIndex = 0;

				newCtrl.RegisterMouseUp(inFrmOptPlan.OnOptComponentMouseUp);
				newCtrl.RegisterMouseDown(inFrmOptPlan.OnControlMouseDown);
				newCtrl.RegisterMouseMove(inFrmOptPlan.OnControlMouseMove);
				newCtrl.RegisterDeleteObject(inFrmOptPlan.OnDeleteThisObject);

				return newCtrl;
			}
			else if (inComp is LoadedTemplateAlgorithm )
			{
				VisualAlgorithmControl newCtrl = new VisualAlgorithmControl(inComp.ComponentName);

				newCtrl.Location = new System.Drawing.Point(x, y);
				newCtrl.Name = "Algorithm1";
				newCtrl.Size = new System.Drawing.Size(100, 100);
				newCtrl.TabIndex = 0;

				newCtrl.RegisterMouseUp(inFrmOptPlan.OnOptComponentMouseUp);
				newCtrl.RegisterMouseDown(inFrmOptPlan.OnControlMouseDown);
				newCtrl.RegisterMouseMove(inFrmOptPlan.OnControlMouseMove);
				newCtrl.RegisterDeleteObject(inFrmOptPlan.OnDeleteThisObject);

				return newCtrl;
			}
			else if (inComp is LoadedCompleteProblem)
			{
				VisualProblemControl newCtrl = new VisualProblemControl(inComp.ComponentName);

				newCtrl.Location = new System.Drawing.Point(x, y);
				newCtrl.Name = "Problem1";
				newCtrl.Size = new System.Drawing.Size(100, 100);
				newCtrl.TabIndex = 0;

				newCtrl.RegisterMouseUp(inFrmOptPlan.OnOptComponentMouseUp);
				newCtrl.RegisterMouseDown(inFrmOptPlan.OnControlMouseDown);
				newCtrl.RegisterMouseMove(inFrmOptPlan.OnControlMouseMove);
				newCtrl.RegisterDeleteObject(inFrmOptPlan.OnDeleteThisObject);

				return newCtrl;
			}
			else if (inComp is LoadedParameterProblem)
			{
				VisualParametrizedProblemControl newCtrl = new VisualParametrizedProblemControl(inComp.ComponentName);

				newCtrl.Location = new System.Drawing.Point(x, y);
				newCtrl.Name = "Problem1";
				newCtrl.Size = new System.Drawing.Size(100, 100);
				newCtrl.TabIndex = 0;

				newCtrl.RegisterMouseUp(inFrmOptPlan.OnOptComponentMouseUp);
				newCtrl.RegisterMouseDown(inFrmOptPlan.OnControlMouseDown);
				newCtrl.RegisterMouseMove(inFrmOptPlan.OnControlMouseMove);
				newCtrl.RegisterDeleteObject(inFrmOptPlan.OnDeleteThisObject);

				return newCtrl;
			}

			return null;
		}

		public static VisualObserverObject CreateObserverControl(int inNumObs, string inDesc, int inExecX, int inExecY)
		{
			VisualObserverObject newCtrl = new VisualObserverObject(inDesc);

			int x = inExecX + 100;
			int y = inExecY + inNumObs * 62 - 60;
			newCtrl.Location = new System.Drawing.Point(x, y);
			newCtrl.Name = "Operator1";
			newCtrl.Size = new System.Drawing.Size(160, 60);
			newCtrl.TabIndex = 0;

			return newCtrl;
		}

		public static VisualOperatorControl CreateOperatorControl(string inName, int inX, int inY)
		{
			VisualOperatorControl newCtrl = new VisualOperatorControl(inName);

			newCtrl.Location = new System.Drawing.Point(inX, inY);
			newCtrl.Name = "Operator1";
			newCtrl.Size = new System.Drawing.Size(100, 30);
			newCtrl.TabIndex = 0;

			return newCtrl;
		}
	}
}
