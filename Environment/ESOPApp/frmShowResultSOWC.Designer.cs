namespace ESOPApp
{
	partial class frmShowResultSOWC
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label8 = new System.Windows.Forms.Label();
			this.txtObjectiveValue = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtSolution = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtNumViolatedconstraints = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this._ctrlListConstr = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.label11 = new System.Windows.Forms.Label();
			this.txtTotalNumConstr = new System.Windows.Forms.TextBox();
			this.cmdOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(16, 411);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(118, 13);
			this.label8.TabIndex = 24;
			this.label8.Text = "Objective function value";
			// 
			// txtObjectiveValue
			// 
			this.txtObjectiveValue.Location = new System.Drawing.Point(142, 408);
			this.txtObjectiveValue.Name = "txtObjectiveValue";
			this.txtObjectiveValue.Size = new System.Drawing.Size(94, 20);
			this.txtObjectiveValue.TabIndex = 23;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(16, 447);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 13);
			this.label2.TabIndex = 25;
			this.label2.Text = "Solution";
			// 
			// txtSolution
			// 
			this.txtSolution.Location = new System.Drawing.Point(31, 468);
			this.txtSolution.Multiline = true;
			this.txtSolution.Name = "txtSolution";
			this.txtSolution.Size = new System.Drawing.Size(236, 95);
			this.txtSolution.TabIndex = 26;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(450, 411);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 13);
			this.label9.TabIndex = 27;
			this.label9.Text = "Num.violated constr.";
			// 
			// txtNumViolatedconstraints
			// 
			this.txtNumViolatedconstraints.Location = new System.Drawing.Point(552, 408);
			this.txtNumViolatedconstraints.Name = "txtNumViolatedconstraints";
			this.txtNumViolatedconstraints.Size = new System.Drawing.Size(64, 20);
			this.txtNumViolatedconstraints.TabIndex = 28;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(292, 447);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(89, 13);
			this.label10.TabIndex = 29;
			this.label10.Text = "Constraints values";
			// 
			// _ctrlListConstr
			// 
			this._ctrlListConstr.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
			this._ctrlListConstr.Location = new System.Drawing.Point(317, 468);
			this._ctrlListConstr.Name = "_ctrlListConstr";
			this._ctrlListConstr.Size = new System.Drawing.Size(299, 95);
			this._ctrlListConstr.TabIndex = 30;
			this._ctrlListConstr.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Constr.name";
			this.columnHeader1.Width = 100;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Constr.value";
			this.columnHeader2.Width = 100;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(261, 411);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(91, 13);
			this.label11.TabIndex = 31;
			this.label11.Text = "Num.of constraints";
			// 
			// txtTotalNumConstr
			// 
			this.txtTotalNumConstr.Location = new System.Drawing.Point(355, 408);
			this.txtTotalNumConstr.Name = "txtTotalNumConstr";
			this.txtTotalNumConstr.Size = new System.Drawing.Size(64, 20);
			this.txtTotalNumConstr.TabIndex = 32;
			// 
			// cmdOK
			// 
			this.cmdOK.Location = new System.Drawing.Point(282, 574);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(75, 23);
			this.cmdOK.TabIndex = 33;
			this.cmdOK.Text = "OK";
			// 
			// frmShowResultSOWC
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(639, 609);
			this.Controls.Add(this.cmdOK);
			this.Controls.Add(this.txtTotalNumConstr);
			this.Controls.Add(this.label11);
			this.Controls.Add(this._ctrlListConstr);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.txtNumViolatedconstraints);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.txtSolution);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.txtObjectiveValue);
			this.Name = "frmShowResultSOWC";
			this.Text = "Single objective problem with constraints - result";
			this.Controls.SetChildIndex(this.txtObjectiveValue, 0);
			this.Controls.SetChildIndex(this.label8, 0);
			this.Controls.SetChildIndex(this.label2, 0);
			this.Controls.SetChildIndex(this.txtSolution, 0);
			this.Controls.SetChildIndex(this.label9, 0);
			this.Controls.SetChildIndex(this.txtNumViolatedconstraints, 0);
			this.Controls.SetChildIndex(this.label10, 0);
			this.Controls.SetChildIndex(this._ctrlListConstr, 0);
			this.Controls.SetChildIndex(this.label11, 0);
			this.Controls.SetChildIndex(this.txtTotalNumConstr, 0);
			this.Controls.SetChildIndex(this.cmdOK, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtObjectiveValue;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSolution;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtNumViolatedconstraints;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ListView _ctrlListConstr;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtTotalNumConstr;
		private System.Windows.Forms.Button cmdOK;
	}
}