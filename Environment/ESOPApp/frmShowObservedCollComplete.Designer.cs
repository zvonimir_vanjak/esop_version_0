namespace ESOPApp
{
	partial class frmShowObservedCollComplete
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.cmbListAvailableCtrls = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select visualization control";
			// 
			// cmbListAvailableCtrls
			// 
			this.cmbListAvailableCtrls.FormattingEnabled = true;
			this.cmbListAvailableCtrls.Location = new System.Drawing.Point(155, 16);
			this.cmbListAvailableCtrls.Name = "cmbListAvailableCtrls";
			this.cmbListAvailableCtrls.Size = new System.Drawing.Size(229, 21);
			this.cmbListAvailableCtrls.TabIndex = 1;
			this.cmbListAvailableCtrls.SelectedIndexChanged += new System.EventHandler(this.cmbListAvailableCtrls_SelectedIndexChanged);
			// 
			// frmShowObservedCollComplete
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 448);
			this.Controls.Add(this.cmbListAvailableCtrls);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "frmShowObservedCollComplete";
			this.Text = "Complete collection visualization";
			this.Resize += new System.EventHandler(this.frmShowObservedCollComplete_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbListAvailableCtrls;
	}
}