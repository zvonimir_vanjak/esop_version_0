namespace ESOPApp
{
	partial class frmShowResultSO
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtObjectiveValue = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.cmdOK = new System.Windows.Forms.Button();
			this.txtSolution = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtObjectiveValue
			// 
			this.txtObjectiveValue.Location = new System.Drawing.Point(146, 411);
			this.txtObjectiveValue.Name = "txtObjectiveValue";
			this.txtObjectiveValue.Size = new System.Drawing.Size(112, 20);
			this.txtObjectiveValue.TabIndex = 10;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 414);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(118, 13);
			this.label8.TabIndex = 22;
			this.label8.Text = "Objective function value";
			// 
			// cmdOK
			// 
			this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.cmdOK.Location = new System.Drawing.Point(279, 488);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(75, 23);
			this.cmdOK.TabIndex = 23;
			this.cmdOK.Text = "OK";
			// 
			// txtSolution
			// 
			this.txtSolution.Location = new System.Drawing.Point(345, 411);
			this.txtSolution.Multiline = true;
			this.txtSolution.Name = "txtSolution";
			this.txtSolution.Size = new System.Drawing.Size(271, 60);
			this.txtSolution.TabIndex = 27;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(297, 414);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(41, 13);
			this.label9.TabIndex = 28;
			this.label9.Text = "Solution";
			// 
			// frmShowResultSO
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(637, 523);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.txtSolution);
			this.Controls.Add(this.cmdOK);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.txtObjectiveValue);
			this.Name = "frmShowResultSO";
			this.Text = "Single objective problem - results";
			this.Controls.SetChildIndex(this.txtObjectiveValue, 0);
			this.Controls.SetChildIndex(this.label8, 0);
			this.Controls.SetChildIndex(this.cmdOK, 0);
			this.Controls.SetChildIndex(this.txtSolution, 0);
			this.Controls.SetChildIndex(this.label9, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtObjectiveValue;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.TextBox txtSolution;
		private System.Windows.Forms.Label label9;

	}
}