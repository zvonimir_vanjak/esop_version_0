namespace ESOPApp
{
	partial class frmShowSingleResultBase
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._listIntermedRes = new System.Windows.Forms.ListView();
			this.CollectionDesc = new System.Windows.Forms.ColumnHeader();
			this.ObsVarNum = new System.Windows.Forms.ColumnHeader();
			this.Collectiontype = new System.Windows.Forms.ColumnHeader();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.cmdVisualizeComplete = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdVisualizeIndividual = new System.Windows.Forms.ToolStripMenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.txtProblemName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label67 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.txtAlgorithmDesc = new System.Windows.Forms.TextBox();
			this.txtAlgorithmName = new System.Windows.Forms.TextBox();
			this.txtSelRepresentation = new System.Windows.Forms.TextBox();
			this.txtTotalIterNum = new System.Windows.Forms.TextBox();
			this.txtDuration = new System.Windows.Forms.TextBox();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// _listIntermedRes
			// 
			this._listIntermedRes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CollectionDesc,
            this.ObsVarNum,
            this.Collectiontype});
			this._listIntermedRes.ContextMenuStrip = this.contextMenuStrip1;
			this._listIntermedRes.FullRowSelect = true;
			this._listIntermedRes.Location = new System.Drawing.Point(31, 195);
			this._listIntermedRes.Name = "_listIntermedRes";
			this._listIntermedRes.Size = new System.Drawing.Size(585, 195);
			this._listIntermedRes.TabIndex = 0;
			this._listIntermedRes.View = System.Windows.Forms.View.Details;
			// 
			// CollectionDesc
			// 
			this.CollectionDesc.Text = "Collection desc.";
			this.CollectionDesc.Width = 200;
			// 
			// ObsVarNum
			// 
			this.ObsVarNum.Text = "Num obs.values";
			this.ObsVarNum.Width = 100;
			// 
			// Collectiontype
			// 
			this.Collectiontype.Text = "Observed values type";
			this.Collectiontype.Width = 150;
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Enabled = true;
			this.contextMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdVisualizeComplete,
            this.cmdVisualizeIndividual});
			this.contextMenuStrip1.Location = new System.Drawing.Point(25, 66);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.contextMenuStrip1.Size = new System.Drawing.Size(197, 48);
			// 
			// cmdVisualizeComplete
			// 
			this.cmdVisualizeComplete.Name = "cmdVisualizeComplete";
			this.cmdVisualizeComplete.Text = "Visualize complete collection";
			this.cmdVisualizeComplete.Click += new System.EventHandler(this.cmdVisualizeComplete_Click);
			// 
			// cmdVisualizeIndividual
			// 
			this.cmdVisualizeIndividual.Name = "cmdVisualizeIndividual";
			this.cmdVisualizeIndividual.Text = "Visualize individual values";
			this.cmdVisualizeIndividual.Click += new System.EventHandler(this.cmdVisualizeIndividual_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 177);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(207, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Observed collections of intermediate results";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(75, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Algorithm name";
			// 
			// txtProblemName
			// 
			this.txtProblemName.Location = new System.Drawing.Point(136, 7);
			this.txtProblemName.Name = "txtProblemName";
			this.txtProblemName.Size = new System.Drawing.Size(253, 20);
			this.txtProblemName.TabIndex = 10;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 10);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(70, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Problem name";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 94);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Algorithm description";
			// 
			// label67
			// 
			this.label67.AutoSize = true;
			this.label67.Location = new System.Drawing.Point(12, 38);
			this.label67.Name = "label67";
			this.label67.Size = new System.Drawing.Size(115, 13);
			this.label67.TabIndex = 13;
			this.label67.Text = "Selected representation";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(413, 10);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(90, 13);
			this.label6.TabIndex = 14;
			this.label6.Text = "Total iteration num";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(413, 41);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(65, 13);
			this.label7.TabIndex = 15;
			this.label7.Text = "Duration (ms)";
			// 
			// txtAlgorithmDesc
			// 
			this.txtAlgorithmDesc.Location = new System.Drawing.Point(136, 91);
			this.txtAlgorithmDesc.Multiline = true;
			this.txtAlgorithmDesc.Name = "txtAlgorithmDesc";
			this.txtAlgorithmDesc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtAlgorithmDesc.Size = new System.Drawing.Size(480, 83);
			this.txtAlgorithmDesc.TabIndex = 17;
			this.txtAlgorithmDesc.WordWrap = false;
			// 
			// txtAlgorithmName
			// 
			this.txtAlgorithmName.Location = new System.Drawing.Point(136, 61);
			this.txtAlgorithmName.Name = "txtAlgorithmName";
			this.txtAlgorithmName.Size = new System.Drawing.Size(253, 20);
			this.txtAlgorithmName.TabIndex = 18;
			// 
			// txtSelRepresentation
			// 
			this.txtSelRepresentation.Location = new System.Drawing.Point(136, 35);
			this.txtSelRepresentation.Name = "txtSelRepresentation";
			this.txtSelRepresentation.Size = new System.Drawing.Size(253, 20);
			this.txtSelRepresentation.TabIndex = 19;
			// 
			// txtTotalIterNum
			// 
			this.txtTotalIterNum.Location = new System.Drawing.Point(516, 7);
			this.txtTotalIterNum.Name = "txtTotalIterNum";
			this.txtTotalIterNum.Size = new System.Drawing.Size(100, 20);
			this.txtTotalIterNum.TabIndex = 20;
			// 
			// txtDuration
			// 
			this.txtDuration.Location = new System.Drawing.Point(516, 35);
			this.txtDuration.Name = "txtDuration";
			this.txtDuration.Size = new System.Drawing.Size(100, 20);
			this.txtDuration.TabIndex = 21;
			// 
			// frmShowSingleResultBase
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(639, 510);
			this.Controls.Add(this.txtSelRepresentation);
			this.Controls.Add(this.txtDuration);
			this.Controls.Add(this.txtTotalIterNum);
			this.Controls.Add(this.txtAlgorithmName);
			this.Controls.Add(this.txtAlgorithmDesc);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label67);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtProblemName);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this._listIntermedRes);
			this.Name = "frmShowSingleResultBase";
			this.Text = "frmResultGeneral";
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView _listIntermedRes;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ColumnHeader CollectionDesc;
		private System.Windows.Forms.ColumnHeader ObsVarNum;
		private System.Windows.Forms.ColumnHeader Collectiontype;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtProblemName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtAlgorithmDesc;
		private System.Windows.Forms.TextBox txtAlgorithmName;
		private System.Windows.Forms.TextBox txtSelRepresentation;
		private System.Windows.Forms.TextBox txtTotalIterNum;
		private System.Windows.Forms.TextBox txtDuration;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem cmdVisualizeComplete;
		private System.Windows.Forms.ToolStripMenuItem cmdVisualizeIndividual;
	}
}