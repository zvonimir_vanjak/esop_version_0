using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevComponents.DotNetBar;
using OptimizationManagementLib;

namespace ESOPApp
{
	public partial class MainForm : Form
	{
		// globalno dostupna instanca koja rukuje sa opt. komponentama
		public static OptComponentsManager _OptComponentsManager = new OptComponentsManager();
		public static OptComponentsLoader _OptCompLoader = new OptComponentsLoader();
		public static VisualizationControlsManager _VisCtrlManager = new VisualizationControlsManager();

		private OptPlanManager				_OptPlanManager = new OptPlanManager();
		private OptComponentsToolbar	_OptToolbar = new OptComponentsToolbar(_OptComponentsManager);

		private OptPlanResults _Results;
		private frmResults _frmResults = new frmResults();

		private static bool _bCreatingExecUnitMode;

		public MainForm()
		{
			InitializeComponent();

			_Results = new OptPlanResults();
			
			LoadStandardComponents();
			NewOptPlan();

			_bCreatingExecUnitMode = false;
			_frmResults.Hide();
		}

		private void _BarManager_ContainerLoadControl(object sender, EventArgs e)
		{
			// Always cast to the BaseItem first since there could be different types coming in
			BaseItem item = sender as BaseItem;
			DockContainerItem dockitem = null;

			if (item.Name == "dockContainerOptComponents")
			{
				dockitem = item as DockContainerItem;
				dockitem.Control = _OptToolbar;
				item.Displayed = true;
			}
		}

		public static void setCreateExecutionUnitFlag(bool inVal)
		{
			_bCreatingExecUnitMode = inVal;
		}
		public static bool getCreateExecutionUnitFlag()
		{
			return _bCreatingExecUnitMode;
		}

		private void _BarManager_ItemClick(object sender, EventArgs e)
		{
			BaseItem item = sender as BaseItem;
			if (item.Name == "cmdNewOptPlan")
				NewOptPlan();
			else if (item.Name == "cmdOpenOptPlan")
				OpenOptPlanUI();
			else if (item.Name == "cmdSaveOptPlan")
				SaveOptPlan();
			else if (item.Name == "cmdLoadDLL")
				LoadDLL();
			else if (item.Name == "cmdLoadAlgorithms")
				LoadAlgorithms();
			else if (item.Name == "cmdLoadProblems")
				LoadProblems();
			else if (item.Name == "cmdLoadOperators")
				LoadOperators();
			else if (item.Name == "cmdLoadDesignVariables")
				LoadDesignVariables();
			else if (item.Name == "cmdLoadVisualizationControls")
				LoadVisualizationControls();
			else if (item.Name == "cmdCreateExecutionUnit")
				setCreateExecutionUnitFlag(true);
			else if (item.Name == "cmdViewResults")
				ViewResults();
			else if (item.Name == "cmdViewVisualizationControls")
				ViewVisualizationControls();
			else if (item.Name == "cmdViewDesignVariables")
				ViewDesignVariables();
			else if (item.Name == "cmdExit")
				Application.Exit();
			else if (item.Name == "cmdLoad1")
				OpenOptPlan("D:\\Doktorat\\ESOP\\Example problems\\Sinus function - Complete GA.Esop");
			else if (item.Name == "cmdLoad6")
				OpenOptPlan("D:\\Doktorat\\ESOP\\Example problems\\Ackley function - Generational GA.Esop");
			else if (item.Name == "cmdLoad2")
				OpenOptPlan("D:\\Doktorat\\ESOP\\Example problems\\Schaffer MO problem - SPEA.Esop");
			else if (item.Name == "cmdLoad3")
				OpenOptPlan("D:\\Doktorat\\ESOP\\Example problems\\ValenzuelaRendon MO problem - SPEA.Esop");
			else if (item.Name == "cmdLoad4")
				OpenOptPlan("D:\\Doktorat\\ESOP\\Example problems\\BNH MOWC problem - CHNA.Esop");
			else if (item.Name == "cmdLoad5")
				OpenOptPlan("D:\\Doktorat\\ESOP\\Example problems\\TNK MOWC problem - CHNA.Esop");
		}
		public void NewOptPlan()
		{
			OptPlan newOptPlan = _OptPlanManager.AddNewOptPlan(_Results);

			frmOptPlan newOptPlanForm = new frmOptPlan(newOptPlan);
			newOptPlanForm.MdiParent = this;
			newOptPlanForm.Show();

			newOptPlan.SetForm(newOptPlanForm);
		}
		public void OpenOptPlanUI()
		{
			FileDialog fileDlg = new OpenFileDialog();

			fileDlg.InitialDirectory = "D:\\Doktorat\\ESOP\\Example problems";
			fileDlg.Filter = "Esop files (*.Esop)|*.Esop";
			fileDlg.FilterIndex = 1;

			if (fileDlg.ShowDialog() == DialogResult.OK)
				OpenOptPlan(fileDlg.FileName);
		}
		public void OpenOptPlan(string FileName)
		{
			OptPlan newOptPlan = OptPlan.LoadOptPlan(FileName, _Results);
			_OptPlanManager.AddOptPlan(newOptPlan);

			frmOptPlan newOptPlanForm = new frmOptPlan(newOptPlan);
			newOptPlanForm.MdiParent = this;
			newOptPlanForm.Show();

			newOptPlan.SetForm(newOptPlanForm);

			newOptPlanForm.ReAssignLoadedComponentTypes();
			newOptPlanForm.RecreateAllControls();
		}
		public void SaveOptPlan()
		{
			FileDialog fileDlg = new SaveFileDialog();

			fileDlg.InitialDirectory = "D:\\Doktorat\\ESOP\\Example problems";
			fileDlg.Filter = "Esop files (*.Esop)|*.Esop";
			fileDlg.FilterIndex = 1;
			fileDlg.AddExtension = true;

			if (fileDlg.ShowDialog() == DialogResult.OK)
			{
				OptPlan currOptPlan = _OptPlanManager.GetActiveOptPlan();

				OptPlan.SaveOptPlan(fileDlg.FileName, currOptPlan);
			}
		}
		/// <summary>
		/// Ovo je klju�na funkcija za ubacivanje optimizacijskih komponenti u kontekst aplikacije
		/// -- otvara dijalog za nala�enje dll-a
		/// -- kori�tenjem Loader-a u�itava komponente, i kreira wrappere za njih
		/// -- vra�a popis u�itanih komponenti (u 4 vrste - problemi, algoritmi, reprezentacije, operatori)
		/// </summary>
		public void LoadDLL()
		{
			OpenFileDialog dlgFile = new OpenFileDialog();

			dlgFile.InitialDirectory = "c:\\";
			dlgFile.Filter = "dll components (*.dll)|*.dll";
			dlgFile.FilterIndex = 1;

			if (dlgFile.ShowDialog() == DialogResult.OK)
			{
				_OptCompLoader.LoadDLLOptComponentes(dlgFile.FileName, _OptComponentsManager);

				_OptToolbar.RefreshControlList();
			}
		}
		public void LoadAlgorithms()
		{
			OpenFileDialog dlgFile = new OpenFileDialog();

			dlgFile.InitialDirectory = "c:\\";
			dlgFile.Filter = "dll components (*.dll)|*.dll";
			dlgFile.FilterIndex = 1;

			if (dlgFile.ShowDialog() == DialogResult.OK)
			{
				_OptCompLoader.LoadESOPAlgorithms(dlgFile.FileName, _OptComponentsManager);

				_OptToolbar.RefreshControlList();
			}
		}
		public void LoadProblems()
		{
			OpenFileDialog dlgFile = new OpenFileDialog();

			dlgFile.InitialDirectory = "c:\\";
			dlgFile.Filter = "dll components (*.dll)|*.dll";
			dlgFile.FilterIndex = 1;

			if (dlgFile.ShowDialog() == DialogResult.OK)
			{
				_OptCompLoader.LoadESOPProblems(dlgFile.FileName, _OptComponentsManager);

				_OptToolbar.RefreshControlList();
			}
		}
		public void LoadOperators()
		{
			OpenFileDialog dlgFile = new OpenFileDialog();

			dlgFile.InitialDirectory = "c:\\";
			dlgFile.Filter = "dll components (*.dll)|*.dll";
			dlgFile.FilterIndex = 1;

			if (dlgFile.ShowDialog() == DialogResult.OK)
			{
				_OptCompLoader.LoadESOPOperators(dlgFile.FileName, _OptComponentsManager);

				_OptToolbar.RefreshControlList();
			}
		}
		public void LoadDesignVariables()
		{
			OpenFileDialog dlgFile = new OpenFileDialog();

			dlgFile.InitialDirectory = "c:\\";
			dlgFile.Filter = "dll components (*.dll)|*.dll";
			dlgFile.FilterIndex = 1;

			if (dlgFile.ShowDialog() == DialogResult.OK)
			{
				_OptCompLoader.LoadESOPDesignVariables(dlgFile.FileName, _OptComponentsManager);

				_OptToolbar.RefreshControlList();
			}
		}
		public void LoadVisualizationControls()
		{
			OpenFileDialog dlgFile = new OpenFileDialog();

			dlgFile.InitialDirectory = "c:\\";
			dlgFile.Filter = "dll components (*.dll)|*.dll";
			dlgFile.FilterIndex = 1;

			if (dlgFile.ShowDialog() == DialogResult.OK)
			{
				_VisCtrlManager.LoadVisualizationControls(dlgFile.FileName);
				_OptToolbar.RefreshControlList();
			}
		}
		public void LoadStandardComponents()
		{
			_OptCompLoader.LoadDLLOptComponentes(@"E:\Projects\ESOP_version_0\Debug\Components.dll", _OptComponentsManager);
			_OptCompLoader.LoadDLLOptComponentes(@"E:\Projects\ESOP_version_0\Debug\TestProblems.dll", _OptComponentsManager);
			_VisCtrlManager.LoadVisualizationControls(@"E:\Projects\ESOP_version_0\Environment\ESOPVisualizationControls\bin\Debug\ESOPVisualizationControls.dll");
			_OptToolbar.RefreshControlList();
		}
		public void ViewResults()
		{
			OptPlan ActivePlan = _OptPlanManager.GetActiveOptPlan();
			_frmResults.ShowResults(ActivePlan.GetResults());
		}
		public void ViewVisualizationControls()
		{
			frmViewVisualizationControls frm = new frmViewVisualizationControls(_VisCtrlManager);

			frm.ShowDialog();
		}
		public void ViewDesignVariables()
		{
		}
	}
}