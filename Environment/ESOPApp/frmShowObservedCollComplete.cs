using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;

using ESOP.Framework.Base;
using ESOPVisualizationControls;

namespace ESOPApp
{
	public partial class frmShowObservedCollComplete : Form
	{
		private ArrayList _AvailableCtrls = new ArrayList();
		private UserControl _VisCtrl;
		private ObservedValuesCollection _Coll;

		public frmShowObservedCollComplete()
		{
			InitializeComponent();
		}

		public frmShowObservedCollComplete(ObservedValuesCollection inColl)
		{
			_Coll = inColl;
			InitializeComponent();

			Type thisType = inColl._arrValues[0].GetType();
			_AvailableCtrls = MainForm._VisCtrlManager.getAvailableCompleteCtrlForType(thisType);

			for (int i = 0; i < _AvailableCtrls.Count; i++)
				cmbListAvailableCtrls.Items.Add(((LoadedVisControl)_AvailableCtrls[i]).ControlName);

			if( _AvailableCtrls.Count > 0 )
				cmbListAvailableCtrls.SelectedIndex = 0;			// ova promjena indeksa �e pozvati handler za combo-box i u njemu �e se kreirati kontrola
		}

		private void frmShowObservedCollComplete_Resize(object sender, EventArgs e)
		{
			if( _VisCtrl != null )
			{
				_VisCtrl.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height - 55);
				Refresh();
			}
		}
		private void CreateVisControl()
		{
			if (_AvailableCtrls.Count >= 1)
			{
				int SelInd = cmbListAvailableCtrls.SelectedIndex;

				_VisCtrl = (UserControl)Activator.CreateInstance(((LoadedVisControl)_AvailableCtrls[SelInd])._Type);

				_VisCtrl.Location = new System.Drawing.Point(0, 75);
				_VisCtrl.Name = "Operator1";
				_VisCtrl.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height - 55);
				_VisCtrl.TabIndex = 0;
				this.Controls.Add(_VisCtrl);

				Object[] args = new object[1];
				args[0] = _Coll;
				((LoadedVisControl)_AvailableCtrls[SelInd])._Type.InvokeMember("ShowCollection",
																							BindingFlags.DeclaredOnly |
																							BindingFlags.Public | BindingFlags.NonPublic |
																							BindingFlags.Instance | BindingFlags.InvokeMethod, null, _VisCtrl, args);
			}
			else
				MessageBox.Show("No controls available for visualization of type : " + ((LoadedVisControl)_AvailableCtrls[0])._Type.Name);
		}

		private void cmbListAvailableCtrls_SelectedIndexChanged(object sender, EventArgs e)
		{
			// najprije se treba rije�iti ve� kreirane kontrole
			this.Controls.Remove(_VisCtrl);

			CreateVisControl();
		}
	}
}