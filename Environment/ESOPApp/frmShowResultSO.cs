using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace ESOPApp
{
	public partial class frmShowResultSO : frmShowSingleResultBase
	{
		public frmShowResultSO()
		{
		}
		public frmShowResultSO(OptimizationResult inRes) : base(inRes)
		{
			InitializeComponent();	

			SOResult res = _optRes._FinalResult as SOResult;
			ISingleObjectiveSolution sol = res.getResult();
			txtObjectiveValue.Text = sol.getCurrObjectiveValue().ToString();
			txtSolution.Text = sol.GetStringRepr();
		}
	}
}