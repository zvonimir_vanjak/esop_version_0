using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;
using ESOPVisualizationControls;

namespace ESOPApp
{
	public partial class frmShowResultMO : frmShowSingleResultBase
	{
		public frmShowResultMO()
		{
			InitializeComponent();
		}
		public frmShowResultMO(OptimizationResult inRes)
			: base(inRes)
		{
			InitializeComponent();

			// najprije treba inicijalizirati list kontrolu
			MOResult res = (MOResult) inRes._FinalResult;

			int NumObj = res._NumObjectives;

			// ovdje bi trebalo i onemogućiti prikaz onih kolona koje odgovaraju nepostojećim funkcijama cilje
			for (int i = NumObj+1; i < 6; i++)
				listParetoSet.Columns[i].Width = 0;

			for (int i = 0; i < res.getNumResult(); i++)
			{
				StringBuilder strID = new StringBuilder();
				strID.Append(i);
				listParetoSet.Items.Add(strID.ToString());
				
				for( int j=0; j<NumObj; j++ )
					listParetoSet.Items[i].SubItems.Add(res.getResult(i).getCurrObjectiveValues().getValue(j).ToString());
				for (int k = NumObj+1; k < 6; k++)
					listParetoSet.Items[i].SubItems.Add(" ");

				// a treba dodati i reprezentaciju rješenja
				listParetoSet.Items[i].SubItems.Add(res.getResult(i).GetStringRepr());
			}

			// a sada bi trebalo vidjeti i kako vizualizirati ParetoSet
			ParetoSet2D_ScatterGraph_Nevron newCtrl = new ParetoSet2D_ScatterGraph_Nevron();
//			ParetoSet2DColl_as_ScatterGraph newCtrl = new ParetoSet2DColl_as_ScatterGraph();
			newCtrl.Size = new System.Drawing.Size(panelParetoSet.Size.Width, panelParetoSet.Size.Height);
			panelParetoSet.Controls.Add(newCtrl);

			// i sada bi trebalo prikazati rezultate
			ISolutionMO[] obj = new ISolutionMO[res.getNumResult()];
			for (int i = 0; i < res.getNumResult(); i++)
				obj[i] = res.getResult(i);

			newCtrl.Show(obj);
		}

	}
}