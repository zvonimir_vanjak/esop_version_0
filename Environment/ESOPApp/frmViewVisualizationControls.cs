using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ESOPApp
{
	public partial class frmViewVisualizationControls : Form
	{
		public frmViewVisualizationControls(VisualizationControlsManager inManager)
		{
			InitializeComponent();

			for (int i = 0; i < inManager.getNumComplete(); i++)
			{
				LoadedVisControl visCtrl = (LoadedVisControl) inManager._arlCompleteVisCtrl[i];

				_listCompleteControls.Items.Add(visCtrl.ControlName);
				_listCompleteControls.Items[i].SubItems.Add(visCtrl._RequestedCollMemeberType.Name);
			}

			for (int i = 0; i < inManager.getNumIndividual(); i++)
			{
				LoadedVisControl visCtrl = (LoadedVisControl)inManager._arlIndividualMemberVisCtrl[i];

				_listIndividualControls.Items.Add(visCtrl.ControlName);
				_listIndividualControls.Items[i].SubItems.Add(visCtrl._RequestedCollMemeberType.Name);
			}
		}
	}
}