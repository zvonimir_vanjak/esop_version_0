using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;
using OptimizationManagementLib;

namespace ESOPApp
{
	public partial class frmShowSingleResultBase : Form
	{
		protected OptimizationResult _optRes;

		public frmShowSingleResultBase()
		{
			InitializeComponent();
		}
		public frmShowSingleResultBase(OptimizationResult inRes)
		{
			_optRes = inRes;
			InitializeComponent();

			_listIntermedRes.Items.Clear();

			// i sada mo�emo popuniti list box sa kolekcijama me�urezultata
			for (int i = 0; i < _optRes._arlIntermedResults.Count; i++)
			{
				ObservedValuesCollection coll = (ObservedValuesCollection) _optRes._arlIntermedResults[i];
				_listIntermedRes.Items.Add(coll._strDesc);
				_listIntermedRes.Items[i].SubItems.Add(coll._arrValues.Count.ToString());
				_listIntermedRes.Items[i].SubItems.Add(coll._arrValues[i].GetType().Name);
			}

			// idemo popuniti standardne podatke
			txtProblemName.Text = _optRes._ProblemName;
			txtAlgorithmName.Text = _optRes._AlgorithmName;
			txtAlgorithmDesc.Lines = _optRes._AlgorithmDesc;
			txtTotalIterNum.Text = _optRes._TotalIterNum.ToString();
			txtDuration.Text = _optRes._DurationMs.ToString();
			txtSelRepresentation.Text = _optRes._Representation;
/*
			String[] strArr = new string[5];
			strArr[0] = "Prdo";
			strArr[1] = "Prdo";
			strArr[2] = "Prdo";
			strArr[3] = "Prdo";
			strArr[4] = "Prdo";
			txtAlgorithmDesc.Lines = strArr;
 */
		}

		private void cmdVisualizeComplete_Click(object sender, EventArgs e)
		{
			int	SelInd = 0;

			if( _listIntermedRes.SelectedIndices.Count > 0 )
				SelInd = _listIntermedRes.SelectedIndices[0];

			frmShowObservedCollComplete frmComplete = new frmShowObservedCollComplete((ObservedValuesCollection)_optRes._arlIntermedResults[SelInd]);

			frmComplete.ShowDialog();
		}

		private void cmdVisualizeIndividual_Click(object sender, EventArgs e)
		{
			int	SelInd = 0;

			if( _listIntermedRes.SelectedIndices.Count > 0 )
				SelInd = _listIntermedRes.SelectedIndices[0];

			frmShowObservedCollIndividual frmInd = new frmShowObservedCollIndividual((ObservedValuesCollection)_optRes._arlIntermedResults[SelInd]);
			frmInd.ShowDialog();
		}
	}
}