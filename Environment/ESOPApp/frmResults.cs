using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;
using OptimizationManagementLib;

namespace ESOPApp
{
	public partial class frmResults : Form
	{
		OptPlanResults _Results;

		public frmResults()
		{
			InitializeComponent();
		}

		private void cmdHide_Click(object sender, EventArgs e)
		{
			this.Hide();
		}

		private void _ctrlListResults_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			// TODO - ovisno o vrsti selektiranog rješenja, kreirati odgovarajuću formu za precizniji prikaz rezultata
			int SelItem = _ctrlListResults.SelectedIndices[0];

			OptimizationResult optRes = _Results.GetResult(SelItem);
			if (optRes._FinalResult is SOResult)
			{
				SOResult res = optRes._FinalResult as SOResult;

				frmShowResultSO frm = new frmShowResultSO(optRes);
				frm.ShowDialog();
			}
			else if (optRes._FinalResult is SOWCResult)
			{
				SOWCResult res = optRes._FinalResult as SOWCResult;

				frmShowResultSOWC frm = new frmShowResultSOWC(optRes);
				frm.ShowDialog();
			}
			else if (optRes._FinalResult is MOResult)
			{
				MOResult res = optRes._FinalResult as MOResult;

				frmShowResultMO frm = new frmShowResultMO(optRes);
				frm.ShowDialog();
			}
			else if (optRes._FinalResult is MOWCResult)
			{
				MOWCResult res = optRes._FinalResult as MOWCResult;

				frmShowResultMOWC frm = new frmShowResultMOWC(optRes);
				frm.ShowDialog();
			}
		}

		private void RefreshList(OptPlanResults inRes)
		{
			_Results = inRes;

			_ctrlListResults.Items.Clear();

			for( int i=0; i<inRes.GetResultNum(); i++ )
			{
				OptimizationResult optRes = inRes.GetResult(i);

				_ctrlListResults.Items.Add(optRes._ProblemName);
				_ctrlListResults.Items[i].SubItems.Add(optRes._Representation);
				
				StringBuilder str = new StringBuilder();
				for (int k = 0; k < optRes._AlgorithmDesc.GetLength(0); k++)
					str.Append(optRes._AlgorithmDesc[k] + " ;   ");
				_ctrlListResults.Items[i].SubItems.Add(str.ToString());
				
				_ctrlListResults.Items[i].SubItems.Add(optRes._TotalIterNum.ToString());
				_ctrlListResults.Items[i].SubItems.Add(optRes._DurationMs.ToString() + " ms");
				_ctrlListResults.Items[i].SubItems.Add(optRes._Result);
				_ctrlListResults.Items[i].SubItems.Add(optRes._arlIntermedResults.Count.ToString());
			}
		}

		public void ShowResults(OptPlanResults inRes)
		{
			_Results = inRes;

			if( inRes != null )
				RefreshList(inRes);
			this.Show();
		}
	}
}