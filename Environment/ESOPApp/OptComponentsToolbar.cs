using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using OptimizationManagementLib;

namespace ESOPApp
{
	public partial class OptComponentsToolbar : UserControl
	{
		private OptComponentsManager _OptCompManager;

		private String _CurrSelCtrlText;

		private Color _DefaultButtonColor = new Color();
		private Color _HooverButtonColor = new Color();
		private Color _ClickedButtonColor = new Color();

		bool _bProblemsShown;
		bool _bAlgorithmsShown;
		bool _bOperatorsShown;

		public OptComponentsToolbar(OptComponentsManager inOptCompManager)
		{
			_bProblemsShown = true;
			_bAlgorithmsShown = true;
			_bOperatorsShown = true;

			_OptCompManager = inOptCompManager;

			_DefaultButtonColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			_ClickedButtonColor = System.Drawing.SystemColors.GradientInactiveCaption; // System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(192)));
			_HooverButtonColor = System.Drawing.SystemColors.MenuHighlight; //System.Drawing.SystemColors.InactiveCaptionText; ; // System.Drawing.Color.FromArgb(((System.Byte)(120)), ((System.Byte)(242)), ((System.Byte)(222)));

			InitializeComponent();

			_CurrSelCtrlText = "Pointer";
			this.Refresh();
		}

		public void RefreshControlList()
		{
			// bri�e postoje�e kontrole
			this.Controls.Clear();

			int AlreadyButtons = 0;

			#region Najprije na vrhu dodajemo pointer buttom
			Label buttPointer = new Label();

			buttPointer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			buttPointer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			buttPointer.AutoEllipsis = true;

			buttPointer.Location = new System.Drawing.Point(2, 0);
			buttPointer.ForeColor = System.Drawing.Color.Red;
			buttPointer.Name = "Pointer";
			buttPointer.Size = new System.Drawing.Size(this.Width - 5, 20);
			buttPointer.TabIndex = 0;
			buttPointer.Text = "Pointer";
			buttPointer.Visible = true;
			if (_CurrSelCtrlText == "Pointer")
				buttPointer.BackColor = _ClickedButtonColor;
			else
				buttPointer.BackColor = _DefaultButtonColor;
			buttPointer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

			buttPointer.Click += new System.EventHandler(this.OptComponent_Click);
			buttPointer.MouseHover += new System.EventHandler(this.OptComponent_MouseHover);
			buttPointer.MouseLeave += new System.EventHandler(this.OptComponent_MouseLeave);

			System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
			ToolTip1.AutoPopDelay = 1000;
			ToolTip1.InitialDelay = 1000;
			ToolTip1.ReshowDelay = 500;
			// Force the ToolTip text to be displayed whether or not the form is active.
			ToolTip1.ShowAlways = true;

			ToolTip1.SetToolTip(buttPointer, "Pointer for selection ");

			this.Controls.Add(buttPointer);
			AlreadyButtons++;
			#endregion

			// Pa redom dodajemo tri vrste labele zajedno sa pripadaju�im +/- buttonom
			#region Problems
			Button plusButt1 = new Button();
			if( _bProblemsShown == true )
				plusButt1.Text = "-";
			else
				plusButt1.Text = "+";
			plusButt1.Location = new System.Drawing.Point(2, 20);
			plusButt1.ForeColor = System.Drawing.Color.Black;
			plusButt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			plusButt1.Name = "Pointer";
			plusButt1.Size = new System.Drawing.Size(20, 20);
			plusButt1.TabIndex = 0;
			plusButt1.Visible = true;
			plusButt1.Click += new System.EventHandler(this.ProblemsButton_Click);
			this.Controls.Add(plusButt1);

			Label buttGroup1 = new Label();

			buttGroup1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			buttGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			buttGroup1.AutoEllipsis = true;

			buttGroup1.Location = new System.Drawing.Point(22, 20);
//			buttGroup1.ForeColor = System.Drawing.Color.Silver;
			buttGroup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));

			buttGroup1.Name = "Problems";
			buttGroup1.Size = new System.Drawing.Size(this.Width - 25, 20);
			buttGroup1.TabIndex = 0;
			buttGroup1.Text = "Problems";
			buttGroup1.Visible = true;
			buttGroup1.BackColor = System.Drawing.Color.Silver;
			buttGroup1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.Controls.Add(buttGroup1);

			AlreadyButtons++;

			if (_bProblemsShown == true)
			{
				foreach (LoadedProblem cmp in _OptCompManager._listProbComp)
				{
					Label newButton = new Label();

					newButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
					newButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
					newButton.AutoEllipsis = true;
					newButton.Location = new System.Drawing.Point(2, AlreadyButtons * 20);
					newButton.Name = "Label " + AlreadyButtons.ToString();
					newButton.Size = new System.Drawing.Size(this.Width - 5, 20);
					newButton.TabIndex = AlreadyButtons;
					newButton.Text = cmp.ComponentName;
					newButton.Visible = true;
					newButton.BackColor = _DefaultButtonColor;
					newButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

					newButton.Click += new System.EventHandler(this.OptComponent_Click);
					newButton.MouseHover += new System.EventHandler(this.OptComponent_MouseHover);
					newButton.MouseLeave += new System.EventHandler(this.OptComponent_MouseLeave);

					System.Windows.Forms.ToolTip ToolTipProb = new System.Windows.Forms.ToolTip();
					ToolTipProb.AutoPopDelay = 3000;
					ToolTipProb.InitialDelay = 1000;
					ToolTipProb.ReshowDelay = 500;
					// Force the ToolTip text to be displayed whether or not the form is active.
					ToolTipProb.ShowAlways = true;
					ToolTipProb.SetToolTip(newButton, cmp._ProbDesc);

					this.Controls.Add(newButton);

					AlreadyButtons++;
				}
			}
			#endregion

			#region Algorithms
			Button plusButt2 = new Button();
			if (_bAlgorithmsShown == true)
				plusButt2.Text = "-";
			else
				plusButt2.Text = "+";
			plusButt2.Location = new System.Drawing.Point(2, AlreadyButtons * 20);
			plusButt2.ForeColor = System.Drawing.Color.Black;
			plusButt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			plusButt2.Name = "Pointer";
			plusButt2.Size = new System.Drawing.Size(20, 20);
			plusButt2.TabIndex = 0;
			plusButt2.Visible = true;
			plusButt2.Click += new System.EventHandler(this.AlgorithmsButton_Click);
			this.Controls.Add(plusButt2);
			
			Label buttGroup2 = new Label();

			buttGroup2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			buttGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			buttGroup2.AutoEllipsis = true;

			buttGroup2.Location = new System.Drawing.Point(22, AlreadyButtons * 20);
			buttGroup2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			buttGroup2.Name = "Algorithms";
			buttGroup2.Size = new System.Drawing.Size(this.Width - 25, 20);
			buttGroup2.TabIndex = 0;
			buttGroup2.Text = "Algorithms";
			buttGroup2.Visible = true;
			buttGroup2.BackColor = System.Drawing.Color.Silver;
			buttGroup2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.Controls.Add(buttGroup2);
			
			AlreadyButtons++;

			if (_bAlgorithmsShown == true)
			{
				foreach (LoadedAlgorithm cmp in _OptCompManager._listAlgComp)
				{
					Label newButton = new Label();

					newButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
					newButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
					newButton.AutoEllipsis = true;
					newButton.Location = new System.Drawing.Point(2, AlreadyButtons * 20);
					newButton.Name = "Label " + AlreadyButtons.ToString();
					newButton.Size = new System.Drawing.Size(this.Width - 5, 20);
					newButton.TabIndex = AlreadyButtons;
					newButton.Text = cmp.ComponentName;
					newButton.Visible = true;
					newButton.BackColor = _DefaultButtonColor;
					newButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

					newButton.Click += new System.EventHandler(this.OptComponent_Click);
					newButton.MouseHover += new System.EventHandler(this.OptComponent_MouseHover);
					newButton.MouseLeave += new System.EventHandler(this.OptComponent_MouseLeave);

					ToolTip ToolTipAlg = new System.Windows.Forms.ToolTip();
					ToolTipAlg.AutoPopDelay = 3000;
					ToolTipAlg.InitialDelay = 1000;
					ToolTipAlg.ReshowDelay = 500;
					// Force the ToolTip text to be displayed whether or not the form is active.
					ToolTipAlg.ShowAlways = true;
					ToolTipAlg.SetToolTip(newButton, cmp._AlgDesc);

					this.Controls.Add(newButton);

					AlreadyButtons++;
				}
			}
			#endregion

			#region Operators
			Button plusButt3 = new Button();
			if (_bOperatorsShown == true)
				plusButt3.Text = "-";
			else
				plusButt3.Text = "+";
			plusButt3.Location = new System.Drawing.Point(2, AlreadyButtons * 20);
			plusButt3.ForeColor = System.Drawing.Color.Black;
			plusButt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			plusButt3.Name = "Pointer";
			plusButt3.Size = new System.Drawing.Size(20, 20);
			plusButt3.TabIndex = 0;
			plusButt3.Visible = true;
			plusButt3.Click += new System.EventHandler(this.OperatorsButton_Click);
			this.Controls.Add(plusButt3);

			Label buttGroup3 = new Label();

			buttGroup3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			buttGroup3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			buttGroup3.AutoEllipsis = true;

			buttGroup3.Location = new System.Drawing.Point(22, AlreadyButtons * 20);
			buttGroup3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			buttGroup3.Name = "Operators";
			buttGroup3.Size = new System.Drawing.Size(this.Width - 25, 20);
			buttGroup3.TabIndex = 0;
			buttGroup3.Text = "Operators";
			buttGroup3.Visible = true;
			buttGroup3.BackColor = System.Drawing.Color.Silver;
			buttGroup3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.Controls.Add(buttGroup3);

			AlreadyButtons++;

			if (_bOperatorsShown == true)
			{
				foreach (LoadedOperator cmp in _OptCompManager._listOperComp)
				{
					Label newButton = new Label();

					newButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
					newButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
					newButton.AutoEllipsis = true;
					newButton.Location = new System.Drawing.Point(2, AlreadyButtons * 20);
					newButton.Name = "Label " + AlreadyButtons.ToString();
					newButton.Size = new System.Drawing.Size(this.Width - 5, 20);
					newButton.TabIndex = AlreadyButtons;
					newButton.Text = cmp.ComponentName;
					newButton.Visible = true;
					newButton.BackColor = _DefaultButtonColor;
					newButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

					newButton.Click += new System.EventHandler(this.OptComponent_Click);
					newButton.MouseHover += new System.EventHandler(this.OptComponent_MouseHover);
					newButton.MouseLeave += new System.EventHandler(this.OptComponent_MouseLeave);

					ToolTip ToolTipOper = new System.Windows.Forms.ToolTip();
					ToolTipOper.AutoPopDelay = 3000;
					ToolTipOper.InitialDelay = 1000;
					ToolTipOper.ReshowDelay = 500;
					// Force the ToolTip text to be displayed whether or not the form is active.
					ToolTipOper.ShowAlways = true;
					ToolTipOper.SetToolTip(newButton, cmp._OperDesc);

					this.Controls.Add(newButton);

					AlreadyButtons++;
				}
			}
			#endregion

			this.Refresh();
		}

		private void ProblemsButton_Click(object sender, System.EventArgs e)
		{
			if (_bProblemsShown == true)
				_bProblemsShown = false;
			else
				_bProblemsShown = true;

			RefreshControlList();
		}
		private void AlgorithmsButton_Click(object sender, System.EventArgs e)
		{
			if (_bAlgorithmsShown == true)
				_bAlgorithmsShown = false;
			else
				_bAlgorithmsShown = true;

			RefreshControlList();
		}
		private void OperatorsButton_Click(object sender, System.EventArgs e)
		{
			if (_bOperatorsShown== true)
				_bOperatorsShown = false;
			else
				_bOperatorsShown = true;

			RefreshControlList();
		}
		private void OptComponent_Click(object sender, System.EventArgs e)
		{
			Label clickedLabel = sender as Label;

//			if (!(clickedLabel is null ))
			{
				// treba onu koja je prije bila selektirana, deselektirati
				if (_CurrSelCtrlText != clickedLabel.Text)
				{
					foreach(Control ctrl in this.Controls )
					{
						if (ctrl is Label)
						{
							Label lab = ctrl as Label;
							if (lab.Text == _CurrSelCtrlText)
								lab.BackColor = _DefaultButtonColor;
						}
					}
				}

				// idemo saznati na koju smo stisnuli
				String s = clickedLabel.Name;

				//					clickedLabel.BorderStyle = BorderStyle.Fixed3D;
				clickedLabel.BackColor = _ClickedButtonColor;
				clickedLabel.Refresh();

				_CurrSelCtrlText = clickedLabel.Text;
				_OptCompManager.setCurrSelComp(clickedLabel.Text);
			}
		}

		private void OptComponent_MouseHover(object sender, System.EventArgs e)
		{
			Label clickedLabel = sender as Label;

//			clickedLabel.BorderStyle = BorderStyle.Fixed3D;
			if (_CurrSelCtrlText != clickedLabel.Text)
			{
				clickedLabel.BackColor = _HooverButtonColor;
				clickedLabel.Refresh();
			}
		}

		private void OptComponent_MouseLeave(object sender, System.EventArgs e)
		{
			Label clickedLabel = sender as Label;

			// ako smo iza�li sa kontrole koja trenutno nije selektirana
			if (_CurrSelCtrlText != clickedLabel.Text )
			{
//				clickedLabel.BorderStyle = BorderStyle.None;
				clickedLabel.BackColor = _DefaultButtonColor;
				clickedLabel.Refresh();
			}
		}

		private void OptComponentsToolbar_Resize(object sender, EventArgs e)
		{
//			MessageBox.Show("Smudi");
			RefreshControlList();
		}
	}
}
