namespace ESOPApp
{
	partial class frmResults
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._ctrlListResults = new System.Windows.Forms.ListView();
			this.ProblemName = new System.Windows.Forms.ColumnHeader();
			this.Representation = new System.Windows.Forms.ColumnHeader();
			this.Algorithm = new System.Windows.Forms.ColumnHeader();
			this.TotalIterNum = new System.Windows.Forms.ColumnHeader();
			this.Duration = new System.Windows.Forms.ColumnHeader();
			this.Result = new System.Windows.Forms.ColumnHeader();
			this.NumObsCollections = new System.Windows.Forms.ColumnHeader();
			this.cmdHide = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// _ctrlListResults
			// 
			this._ctrlListResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ProblemName,
            this.Representation,
            this.Algorithm,
            this.TotalIterNum,
            this.Duration,
            this.Result,
            this.NumObsCollections});
			this._ctrlListResults.FullRowSelect = true;
			this._ctrlListResults.Location = new System.Drawing.Point(12, 12);
			this._ctrlListResults.Name = "_ctrlListResults";
			this._ctrlListResults.Size = new System.Drawing.Size(1045, 418);
			this._ctrlListResults.TabIndex = 0;
			this._ctrlListResults.View = System.Windows.Forms.View.Details;
			this._ctrlListResults.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this._ctrlListResults_MouseDoubleClick);
			// 
			// ProblemName
			// 
			this.ProblemName.Text = "Problem name";
			this.ProblemName.Width = 97;
			// 
			// Representation
			// 
			this.Representation.Text = "Representation";
			this.Representation.Width = 150;
			// 
			// Algorithm
			// 
			this.Algorithm.Text = "Algorithm";
			this.Algorithm.Width = 150;
			// 
			// TotalIterNum
			// 
			this.TotalIterNum.DisplayIndex = 5;
			this.TotalIterNum.Text = "Iter.num";
			this.TotalIterNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// Duration
			// 
			this.Duration.DisplayIndex = 6;
			this.Duration.Text = "Duration (ms)";
			this.Duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// Result
			// 
			this.Result.DisplayIndex = 3;
			this.Result.Text = "Result";
			this.Result.Width = 420;
			// 
			// NumObsCollections
			// 
			this.NumObsCollections.DisplayIndex = 4;
			this.NumObsCollections.Text = "Obs.collection num";
			this.NumObsCollections.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.NumObsCollections.Width = 100;
			// 
			// cmdHide
			// 
			this.cmdHide.Location = new System.Drawing.Point(481, 459);
			this.cmdHide.Name = "cmdHide";
			this.cmdHide.Size = new System.Drawing.Size(75, 23);
			this.cmdHide.TabIndex = 1;
			this.cmdHide.Text = "Hide";
			this.cmdHide.Click += new System.EventHandler(this.cmdHide_Click);
			// 
			// frmResults
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1079, 494);
			this.ControlBox = false;
			this.Controls.Add(this.cmdHide);
			this.Controls.Add(this._ctrlListResults);
			this.Name = "frmResults";
			this.Text = "Optimization results";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView _ctrlListResults;
		private System.Windows.Forms.ColumnHeader ProblemName;
		private System.Windows.Forms.ColumnHeader Representation;
		private System.Windows.Forms.ColumnHeader Algorithm;
		private System.Windows.Forms.ColumnHeader Result;
		private System.Windows.Forms.Button cmdHide;
		private System.Windows.Forms.ColumnHeader NumObsCollections;
		private System.Windows.Forms.ColumnHeader TotalIterNum;
		private System.Windows.Forms.ColumnHeader Duration;
	}
}