using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;

using ESOP.Framework.Base;
using ESOPVisualizationControls;

namespace ESOPApp
{
	public partial class frmShowObservedCollIndividual : Form
	{
		private int _CurrInd;
		private int _TotalNum;

		private ArrayList _AvailableCtrls = new ArrayList();
		private ObservedValuesCollection _Coll;
		private UserControl _VisCtrl;

		LoadedVisControl _selVisCtrlObj;

		public frmShowObservedCollIndividual()
		{
			InitializeComponent();
		}

		public frmShowObservedCollIndividual(ObservedValuesCollection inColl)
		{
			_Coll = inColl;
			_CurrInd = 0;
			_TotalNum = _Coll._arrValues.Count;

			InitializeComponent();

			Type thisType = inColl._arrValues[0].GetType();
			_AvailableCtrls = MainForm._VisCtrlManager.getAvailableIndividualCtrlForType(thisType);

			for (int i = 0; i < _AvailableCtrls.Count; i++)
				cmbListAvailableCtrls.Items.Add(((LoadedVisControl)_AvailableCtrls[i]).ControlName);

			if (_AvailableCtrls.Count > 0)
				cmbListAvailableCtrls.SelectedIndex = 0;			// ova promjena indeksa �e pozvati handler za combo-box i u njemu �e se kreirati kontrola
		}

		private void CreateVisControl()
		{
			if (_AvailableCtrls.Count >= 1)
			{
				int SelInd = cmbListAvailableCtrls.SelectedIndex;

				txtObsValType.Text = ((LoadedVisControl)_AvailableCtrls[SelInd])._Type.Name;

				_selVisCtrlObj = (LoadedVisControl)_AvailableCtrls[SelInd];
				_VisCtrl = (UserControl)Activator.CreateInstance(_selVisCtrlObj._Type);

				//				newCtrl.Location = new System.Drawing.Point(0, 75);
				_VisCtrl.Name = "Operator1";
				_VisCtrl.Size = new System.Drawing.Size(panelCtrl.Width, panelCtrl.Height);
				_VisCtrl.TabIndex = 0;
				panelCtrl.Controls.Add(_VisCtrl);

				RefreshShownObject();
			}
			else
				MessageBox.Show("No controls available for visualization of type : " + ((LoadedVisControl)_AvailableCtrls[0])._Type.Name);
		}

		private void RefreshShownObject()
		{
			txtCurrInd.Text = _CurrInd.ToString() + "/" + _Coll._arrValues.Count.ToString();

			Object[] args = new object[1];
			args[0] = _Coll._arrValues[_CurrInd];
			_selVisCtrlObj._Type.InvokeMember("Show",
																				BindingFlags.DeclaredOnly |
																				BindingFlags.Public | BindingFlags.NonPublic |
																				BindingFlags.Instance | BindingFlags.InvokeMethod, null, _VisCtrl, args);
		}

		private void cmbListAvailableCtrls_SelectedIndexChanged(object sender, EventArgs e)
		{
			// najprije se treba rije�iti ve� kreirane kontrole
			this.Controls.Remove(_VisCtrl);

			CreateVisControl();
		}

		private void frmShowObservedCollIndividual_Resize(object sender, EventArgs e)
		{
			if (_VisCtrl != null)
			{
				_VisCtrl.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height - 55);
				Refresh();
			}
		}
		private void cmdFirst_Click(object sender, EventArgs e)
		{
			_CurrInd = 0;
			RefreshShownObject();
		}

		private void cmdPrev5_Click(object sender, EventArgs e)
		{
			_CurrInd--;
			if( _CurrInd < 0 )
				_CurrInd = 0;
			RefreshShownObject();
		}

		private void cmdPrev_Click(object sender, EventArgs e)
		{
			_CurrInd--;
			if (_CurrInd < 0)
				_CurrInd = 0;
			RefreshShownObject();
		}

		private void cmdNext_Click(object sender, EventArgs e)
		{
			_CurrInd++;
			if (_CurrInd >= _TotalNum)
				_CurrInd = _TotalNum - 1;
			RefreshShownObject();
		}

		private void cmdNext5_Click(object sender, EventArgs e)
		{
			_CurrInd++;
			if (_CurrInd >= _TotalNum)
				_CurrInd = _TotalNum - 1;
			RefreshShownObject();
		}

		private void cmdLast_Click(object sender, EventArgs e)
		{
			_CurrInd = _Coll._arrValues.Count - 1;
			RefreshShownObject();
		}
	}
}