using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using System.Reflection;
using ESOPVisualizationControls;

namespace ESOPApp
{
	public class LoadedVisControl
	{
		public string Namespace;
		public string DLLName;
		public string ControlName;

		public Type _Type;
		public Type _RequestedCollMemeberType;

		// podaci potrebni za rekreiranje dinamičkog objekta za optimizaciju
		public LoadedVisControl(string inNamespace, string inDLLName, string inControlName, Type inType, Type inReqMem)
		{
			Namespace = inNamespace;
			DLLName = inDLLName;
			ControlName = inControlName;

			_Type = inType;
			_RequestedCollMemeberType = inReqMem;
		}
	}

	public class VisualizationControlsManager
	{
		public ArrayList _arlCompleteVisCtrl = new ArrayList();
		public ArrayList _arlIndividualMemberVisCtrl = new ArrayList();

		public static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
		{
			if (typeObj.ToString() == criteriaObj.ToString())
				return true;
			else
				return false;
		}

		public bool LoadVisualizationControls(String filePath)
		{
			Assembly a = Assembly.LoadFrom(filePath);
			if (a == null)
				return false;

			Type[] mytypes = a.GetExportedTypes();

			foreach (Type t in mytypes)
			{
				TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

				Type[] myInterfaces = t.FindInterfaces(myFilter, "ESOPVisualizationControls.ICompleteCollVisualizationControl");
				if (myInterfaces.Length > 0)
				{
					object obj = Activator.CreateInstance(t);

					Type reqType = (Type)t.InvokeMember("GetRequestedMemberType",
																								BindingFlags.DeclaredOnly |
																								BindingFlags.Public | BindingFlags.NonPublic |
																								BindingFlags.Instance | BindingFlags.InvokeMethod, null, obj, null);

					LoadedVisControl ctrl = new LoadedVisControl(t.Namespace, filePath, t.Name, t, reqType);

					_arlCompleteVisCtrl.Add(ctrl);
				}
				myInterfaces = t.FindInterfaces(myFilter, "ESOPVisualizationControls.IIndividualCollMemberVisualizationControl");
				if (myInterfaces.Length > 0)
				{
					object obj = Activator.CreateInstance(t);

					Type reqType = (Type)t.InvokeMember("GetRequestedMemberType",
																								BindingFlags.DeclaredOnly |
																								BindingFlags.Public | BindingFlags.NonPublic |
																								BindingFlags.Instance | BindingFlags.InvokeMethod, null, obj, null);
					LoadedVisControl ctrl = new LoadedVisControl(t.Namespace, filePath, t.Name, t, reqType);

					_arlIndividualMemberVisCtrl.Add(ctrl);
				}
			}

			return true;
		}

		public int getNumComplete()
		{
			return _arlCompleteVisCtrl.Count;
		}
		public int getNumIndividual()
		{
			return _arlIndividualMemberVisCtrl.Count;
		}
		public ArrayList getAvailableCompleteCtrlForType(Type inType)
		{
			ArrayList arr = new ArrayList();

			for (int i = 0; i < _arlCompleteVisCtrl.Count; i++)
			{
				LoadedVisControl loaded = _arlCompleteVisCtrl[i] as LoadedVisControl;
				if (loaded._RequestedCollMemeberType == inType)
					arr.Add(loaded);
			}
			return arr;
		}
		public ArrayList getAvailableIndividualCtrlForType(Type inType)
		{
			ArrayList arr = new ArrayList();

			for (int i = 0; i < _arlIndividualMemberVisCtrl.Count; i++)
			{
				LoadedVisControl loaded = _arlIndividualMemberVisCtrl[i] as LoadedVisControl;
				if (loaded._RequestedCollMemeberType == inType)
					arr.Add(loaded);
			}
			return arr;
		}
	}
}
