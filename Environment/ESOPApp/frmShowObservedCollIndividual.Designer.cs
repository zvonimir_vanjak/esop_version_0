namespace ESOPApp
{
	partial class frmShowObservedCollIndividual
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdPrev5 = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.txtObsValType = new System.Windows.Forms.TextBox();
			this.cmdPrev = new System.Windows.Forms.Button();
			this.cmdNext = new System.Windows.Forms.Button();
			this.cmdNext5 = new System.Windows.Forms.Button();
			this.cmdFirst = new System.Windows.Forms.Button();
			this.cmdLast = new System.Windows.Forms.Button();
			this.panelCtrl = new System.Windows.Forms.Panel();
			this.txtCurrInd = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.cmbListAvailableCtrls = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// cmdPrev5
			// 
			this.cmdPrev5.Location = new System.Drawing.Point(92, 50);
			this.cmdPrev5.Name = "cmdPrev5";
			this.cmdPrev5.Size = new System.Drawing.Size(75, 23);
			this.cmdPrev5.TabIndex = 0;
			this.cmdPrev5.Text = "<<  (5 %)";
			this.cmdPrev5.Click += new System.EventHandler(this.cmdPrev5_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 14);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(79, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Obs.values type";
			// 
			// txtObsValType
			// 
			this.txtObsValType.Location = new System.Drawing.Point(107, 11);
			this.txtObsValType.Name = "txtObsValType";
			this.txtObsValType.Size = new System.Drawing.Size(100, 20);
			this.txtObsValType.TabIndex = 4;
			// 
			// cmdPrev
			// 
			this.cmdPrev.Location = new System.Drawing.Point(173, 50);
			this.cmdPrev.Name = "cmdPrev";
			this.cmdPrev.Size = new System.Drawing.Size(75, 23);
			this.cmdPrev.TabIndex = 5;
			this.cmdPrev.Text = "<<";
			this.cmdPrev.Click += new System.EventHandler(this.cmdPrev_Click);
			// 
			// cmdNext
			// 
			this.cmdNext.Location = new System.Drawing.Point(367, 50);
			this.cmdNext.Name = "cmdNext";
			this.cmdNext.Size = new System.Drawing.Size(75, 23);
			this.cmdNext.TabIndex = 6;
			this.cmdNext.Text = ">>";
			this.cmdNext.Click += new System.EventHandler(this.cmdNext_Click);
			// 
			// cmdNext5
			// 
			this.cmdNext5.Location = new System.Drawing.Point(449, 50);
			this.cmdNext5.Name = "cmdNext5";
			this.cmdNext5.Size = new System.Drawing.Size(75, 23);
			this.cmdNext5.TabIndex = 7;
			this.cmdNext5.Text = " (5 %) >>";
			this.cmdNext5.Click += new System.EventHandler(this.cmdNext5_Click);
			// 
			// cmdFirst
			// 
			this.cmdFirst.Location = new System.Drawing.Point(11, 50);
			this.cmdFirst.Name = "cmdFirst";
			this.cmdFirst.Size = new System.Drawing.Size(75, 23);
			this.cmdFirst.TabIndex = 8;
			this.cmdFirst.Text = "First";
			this.cmdFirst.Click += new System.EventHandler(this.cmdFirst_Click);
			// 
			// cmdLast
			// 
			this.cmdLast.Location = new System.Drawing.Point(530, 50);
			this.cmdLast.Name = "cmdLast";
			this.cmdLast.Size = new System.Drawing.Size(75, 23);
			this.cmdLast.TabIndex = 9;
			this.cmdLast.Text = "Last";
			this.cmdLast.Click += new System.EventHandler(this.cmdLast_Click);
			// 
			// panelCtrl
			// 
			this.panelCtrl.Location = new System.Drawing.Point(11, 91);
			this.panelCtrl.Name = "panelCtrl";
			this.panelCtrl.Size = new System.Drawing.Size(594, 408);
			this.panelCtrl.TabIndex = 10;
			// 
			// txtCurrInd
			// 
			this.txtCurrInd.Location = new System.Drawing.Point(311, 53);
			this.txtCurrInd.Name = "txtCurrInd";
			this.txtCurrInd.Size = new System.Drawing.Size(43, 20);
			this.txtCurrInd.TabIndex = 11;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(266, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(42, 13);
			this.label3.TabIndex = 12;
			this.label3.Text = "Curr.ind.";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(233, 14);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(128, 13);
			this.label4.TabIndex = 13;
			this.label4.Text = "Select visualization control";
			// 
			// cmbListAvailableCtrls
			// 
			this.cmbListAvailableCtrls.FormattingEnabled = true;
			this.cmbListAvailableCtrls.Location = new System.Drawing.Point(367, 12);
			this.cmbListAvailableCtrls.Name = "cmbListAvailableCtrls";
			this.cmbListAvailableCtrls.Size = new System.Drawing.Size(238, 21);
			this.cmbListAvailableCtrls.TabIndex = 14;
			this.cmbListAvailableCtrls.SelectedIndexChanged += new System.EventHandler(this.cmbListAvailableCtrls_SelectedIndexChanged);
			// 
			// frmShowObservedCollIndividual
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(624, 515);
			this.Controls.Add(this.cmbListAvailableCtrls);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtCurrInd);
			this.Controls.Add(this.panelCtrl);
			this.Controls.Add(this.cmdLast);
			this.Controls.Add(this.cmdFirst);
			this.Controls.Add(this.cmdNext5);
			this.Controls.Add(this.cmdNext);
			this.Controls.Add(this.cmdPrev);
			this.Controls.Add(this.txtObsValType);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cmdPrev5);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "frmShowObservedCollIndividual";
			this.Text = "Individual values in collection";
			this.Resize += new System.EventHandler(this.frmShowObservedCollIndividual_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button cmdPrev5;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtObsValType;
		private System.Windows.Forms.Button cmdPrev;
		private System.Windows.Forms.Button cmdNext;
		private System.Windows.Forms.Button cmdNext5;
		private System.Windows.Forms.Button cmdFirst;
		private System.Windows.Forms.Button cmdLast;
		private System.Windows.Forms.Panel panelCtrl;
		private System.Windows.Forms.TextBox txtCurrInd;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cmbListAvailableCtrls;
	}
}