namespace ESOPApp
{
	partial class frmShowResultMO
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listParetoSet = new System.Windows.Forms.ListView();
			this.panelParetoSet = new System.Windows.Forms.Panel();
			this.ID = new System.Windows.Forms.ColumnHeader();
			this.columnF1 = new System.Windows.Forms.ColumnHeader();
			this.columnF2 = new System.Windows.Forms.ColumnHeader();
			this.columnF3 = new System.Windows.Forms.ColumnHeader();
			this.columnF4 = new System.Windows.Forms.ColumnHeader();
			this.columnF5 = new System.Windows.Forms.ColumnHeader();
			this.columnSolRepr = new System.Windows.Forms.ColumnHeader();
			this.SuspendLayout();
			// 
			// listParetoSet
			// 
			this.listParetoSet.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.columnF1,
            this.columnF2,
            this.columnF3,
            this.columnF4,
            this.columnF5,
            this.columnSolRepr});
			this.listParetoSet.Location = new System.Drawing.Point(31, 402);
			this.listParetoSet.Name = "listParetoSet";
			this.listParetoSet.Size = new System.Drawing.Size(280, 236);
			this.listParetoSet.TabIndex = 22;
			this.listParetoSet.View = System.Windows.Forms.View.Details;
			// 
			// panelParetoSet
			// 
			this.panelParetoSet.Location = new System.Drawing.Point(327, 402);
			this.panelParetoSet.Name = "panelParetoSet";
			this.panelParetoSet.Size = new System.Drawing.Size(289, 236);
			this.panelParetoSet.TabIndex = 23;
			// 
			// ID
			// 
			this.ID.Text = "ID";
			// 
			// columnF1
			// 
			this.columnF1.Text = "f1";
			// 
			// columnF2
			// 
			this.columnF2.Text = "f2";
			// 
			// columnF3
			// 
			this.columnF3.Text = "f3";
			// 
			// columnF4
			// 
			this.columnF4.Text = "f4";
			// 
			// columnF5
			// 
			this.columnF5.Text = "f5";
			// 
			// columnSolRepr
			// 
			this.columnSolRepr.Text = "Solution";
			// 
			// frmShowResultMO
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(639, 650);
			this.Controls.Add(this.listParetoSet);
			this.Controls.Add(this.panelParetoSet);
			this.Name = "frmShowResultMO";
			this.Text = "frmShowResultMO";
			this.Controls.SetChildIndex(this.panelParetoSet, 0);
			this.Controls.SetChildIndex(this.listParetoSet, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView listParetoSet;
		private System.Windows.Forms.Panel panelParetoSet;
		private System.Windows.Forms.ColumnHeader ID;
		private System.Windows.Forms.ColumnHeader columnF1;
		private System.Windows.Forms.ColumnHeader columnF2;
		private System.Windows.Forms.ColumnHeader columnF3;
		private System.Windows.Forms.ColumnHeader columnF4;
		private System.Windows.Forms.ColumnHeader columnF5;
		private System.Windows.Forms.ColumnHeader columnSolRepr;
	}
}