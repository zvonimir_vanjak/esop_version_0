using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using OptimizationManagementLib;
using ESOPPresentationControls;
using ESOP.Framework.DynamicSupport;

namespace ESOPApp
{
	public partial class frmOptPlan : Form
	{
		private OptPlan				_OptPlan;
		private ExecutionPlan _ExecPlan;
		private OptPlanWrapperFactory _WrapperFactory = new OptPlanWrapperFactory();

		private bool	_bFirstCompSelected;
		private int		_MouseDownX, _MouseDownY;
		private int		_MouseCurrX, _MouseCurrY;
		
		UserControl		_firstSelCtrl = null;
		UserControl		_secondSelCtrl = null;

		private UserControl _SelCtrl;
		
		public	bool	_bControlSelected;
		private int		_StartX, _StartY;
		private int		_DX, _DY;

		public frmOptPlan(OptPlan inOptPlan )
		{
			_OptPlan	= inOptPlan;
			_ExecPlan = new ExecutionPlan();

			InitializeComponent();

			_bFirstCompSelected = false;
			_bControlSelected = false;
	//		this.SuspendLayout();
		}

		private void frmOptPlan_Paint(object sender, PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			
			if (_bFirstCompSelected)
				g.DrawLine(new Pen(Color.BlueViolet, 1), _MouseDownX, _MouseDownY, _MouseCurrX, _MouseCurrY);

			DrawExecutionUnits(g);
		}
		private void DrawExecutionUnits(Graphics g)
		{
			// pro�i kroz sve kreirane exec.units i iscrtati linije izme�u opt.componenti i exec.unit kontrole
			for (int i = 0; i < _OptPlan._listExecUnits.Count; i++)
			{
				ExecutionUnit execUnit = (ExecutionUnit) _OptPlan._listExecUnits[i];
				g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

				int x1, y1, x2, y2;
				x1 = execUnit.getAlgWrapper().getUserControlLocation().X + execUnit.getAlgWrapper().getUserControlSize().Width / 2;
				y1 = execUnit.getAlgWrapper().getUserControlLocation().Y + execUnit.getAlgWrapper().getUserControlSize().Height / 2;
				x2 = execUnit.getUserControlLocation().X + execUnit.getUserControlSize().Width / 2;
				y2 = execUnit.getUserControlLocation().Y + execUnit.getUserControlSize().Height / 2;
				g.DrawLine(new Pen(Color.Black, 2), x1, y1, x2, y2);

				x1 = execUnit.getProbWrapper().getUserControlLocation().X + execUnit.getProbWrapper().getUserControlSize().Width / 2;
				y1 = execUnit.getProbWrapper().getUserControlLocation().Y + execUnit.getProbWrapper().getUserControlSize().Height / 2;
				x2 = execUnit.getUserControlLocation().X + execUnit.getUserControlSize().Width / 2;
				y2 = execUnit.getUserControlLocation().Y + execUnit.getUserControlSize().Height / 2;

				g.DrawLine(new Pen(Color.Black, 2), x1, y1, x2, y2);

				// TODO - iscrtati veze izme�u ExecutionUnit-a i pripadnih kontrola
			}
		}
		private void DrawOperatorConnectors(Graphics g)
		{
		}

		private void frmOptPlan_MouseDown(object sender, MouseEventArgs e)
		{
			if (_bFirstCompSelected == false) {
				if (MainForm._OptComponentsManager.IsOptComponentSelected()) 
				{
					// znaci da imamo selektiranu optimizacijsku komponentu i da smo kliknuli mi�em na povr�inu  opt plana
					if (MainForm._OptComponentsManager.getCurrSelComp() is LoadedCompleteProblem)
					{
						VisualProblemControl newCtrl = (VisualProblemControl)_WrapperFactory.CreateWrapperObjectUserControl(this, MainForm._OptComponentsManager.getCurrSelComp(), e.X, e.Y);
						OptPlanProblem newOptProblem = _OptPlan.AddNewCompleteProblem(MainForm._OptComponentsManager.getCurrSelComp() as LoadedCompleteProblem, newCtrl);

						this.Controls.Add(newCtrl);
					}
					else if (MainForm._OptComponentsManager.getCurrSelComp() is LoadedParameterProblem)
					{
						VisualParametrizedProblemControl  newCtrl = (VisualParametrizedProblemControl) _WrapperFactory.CreateWrapperObjectUserControl(this, MainForm._OptComponentsManager.getCurrSelComp(), e.X, e.Y);
						OptPlanProblem newOptProblem = _OptPlan.AddNewParamProblem(MainForm._OptComponentsManager.getCurrSelComp() as LoadedParameterProblem, newCtrl);

						newCtrl.SetProblem(newOptProblem);
						this.Controls.Add(newCtrl);
					}
					else if (MainForm._OptComponentsManager.getCurrSelComp() is LoadedCompleteAlgorithm)
					{
						VisualAlgorithmControl newCtrl = (VisualAlgorithmControl)_WrapperFactory.CreateWrapperObjectUserControl(this, MainForm._OptComponentsManager.getCurrSelComp(), e.X, e.Y);
						_OptPlan.AddNewCompleteAlgorithm(MainForm._OptComponentsManager.getCurrSelComp() as LoadedCompleteAlgorithm, newCtrl);

						this.Controls.Add(newCtrl);
					}
					else if (MainForm._OptComponentsManager.getCurrSelComp() is LoadedTemplateAlgorithm)
					{
						VisualAlgorithmControl newCtrl = (VisualAlgorithmControl)_WrapperFactory.CreateWrapperObjectUserControl(this, MainForm._OptComponentsManager.getCurrSelComp(), e.X, e.Y);
						_OptPlan.AddNewTemplateAlgorithm(MainForm._OptComponentsManager.getCurrSelComp() as LoadedTemplateAlgorithm, newCtrl);

						this.Controls.Add(newCtrl);
					}
				}
			}
			else {
				// zna�i da je tijekom razvla�enja linije kliknuto negdje na formu gdje nema opt.komponente
			}
		}
		private void frmOptPlan_MouseMove(object sender, MouseEventArgs e)
		{
			if (MainForm.getCreateExecutionUnitFlag() == true)
			{
				if (_bFirstCompSelected)
				{
					_MouseCurrX = e.X;
					_MouseCurrY = e.Y;
					
					this.Refresh();
				}
			}
		}

		public void OnOptComponentMouseUp(object sender, int inX, int inY)
		{
			if (MainForm.getCreateExecutionUnitFlag())
			{
				if (_bFirstCompSelected == false)
				{
					_firstSelCtrl = sender as UserControl;
					_bFirstCompSelected = true;
					_MouseDownX = inX;
					_MouseDownY = inY;
				}
				else
				{
					_secondSelCtrl = sender as UserControl;

					// tra�imo pokaziva�e na odabrani problem i algoritam opt objekt
					OptPlanAlgorithm Alg;
					OptPlanProblem Prob;
					if (_firstSelCtrl is VisualAlgorithmControl)
					{
						Alg = _OptPlan.GetAlgWrapperFromControl(_firstSelCtrl);
						Prob = _OptPlan.GetProbWrapperFromControl(_secondSelCtrl);
					}
					else {
						Alg = _OptPlan.GetAlgWrapperFromControl(_secondSelCtrl);
						Prob = _OptPlan.GetProbWrapperFromControl(_firstSelCtrl);
					}

					// kreiramo novi ExecutionUnit
					CreateNewExecUnit(Alg, Prob, inX, inY);

					_bFirstCompSelected = false;
					MainForm.setCreateExecutionUnitFlag(false);
				}
			}
			else
			{
				// zna�i da nismo u procesu dodavanja ExecUnit-a ve� smo kliknuli na komponentu
				_bControlSelected = false;
			}
		}
		public void OnControlMouseDown(object sender, int inX, int inY)
		{
			if (!MainForm.getCreateExecutionUnitFlag())
			{
				_bControlSelected = true;
				_SelCtrl = sender as UserControl;

				_StartX = _SelCtrl.Location.X;
				_StartY = _SelCtrl.Location.Y;
				_DX = inX;
				_DY = inY;
			}
		}
		public void OnControlMouseMove(object sender, int inX, int inY)
		{
			if (_bControlSelected == true && !(inX == _DX && inY == _DY))
			{
				int newX = _StartX + (inX - _DX);
				int newY = _StartY + (inY - _DY);
				_SelCtrl.Location = new Point(newX, newY);

				// na Opt objekt �ija je ovo kontrola tako�er postaviti novu vrijednost
				_OptPlan.SetLocationForControl(_SelCtrl, newX, newY);

				_StartX += (inX - _DX);
				_StartY += (inY - _DY);

				this.Refresh();
			}
		}
		public void OnClickedOperatorOnAlgorithm(OptPlanAlgorithmContext inContext, int SelOperIndex, int inX, int inY)
		{
			// provjeriti da li je selektiran operator
			ILoadedComponent comp = MainForm._OptComponentsManager.getCurrSelComp();

			if (comp is LoadedOperator)
			{
				// pa treba provjeriti da li selektirana komponenta odgovara onome �to se tra�i na tom konektoru
				LoadedOperator oper = comp as LoadedOperator;
				string OperInterfaceName = inContext._AlgOper.getOperType(SelOperIndex);
				
				if (OptComponentsLoader.DoesTypeSupportInterface(oper._Type, "ESOP.Framework.Operators.GeneticAlgorithms." + OperInterfaceName))
				{
					// TODO - treba i provjeriti da li odgovara po vrsti reprezentacije nad kojom operira
					int x = inX + 135;
					int y = inY + SelOperIndex * 33 - 10;

					CreateNewOperator(inContext, oper, SelOperIndex, x,y);
				}
			}
//				else
//					MessageBox.Show("Nije selektiran operator !!!");
		}
		public void OnDeleteThisObject(object sender)
		{
			if (sender is VisualProblemControl || sender is VisualParametrizedProblemControl)
			{
				foreach (OptPlanProblem prob in _OptPlan._listProb)
				{
					if (prob.isTheSameControl(sender as UserControl) == true)
					{
						// najprije treba izbaciti sve ExecutionUnit-e !
						bool bDeleted = true;
						while (bDeleted == true)
						{
							bDeleted = false;
							foreach (ExecutionUnit exec in _OptPlan._listExecUnits)
							{
								if (exec.getProbWrapper() == prob)
								{
									DeleteExecutionUnit(exec);			// brisanje ExecUnit-a iz kolekcije �e poremetiti iterator po kolekciji, i zato foreach moramo restartati
									bDeleted = true;
									break;
								}
							}
						}
						// treba izbaciti odgovaraju�u kontrolu
						this.Controls.Remove(prob.getProbUserControl());

						// i zatim ga treba izbaciti iz liste problema
						_OptPlan._listProb.Remove(prob);

						return;
					}
				}
			}
			else if (sender is VisualAlgorithmControl)
			{
				foreach (OptPlanAlgorithm alg in _OptPlan._listAlg)
				{
					if (alg.isTheSameControl(sender as UserControl) == true)
					{
						// najprije treba izbaciti sve ExecutionUnit-e !
						bool bDeleted = true;
						while (bDeleted == true)
						{
							bDeleted = false;
							foreach (ExecutionUnit exec in _OptPlan._listExecUnits)
							{
								if (exec.getAlgWrapper() == alg)
								{
									DeleteExecutionUnit(exec);			// brisanje ExecUnit-a iz kolekcije �e poremetiti iterator po kolekciji, i zato foreach moramo restartati
									bDeleted = true;
									break;
								}
							}
						}
						// treba izbaciti odgovaraju�u kontrolu
						this.Controls.Remove(alg.getAlgUserControl());

						// i zatim ga treba izbaciti iz liste problema
						_OptPlan._listProb.Remove(alg);

						return;
					}
				}
			}
			else if (sender is VisualExecUnitControl)
			{
				foreach (ExecutionUnit exec in _OptPlan._listExecUnits)
				{
					if (exec.getExecUnitControl().Equals(sender))
					{
						DeleteExecutionUnit(exec);
						return;
					}
				}
			}
			else if (sender is VisualObserverObject)
			{
				// najprije treba na�i kojem ExecUnit-u pripada taj observer !!!
				MessageBox.Show("Not yet implemented :-( !");
			}

			this.Refresh();
		}
		public void DeleteExecutionUnit(ExecutionUnit inExec)
		{
			// najprije �emo ukloniti sve vizualne kontrole
			this.Controls.Remove(inExec._OptTerminator._VisCtrl);
			this.Controls.Remove(inExec._SolObject._VisCtrl);
			this.Controls.Remove(inExec.getExecUnitControl());
			
			// kod AlgContext-a treba najprije ukloniti sve operator
			OptPlanAlgorithmContext algContext = inExec._AlgContext;
			int NumOper = algContext._AlgOper.getOperNum();
			for (int i = 0; i < NumOper; i++)
			{
				if (algContext._AlgOper.getOperator(i) != null)
					this.Controls.Remove(algContext._AlgOper.getOperator(i)._VisCtrl);
			}
			this.Controls.Remove(inExec._AlgContext._VisCtrl);

			// a kod master observer-a treba najprije ukloniti sve observere
			OptPlanIntermediateResultsObserver intermedRes = inExec._IntermedRes;
			int NumObs= intermedRes.getNumObservers();
			for (int i = 0; i < NumObs; i++)
				this.Controls.Remove(intermedRes.getObserver(i)._VisCtrl);

			this.Controls.Remove(inExec._IntermedRes._VisCtrl);

			// a onda treba ukloniti i odgovaraju�e objekte iz samog ExecutionUnit-a
			inExec.DeleteAllParts();

			// i kona�nom, uklanjamo ExecutionUnit iz liste
			_OptPlan._listExecUnits.Remove(inExec);
		}
		public void CreateNewOperator(OptPlanAlgorithmContext inAlgContext, LoadedOperator inOper, int SelOperIndex, int inX, int inY)
		{
			OptPlanOperator newOptPlanOper = new OptPlanOperator(inOper);

			CreateVisControlForOperator(newOptPlanOper, inX, inY);

			inAlgContext.SetOperatorWrapperObject(SelOperIndex, newOptPlanOper);
		}
		public void CreateVisControlForOperator(OptPlanOperator newOptPlanOper, int inX, int inY)
		{
			// ako je sve u redu treba u AlgContextu postaviti selektiranu komp. kao Loaded operator i postaviti vizualizacijski operator na povr�inu
			VisualOperatorControl newCtrl = OptPlanWrapperFactory.CreateOperatorControl(newOptPlanOper._Operator.ComponentName, inX, inY);

			// OptPlanOperator i VisualOperatorControl imaju uzajamno referencu jedan na drugoga
			newOptPlanOper.AssignVisualControl(newCtrl);
			newCtrl.SetOperator(newOptPlanOper);
			this.Controls.Add(newCtrl);
		}
		public void CreateNewObserver(OptPlanIntermediateResultsObserver masterObs, LoadedObserverSaver selObs, int inX, int inY)
		{
			int CurrNumObservers = masterObs.getNumObservers();

			OptPlanObserver	newObs = new OptPlanObserver(selObs);

			CreateVisControlForObserver(newObs, CurrNumObservers, selObs.ObserverDesc, inX, inY);
			
			masterObs.addNewObserver(newObs);
		}
		public void CreateVisControlForObserver(OptPlanObserver inObs, int CtrlIndex, string CtrlDesc, int inX, int inY)
		{
			VisualObserverObject newCtrl = OptPlanWrapperFactory.CreateObserverControl(CtrlIndex, CtrlDesc, inX, inY);

			inObs.AssignVisualControl(newCtrl);
			newCtrl.SetObserver(inObs);
			newCtrl.RegisterDeleteObject(this.OnDeleteThisObject);

			this.Controls.Add(newCtrl);
		}
		public void CreateNewExecUnit(OptPlanAlgorithm inAlg, OptPlanProblem inProb, int inX, int inY)
		{
			ExecutionUnit wrapExecUnit = _OptPlan.AddNewExecUnit(inAlg, inProb, MainForm._OptComponentsManager);

			_ExecPlan.AddExecUnit(wrapExecUnit);

			int x = (_MouseDownX + inX) / 2 - 35;
			int y = (_MouseDownY + inY) / 2 - 25;
			CreateVisControlsForExecUnit(wrapExecUnit, x, y);
		}
		public void CreateVisControlsForExecUnit(ExecutionUnit inExecUnit, int x, int y)
		{
			OptPlanAlgorithm	inAlg = inExecUnit.getAlgWrapper();
			OptPlanProblem		inProb = inExecUnit.getProbWrapper();

			#region Kreiramo OptPlanSolObject user control
			VisualSolObjectControl visCtrlSolObject = new VisualSolObjectControl();
			int x1 = x - 120;
			int y1 = y + 55;
			visCtrlSolObject.Location = new System.Drawing.Point(x1, y1);
			visCtrlSolObject.Name = "solobject";
			visCtrlSolObject.Size = new System.Drawing.Size(140, 50);
			visCtrlSolObject.TabIndex = 0;
			this.Controls.Add(visCtrlSolObject);
			#endregion
			#region Kreiramo OptPlanAlgorithmContext user control
			VisualAlgorithmContextControl visCtrlAlgContext = new VisualAlgorithmContextControl();
			int x2 = x + 50;
			int y2 = y + 55;
			visCtrlAlgContext.Location = new System.Drawing.Point(x2, y2);
			visCtrlAlgContext.Name = "AlgorithmContext";
			visCtrlAlgContext.TabIndex = 0;
			visCtrlAlgContext.RegisterClickEvent(this.OnClickedOperatorOnAlgorithm);
			this.Controls.Add(visCtrlAlgContext);
			#endregion
			#region Kreiramo OptPlanIntermediateResultsObserver user control
			VisualIntermediateResultsControl visCtrlIntermedRes = new VisualIntermediateResultsControl(MainForm._OptComponentsManager);
			int x3 = x + 50;
			int y3 = y - 70;
			visCtrlIntermedRes.Location = new System.Drawing.Point(x3, y3);
			visCtrlIntermedRes.Name = "IntermedObserver";
			visCtrlIntermedRes.Size = new System.Drawing.Size(90, 63);
			visCtrlIntermedRes.RegisterAddNewObserverHandler(this.CreateNewObserver);
			this.Controls.Add(visCtrlIntermedRes);
			#endregion// a ovdje treba kreiramo novi ExecutionUnit i pobrinuti se za njegovo dodavanje kao kontrole
			#region Kreiramo OptPlanTerminator user control
			VisualOptimizationTerminator visCtrlOptTerm = new VisualOptimizationTerminator(inAlg.getLoadedAlgObj(), MainForm._OptComponentsManager);
			int x4 = x - 120;
			int y4 = y - 70;
			visCtrlOptTerm.Location = new System.Drawing.Point(x4, y4);
			visCtrlOptTerm.Name = "OptTerminator";
			visCtrlOptTerm.Size = new System.Drawing.Size(140, 63);
			this.Controls.Add(visCtrlOptTerm);
			#endregion// a ovdje treba kreiramo novi ExecutionUnit i pobrinuti se za njegovo dodavanje kao kontrole
			#region Kreiramo ExecutionUnit user control
			VisualExecUnitControl visCtrlExecUnit = new VisualExecUnitControl();
			visCtrlExecUnit.Location = new System.Drawing.Point(x, y);
			visCtrlExecUnit.Name = "ExecUnit";
			visCtrlExecUnit.Size = new System.Drawing.Size(70, 50);
			visCtrlExecUnit.TabIndex = 0;

			visCtrlExecUnit.RegisterMouseUp(this.OnOptComponentMouseUp);
			visCtrlExecUnit.RegisterMouseDown(this.OnControlMouseDown);
			visCtrlExecUnit.RegisterMouseMove(this.OnControlMouseMove);
			visCtrlExecUnit.RegisterDeleteObject(this.OnDeleteThisObject);

			this.Controls.Add(visCtrlExecUnit);
			#endregion

			inExecUnit.AssignVisualControl(visCtrlExecUnit); visCtrlExecUnit.SetExecUnit(inExecUnit);

			inExecUnit._AlgContext.AssignVisualControl(visCtrlAlgContext);
			inExecUnit._SolObject.AssignVisualControl(visCtrlSolObject);
			inExecUnit._IntermedRes.AssignVisualControl(visCtrlIntermedRes);
			inExecUnit._OptTerminator.AssignVisualControl(visCtrlOptTerm);

			visCtrlSolObject.SetSolObject(inExecUnit._SolObject);
			visCtrlAlgContext.SetAlgContext(inExecUnit._AlgContext);
			visCtrlIntermedRes.setObserverMaster(inExecUnit._IntermedRes);
			visCtrlOptTerm.SetOptTerminator(inExecUnit._OptTerminator);

			visCtrlAlgContext.Size = new System.Drawing.Size(125, 15 * inExecUnit._AlgContext._AlgOper.getOperNum() + 25);

			// i jo� treba vidjeti da li ima ve� pridru�enih observera, pa i za njih kreirati kontrole
			int NumObs = inExecUnit._IntermedRes.getNumObservers();
			for (int i = 0; i < NumObs; i++)
			{
				OptPlanObserver  obs = inExecUnit._IntermedRes.getObserver(i);
				CreateVisControlForObserver(obs, i, obs._ObserverSaver.ObserverDesc, inExecUnit._IntermedRes._X, inExecUnit._IntermedRes._Y);
			}

			// a isto to i za operatore
			int NumOper = inExecUnit._AlgContext._AlgOper.getOperNum();
			for (int i = 0; i < NumOper; i++)
			{
				OptPlanOperator newOper = inExecUnit._AlgContext._AlgOper.getOperator(i);

				if (newOper != null)
				{
					CreateVisControlForOperator(newOper, newOper._X, newOper._Y);
					inExecUnit._AlgContext.SetOperatorWrapperObject(i, newOper);
				}
			}

			Invalidate();
		}

		public void ReAssignLoadedComponentTypes()
		{
			foreach (OptPlanProblem prob in _OptPlan._listProb)
			{
				LoadedProblem loadProb = prob.getLoadedProblemObj();

				loadProb._Type = MainForm._OptComponentsManager.getTypeForLoadedProblem(loadProb);
			}
			foreach (OptPlanAlgorithm alg in _OptPlan._listAlg)
			{
				LoadedAlgorithm loadAlg = alg.getLoadedAlgObj();

				loadAlg._Type = MainForm._OptComponentsManager.getTypeForLoadedAlgorithm(loadAlg);
			}
			foreach (ExecutionUnit exec in _OptPlan._listExecUnits)
			{
				LoadedOptTerminator loadOptTerm = exec._OptTerminator._OptTerminator;
				loadOptTerm._Type = MainForm._OptComponentsManager.getTypeForLoadedOptTerminator(loadOptTerm);

				LoadedRepresentation loadRepr = exec._SolObject.getSelLoadedRepr();
				if( loadRepr != null )
					loadRepr._Type = MainForm._OptComponentsManager.getTypeForLoadedRepresentation(loadRepr);

				int NumObs = exec._IntermedRes.getNumObservers();
				for (int i = 0; i < NumObs; i++)
				{
					LoadedObserverSaver loadObs = exec._IntermedRes.getObserver(i)._ObserverSaver;
					loadObs._Type = MainForm._OptComponentsManager.getTypeForLoadedObsSaver(loadObs);
				}

				int NumOper = exec._AlgContext._AlgOper.getOperNum();
				for (int i = 0; i < NumOper; i++)
				{
					if (exec._AlgContext._AlgOper.getOperator(i) != null)
					{
						LoadedOperator loadOper = exec._AlgContext._AlgOper.getOperator(i)._Operator;
						loadOper._Type = MainForm._OptComponentsManager.getTypeForLoadedOperator(loadOper);
					}
				}
			}
		}
		public void RecreateAllControls()
		{
			// idemo najprije dodati probleme i algoritme
			foreach (OptPlanProblem prob in _OptPlan._listProb)
			{
				UserControl newCtrl = _WrapperFactory.CreateWrapperObjectUserControl(this, prob._refLoadedProblemObj, prob._X, prob._Y);

				if (newCtrl is VisualParametrizedProblemControl)
				{
					VisualParametrizedProblemControl newCtrl2 = newCtrl as VisualParametrizedProblemControl;
					newCtrl2.SetProblem(prob);
				}
				prob._VisCtrl = newCtrl;
				this.Controls.Add(newCtrl);
			}
			foreach (OptPlanAlgorithm alg in _OptPlan._listAlg)
			{
				VisualAlgorithmControl newCtrl = (VisualAlgorithmControl)_WrapperFactory.CreateWrapperObjectUserControl(this, alg._refLoadedAlgObj, alg._X, alg._Y);

				alg._VisCtrl = newCtrl;
				this.Controls.Add(newCtrl);
			}
			foreach (ExecutionUnit exec in _OptPlan._listExecUnits)
			{
				CreateVisControlsForExecUnit(exec, exec._X, exec._Y);
			}
		}
	}
}