namespace ESOPApp
{
	partial class frmViewVisualizationControls
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._listCompleteControls = new System.Windows.Forms.ListView();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this._listIndividualControls = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.SuspendLayout();
			// 
			// _listCompleteControls
			// 
			this._listCompleteControls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
			this._listCompleteControls.Location = new System.Drawing.Point(12, 34);
			this._listCompleteControls.Name = "_listCompleteControls";
			this._listCompleteControls.Size = new System.Drawing.Size(429, 152);
			this._listCompleteControls.TabIndex = 0;
			this._listCompleteControls.View = System.Windows.Forms.View.Details;
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(191, 407);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "OK";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(5, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(210, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Controls for complete collection visualization";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(5, 208);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(251, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Controls for individual collection member visualization";
			// 
			// _listIndividualControls
			// 
			this._listIndividualControls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
			this._listIndividualControls.Location = new System.Drawing.Point(12, 224);
			this._listIndividualControls.Name = "_listIndividualControls";
			this._listIndividualControls.Size = new System.Drawing.Size(429, 160);
			this._listIndividualControls.TabIndex = 4;
			this._listIndividualControls.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Control name";
			this.columnHeader1.Width = 211;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Type of collection members";
			this.columnHeader2.Width = 214;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Control name";
			this.columnHeader3.Width = 213;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Type of collection members";
			this.columnHeader4.Width = 211;
			// 
			// frmViewVisualizationControls
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(453, 447);
			this.Controls.Add(this._listIndividualControls);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this._listCompleteControls);
			this.Name = "frmViewVisualizationControls";
			this.Text = "Available visualization controls";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView _listCompleteControls;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListView _listIndividualControls;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;



	}
}