using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OptimizationManagementLib
{
	public class OptComponentsManager
	{
		public ArrayList _listProbComp = new ArrayList();
		public ArrayList _listAlgComp	= new ArrayList();
		public ArrayList _listOperComp = new ArrayList();
		public ArrayList _listReprComp = new ArrayList();

		public ArrayList _listAvailableObservers = new ArrayList();
		public ArrayList _listAvailableOptTerm = new ArrayList();
		public ArrayList _listAvailableDesignVar = new ArrayList();

		ILoadedComponent _CurrSelComp;

		public OptComponentsManager()
		{
		}

		public bool IsOptComponentSelected()
		{
			return (_CurrSelComp != null);
		}

		public Type getTypeForLoadedProblem(LoadedProblem inProb)
		{
			foreach (LoadedProblem prob in _listProbComp)
			{
				if (prob.ComponentName == inProb.ComponentName && prob.DLLName == inProb.DLLName && prob.Namespace == inProb.Namespace)
					return prob._Type;
			}
			return null;
		}
		public Type getTypeForLoadedAlgorithm(LoadedAlgorithm inAlg)
		{
			foreach (LoadedAlgorithm alg in _listAlgComp)
			{
				if (alg.ComponentName == inAlg.ComponentName && alg.DLLName == inAlg.DLLName && alg.Namespace == inAlg.Namespace)
					return alg._Type;
			}
			return null;
		}
		public Type getTypeForLoadedOptTerminator(LoadedOptTerminator inOptTerm)
		{
			foreach (LoadedOptTerminator optTerm in _listAvailableOptTerm)
			{
				if (optTerm.ComponentName == inOptTerm.ComponentName && optTerm.DLLName == inOptTerm.DLLName && optTerm.Namespace == inOptTerm.Namespace)
					return optTerm._Type;
			}
			return null;
		}
		public Type getTypeForLoadedObsSaver(LoadedObserverSaver inObsSaver)
		{
			foreach (LoadedObserverSaver obsSaver in _listAvailableObservers)
			{
				if (obsSaver.ComponentName == inObsSaver.ComponentName && obsSaver.DLLName == inObsSaver.DLLName && obsSaver.Namespace == inObsSaver.Namespace)
					return obsSaver._Type;
			}
			return null;
		}
		public Type getTypeForLoadedRepresentation(LoadedRepresentation inRepr)
		{
			foreach (LoadedRepresentation repr in _listReprComp)
			{
				if (repr.ComponentName == inRepr.ComponentName && repr.DLLName == inRepr.DLLName && repr.Namespace == inRepr.Namespace)
					return repr._Type;
			}
			return null;
		}
		public Type getTypeForLoadedOperator(LoadedOperator inOper)
		{
			foreach (LoadedOperator oper in _listOperComp)
			{
				if (oper.ComponentName == inOper.ComponentName && oper.DLLName == inOper.DLLName && oper.Namespace == inOper.Namespace)
					return oper._Type;
			}
			return null;
		}
		public void AddNewProbComp(LoadedProblem inProb)
		{
			_listProbComp.Add(inProb);
		}
		public void AddNewAlgComp(LoadedAlgorithm inAlg)
		{
			_listAlgComp.Add(inAlg);
		}
		public void AddNewOperComp(LoadedOperator inOper)
		{
			_listOperComp.Add(inOper);
		}
		public void AddNewReprComp(LoadedRepresentation inRepr)
		{
			_listReprComp.Add(inRepr);
		}
		public void AddNewObserver(LoadedObserverSaver inObs)
		{
			_listAvailableObservers.Add(inObs);
		}
		public void AddNewOptTerminator(LoadedOptTerminator inOptTerm)
		{
			_listAvailableOptTerm.Add(inOptTerm);
		}
		public void AddNewDesignVar(string inVarName)
		{
			// provjeri da li ve� postoji
			if( !_listAvailableDesignVar.Contains(inVarName) )
				_listAvailableDesignVar.Add(inVarName);
		}
		public bool GetAvailableRepresentationsForDesignVar(string inVarNam, ArrayList retList)
		{
			// najprije da provjerimo da li uop�e postoji
			if (!_listAvailableDesignVar.Contains(inVarNam))
				return false;

			foreach (LoadedRepresentation repr in _listReprComp)
			{
				if (repr.RealisedDesignVar == inVarNam)
					retList.Add(repr);
			}

			if( retList.Count > 0 )
				return true;
			else 
				return false;
		}

		public ILoadedComponent getCurrSelComp()
		{
			return _CurrSelComp;
		}

		public bool setCurrSelComp(String inCompName)
		{
			// pro�i �emo kroz sve liste i tzra�iti onu s tim imenom
			foreach (ILoadedComponent comp in _listProbComp) {
				if (comp.ComponentName == inCompName) {
					_CurrSelComp = comp;
					return true;
				}
			}
			foreach (ILoadedComponent comp in _listAlgComp) {
				if (comp.ComponentName == inCompName) {
					_CurrSelComp = comp;
					return true;
				}
			}
			foreach (ILoadedComponent comp in _listOperComp) {
				if (comp.ComponentName == inCompName) {
					_CurrSelComp = comp;
					return true;
				}
			}
			foreach (ILoadedComponent comp in _listReprComp) {
				if (comp.ComponentName == inCompName) {
					_CurrSelComp = comp;
					return true;
				}
			}
			// ako nismo na�li
			_CurrSelComp = null;
			return false;
		}
	}
}
