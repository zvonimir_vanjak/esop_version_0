using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using ESOP.Framework.DynamicSupport;

namespace OptimizationManagementLib
{
	[Serializable()]
	public class OptPlan
	{
		[NonSerialized()]		Form _frmOptPlan;
		[NonSerialized()]		OptPlanResults _Results;

		public ArrayList _listAlg = new ArrayList();
		public ArrayList _listProb = new ArrayList();
		public ArrayList _listExecUnits = new ArrayList();

		public OptPlan(OptPlanResults inResults)
		{
			_Results = inResults;
		}
		public static void SaveOptPlan(String fileName, OptPlan inOptPlanToSave)
		{
			Stream stream = File.Open(fileName, FileMode.Create);
			BinaryFormatter formatter = new BinaryFormatter();

			formatter.Serialize(stream, inOptPlanToSave);
			stream.Close();
		}
		public static OptPlan LoadOptPlan(String fileName, OptPlanResults  inResults)
		{
			Stream stream = File.Open(fileName, FileMode.Open);
			BinaryFormatter formatter = new BinaryFormatter();

			OptPlan retOptPlan = (OptPlan)formatter.Deserialize(stream);
			stream.Close();

			// trebamo postaviti referencu na rezultate za OptPlan
			retOptPlan._Results = inResults;

			// a isto tako i za sve u�itane ExecutionUnit-e
			foreach(ExecutionUnit execUnit in retOptPlan._listExecUnits)
			{
				execUnit.SetResults(inResults);
			}
			return retOptPlan;
		}
		public void SetForm(Form inForm)
		{
			_frmOptPlan = inForm;
		}
		public Form GetForm()
		{
			return _frmOptPlan;
		}
		public OptPlanResults GetResults()
		{
			return _Results;
		}

		public void SetLocationForControl(UserControl inCtrl, int inX, int inY)
		{
			foreach (OptPlanAlgorithm alg in _listAlg)
			{
				if (alg.isTheSameControl(inCtrl))
				{
					alg._X = inX;
					alg._Y = inY;
				}
			}
			foreach (OptPlanProblem prob in _listProb)
			{
				if (prob.isTheSameControl(inCtrl))
				{
					prob._X = inX;
					prob._Y = inY;
				}
			}
			foreach (ExecutionUnit exec in _listExecUnits)
			{
				if (exec.isTheSameControl(inCtrl))
				{
					exec._X = inX;
					exec._Y = inY;
				}
			}
		}
		public OptPlanProblem AddNewParamProblem(LoadedParameterProblem inComp, UserControl inCtrl)
		{
			OptPlanProblem newProb = new OptPlanProblem(inComp);

			newProb.AssignVisualControl(inCtrl);
			_listProb.Add(newProb);

			return newProb;
		}
		public OptPlanProblem AddNewCompleteProblem(LoadedCompleteProblem inComp, UserControl inCtrl)
		{
			OptPlanProblem newProb = new OptPlanProblem(inComp);

			newProb.AssignVisualControl(inCtrl);
			_listProb.Add(newProb);

			return newProb;
		}
		public void AddNewCompleteAlgorithm(LoadedCompleteAlgorithm inComp, UserControl inCtrl)
		{
			OptPlanAlgorithm newAlg = new OptPlanAlgorithm(inComp);

			newAlg.AssignVisualControl(inCtrl); 
			_listAlg.Add(newAlg);
		}
		public void AddNewTemplateAlgorithm(LoadedTemplateAlgorithm inComp, UserControl inCtrl)
		{
			OptPlanAlgorithm newAlg = new OptPlanAlgorithm(inComp);

			newAlg.AssignVisualControl(inCtrl);
			_listAlg.Add(newAlg);
		}
		public ExecutionUnit AddNewExecUnit(OptPlanAlgorithm inAlg, OptPlanProblem inProb, OptComponentsManager inManager)
		{
			ExecutionUnit												wrapExecUnit		= new ExecutionUnit(inAlg, inProb);

			OptPlanAlgorithmContext							wrapAlgContext	= new OptPlanAlgorithmContext(inAlg.getLoadedAlgObj());
			OptPlanSolObject										wrapSolObject		= new OptPlanSolObject();
			OptPlanIntermediateResultsObserver	wrapIntermedRes = new OptPlanIntermediateResultsObserver(inAlg.getLoadedAlgObj());
			OptPlanTerminator										wrapOptTerm			= new OptPlanTerminator();

			wrapExecUnit.setAlgContext(wrapAlgContext);
			wrapExecUnit.setSolObject(wrapSolObject);
			wrapExecUnit.setIntermedRes(wrapIntermedRes);
			wrapExecUnit.setOptTerminator(wrapOptTerm);

			wrapExecUnit.SetResults(_Results);

			#region Definiramo listu dostupnih reprezentacija i popunjavamo Sol object
			ArrayList reprList = new ArrayList();
			string		designVarName = inProb._refLoadedProblemObj._RequestedDesignVar;

			// treba pro�i kroz sve dostupne reprezentacije i vidjeti koja zadovoljava tra�eno su�elje
			if (inManager.GetAvailableRepresentationsForDesignVar(designVarName, reprList))
			{
				wrapSolObject.SetPossibleRepresentations(reprList);
				// u vis.kontroli �e se refresho baviti kod pridru�ivanja OptPLanSolObjecta samoj kontroli
			}
			else
				MessageBox.Show("No representation available for Design Variable" + designVarName);
			#endregion

			_listExecUnits.Add(wrapExecUnit);

			return wrapExecUnit;
		}

		public ILoadedComponent GetLoadedCompFromControl(UserControl inCtrl)
		{
			foreach (OptPlanAlgorithm alg in _listAlg)
			{
				if (alg.isTheSameControl(inCtrl))
					return alg.getLoadedAlgObj();
			}
			foreach (OptPlanProblem prob in _listProb)
			{
				if (prob.isTheSameControl(inCtrl))
					return prob.getLoadedProblemObj();
			}
			return null;
		}
		public OptPlanAlgorithm GetAlgWrapperFromControl(UserControl inCtrl)
		{
			foreach (OptPlanAlgorithm alg in _listAlg)
			{
				if (alg.isTheSameControl(inCtrl))
					return alg;
			}
			return null;
		}
		public OptPlanProblem GetProbWrapperFromControl(UserControl inCtrl)
		{
			foreach (OptPlanProblem prob in _listProb)
			{
				if (prob.isTheSameControl(inCtrl))
					return prob;
			}
			return null;
		}
	}
}
