using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
/*
using ESOP.Framework.StandardRepresentations;
*/
using ESOP.Framework.Base;
using ESOP.Framework.DynamicSupport;

namespace OptimizationManagementLib
{
	/// <summary>
	/// Klasa ExecutionUnit je sredi�nja klasa za dinami�ko izvo�enje optimizacije
	/// -- sadr�i reference na wrapper objekte potrebne za obavljanje optimizacije
	/// -- u funkciji Execute instancira potrebne objekte i pokre�e optimizaciju
	/// </summary>
	[Serializable()]
	public class ExecutionUnit
	{
		[NonSerialized()]	OptPlanResults _Results;

		private OptPlanAlgorithm	_Algorithm;
		private OptPlanProblem		_Problem;

		public OptPlanAlgorithmContext						_AlgContext;
		public OptPlanSolObject										_SolObject;
		public OptPlanIntermediateResultsObserver _IntermedRes;
		public OptPlanTerminator									_OptTerminator;

		public int _X, _Y;

		[NonSerialized()]		private UserControl _VisCtrl;

		public ExecutionUnit(OptPlanAlgorithm inAlg, OptPlanProblem inProb)
		{
			_Algorithm = inAlg;
			_Problem = inProb;
		}
		public void SetResults(OptPlanResults inRes)
		{
			_Results = inRes;
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public void DeleteAllParts()
		{
			_Results = null;
			_Algorithm = null;
			_Problem = null;

			_SolObject = null;
			_OptTerminator = null;

			_AlgContext._AlgOper = null;
			_AlgContext._AlgParam = null;
			_AlgContext = null;

			_IntermedRes._arlDefinedObservers = null;
			_IntermedRes = null;
		}
		public bool isTheSameControl(UserControl inCtrl)
		{
			return _VisCtrl.Equals(inCtrl);
		}
		public OptPlanAlgorithm getAlgWrapper()
		{
			return _Algorithm;
		}
		public OptPlanProblem getProbWrapper()
		{
			return _Problem;
		}
		public void setAlgContext(OptPlanAlgorithmContext inctx)
		{
			_AlgContext = inctx;
		}
		public void setSolObject(OptPlanSolObject inSol)
		{
			_SolObject = inSol;
		}
		public void setIntermedRes(OptPlanIntermediateResultsObserver inIntermedRes)
		{
			_IntermedRes = inIntermedRes;
		}
		public void setOptTerminator(OptPlanTerminator intOptTerm)
		{
			_OptTerminator = intOptTerm;
		}
		public UserControl getExecUnitControl()
		{
			return _VisCtrl;
		}
		public Point getUserControlLocation()
		{
			if (_VisCtrl == null)
				return new Point(_X, _Y);
			else
				return _VisCtrl.Location;
		}
		public Size getUserControlSize()
		{
			if (_VisCtrl == null)
				return new Size(100, 100);
			else
				return _VisCtrl.Size;
		}

		/// <summary>
		/// Sumarno
		/// </summary>
		/// <remarks>Executes</remarks>
		public double Execute()
		{
			LoadedAlgorithm alg		= _Algorithm.getLoadedAlgObj();
			LoadedProblem		prob	= _Problem.getLoadedProblemObj();

			#region Kreiramo objekt problema
			IESOPProblem		problem = (IESOPProblem) Activator.CreateInstance(prob._Type);

			// TODO - dodati razli�ite na�ine kreiranja za ParametrizedProblem, FileInitProblem, ...
			if (problem is IESOPParametrizedProblem)
			{
				IESOPParametrizedProblem parProb = problem as IESOPParametrizedProblem;

				for (int i = 0; i < _Problem._ProblemInit._NumParam; i++)
					parProb.setParamValue(i, _Problem._ProblemInit._arrVal[i]);
			}
			#endregion

			#region Sada treba kreirati konkretan objekt reprezentacije za Design Variable
			LoadedRepresentation selRepr = _SolObject.getSelLoadedRepr();
			IESOPDesignVariable designVar = (IESOPDesignVariable)Activator.CreateInstance(selRepr._Type);

			// i sada treba prenijeti parametre iz RepresentationInitializera u konkretan objekt
			// moramo vidjeti da li reprezentacija ima neke konstruktorske parametre
			if (selRepr is LoadedConstructedRepresentation)
			{
				IESOPConstructedDesignVariable ctorVar = designVar as IESOPConstructedDesignVariable;

				for (int i = 0; i < _SolObject._ReprInit._NumConstructorParam; i++)
					ctorVar.setConstructorParamValue(i, _SolObject._ReprInit._arrConstructorParVal[i]);
			}

			for (int i = 0; i < _SolObject._ReprInit._NumParam; i++)
				designVar.setParamValue(i, _SolObject._ReprInit._arrVal[i]);
			#endregion

			if( alg is LoadedCompleteAlgorithm )
			{
				#region Complete algorithm
				IESOPCompleteAlgorithm algor = (IESOPCompleteAlgorithm)Activator.CreateInstance(alg._Type);

				algor.setESOPDesignVariable(designVar);
				algor.setESOPProblemInstance(problem);

				for (int i = 0; i < _AlgContext._AlgParam._NumParam; i++)
					algor.setParamValue(i, _AlgContext._AlgParam._arrVal[i]);

				algor.ESOP_Initialize();
				OptimizationResult outRes = new OptimizationResult();

				// cast na IterativeAlgorithm
				IterativeAlgorithm algObject = algor as IterativeAlgorithm;

				LoadedOptTerminator optTerm = _OptTerminator._OptTerminator;
				IOptimizationTerminator newOptTerm = (IOptimizationTerminator)Activator.CreateInstance(optTerm._Type);

				newOptTerm.SetParameter(_OptTerminator._SetParamValue);
				algObject.setOptTerminator(newOptTerm);
				
				#region Intermediate results
				// kreiraj IntermediateValueObserver
				// kreiraj novu kolekciju za me�urezultate
				// postavi odabrani Timer objekt
				// postavi odabrani Saver objekt
				// dodaj observer u algoritam
				for (int i = 0; i < _IntermedRes.getNumObservers(); i++)
				{
					LoadedObserverSaver obsSaver = _IntermedRes.getObserver(i)._ObserverSaver;

					ObservedValuesCollection newColl = outRes.AddNewObsCollection(obsSaver.ObserverDesc);

					IntermediateValueObserver newObs = new IntermediateValueObserver();
					IIntermediateTimer newTimer;

					if (_IntermedRes.getObserver(i)._SelTimerType == "IntermedTimerOnEveryIterNum")
						newTimer = new IntermedTimerOnEveryIterNum();
					else //if (_IntermedRes.getObserver(i)._SelTimerType == "IntermedTimerOnTimeInterval")
						newTimer = new IntermedTimerOnTimeInterval();

					newTimer.SetParameter(_IntermedRes.getObserver(i)._TimerParameter);

					IObservedValueSaver newSaver = (IObservedValueSaver)Activator.CreateInstance(obsSaver._Type);
					//						CurrObjectiveValueSaver newSaver = new CurrObjectiveValueSaver();

					IIntermediateValueProvider provider = algObject as IIntermediateValueProvider;

					newSaver.setCollection(newColl);
					newSaver.setProvider(provider);

					newObs.setTimer(newTimer);
					newObs.setSaver(newSaver);

					algObject.getMasterObserver().AddObserver(newObs);
				}
				#endregion

				algor.ESOP_Run(outRes);

				_Results.AddResult(outRes);
				#endregion
			}
			else
			{
				#region Template algorithm
				IESOPTemplateAlgorithm algor = (IESOPTemplateAlgorithm)Activator.CreateInstance(alg._Type);

				algor.setESOPDesignVariable(designVar);
				algor.setESOPProblemInstance(problem);

				for (int i = 0; i < _AlgContext._AlgParam._NumParam; i++)
					algor.setParamValue(i, _AlgContext._AlgParam._arrVal[i]);

				// ovdje treba postaviti i reference na operatore
				for (int i = 0; i < _AlgContext._AlgOper.getOperNum(); i++)
				{
					LoadedOperator currOper = _AlgContext._AlgOper.getOperator(i)._Operator;

					IESOPOperator createdOper = (IESOPOperator)Activator.CreateInstance(currOper._Type);
					algor.setESOPOperator(i, createdOper);

					if (createdOper is IESOPParameterOperator)
					{
						// postaviti parametre operatora
						IESOPParameterOperator oper = createdOper as IESOPParameterOperator;

						for (int j = 0; j < _AlgContext._AlgOper.getOperator(i)._OperInit._NumParam; j++)
							oper.setParamValue(j, _AlgContext._AlgOper.getOperator(i)._OperInit._arrVal[j]);
					}
				}

				algor.ESOP_Initialize();
				OptimizationResult  outRes = new OptimizationResult();

				// cast na IterativeAlgorithm
				IterativeAlgorithm algObject = algor as IterativeAlgorithm;

				LoadedOptTerminator optTerm = _OptTerminator._OptTerminator;
				IOptimizationTerminator newOptTerm = (IOptimizationTerminator)Activator.CreateInstance(optTerm._Type);

				newOptTerm.SetParameter(_OptTerminator._SetParamValue);
				algObject.setOptTerminator(newOptTerm);

				for (int i = 0; i < _IntermedRes.getNumObservers(); i++)
				{
					LoadedObserverSaver obsSaver = _IntermedRes.getObserver(i)._ObserverSaver;

					ObservedValuesCollection newColl = outRes.AddNewObsCollection(obsSaver.ObserverDesc);

					IntermediateValueObserver newObs = new IntermediateValueObserver();

					IIntermediateTimer newTimer;
					if (_IntermedRes.getObserver(i)._SelTimerType == "IntermedTimerOnEveryIterNum")
						newTimer = new IntermedTimerOnEveryIterNum();
					else //if (_IntermedRes.getObserver(i)._SelTimerType == "IntermedTimerOnTimeInterval")
						newTimer = new IntermedTimerOnTimeInterval();

					newTimer.SetParameter(_IntermedRes.getObserver(i)._TimerParameter);

					IObservedValueSaver newSaver = (IObservedValueSaver)Activator.CreateInstance(obsSaver._Type);

					IIntermediateValueProvider provider = algObject as IIntermediateValueProvider;

					newSaver.setCollection(newColl);
					newSaver.setProvider(provider);
					newObs.setTimer(newTimer);
					newObs.setSaver(newSaver);

					algObject.getMasterObserver().AddObserver(newObs);
				}

				algor.ESOP_Run(outRes);

				_Results.AddResult(outRes);
				#endregion
			}

			return 0;
		}
	}

}
