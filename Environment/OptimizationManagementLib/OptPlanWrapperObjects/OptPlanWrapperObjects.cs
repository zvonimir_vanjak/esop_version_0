using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using ESOP.Framework.DynamicSupport;

namespace OptimizationManagementLib
{
	// TODO - napraviti da svi imaju konstruktor sa ILoadedcomponent i UserControl
	[Serializable()]	public class OptPlanAlgorithm
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public LoadedAlgorithm _refLoadedAlgObj;

		public OptPlanAlgorithm(ILoadedComponent inAlg)
		{
			_refLoadedAlgObj = (LoadedAlgorithm) inAlg;
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public bool isTheSameControl(UserControl inCtrl)
		{
			return _VisCtrl.Equals(inCtrl);
		}
		public LoadedAlgorithm getLoadedAlgObj()
		{
			return _refLoadedAlgObj;
		}
		public UserControl getAlgUserControl()
		{
			return _VisCtrl;
		}
		public Point getUserControlLocation()
		{
			if (_VisCtrl == null)
				return new Point(_X, _Y);
			else
			return _VisCtrl.Location;
		}
		public Size getUserControlSize()
		{
			if (_VisCtrl == null)
				return new Size(100, 100);
			else
				return _VisCtrl.Size;
		}
	}

	[Serializable()]	public class OptPlanProblem
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public LoadedProblem _refLoadedProblemObj;
		public ProblemParametersInitializer _ProblemInit;

		public OptPlanProblem(ILoadedComponent inProb)
		{
			_refLoadedProblemObj = (LoadedProblem) inProb;
			_ProblemInit = new ProblemParametersInitializer();

			IESOPParametrizedProblem var = Activator.CreateInstance(_refLoadedProblemObj._Type) as IESOPParametrizedProblem;

			if (var != null)
			{
				_ProblemInit.SetParamNum(var.getParamNum());
				for (int i = 0; i < var.getParamNum(); i++)
				{
					_ProblemInit._arrName[i] = var.getParamName(i);
					_ProblemInit._arrVal[i] = var.getParamValue(i);
				}
			}
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public bool isTheSameControl(UserControl inCtrl)
		{
			return _VisCtrl.Equals(inCtrl);
		}
		public LoadedProblem getLoadedProblemObj()
		{
			return _refLoadedProblemObj;
		}
		public UserControl getProbUserControl()
		{
			return _VisCtrl;
		}
		public Point getUserControlLocation()
		{
			if (_VisCtrl == null)
				return new Point(_X, _Y);
			else
				return _VisCtrl.Location;
		}
		public Size getUserControlSize()
		{
			if (_VisCtrl == null)
				return new Size(100, 100);
			else
				return _VisCtrl.Size;
		}
	}
	[Serializable()]	public class ProblemParametersInitializer
	{
		public int _NumParam;
		public string[] _arrName;
		public double[] _arrVal;

		public void SetParamNum(int inNumParam)
		{
			_NumParam = inNumParam;

			_arrName = new string[_NumParam];
			_arrVal = new double[_NumParam];
		}
	}

	[Serializable()]	public class OptPlanSolObject
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		[Serializable()]
		public class RepresentationInitializer
		{
			public int _NumParam;
			public string[] _arrName;
			public double[] _arrVal;

			public int _NumConstructorParam;
			public string[] _arrConstructorParName;
			public double[] _arrConstructorParVal;

			public void SetParamNum(int inNumParam)
			{
				_NumParam = inNumParam;

				_arrName = new string[_NumParam];
				_arrVal = new double[_NumParam];
			}

			public void SetConstructorParamNum(int inNumParam)
			{
				_NumConstructorParam = inNumParam;

				_arrConstructorParName = new string[_NumParam];
				_arrConstructorParVal = new double[_NumParam];
			}
		}

		public	int _X, _Y;
		
		private ArrayList _listPossibleRepr;
		public	LoadedRepresentation _selRepr;

		public	RepresentationInitializer _ReprInit;

		public OptPlanSolObject()
		{
			_ReprInit = new RepresentationInitializer();
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public void SetPossibleRepresentations(ArrayList inList)
		{
			_listPossibleRepr = inList;
		}
		public ArrayList GetPossibleRepresentations()
		{
			return _listPossibleRepr;
		}
		public bool isTheSameControl(UserControl inCtrl)
		{
			return _VisCtrl.Equals(inCtrl);
		}
		public UserControl getSolObjectUserControl()
		{
			return _VisCtrl;
		}
		/// <summary>
		/// Nakon postavljanja odabrane reprezentacije, u ovoj klasi se nalaze svi potrebni podaci za kreiranje konkretnog objekta odabrane reprezentacije
		/// </summary>
		/// <param name="inRepr"></param>
		public void SetSelRepr(LoadedRepresentation inRepr)
		{
			// najprije provjeravamo da li je do�lo do promjene odabrane reprezentacije
			if (_selRepr != inRepr)
			{
				_selRepr = inRepr;

				// treba ponovno reinicijalizirati Initializer
				IESOPDesignVariable var = (IESOPDesignVariable)Activator.CreateInstance(_selRepr._Type);

				_ReprInit.SetParamNum(var.getParamNum());
				for (int i = 0; i < var.getParamNum(); i++)
				{
					_ReprInit._arrName[i] = var.getParamName(i);
					_ReprInit._arrVal[i] = var.getParamValue(i);
				}

				// ako se radi o konstruktorskoj design var., onda treba pokupiti i te parametre
				if (var is IESOPConstructedDesignVariable)
				{
					IESOPConstructedDesignVariable ctorVar = var as IESOPConstructedDesignVariable;

					_ReprInit.SetConstructorParamNum(ctorVar.getConstructorParamNum());
					for (int i = 0; i < ctorVar.getConstructorParamNum(); i++)
					{
						_ReprInit._arrConstructorParName[i] = ctorVar.getConstructorParamName(i);
						_ReprInit._arrConstructorParVal[i] = ctorVar.getConstructorParamValue(i);
					}
				}
			}
 		}
		public void ReCheckSelRepr()
		{
			if (_selRepr is LoadedConstructedRepresentation)
			{
				IESOPConstructedDesignVariable ctorVar = (IESOPConstructedDesignVariable)Activator.CreateInstance(getSelLoadedRepr()._Type);

				// najprije postavljamo konstruktorske parametre
				for (int i = 0; i < ctorVar.getConstructorParamNum(); i++)
				{
					ctorVar.setConstructorParamValue(i, _ReprInit._arrConstructorParVal[i]);
				}

				_ReprInit.SetParamNum(ctorVar.getParamNum());
				for (int i = 0; i < ctorVar.getParamNum(); i++)
				{
					_ReprInit._arrName[i] = ctorVar.getParamName(i);
					_ReprInit._arrVal[i] = ctorVar.getParamValue(i);
				}
			}
		}
		public LoadedRepresentation getSelLoadedRepr()
		{
			return _selRepr;
		}
	}


	[Serializable()]	public class OptPlanAlgorithmContext
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public int _Width, _Height;

		private LoadedAlgorithm _refLoadedAlgObj;
		public AlgorithmParametersInitializer _AlgParam;
		public AlgorithmOperatorsInitializer _AlgOper;

		public OptPlanAlgorithmContext(LoadedAlgorithm inAlg)
		{
			_AlgParam = new AlgorithmParametersInitializer();
			_AlgOper = new AlgorithmOperatorsInitializer();
			_refLoadedAlgObj = inAlg;

			IESOPCompleteAlgorithm var = (IESOPCompleteAlgorithm)Activator.CreateInstance(_refLoadedAlgObj._Type);

			_AlgParam.SetParamNum(var.getParamNum());
			for (int i = 0; i < var.getParamNum(); i++)
			{
				_AlgParam._arrName[i] = var.getParamName(i);
				_AlgParam._arrVal[i] = var.getParamValue(i);
			}

			// idemo provjeriti da li se mo�da radi o Template algoritmu
			if (_refLoadedAlgObj is LoadedTemplateAlgorithm)
			{
				IESOPTemplateAlgorithm algTemp = (IESOPTemplateAlgorithm)Activator.CreateInstance(_refLoadedAlgObj._Type);

				_AlgOper.setOperNum(algTemp.getOperatorNum());
				for (int i = 0; i < algTemp.getOperatorNum(); i++)
				{
					_AlgOper.setOperType(i, algTemp.getOperatorType(i));
					_AlgOper.setOperID(i, algTemp.getOperatorID(i));
					// dio vezan uz LoadedOperator �e se postaviti pozivom funkcije SetOperatorWrapperObject nakon �to korisnik odabere operator iz liste
				}
			}
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}

		public bool isTheSameControl(UserControl inCtrl)
		{
			return _VisCtrl.Equals(inCtrl);
		}
		public UserControl getAlgContextUserControl()
		{
			return _VisCtrl;
		}
		public void SetOperatorWrapperObject( int OperIndex, OptPlanOperator inOper)
		{
//			_OperInit._arrLoadedOper[OperIndex] = inOper;
//			_listOperControls.Add(inOper);
			_AlgOper.setOperator(OperIndex, inOper);
		}
	}
	[Serializable()]	public class AlgorithmParametersInitializer
	{
		public int _NumParam;
		public string[] _arrName;
		public double[] _arrVal;

		public void SetParamNum(int inNumParam)
		{
			_NumParam = inNumParam;

			_arrName = new string[_NumParam];
			_arrVal = new double[_NumParam];
		}
	}
	[Serializable()]	public class AlgorithmOperatorsInitializer
	{
		private int _NumOper;
		private string[] _arrOperType;
		private string[] _arrOperID;
		private OptPlanOperator[] _arlOptOper;
		//		public LoadedOperator[] _arrLoadedOper;

		public int getOperNum()
		{
			return _NumOper;
		}
		public void setOperNum(int inNumOper)
		{
			_NumOper = inNumOper;

			_arrOperType = new string[_NumOper];
			_arrOperID	 = new string[_NumOper];
			_arlOptOper  = new OptPlanOperator[_NumOper];
		}
		public void setOperType(int Ind, string inOperType)
		{
			_arrOperType[Ind] = inOperType;
		}
		public string getOperType(int Ind)
		{
			return _arrOperType[Ind];
		}
		public void setOperID(int Ind, string inOperID)
		{
			_arrOperID[Ind] = inOperID;
		}
		public string getOperID(int Ind)
		{
			return _arrOperID[Ind];
		}
		public void setOperator(int Ind, OptPlanOperator inOptOper)
		{
			_arlOptOper[Ind] = inOptOper;
		}
		public OptPlanOperator getOperator(int Ind)
		{
			return _arlOptOper[Ind];
		}
	}

	[Serializable()]	public class OptPlanOperator
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public  LoadedOperator	_Operator;
		public	OperatorParametersInitializer _OperInit;

		public OptPlanOperator(LoadedOperator inOper)
		{
			_Operator = inOper;
			_OperInit = new OperatorParametersInitializer();

			IESOPParameterOperator var = (IESOPOperator)Activator.CreateInstance(_Operator._Type) as IESOPParameterOperator;

			if (var != null)
			{
				_OperInit.SetParamNum(var.getParamNum());
				for (int i = 0; i < var.getParamNum(); i++)
				{
					_OperInit._arrName[i] = var.getParamName(i);
					_OperInit._arrVal[i] = var.getParamValue(i);
				}
			}
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
	
		public bool isTheSameControl(UserControl inCtrl)
		{
			return _VisCtrl.Equals(inCtrl);
		}
		/// <summary>
		/// ptdo
		/// </summary>
		/// <returns></returns>
		public UserControl getOperatorUserControl()
		{
			return _VisCtrl;
		}
	}
	[Serializable()]	public class OperatorParametersInitializer
	{
		public int _NumParam;
		public string[] _arrName;
		public double[] _arrVal;

		public void SetParamNum(int inNumParam)
		{
			_NumParam = inNumParam;

			_arrName = new string[_NumParam];
			_arrVal = new double[_NumParam];
		}
	}

	[Serializable()]	public class OptPlanObserver
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public LoadedObserverSaver _ObserverSaver;
		public string _SelTimerType;
		public int _TimerParameter;

		public OptPlanObserver(LoadedObserverSaver inObs)
		{
			_ObserverSaver = inObs;
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public void MoveControl(int DX, int DY)
		{
			_VisCtrl.Location = new Point(_VisCtrl.Location.X + DX, _VisCtrl.Location.Y + DY);
		}
	}

	[Serializable()]	public class OptPlanIntermediateResultsObserver
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public ArrayList _arlDefinedObservers = new ArrayList();

		public LoadedAlgorithm _SelAlg;

		public OptPlanIntermediateResultsObserver(LoadedAlgorithm inAlg)
		{
			_SelAlg = inAlg;
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public int getNumObservers()
		{
			return _arlDefinedObservers.Count;
		}
		public void addNewObserver(OptPlanObserver inObs)
		{
			_arlDefinedObservers.Add(inObs);
		}
		public OptPlanObserver getObserver(int Ind)
		{
			return (OptPlanObserver)_arlDefinedObservers[Ind];
		}
		public void MoveControl(int DX, int DY)
		{
			_VisCtrl.Location = new Point(_VisCtrl.Location.X + DX, _VisCtrl.Location.Y + DY);

			foreach (OptPlanObserver obs in _arlDefinedObservers)
			{
				obs.MoveControl(DX, DY);
			}
		}
	}

	[Serializable()]	public class OptPlanTerminator
	{
		[NonSerialized()]		public UserControl _VisCtrl;

		public int _X, _Y;
		public LoadedOptTerminator _OptTerminator;
		public int _SetParamValue;

		public OptPlanTerminator()
		{
			_SetParamValue = 10;
		}
		public void AssignVisualControl(UserControl inCtrl)
		{
			_VisCtrl = inCtrl;
			_X = _VisCtrl.Location.X;
			_Y = _VisCtrl.Location.Y;
		}
		public void setTerminator(LoadedOptTerminator inOptTerm)
		{
			_OptTerminator = inOptTerm;
		}
	}
}
