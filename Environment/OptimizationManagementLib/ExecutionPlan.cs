using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OptimizationManagementLib
{
	public class ExecutionPlan
	{
		private ArrayList _listExecUnit = new ArrayList();

		public void AddExecUnit(ExecutionUnit inExecUnit)
		{
			_listExecUnit.Add(inExecUnit);
		}

		public int GetNum()
		{
			return _listExecUnit.Count;
		}
		public ExecutionUnit GetExecUnit(int Index)
		{
			return (ExecutionUnit) _listExecUnit[Index];
		}
	}
}
