using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using System.Reflection;

using ESOP.Framework.Base;
using ESOP.Framework.DynamicSupport;

namespace OptimizationManagementLib
{
	public class OptComponentsLoader
	{
		public static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
		{
			if (typeObj.ToString() == criteriaObj.ToString())
				return true;
			else
				return false;
		}
		public static bool DoesTypeSupportInterface(Type inType, string inInterfaceName)
		{
			TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

			Type[] myInterfaces = inType.FindInterfaces(myFilter, inInterfaceName);
			if (myInterfaces.Length > 0)
				return true;
			else
				return false;
		}
		public static bool DoesTypeInheritsFrom(Type inType, string inBaseTypeName)
		{
			Type BaseType = inType.BaseType;

			Type pp = inType;
			Type p	= inType.BaseType;
			// treba naci zadnji BaseType prije System.Object-a
			while (p != null && p.Name != "Object")
			{
				if (p.Name == inBaseTypeName)
					return true;

				pp = p;
				p = p.BaseType;
			}
			return false;
		}

		/**************************************************************************************************/
		// za interface koji su me�usobno povezani naslje�ivanje, obavezno prvo provjeriti za onaj izvedeni, pa tek onda za osnovni
		// (zato �to �e se dopustiti samo jednome da bude u listi)
		public bool LoadESOPProblems(String filePath, OptComponentsManager inOptCompManager)
		{
			Assembly	asmLoadedAssembly	= Assembly.LoadFrom(filePath);
			if (asmLoadedAssembly == null) 	
				return false;

			Type[] mytypes = asmLoadedAssembly.GetExportedTypes();
			foreach (Type t in mytypes)
			{
				TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

				Type[] myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPCompleteProblem");
				if (myInterfaces.Length > 0)
				{
					if (!t.IsAbstract)
					{
						Type[] mySecInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPParametrizedProblem");
						if (mySecInterfaces.Length == 0)
						{
							IESOPProblem problemObj = (IESOPProblem)Activator.CreateInstance(t);

							LoadedCompleteProblem prob = new LoadedCompleteProblem(t.Namespace, filePath, problemObj.getESOPComponentName(), t, problemObj.getESOPComponentDesc(), problemObj.getRequestedDesignVar());
							inOptCompManager.AddNewProbComp(prob);
						}
					}
				}
				myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPParametrizedProblem");
				if (myInterfaces.Length > 0)
				{
					IESOPParametrizedProblem problemObj = (IESOPParametrizedProblem)Activator.CreateInstance(t);

					LoadedParameterProblem prob = new LoadedParameterProblem(t.Namespace, filePath, problemObj.getESOPComponentName(), t, problemObj.getESOPComponentDesc(), problemObj.getRequestedDesignVar());
					inOptCompManager.AddNewProbComp(prob);
				}

				myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPFileInitProblem");
				if (myInterfaces.Length > 0)
				{
					IESOPProblem problemObj = (IESOPProblem)Activator.CreateInstance(t);

					LoadedFileInitProblem prob = new LoadedFileInitProblem(t.Namespace, filePath, problemObj.getESOPComponentName(), t, problemObj.getESOPComponentDesc(), problemObj.getRequestedDesignVar());
					inOptCompManager.AddNewProbComp(prob);
				}
				myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPRuntimeInitProblem");
				if (myInterfaces.Length > 0)
				{
					IESOPProblem problemObj = (IESOPProblem)Activator.CreateInstance(t);

					LoadedRuntimeInitProblem prob = new LoadedRuntimeInitProblem(t.Namespace, filePath, problemObj.getESOPComponentName(), t, problemObj.getESOPComponentDesc(), problemObj.getRequestedDesignVar());
					inOptCompManager.AddNewProbComp(prob);
				}
			}

			return true;
		}
		public bool LoadESOPAlgorithms(String filePath, OptComponentsManager inManager)
		{
			Assembly asmLoadedAssembly = Assembly.LoadFrom(filePath);
			if (asmLoadedAssembly == null)
				return false;

			Type[] mytypes = asmLoadedAssembly.GetExportedTypes();
			foreach (Type t in mytypes)
			{
				TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

				Type[] myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPCompleteAlgorithm");
				if (myInterfaces.Length > 0)
				{
					// svaki Template algoritam je istovremeno i Complete algoritma, a u tom slu�aju ga stavljamo samo kao Template algoritam
					Type[] mySecInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPTemplateAlgorithm");
					if (mySecInterfaces.Length == 0)
					{
						IESOPAlgorithm algor = (IESOPAlgorithm)Activator.CreateInstance(t);

						LoadedCompleteAlgorithm alg = new LoadedCompleteAlgorithm(t.Namespace, filePath, algor.getESOPComponentName(), t, algor.getESOPComponentDesc());
						inManager.AddNewAlgComp(alg);
					}
				}
				myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPTemplateAlgorithm");
				if (myInterfaces.Length > 0)
				{
					IESOPAlgorithm algor = (IESOPAlgorithm)Activator.CreateInstance(t);

					LoadedTemplateAlgorithm alg = new LoadedTemplateAlgorithm(t.Namespace, filePath, algor.getESOPComponentName(), t, algor.getESOPComponentDesc());
					inManager.AddNewAlgComp(alg);
				}
			}

			return true;
		}
		public bool LoadESOPOperators(String filePath, OptComponentsManager inManager)
		{
			Assembly asmLoadedAssembly = Assembly.LoadFrom(filePath);
			if (asmLoadedAssembly == null)
				return false;

			Type[] mytypes = asmLoadedAssembly.GetExportedTypes();
			foreach (Type t in mytypes)
			{
				TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

				Type[] myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPOperator");
				if (myInterfaces.Length > 0)
				{
					Type[] mySecInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPParameterOperator");
					if (mySecInterfaces.Length == 0)
					{
						// dodajemo ga samo ako nije i ParameterOperator
						IESOPOperator operObj = (IESOPOperator)Activator.CreateInstance(t);

						LoadedOperator oper = new LoadedOperator(t.Namespace, filePath, operObj.getESOPComponentName(), t, operObj.getESOPComponentDesc());
						inManager.AddNewOperComp(oper);
					}
				}
				myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPParameterOperator");
				if (myInterfaces.Length > 0)
				{
					IESOPOperator operObj = (IESOPOperator)Activator.CreateInstance(t);

					LoadedOperator oper = new LoadedOperator(t.Namespace, filePath, operObj.getESOPComponentName(), t, operObj.getESOPComponentDesc());
					inManager.AddNewOperComp(oper);
				}
			}

			return true;
		}
		public bool LoadESOPDesignVariables(String filePath, OptComponentsManager inManager)
		{
			Assembly asmLoadedAssembly = Assembly.LoadFrom(filePath);
			if (asmLoadedAssembly == null)
				return false;

			Type[] mytypes = asmLoadedAssembly.GetExportedTypes();
			foreach (Type t in mytypes)
			{
				TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

				Type[] myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPDesignVariable");
				if (myInterfaces.Length > 0)
				{
					// treba na�i ime dizajn varijable
					// to �emo napraviti tako da �emo u listi implementiranih interface-a na�i IDesignVar i uzeti onaj neposredno prije
					Type[] arr = t.GetInterfaces();
					string VarName = "";
					for (int i = 0; i < arr.Length; i++)
					{
						if (arr[i].Name == "IDesignVariable")
						{
							VarName = arr[i - 1].Name;
							break;
						}
					}
					// ukoliko je sljede�i u nizu naslije�enih interface-a ICompositeVariable, zna�i da je korisnik sam definirao svoju
					// kompozitnu reprezentaciju i u tom slu�aju, uzimamo ba� ime tipa, a ne ime interface-a
					// (vidi primjer sa SpeedReducerDesign problemom)
					if (VarName == "ICompositeVariable")
						VarName = t.Name;

					inManager.AddNewDesignVar(VarName);

					// a sada da vidimo o kakvoj se reprezentaciji radi
					Type[] mySecInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.DynamicSupport.IESOPConstructedDesignVariable");
					if (mySecInterfaces.Length > 0)
					{
						LoadedConstructedRepresentation repr = new LoadedConstructedRepresentation(t.Namespace, filePath, t.Name, t, VarName);
						inManager.AddNewReprComp(repr);
					}
					else
					{
						LoadedRepresentation repr = new LoadedRepresentation(t.Namespace, filePath, t.Name, t, VarName);
						inManager.AddNewReprComp(repr);
					}
				}
			}

			return true;
		}
		public bool LoadESOPHelperObjects(String filePath, OptComponentsManager inManager)
		{
			Assembly	a	= Assembly.LoadFrom(filePath);
			if (a == null) 
				return false;

			Type[]		mytypes = a.GetExportedTypes();

			foreach (Type t in mytypes)
			{
				TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);

				Type[] myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.Base.IObservedValueSaver");
				if (myInterfaces.Length > 0)
				{
					IObservedValueSaver obsObject = (IObservedValueSaver)Activator.CreateInstance(t);
					LoadedObserverSaver obs = new LoadedObserverSaver(t.Namespace, filePath, t.Name, t, obsObject.getRequestedValueProviderClassName(), obsObject.getObserverDesc());

					inManager.AddNewObserver(obs);
				}
				myInterfaces = t.FindInterfaces(myFilter, "ESOP.Framework.Base.IOptimizationTerminator");
				if (myInterfaces.Length > 0)
				{
					IOptimizationTerminator termObj = (IOptimizationTerminator)Activator.CreateInstance(t);
					LoadedOptTerminator term = new LoadedOptTerminator(t.Namespace, filePath, t.Name, t, termObj.GetRequestedValueProviderClassName(), termObj.GetTerminatorDesc());

					inManager.AddNewOptTerminator(term);
				}
			}
			return true;
		}

		public bool LoadDLLOptComponentes(String filePath, OptComponentsManager inManager)
		{
			bool ret = false;

			LoadESOPProblems(filePath, inManager);
			LoadESOPAlgorithms(filePath, inManager);
			LoadESOPOperators(filePath, inManager);
			LoadESOPDesignVariables(filePath, inManager);
			LoadESOPHelperObjects(filePath, inManager);

			return ret;
		}
	}
}
