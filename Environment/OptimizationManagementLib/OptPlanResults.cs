using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using ESOP.Framework.Base;

namespace OptimizationManagementLib
{
	public class OptPlanResults
	{
		private ArrayList _arrResults;

		public OptPlanResults()
		{
			_arrResults = new ArrayList();
		}
		public void AddResult(OptimizationResult inRes)
		{
			_arrResults.Add(inRes);
		}
		public OptimizationResult GetResult(int Ind)
		{
			return _arrResults[Ind] as OptimizationResult;
		}
		public int GetResultNum()
		{
			return _arrResults.Count;
		}
	}
}
