using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OptimizationManagementLib
{
	public class OptPlanManager
	{
		ArrayList _listOpetPlans = new ArrayList();

		public OptPlan AddNewOptPlan(OptPlanResults inRes)
		{
			OptPlan newOptPlan = new OptPlan(inRes);

			_listOpetPlans.Add(newOptPlan);
			return newOptPlan;
		}
		public void AddOptPlan(OptPlan inPLan)
		{
			_listOpetPlans.Add(inPLan);
		}
		public OptPlan GetActiveOptPlan()
		{
			foreach (OptPlan plan in _listOpetPlans)
			{
				if (plan.GetForm().ContainsFocus == true)
					return plan;
			}
			return _listOpetPlans[0] as OptPlan;
//			return null;
		}
	}
}
