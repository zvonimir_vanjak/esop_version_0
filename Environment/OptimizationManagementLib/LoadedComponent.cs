using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OptimizationManagementLib
{
	[Serializable()]
	public class ILoadedComponent
	{
		public string Namespace;
		public string DLLName;
		public string ComponentName;

		[NonSerialized()]		public Type _Type;

		public ILoadedComponent()
		{
		}
		// podaci potrebni za rekreiranje dinamičkog objekta za optimizaciju
		public ILoadedComponent(string inNamespace, string inDLLName, string inComponentName, Type inType)
		{
			Namespace = inNamespace;
			DLLName = inDLLName;
			ComponentName = inComponentName;

			_Type = inType;
		}
	};

	[Serializable()]
	public class LoadedAlgorithm : ILoadedComponent
	{
		public string _AlgDesc;
		public LoadedAlgorithm(string inNamespace, string inDLLName, string inComponentName, Type inType, string inAlgDesc)
			: base(inNamespace, inDLLName, inComponentName, inType)
		{
			_AlgDesc = inAlgDesc;
		}
	}
	[Serializable()]
	public class LoadedCompleteAlgorithm : LoadedAlgorithm
	{
		public LoadedCompleteAlgorithm(string inNamespace, string inDLLName, string inComponentName, Type inType, string inAlgDesc)
			: base(inNamespace, inDLLName, inComponentName, inType, inAlgDesc)
		{
		}
	}
	[Serializable()]
	public class LoadedTemplateAlgorithm : LoadedAlgorithm
	{
		public LoadedTemplateAlgorithm(string inNamespace, string inDLLName, string inComponentName, Type inType, string inAlgDesc)
			: base(inNamespace, inDLLName, inComponentName, inType, inAlgDesc)
		{
		}
	}
	[Serializable()]
	public class LoadedScriptAlgorithm : LoadedAlgorithm
	{
		public LoadedScriptAlgorithm(string inNamespace, string inDLLName, string inComponentName, Type inType, string inAlgDesc)
			: base(inNamespace, inDLLName, inComponentName, inType, inAlgDesc)
		{
		}
	}

	[Serializable()]
	public class LoadedProblem : ILoadedComponent
	{
		public string _ProbDesc;
		public string _RequestedDesignVar;

		public LoadedProblem(string inNamespace, string inDLLName, string inComponentName, Type inType, string inProbDesc, string inReqDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType)
		{
			_ProbDesc = inProbDesc;
			_RequestedDesignVar = inReqDesignVar;
		}
	}
	[Serializable()]
	public class LoadedCompleteProblem : LoadedProblem
	{
		public LoadedCompleteProblem(string inNamespace, string inDLLName, string inComponentName, Type inType, string inProbDesc, string inReqDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType, inProbDesc, inReqDesignVar)
		{
		}
	}
	[Serializable()]
	public class LoadedFileInitProblem : LoadedProblem
	{
		public LoadedFileInitProblem(string inNamespace, string inDLLName, string inComponentName, Type inType, string inProbDesc, string inReqDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType, inProbDesc, inReqDesignVar)
		{
		}
	}
	[Serializable()]
	public class LoadedParameterProblem : LoadedProblem
	{
		public LoadedParameterProblem(string inNamespace, string inDLLName, string inComponentName, Type inType, string inProbDesc, string inReqDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType, inProbDesc, inReqDesignVar)
		{
		}
	}
	[Serializable()]
	public class LoadedRuntimeInitProblem : LoadedProblem
	{
		public LoadedRuntimeInitProblem(string inNamespace, string inDLLName, string inComponentName, Type inType, string inProbDesc, string inReqDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType, inProbDesc, inReqDesignVar)
		{
		}
	}

	[Serializable()]
	public class LoadedOperator : ILoadedComponent
	{
		public string _OperDesc;
		public LoadedOperator(string inNamespace, string inDLLName, string inComponentName, Type inType, string inOperDesc)
			: base(inNamespace, inDLLName, inComponentName, inType)
		{
			_OperDesc = inOperDesc;
		}
	}

	[Serializable()]
	public class LoadedRepresentation : ILoadedComponent
	{
		public string RealisedDesignVar;

		public LoadedRepresentation(string inNamespace, string inDLLName, string inComponentName, Type inType, string inDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType)
		{
			RealisedDesignVar = inDesignVar;
		}
	}

	[Serializable()]
	public class LoadedConstructedRepresentation : LoadedRepresentation
	{
		public LoadedConstructedRepresentation(string inNamespace, string inDLLName, string inComponentName, Type inType, string inDesignVar)
			: base(inNamespace, inDLLName, inComponentName, inType, inDesignVar)
		{
		}
	}

	[Serializable()]
	public class LoadedObserverSaver : ILoadedComponent
	{
		public string RequestedIntermediateProvider;
		public string ObserverDesc;

		public LoadedObserverSaver(string inNamespace, string inDLLName, string inComponentName, Type inType, string inReqProvider, string inDesc)
			: base(inNamespace, inDLLName, inComponentName, inType)
		{
			RequestedIntermediateProvider = inReqProvider;
			ObserverDesc = inDesc;
		}
	}

	[Serializable()]
	public class LoadedOptTerminator : ILoadedComponent
	{
		public string _RequestedIntermediateProvider;
		public string _OptTermDesc;

		public LoadedOptTerminator(string inNamespace, string inDLLName, string inComponentName, Type inType, string inReqProvider, string inDesc)
			: base(inNamespace, inDLLName, inComponentName, inType)
		{
			_OptTermDesc = inDesc;
			_RequestedIntermediateProvider = inReqProvider;
		}
	}
}
