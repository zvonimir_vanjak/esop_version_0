using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public partial class ParetoSet2D_ScatterGraph_Nevron : UserControl, IIndividualCollMemberVisualizationControl
	{
		private ISolutionMO[] _ParetoSet;
		//private NChart m_Chart;
		//private NPointSeries m_Point;

		public ParetoSet2D_ScatterGraph_Nevron()
		{
			InitializeComponent();
		}
		public Type GetRequestedMemberType()
		{
			ISolutionMO[] a = new ISolutionMO[1];
			return a.GetType();
		}

		public void Show(Object obj)
		{
			_ParetoSet = (ISolutionMO[])obj;

			//m_ChartControl.Settings.RenderDevice = RenderDevice.GDI;
			//m_ChartControl.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.LightSteelBlue, Color.White);

			//// add a header label
			//NLabel header = m_ChartControl.Labels.AddHeader("XY Scatter Point Chart");
			//header.TextProps.Backplane.Visible = false;
			//header.TextProps.FillEffect.Color = Color.Cornsilk;
			//header.TextProps.Shadow.Type = ShadowType.Solid;
			//header.TextProps.Shadow.Color = Color.FromArgb(128, 64, 64, 64);
			//header.TextProps.HorzAlign = HorzAlign.Left;
			//header.TextProps.VertAlign = VertAlign.Top;
			//header.HorizontalMargin = 2;
			//header.VerticalMargin = 2;

			//m_Chart = m_ChartControl.Charts[0];
			//m_Chart.View.SetPredefinedProjection(PredefinedProjection.Perspective);
			//m_Chart.Axis(StandardAxis.Depth).Visible = false;
			//m_Chart.Axis(StandardAxis.PrimaryY).MajorGridLine.Pattern = LinePattern.Dot;
			//m_Chart.Axis(StandardAxis.PrimaryX).MajorGridLine.Pattern = LinePattern.Dot;
			//m_Chart.Axis(StandardAxis.PrimaryX).SetMajorShowAtWall(ChartWallType.Back, true);

			//m_Point = (NPointSeries)m_Chart.Series.Add(SeriesType.Point);
			//m_Point.DataLabels.Mode = DataLabelsMode.None;
			//m_Point.Legend.Mode = SeriesLegendMode.None;
			//m_Point.Name = "Point Series";
			//m_Point.PointBorder.Width = 0;

			// add xy values
			int PntNum = _ParetoSet.GetLength(0);

			for (int i = 0; i < PntNum; i++)
			{
				double f1 = _ParetoSet[i].getCurrObjectiveValues().getValue(0);
				double f2 = _ParetoSet[i].getCurrObjectiveValues().getValue(1);

//				m_Point.AddXY(f1, f2, "");
			}
			// instruct the chart to use them
//			m_Point.UseXValues = true;

			// change the categories axis in values mode
//			m_Chart.Axis(StandardAxis.PrimaryX).ScaleMode = AxisScaleMode.Numeric;
		}
	}
}
