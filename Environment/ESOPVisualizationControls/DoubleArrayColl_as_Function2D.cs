using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public partial class DoubleArrayColl_as_Function2D : UserControl, ICompleteCollVisualizationControl
	{
		public DoubleArrayColl_as_Function2D()
		{
			InitializeComponent();
		}
		
		public Type GetRequestedMemberType()
		{
			double[] a = new double[2];
			return a.GetType();
		}

		public void ShowCollection(ObservedValuesCollection inColl)
		{
		}
	}
}
