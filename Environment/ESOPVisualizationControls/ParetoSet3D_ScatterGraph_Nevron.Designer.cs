namespace ESOPVisualizationControls
{
	partial class ParetoSet3D_ScatterGraph_Nevron
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParetoSet3D_ScatterGraph_Nevron));
//			this.m_ChartControl = new Nevron.Chart.WinForm.NChartControl();
			this.SuspendLayout();
			// 
			// m_ChartControl
			// 
			//this.m_ChartControl.BackColor = System.Drawing.SystemColors.Control;
			//this.m_ChartControl.Background = ((Nevron.Standard.NBackground)(resources.GetObject("m_ChartControl.Background")));
			//this.m_ChartControl.Charts = ((Nevron.NChart.NChartCollection)(resources.GetObject("m_ChartControl.Charts")));
			//this.m_ChartControl.Dock = System.Windows.Forms.DockStyle.Fill;
			//this.m_ChartControl.InteractivityOperations = ((Nevron.NChart.NInteractivityOperationsCollection)(resources.GetObject("m_ChartControl.InteractivityOperations")));
			//this.m_ChartControl.Labels = ((Nevron.Standard.NLabelCollection)(resources.GetObject("m_ChartControl.Labels")));
			//this.m_ChartControl.Legends = ((Nevron.NChart.NLegendCollection)(resources.GetObject("m_ChartControl.Legends")));
			//this.m_ChartControl.Location = new System.Drawing.Point(0, 0);
			//this.m_ChartControl.Name = "m_ChartControl";
			//this.m_ChartControl.Settings = ((Nevron.NChart.NSettings)(resources.GetObject("m_ChartControl.Settings")));
			//this.m_ChartControl.Size = new System.Drawing.Size(504, 447);
			//this.m_ChartControl.TabIndex = 0;
			//this.m_ChartControl.Text = "m_ChartControl";
			//this.m_ChartControl.Watermarks = ((Nevron.Standard.NWatermarkCollection)(resources.GetObject("m_ChartControl.Watermarks")));
			// 
			// ParetoSet2D_ScatterGraph_Nevron
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//			this.Controls.Add(this.m_ChartControl);
			this.Name = "ParetoSet2D_ScatterGraph_Nevron";
			this.Size = new System.Drawing.Size(504, 447);
			this.ResumeLayout(false);

		}

		#endregion

	//	private Nevron.Chart.WinForm.NChartControl m_ChartControl;
	}
}
