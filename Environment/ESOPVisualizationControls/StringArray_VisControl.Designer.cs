namespace ESOPVisualizationControls
{
	partial class StringArray_VisControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StringArray_VisControl));
			this.listView1 = new System.Windows.Forms.ListView();
			this.label1 = new System.Windows.Forms.Label();
//			this.nChartControl1 = new Nevron.Chart.WinForm.NChartControl();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.listView1.Location = new System.Drawing.Point(0, 213);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(411, 171);
			this.listView1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(31, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "label1";
			// 
			// nChartControl1
			// 
			//this.nChartControl1.BackColor = System.Drawing.SystemColors.Control;
			//this.nChartControl1.Background = ((Nevron.Standard.NBackground)(resources.GetObject("nChartControl1.Background")));
			//this.nChartControl1.Charts = ((Nevron.NChart.NChartCollection)(resources.GetObject("nChartControl1.Charts")));
			//this.nChartControl1.InteractivityOperations = ((Nevron.NChart.NInteractivityOperationsCollection)(resources.GetObject("nChartControl1.InteractivityOperations")));
			//this.nChartControl1.Labels = ((Nevron.Standard.NLabelCollection)(resources.GetObject("nChartControl1.Labels")));
			//this.nChartControl1.Legends = ((Nevron.NChart.NLegendCollection)(resources.GetObject("nChartControl1.Legends")));
			//this.nChartControl1.Location = new System.Drawing.Point(70, 33);
			//this.nChartControl1.Name = "nChartControl1";
			//this.nChartControl1.Settings = ((Nevron.NChart.NSettings)(resources.GetObject("nChartControl1.Settings")));
			//this.nChartControl1.Size = new System.Drawing.Size(285, 163);
			//this.nChartControl1.TabIndex = 2;
			//this.nChartControl1.Text = "nChartControl1";
			//this.nChartControl1.Watermarks = ((Nevron.Standard.NWatermarkCollection)(resources.GetObject("nChartControl1.Watermarks")));
			// 
			// StringArray_VisControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//			this.Controls.Add(this.nChartControl1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.listView1);
			this.Name = "StringArray_VisControl";
			this.Size = new System.Drawing.Size(411, 384);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.Label label1;
//		private Nevron.Chart.WinForm.NChartControl nChartControl1;
	}
}
