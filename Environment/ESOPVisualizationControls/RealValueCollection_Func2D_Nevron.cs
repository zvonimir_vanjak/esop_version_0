using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public partial class RealValueCollection_Func2D_Nevron : UserControl, ICompleteCollVisualizationControl
	{
		private ObservedValuesCollection _Coll;
		//private NChart m_Chart;
		//private NLineSeries m_Line;		

		public RealValueCollection_Func2D_Nevron()
		{
			InitializeComponent();
		}

		public Type GetRequestedMemberType()
		{
			double a = 0.0;
			return a.GetType();
		}

		public void ShowCollection(ObservedValuesCollection inColl)
		{
			_Coll = inColl;

//			m_ChartControl.Settings.RenderDevice = RenderDevice.GDI;
//			m_ChartControl.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant2, Color.Ivory, Color.Khaki);
//			m_ChartControl.Width = 500;

//			// add a header label
//			NLabel header = m_ChartControl.Labels.AddHeader(inColl._strDesc);
//			header.TextProps.Backplane.Visible = false;
//			header.TextProps.FillEffect.Color = Color.Navy;
//			header.TextProps.Shadow.Type = ShadowType.Solid;
//			header.TextProps.HorzAlign = HorzAlign.Left;
//			header.TextProps.VertAlign = VertAlign.Top;
//			header.HorizontalMargin = 2;
//			header.VerticalMargin = 2;

//			m_Chart = m_ChartControl.Charts[0];
//			m_Chart.View.SetPredefinedProjection(PredefinedProjection.Orthogonal);
//			m_Chart.Axis(StandardAxis.Depth).Visible = false;
//			m_Chart.Axis(StandardAxis.PrimaryX).ScaleMode = AxisScaleMode.Numeric;
//			m_Chart.Axis(StandardAxis.PrimaryX).SetMajorShowAtWall(ChartWallType.Back, true);
//			m_Chart.Axis(StandardAxis.PrimaryX).MajorGridLine.Pattern = LinePattern.Dot;
//			m_Chart.Axis(StandardAxis.PrimaryY).MajorGridLine.Pattern = LinePattern.Dot;
//			m_Chart.Wall(ChartWallType.Back).Width = 0;
//			m_Chart.Wall(ChartWallType.Left).Width = 0;
//			m_Chart.Wall(ChartWallType.Floor).Width = 0;

//			m_Chart.Width = 100;

//			// add the line
//			m_Line = (NLineSeries)m_Chart.Series.Add(SeriesType.Line);
//			m_Line.LineStyle = LineSeriesStyle.Line;
//			m_Line.DataLabels.Mode = DataLabelsMode.None;
////			m_Line.Legend.Mode = SeriesLegendMode.DataPoints;
//			m_Line.LineBorder.Color = Color.Indigo;
//			m_Line.LineBorder.Width = 2;
//			m_Line.InflateMargins = true;
////			m_Line.Markers.Visible = true;
//			m_Line.Markers.Border.Color = Color.Indigo;
//			m_Line.Markers.Style = PointStyle.Cylinder;
//			m_Line.Markers.Width = 1.5f;
//			m_Line.Markers.Height = 1.5f;
//			m_Line.Name = "Line Series";

			// add xy values
			for (int i = 0; i < _Coll._arrValues.Count; i++)
			{
				ObservedValueTag tag = (ObservedValueTag) _Coll._arrValueTags[i];
				double val = (double) _Coll._arrValues[i];

				//m_Line. AddXY(val, tag._IterNum);
			}

			// instruct the line series to use x values
//			m_Line.UseXValues = true;

//			m_ChartControl.Refresh();
		}
	}
}
