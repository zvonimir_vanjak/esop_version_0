using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public partial class ParetoSet2DColl_as_ScatterGraph : UserControl, IIndividualCollMemberVisualizationControl
	{
		private ISolutionMO[] _ParetoSet;

		public ParetoSet2DColl_as_ScatterGraph()
		{
			InitializeComponent();
		}

		public Type GetRequestedMemberType()
		{
			ISolutionMO[] a = new ISolutionMO[1];
			return a.GetType();
		}

		public void Show(Object obj)
		{
			_ParetoSet = (ISolutionMO[]) obj;

			Refresh();
		}

		private void ParetoSet2DColl_as_ScatterGraph_Paint(object sender, PaintEventArgs e)
		{
			Graphics g = e.Graphics;

			int PntNum = _ParetoSet.GetLength(0);

			if (PntNum > 0)
			{
				// 1. analiza podataka (odrediti max. i min. vrijednosti
				double MaxX = _ParetoSet[0].getCurrObjectiveValues().getValue(0);
				double MinX = _ParetoSet[0].getCurrObjectiveValues().getValue(0);
				double MaxY = _ParetoSet[0].getCurrObjectiveValues().getValue(1);
				double MinY = _ParetoSet[0].getCurrObjectiveValues().getValue(1);

				double x, y;
				for (int i = 1; i < PntNum; i++)
				{
					x = _ParetoSet[i].getCurrObjectiveValues().getValue(0);
					y = _ParetoSet[i].getCurrObjectiveValues().getValue(1);

					if (x > MaxX) MaxX = x;
					if (x < MinX) MinX = x;
					if (y > MaxY) MaxY = y;
					if (y < MinY) MinY = y;
				}
				// najprije iscrtavamo koordinatni sustav
				Pen blackPen = new Pen(Color.Black);

				//			Size x = new Size(this.Size.Width, this.Size.Height);
				Rectangle rectDrawArea = new Rectangle(20, 10, this.Size.Width - 40, this.Size.Height - 20);

				//			g.DrawRectangle(blackPen, rectDrawArea);
				g.DrawLine(blackPen, rectDrawArea.Left, rectDrawArea.Top, rectDrawArea.Left, rectDrawArea.Bottom);
				g.DrawLine(blackPen, rectDrawArea.Left, rectDrawArea.Bottom, rectDrawArea.Right, rectDrawArea.Bottom);

				for (int i = 0; i < PntNum; i++)
				{
					double f1 = _ParetoSet[i].getCurrObjectiveValues().getValue(0);
					double f2 = _ParetoSet[i].getCurrObjectiveValues().getValue(1);

					float xc, yc;

					xc = (float)(rectDrawArea.Left + (f1 - MinX) / (MaxX - MinX) * rectDrawArea.Width);
					yc = (float)(rectDrawArea.Bottom - (f2 - MinY) / (MaxY - MinY) * rectDrawArea.Height);

					g.DrawEllipse(blackPen, xc - 3, yc - 3, 6, 6);
				}
			}
		}
	}
}
