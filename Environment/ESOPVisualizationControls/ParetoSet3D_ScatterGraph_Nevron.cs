using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public partial class ParetoSet3D_ScatterGraph_Nevron : UserControl, IIndividualCollMemberVisualizationControl
	{
		private ISolutionMO[] _ParetoSet;
		//private NChart m_Chart;
		//private NPointSeries m_Point;

		public ParetoSet3D_ScatterGraph_Nevron()
		{
			InitializeComponent();
		}

		public Type GetRequestedMemberType()
		{
			ISolutionMO[] a = new ISolutionMO[1];
			return a.GetType();
		}

		public void Show(Object obj)
		{
			_ParetoSet = (ISolutionMO[])obj;

			//m_ChartControl.Settings.RenderDevice = RenderDevice.OpenGL;
			//m_ChartControl.InteractivityOperations.Add(new NTrackballDragOperation());
			//m_ChartControl.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.LightSteelBlue, Color.White);

			//// add a header label
			//NLabel header = m_ChartControl.Labels.AddHeader("XYZ Scatter Point Chart");
			//header.TextProps.Backplane.Visible = false;
			//header.TextProps.FillEffect.Color = Color.Cornsilk;
			//header.TextProps.Shadow.Type = ShadowType.Solid;
			//header.TextProps.Shadow.Color = Color.FromArgb(128, 64, 64, 64);
			//header.TextProps.HorzAlign = HorzAlign.Left;
			//header.TextProps.VertAlign = VertAlign.Top;
			//header.HorizontalMargin = 2;
			//header.VerticalMargin = 2;

			//m_Chart = m_ChartControl.Charts[0];
			//m_Chart.MarginMode = MarginMode.Fit;
			//m_Chart.Margins = new RectangleF(8, 8, 84, 84);
			//m_Chart.Depth = 55.0f;
			//m_Chart.Width = 55.0f;
			//m_Chart.Height = 55.0f;
			//m_Chart.View.SetPredefinedProjection(PredefinedProjection.PerspectiveTilted);
			//m_Chart.LightModel.SetPredefinedScheme(LightScheme.MetallicLustre);
			//m_Chart.Axis(StandardAxis.PrimaryX).SetMajorShowAtWall(ChartWallType.Back, true);
			//m_Chart.Axis(StandardAxis.Depth).SetMajorShowAtWall(ChartWallType.Floor, true);
			//m_Chart.Axis(StandardAxis.Depth).SetMajorShowAtWall(ChartWallType.Left, true);
			//m_Chart.Axis(StandardAxis.PrimaryY).MajorGridLine.Pattern = LinePattern.Dot;
			//m_Chart.Axis(StandardAxis.PrimaryX).MajorGridLine.Pattern = LinePattern.Dot;
			//m_Chart.Axis(StandardAxis.Depth).MajorGridLine.Pattern = LinePattern.Dot;

			//m_Point = (NPointSeries)m_Chart.Series.Add(SeriesType.Point);
			//m_Point.Name = "Point Series";
			//m_Point.DataLabels.Mode = DataLabelsMode.None;
			//m_Point.Legend.Mode = SeriesLegendMode.None;
			//m_Point.Legend.Format = "<label>";
			//m_Point.PointStyle = PointStyle.Sphere;
			//m_Point.Appearance.FillMode = AppearanceFillMode.Predefined;

			// add xyz values
			// add xy values
			int PntNum = _ParetoSet.GetLength(0);

			for (int i = 0; i < PntNum; i++)
			{
				double f1 = _ParetoSet[i].getCurrObjectiveValues().getValue(0);
				double f2 = _ParetoSet[i].getCurrObjectiveValues().getValue(1);
				double f3 = _ParetoSet[i].getCurrObjectiveValues().getValue(2);

//			  NDataPoint pnt = new NDataPoint(f1, f2, f3);
//				m_Point.AddDataPoint(pnt); 
			}

			// instruct the series to use the x values
			//m_Point.UseXValues = true;

			//// instruct the chart to use the z values
			//m_Point.UseZValues = true;

			//// use numeric scale for the bottom axis
			//m_Chart.Axis(StandardAxis.PrimaryX).ScaleMode = AxisScaleMode.Numeric;

			//// use numeric scale for the depth axis
			//m_Chart.Axis(StandardAxis.Depth).ScaleMode = AxisScaleMode.Numeric;

			//m_ChartControl.Refresh();
		}
	}
}
