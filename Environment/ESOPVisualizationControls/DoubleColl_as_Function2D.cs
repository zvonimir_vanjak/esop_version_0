using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public partial class DoubleColl_as_Function2D : UserControl, ICompleteCollVisualizationControl
	{
		private ObservedValuesCollection _Coll;

		public DoubleColl_as_Function2D()
		{
			InitializeComponent();
		}

		public Type GetRequestedMemberType()
		{
			double a = 0.0;
			return a.GetType();
		}

		public void ShowCollection(ObservedValuesCollection inColl)
		{
			_Coll = inColl;
			labelDescription.Text = inColl._strDesc;

			Refresh();
		}

		private void DoubleColl_as_Function2D_Paint(object sender, PaintEventArgs e)
		{
			Graphics g = e.Graphics;

			if (_Coll != null)
			{
				int PntNum = _Coll._arrValues.Count;

				// najprije analiza vrijednosti
				double Min = (double)_Coll._arrValues[0];
				double Max = (double)_Coll._arrValues[0];

				for (int i = 1; i < PntNum; i++)
				{
					double val = (double)_Coll._arrValues[i];

					if (val > Max) Max = val;
					if (val < Min) Min = val;
				}

				if (Min > 0)
					Min = 0;

				#region Ovdje pode�avamo koordinatne osi, labele na njima, faktore transformacije, ...
				Pen blackPen = new Pen(Color.Black);
				Pen blackPen3 = new Pen(Color.Black, 3);

				blackPen3.EndCap = LineCap.ArrowAnchor;

				Rectangle rectDrawArea = new Rectangle(50, 30, this.Size.Width - 60, this.Size.Height - 60);

				g.DrawLine(blackPen3, rectDrawArea.Left, rectDrawArea.Top, rectDrawArea.Left, rectDrawArea.Bottom + 10);

				if (Min < 0)
				{
					// treba vidjeti gdje �emo staviti horizontalnu os
				}
				else
					g.DrawLine(blackPen3, rectDrawArea.Left - 10 , rectDrawArea.Bottom, rectDrawArea.Right, rectDrawArea.Bottom);

				// pa bi trebalo ispisati labele na x i y osi
				#endregion

				float x0, y0, x1, y1;
				double f;

				f = (double)_Coll._arrValues[0];
				x0 = rectDrawArea.Left;
				y0 = (float)(rectDrawArea.Bottom - (f - Min) / (Max - Min) * rectDrawArea.Height);

				for (int i = 1; i < PntNum; i++)
				{
					f = (double)_Coll._arrValues[i];

					x1 = (float)(rectDrawArea.Left + i * rectDrawArea.Width / PntNum);
					y1 = (float)(rectDrawArea.Bottom - (f - Min) / (Max - Min) * rectDrawArea.Height);

					g.DrawLine(blackPen, x0, y0, x1, y1);

					x0 = x1;
					y0 = y1;
				}
			}
		}
	}
}
