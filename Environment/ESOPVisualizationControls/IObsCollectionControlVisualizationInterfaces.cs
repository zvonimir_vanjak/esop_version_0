using System;
using System.Collections.Generic;
using System.Text;

using System.Reflection;
using ESOP.Framework.Base;

namespace ESOPVisualizationControls
{
	public interface ICompleteCollVisualizationControl
	{
		Type GetRequestedMemberType();
		void ShowCollection(ObservedValuesCollection inColl);
	}

	public interface IIndividualCollMemberVisualizationControl
	{
		Type GetRequestedMemberType();

		void Show(Object obj);
	}
}

